<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\InformacionAcademica;
use Cet\NominaBundle\Form\InformacionAcademicaType;

/**
 * InformacionAcademica controller.
 *
 */
class InformacionAcademicaController extends Controller
{

    /**
     * Lists all InformacionAcademica entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:InformacionAcademica')->findAll();

        return $this->render('CetNominaBundle:InformacionAcademica:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new InformacionAcademica entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new InformacionAcademica();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('informacionacademica_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:InformacionAcademica:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a InformacionAcademica entity.
    *
    * @param InformacionAcademica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(InformacionAcademica $entity)
    {
        $form = $this->createForm(new InformacionAcademicaType(), $entity, array(
            'action' => $this->generateUrl('informacionacademica_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new InformacionAcademica entity.
     *
     */
    public function newAction()
    {
        $entity = new InformacionAcademica();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:InformacionAcademica:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a InformacionAcademica entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionAcademica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionAcademica.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionAcademica:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing InformacionAcademica entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionAcademica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionAcademica.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionAcademica:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a InformacionAcademica entity.
    *
    * @param InformacionAcademica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(InformacionAcademica $entity)
    {
        $form = $this->createForm(new InformacionAcademicaType(), $entity, array(
            'action' => $this->generateUrl('informacionacademica_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing InformacionAcademica entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionAcademica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionAcademica.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('informacionacademica_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:InformacionAcademica:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a InformacionAcademica entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:InformacionAcademica')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionAcademica.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('informacionacademica'));
    }

    /**
     * Creates a form to delete a InformacionAcademica entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('informacionacademica_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
