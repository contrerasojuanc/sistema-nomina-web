<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalInformacionLaboralType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $array_tipo=array(0=>'Cuenta Corriente', 1 =>'Cuenta de Ahorro');
        
        $builder
            ->add('expediente',null, array('label'=> 'Número de Expediente','attr' => array('help'=>'Ingrese el número de identificación del expediente del funcionario.')))
            ->add('cuentaNomina',null,array(
                'label' => 'Nro. Cuenta Nomina',
                'attr' => array('help'=>'Ingrese el número de la cuenta nomina, este debe tener 20 digitos.'),
            ))
            ->add('tipoCuentaNomina','choice', array(
                'mapped' => true,
                'required' => true,
                'multiple' => false,
                'choices' => $array_tipo
            ))         
            ->add('cuentaFideicomiso',null,array(
                'required' => false,
                'label' => 'Nro. Cuenta Fideicomiso',
                'attr' => array('help'=>'Ingrese el número de la cuenta fideicomiso, este debe tener 20 digitos.')
                ))
            ->add('tipoCuentaFideicomiso','choice', array(
                'mapped' => true,
                'required' => false,
                'multiple' => false,
                'choices' => $array_tipo
            )) 
           ->add('fechaIngreso','date', array(
               'widget' => 'single_text',
               'label'=> 'Fecha de Ingreso',
                'attr' => array('help'=>'Ingrese la fecha de ingreso a la institución.'),
            ))
            ->add('fk_informacion_laboral_tipo_nomina1', null, array(
                'mapped'=> true,
                'empty_value' => 'Seleccione',
                'label' =>'Tipo de Nómina',
                'attr'  =>array('help'=>'Selecione un tipo de nómina'),
            )) 
            ->add('tituloProfesional',null, array('label'=> 'Abreviación de Título Profesional','attr' => array('help'=>'Ingrese la abreviación del título profesional. Ejemplo: Abg., Ing., Lcdo.')))
            ->add('fechaEgreso','date', array(
                'label'=> 'Fecha de Egreso',
                'attr' => array('help'=>'Ingrese la fecha de egreso de la institución.'),
                'widget' => 'single_text',
                'required' => false,
            ))    
            ->add('condicionEgreso', 'choice', array(
                'label'=>'Condición de Egreso',
                'attr'=>array('help'=>'Seleccione una de las opciones en caso del que el funcionario ya no labore para la institución.'),
                'choices' => array('' => 'Seleccione una de las opciones','R' => 'Renuncia', 'D' => 'Despido', 'H' => 'Inhabilitación', 'I' => 'Incapacitación', 'J' => 'Jubilación', 'O' => 'Otro'),
                'required' => false,
            ))
            //->add('personal', null, array('label' => ' ','attr' => array('style'=>'display:none')))//oculta pero deja el espacio del campo
            //->add('personal', null, array('label' => ' ','attr' => array('disabled'=>'disabled'))) bloquea pero no envia datos   
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\InformacionLaboral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_informacionlaboral';
    }
}
