<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Cet\NominaBundle\Form\Subscribers\DireccionSubscriber;

class PersonalHistoricoUnidadOrganizativaType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaInicio','date', array(
                'widget' => 'single_text',
                'label'=> 'Fecha de Ingreso',
                'attr' => array('help'=>'Seleccione la fecha de ingreso en la Dirección.'),
            ))
            ->add('fechaFin','date', array(
                'widget' => 'single_text',
                'label'=> 'Fecha egreso',
                'attr' => array('help'=>'Seleccione la fecha de egreso de la Dirección.'),
                'required' => false,
            ))  
            ->add('fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1',null, array(
                'label'=> 'Dirección',
                'empty_value' => 'Seleccione',
                'attr' => array('help'=>'Seleccione Dirección.'),
                'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione unidad organizativa') 
            ))
            ;
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_historicounidadorganizativa';
    }
}
