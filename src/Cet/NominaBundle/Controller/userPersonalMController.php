<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\userPersonalM;
use Cet\NominaBundle\Form\userPersonalMType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * userPersonalM controller.
 *
 */
class userPersonalMController extends Controller
{

    /**
     * Lists all userPersonalM entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:userPersonalM')->findAll();

        return $this->render('CetNominaBundle:userPersonalM:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new userPersonalM entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new userPersonalM();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('userpersonalm_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:userPersonalM:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a userPersonalM entity.
    *
    * @param userPersonalM $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(userPersonalM $entity, $vacio = false)
    {
        $form = $this->createForm(new userPersonalMType(true, $vacio, $entity->getId()), $entity, array(
            'action' => $this->generateUrl('userpersonalm_create'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager(),
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new userPersonalM entity.
     *
     */
    public function newAction()
    {
        $entity = new userPersonalM();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:userPersonalM:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a userPersonalM entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:userPersonalM')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalM.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:userPersonalM:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing userPersonalM entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:userPersonalM')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalM.');
        }
        
        if($entity->getPersonal() == null)
            $editForm = $this->createEditForm($entity,true);
        else
            $editForm = $this->createEditForm($entity);
       
        $deleteForm = $this->createDeleteForm($id);
        return $this->render('CetNominaBundle:userPersonalM:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a userPersonalM entity.
    *
    * @param userPersonalM $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(userPersonalM $entity, $vacio = false)
    {
            $form = $this->createForm(new userPersonalMType(false, $vacio, $entity->getId()), $entity, array(
                'action' => $this->generateUrl('userpersonalm_update', array('id' => $entity->getId())),
                'method' => 'PUT',   
                'em' => $this->getDoctrine()->getManager()
            ));    

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing userPersonalM entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:userPersonalM')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalM.');
        }

        $originalTags = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($entity->getPersonalSistemas() as $tag) {
            $originalTags->add($tag);
        }
        
        $deleteForm = $this->createDeleteForm($id);
        
        if($entity->getPersonal() == null)
            $editForm = $this->createEditForm($entity,true);
        else
            $editForm = $this->createEditForm($entity);
        
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            // remove the relationship between the tag and the Task
            foreach ($originalTags as $tag) {
                if (false === $entity->getPersonalSistemas()->contains($tag)) {
                    // remove the Task from the Tag
//                    $tag->getIdUsuario()->removeElement($entity);

                    // if it was a many-to-one relationship, remove the relationship like this
                     $tag->setIdUsuario(null);

//                    $em->persist($tag);

                    // if you wanted to delete the Tag entirely, you can also do that
                     $em->remove($tag);
                }
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('userpersonalm_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:userPersonalM:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a userPersonalM entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:userPersonalM')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalM.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('userpersonalm'));
    }

    /**
     * Creates a form to delete a userPersonalM entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('userpersonalm_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
