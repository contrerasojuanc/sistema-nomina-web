<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\DiasPendientes;
use Cet\NominaBundle\Form\DiasPendientesType;

/**
 * DiasPendientes controller.
 *
 */
class DiasPendientesController extends Controller
{

    /**
     * Lists all DiasPendientes entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:DiasPendientes')->findAll();

        return $this->render('CetNominaBundle:DiasPendientes:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    /**
     * Lists all DiasPendientes entities.
     *
     */
    public function indexIndividualAction($cedula)
    {       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:DiasPendientes')->findBy(array('personal'=>$cedula));
//        $entities = $em->getRepository('CetNominaBundle:DiasPendientes')->findAll();
//        $entities = $em->getRepository('CetNominaBundle:Vacaciones')->findAll();
        $entity = new DiasPendientes();
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
        $entity->setPersonal($personal);
        
        return $this->render('CetNominaBundle:DiasPendientes:index.html.twig', array(
            'entities' => $entities,
            'entity'  => $entity,
            'cedula' => $cedula,
        ));
    }
    
    /**
     * Creates a new DiasPendientes entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new DiasPendientes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('diaspendientes_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:DiasPendientes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a DiasPendientes entity.
    *
    * @param DiasPendientes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(DiasPendientes $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new DiasPendientesType(), $entity, array(
            'action' => $this->generateUrl('diaspendientes_create'),
            'method' => 'POST',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new DiasPendientes entity.
     *
     */
    public function newAction($cedula)
    {
        $entity = new DiasPendientes();        
        
        $em = $this->getDoctrine()->getManager();
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
        $entity->setPersonal($personal);
        
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:DiasPendientes:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'cedula' => $cedula
        ));
    }

    /**
     * Finds and displays a DiasPendientes entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DiasPendientes')->find($id);
        
        $cedula = $entity->getPersonal()->getId();
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DiasPendientes.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DiasPendientes:show.html.twig', array(
            'entity'      => $entity,
            'cedula'      => $cedula,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing DiasPendientes entity.
     *
     */
    public function editAction($id, $cedula=null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DiasPendientes')->find($id);
        
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
        $entity->setPersonal($personal);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DiasPendientes.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DiasPendientes:edit.html.twig', array(
            'cedula'      => $cedula,
            'personal'    => $personal,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a DiasPendientes entity.
    *
    * @param DiasPendientes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DiasPendientes $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new DiasPendientesType(), $entity, array(
            'action' => $this->generateUrl('diaspendientes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing DiasPendientes entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DiasPendientes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DiasPendientes.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('diaspendientes_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:DiasPendientes:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a DiasPendientes entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:DiasPendientes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad DiasPendientes.');
            }

            $cedula = $entity->getPersonal()->getId();
            
            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('diaspendientes_individual',array('cedula' => $cedula)));
    }

    /**
     * Creates a form to delete a DiasPendientes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('diaspendientes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
