<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\DatosGlobales;
use Cet\NominaBundle\Form\DatosGlobalesType;

/**
 * DatosGlobales controller.
 *
 */
class DatosGlobalesController extends Controller
{

    /**
     * Lists all DatosGlobales entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:DatosGlobales')->findAll();

        return $this->render('CetNominaBundle:DatosGlobales:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new DatosGlobales entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new DatosGlobales();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('datosglobales_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:DatosGlobales:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a DatosGlobales entity.
    *
    * @param DatosGlobales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(DatosGlobales $entity)
    {
        $form = $this->createForm(new DatosGlobalesType(), $entity, array(
            'action' => $this->generateUrl('datosglobales_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new DatosGlobales entity.
     *
     */
    public function newAction()
    {
        $entity = new DatosGlobales();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:DatosGlobales:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a DatosGlobales entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosGlobales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGlobales.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DatosGlobales:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing DatosGlobales entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosGlobales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGlobales.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DatosGlobales:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a DatosGlobales entity.
    *
    * @param DatosGlobales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DatosGlobales $entity)
    {
        $form = $this->createForm(new DatosGlobalesType(), $entity, array(
            'action' => $this->generateUrl('datosglobales_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing DatosGlobales entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosGlobales')->find($id);

        $valor_anterior = $entity->getValor();
        $nombre_anterior = $entity->getNombre()." (ANTERIOR)" ;
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGlobales.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $entity_anterior = $em->getRepository('CetNominaBundle:DatosGlobales')->findBy(array('nombre'=>$nombre_anterior));
            
                if (!$entity_anterior) { //Si no existe creamos
                    $entity_nueva = new DatosGlobales();
                    $entity_nueva->setNombre($nombre_anterior);
                    $entity_nueva->setValor($valor_anterior);
                    $em->persist($entity_nueva);
                    $em->flush();    
                }
                else{ //Si existe actualizamos
                    foreach($entity_anterior as $anterior){
                        $anterior->setNombre($nombre_anterior);
                        $anterior->setValor($valor_anterior);
                        $em->flush();
                    }
                }            
            return $this->redirect($this->generateUrl('datosglobales_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:DatosGlobales:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a DatosGlobales entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:DatosGlobales')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGlobales.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('datosglobales'));
    }

    /**
     * Creates a form to delete a DatosGlobales entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('datosglobales_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
