<?php

namespace Cet\NominaBundle\Form;
//namespace Cet\NominaBundle\Entity;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Cet\NominaBundle\Form\Subscribers\VariableSubscriber;
use Symfony\Component\Form\FormEvents;

class ConceptoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
//     private $entityManager;
//
//    public function __construct(EntityManager $entityManager)
//    {
//        $this->entityManager = $entityManager;
//    }
//    
//         private function buildStatusNames()
//    {
//        $choices = array();
//        $types = $this
//            ->entityManager
//            ->getRepository('Cet\NominaBundle\Entity\Concepto')
//            ->createQueryBuilder('concepto')
//            ->orderBy('concepto.codigo','ASC')
//            ->getQuery()
//            ->getResult();
//
//        foreach ($types as $type) {
//            // I assume key is retrieved by getId
//            $choices[$type->getId()] = $type->getName();
//        }
//
//        $choices['not closed'] = 'Not closed';
//
//        return $choices;
//    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $array_lis=array(
            'operadores'=>array('(', ')' ,'=','!','OR','AND','hoy()','redondear()'));
        
        $array_tipo=array('Asignación'=>'Asignación', 'Deducción'=>'Deducción');
        
        $builder
            ->add('codigo')
            ->add('denominacion',null,array('label'=>'Concepto','attr' => array('help'=>'Ingrese el nombre del concepto')))
            ->add('identificador',null,array('label'=>'Identificador','attr' => array('help'=>'Campo utilizado para ser gestionado en fórmulas de conceptos')))            
            ->add('descripcion')
            ->add('tipo','choice', array(
                'mapped' => true,
                'required' => true,
                'multiple' => false,
                'choices' => $array_tipo
            ))            
                
           ->add('esAcumulado',null,array(
                'label' => 'Muestra Acumulado',
                'required' => false, 'attr' => array('help'=>'Marque esta casilla para que se muestre al monto acumulado del concepto en el recibo de pago')  ))  
           ->add('fk_concepto_has_concepto2',null,array(
                       'query_builder' => function($r) {
                        return $r->createQueryBuilder('concepto')
                                   ->orderBy('concepto.codigo','ASC');
                        },
                   'label'=>'Concepto es Acumulado por','property'=>'getNombreCompleto','attr' => array('help'=>'Seleccione el o los conceptos que conforman el acumulado del concepto actual')))                
            
              ->add('sql',null,array('label'=>'Fórmula','attr' => array('help'=>'Ingrese fórmula del concepto')))
//            ->add('patronal','checkbox',array(
//                'label' => 'Establecer fórmula de aporte patronal',
//                'mapped' => false,
//                'required' => false
//            ))  
//            ->add('formulaPatronal', 'hidden', array(
//                'mapped' => true,
//                'data' => ''
//            ))
            ->add('formulaPatronal',null,array('label'=>'Fórmula para aporte patronal (Opcional)','attr' => array('help'=>'Ingrese fórmula para el cálculo del aporte patronal del concepto')))
            ->add('Sintaxis','choice', array(
                'mapped' => false,
                'required' => false,
                'multiple' => true,
                'choices' => $array_lis
            ))
        ;
        $factory = $builder->getFormFactory();
        $variableSubscriber = new VariableSubscriber($factory);
        $builder->addEventSubscriber($variableSubscriber);
        
//        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
//            $product = $event->getData();
//            $form = $event->getForm();
//            // check if the Product object is "new"
//            // If no data is passed to the form, the data is "null".
//            if (!$product || null === $product->getId()) {
//                $form->add('name', 'text');
////                $form->add('fk_concepto_has_concepto2',null,array(
////                       'query_builder' => function($r) {
////                        return $r->createQueryBuilder('concepto')
////                                   ->orderBy('concepto.codigo','ASC');
////                        },
////                   'label'=>'Concepto es Acumulado por','property'=>'getNombreCompleto','attr' => array('help'=>'Seleccione el o los conceptos que conforman el acumulado del concepto actual')));                
//                        
//            }
//        });
        
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Concepto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_concepto';
    }
}
