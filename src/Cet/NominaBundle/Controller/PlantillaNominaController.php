<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use Cet\NominaBundle\Entity\PlantillaNomina;
use Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina;
use Cet\NominaBundle\Form\PlantillaNominaType;

/**
 * PlantillaNomina controller.
 *
 */
class PlantillaNominaController extends Controller
{

    /**
     * Lists all PlantillaNomina entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:PlantillaNomina')->findAll();
     
        return $this->render('CetNominaBundle:PlantillaNomina:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PlantillaNomina entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PlantillaNomina();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('plantillanomina_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:PlantillaNomina:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PlantillaNomina entity.
    *
    * @param PlantillaNomina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PlantillaNomina $entity)
    {
        $form = $this->createForm(new PlantillaNominaType(), $entity, array(
            'action' => $this->generateUrl('plantillanomina_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new PlantillaNomina entity.
     *
     */
    public function newAction()
    {
        $entity = new PlantillaNomina();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:PlantillaNomina:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PlantillaNomina entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PlantillaNomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PlantillaNomina.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:PlantillaNomina:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing PlantillaNomina entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PlantillaNomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PlantillaNomina.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:PlantillaNomina:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PlantillaNomina entity.
    *
    * @param PlantillaNomina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PlantillaNomina $entity)
    {
        $form = $this->createForm(new PlantillaNominaType(), $entity, array(
            'action' => $this->generateUrl('plantillanomina_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing PlantillaNomina entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PlantillaNomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PlantillaNomina.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('plantillanomina_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:PlantillaNomina:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PlantillaNomina entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:PlantillaNomina')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad PlantillaNomina.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('plantillanomina'));
    }

    /**
     * Creates a form to delete a PlantillaNomina entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('plantillanomina_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    /**
     * Lists all PlantillaNomina entities.
     *
     */
    public function personalizarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //Obtengo el id del personal seleccionado para realizar la prueba asi como el sql
        $id_plantilla = $request->request->get('id_plantilla');
        
        $plantilla = $em->getRepository('CetNominaBundle:PlantillaNomina')->findById($id_plantilla);
        if(isset($plantilla[0])){
            $tipo=$plantilla[0]->getFkPlantillaNominaTipoNomina1()->getId();
            $consulta = $em->createQuery('SELECT p.id,p.primerNombre,p.segundoNombre,p.primerApellido,p.segundoApellido '
                    . 'FROM CetNominaBundle:Personal p '
                    . 'JOIN p.informacionLaboral il '
                    . 'WHERE il.fk_informacion_laboral_tipo_nomina1 = :idplantilla')->setParameter('idplantilla',$tipo);

            if ($consulta->getResult()){
                $data=$consulta->getResult();
                //Operador & en el foreach se usa para pasar por referencia el vector
                foreach($data as &$datos){
                    //Operador ?: es coalesce para PHP
                    $datos['primerNombre'] = $datos['primerNombre']?:'';
                    $datos['segundoNombre'] = $datos['segundoNombre']?:'';
                    $datos['primerApellido'] = $datos['primerApellido']?:'';
                    $datos['segundoApellido'] = $datos['segundoApellido']?:'';
                }
                return new JsonResponse($data);
            }
        }
        return new JsonResponse("");
    }
    
    public function conceptospersonalizarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id_personal = $request->request->get('id_personal');
        $id_plantilla = $request->request->get('id_plantilla');
        //Buscar los conceptos  que conforman la plantilla y armar un arreglo para el funcionario
        
        //$conceptos=$em->getRepository('CetNominaBundle:Concepto')->findAll();
        $conceptos=$em->getRepository('CetNominaBundle:Concepto')->findBy(array(), array('codigo' => 'ASC'));
        $i=0;
        foreach($conceptos as $concepto){
            $vector_conceptos[$i]['codigo']=$concepto->getCodigo();
            $vector_conceptos[$i]['codigorelleno']=str_pad($concepto->getCodigo(),3,"0",STR_PAD_LEFT) ;
            $vector_conceptos[$i]['nombre']=$concepto->getDenominacion();
            $vector_conceptos[$i]['estado']=0;
            $i++;
        } 
        
        if(isset($vector_conceptos)){
            //Buscar los conceptos  que conforman la plantilla y armar un arreglo para el funcionario
            $plantilla = $em->getRepository('CetNominaBundle:PlantillaNomina')->findById($id_plantilla);
            foreach ($plantilla as $dato){
                $conceptosplantilla=$dato->getConceptos();
                foreach($conceptosplantilla as $conce){
                    $codigoconcepto=$conce->getCodigo();
                    foreach($vector_conceptos as &$vector){
                        if($codigoconcepto==$vector['codigo']){
                            $vector['estado']=1;
                            break;
                        }
                    }
                }
            }
        }
        $conceptospersona = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')->findBy(array('plantillaNominaId'=>$id_plantilla,'personalCedula'=>$id_personal));
        if(isset($conceptospersona)){       
            foreach($conceptospersona as $conceptopersona){
                foreach($vector_conceptos as &$vector){
                    if($conceptopersona->getFkConceptoHasPlantillaNominaConcepto1()->getCodigo()==$vector['codigo']){
                        $vector['estado']=$vector['estado']==0?1:0;
                        break;
                    }
                }
            }
            return new JsonResponse($vector_conceptos);
        }
        return new JsonResponse("");
    }
    
    public function personalizarconceptosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $vectorPantalla=array();
        if(isset($_POST['conceptos_select']))
            $vectorPantalla = $_POST['conceptos_select']; // conceptos seleccionados en la pantalla
        $id_plantilla = $_POST['plantilla_id'];
        $id_persona = $_POST['persona_id'];
        $plantilla = $em->getRepository('CetNominaBundle:PlantillaNomina')->findById($id_plantilla);
        foreach ($plantilla as $dato){
                $conceptosplantilla=$dato->getConceptos(); // conceptos fijos
                foreach($conceptosplantilla as $conce){
                    $vectorFijos[]=$conce->getCodigo();
                }
        }
        $vectorProcesar=array_merge( array_diff($vectorPantalla,$vectorFijos), array_diff($vectorFijos,$vectorPantalla));
        
        //Borra registros de conceptohasplantillanomina
        $plantillaPersona = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')->findBy(array('fk_concepto_has_plantilla_nomina_plantilla_nomina1'=>$id_plantilla,'fk_concepto_has_plantilla_nomina_personal1'=>$id_persona));
        foreach ($plantillaPersona as $pp){
            $em->remove($pp);
        }        
        $em->flush();
        
        //Registra la personalizacion en conceptohasplantillanomina
        foreach($vectorProcesar as $vp){
            $entity = new ConceptoHasPlantillaNomina();
            $id_plantilla_guardar = $em->getRepository('CetNominaBundle:PlantillaNomina')->find($id_plantilla);
            $entity->setFkConceptoHasPlantillaNominaPlantillaNomina1($id_plantilla_guardar);
            $entity->setPlantillaNominaId($id_plantilla);
            $id_persona_guardar = $em->getRepository('CetNominaBundle:Personal')->find($id_persona);
            $entity->setFkConceptoHasPlantillaNominaPersonal1($id_persona_guardar);
            $entity->setPersonalCedula($id_persona);
            $id_concepto_guardar = $em->getRepository('CetNominaBundle:Concepto')->findBy(array('codigo'=>$vp));
            $entity->setFkConceptoHasPlantillaNominaConcepto1($id_concepto_guardar[0]);
            $entity->setConceptoId($vp);
            $entity->setActivo(true);
            $em->persist($entity);
        } 
        $em->flush();
  
        $entities = $em->getRepository('CetNominaBundle:PlantillaNomina')->findAll();     
        return $this->render('CetNominaBundle:PlantillaNomina:index.html.twig', array(
            'entities' => $entities,
        ));
    }
}
