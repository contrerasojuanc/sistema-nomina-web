<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Enfermedad
 *
 * @ORM\Entity(repositoryClass="EnfermedadRepository")
 * @ORM\Table(name="sis_nomina.enfermedad", indexes={@ORM\Index(name="fk_enfermedad_informacion_medica1_idx", columns={"informacion_medica_id"})})
 */
class Enfermedad
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     * @ORM\Column(name="tratamiento", type="text", nullable=true)
     */
    private $tratamiento;

    /**
     * @var float
     * @ORM\Column(name="costo_tratamiento", type="float", nullable=true)
     */
    private $costoTratamiento;

    /**
     * @ORM\ManyToOne(targetEntity="InformacionMedica", inversedBy="enfermedads",cascade={"remove"})
     * @ORM\JoinColumn(name="informacion_medica_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_enfermedad_informacion_medica1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Enfermedad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tratamiento
     *
     * @param string $tratamiento
     * @return Enfermedad
     */
    public function setTratamiento($tratamiento)
    {
        $this->tratamiento = $tratamiento;

        return $this;
    }

    /**
     * Get tratamiento
     *
     * @return string 
     */
    public function getTratamiento()
    {
        return $this->tratamiento;
    }

    /**
     * Set costoTratamiento
     *
     * @param float $costoTratamiento
     * @return Enfermedad
     */
    public function setCostoTratamiento($costoTratamiento)
    {
        $this->costoTratamiento = $costoTratamiento;

        return $this;
    }

    /**
     * Get costoTratamiento
     *
     * @return float 
     */
    public function getCostoTratamiento()
    {
        return $this->costoTratamiento;
    }

    /**
     * Set fk_enfermedad_informacion_medica1
     *
     * @param \Cet\NominaBundle\Entity\InformacionMedica $fkEnfermedadInformacionMedica1
     * @return Enfermedad
     */
    public function setFkEnfermedadInformacionMedica1(\Cet\NominaBundle\Entity\InformacionMedica $fkEnfermedadInformacionMedica1)
    {
        $this->fk_enfermedad_informacion_medica1 = $fkEnfermedadInformacionMedica1;

        return $this;
    }

    /**
     * Get fk_enfermedad_informacion_medica1
     *
     * @return \Cet\NominaBundle\Entity\InformacionMedica 
     */
    public function getFkEnfermedadInformacionMedica1()
    {
        return $this->fk_enfermedad_informacion_medica1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
    $metadata   ->addPropertyConstraint('descripcion', new Assert\NotBlank())
               
                ->addPropertyConstraint('tratamiento', new Assert\NotBlank())
                              
                ->addPropertyConstraint('costoTratamiento', new Assert\NotBlank())
                ->addPropertyConstraint('costoTratamiento', new Assert\Type(array('type' => 'numeric','message' => 'El campo debe ser solo números.')))
                
       ;      
    }
}