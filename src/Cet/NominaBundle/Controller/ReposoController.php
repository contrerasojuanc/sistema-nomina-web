<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Reposo;
use Cet\NominaBundle\Form\ReposoType;

/**
 * Reposo controller.
 *
 */
class ReposoController extends Controller
{

    /**
     * Lists all Reposo entities.
     *
     */
    public function indexAction($cedula = null)
    {
        $em = $this->getDoctrine()->getManager();

        $nombre = "";
        if($cedula === null){
            $entities = $em->getRepository('CetNominaBundle:Reposo')->findAll();
        }
        else{
            $entities = $em->getRepository('CetNominaBundle:Reposo')->findBy(array('personal'=>$cedula));
            $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
            if($personal !== null)
                $nombre = $personal->getPrimerNombre()." ".$personal->getSegundoNombre()." ".$personal->getPrimerApellido()." ".$personal->getSegundoApellido();
        }

        return $this->render('CetNominaBundle:Reposo:index.html.twig', array(
            'entities' => $entities,
            'cedula' => $cedula,
            'nombre' => $nombre,
        ));
    }
    /**
     * Creates a new Reposo entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Reposo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reposo_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:Reposo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Reposo entity.
    *
    * @param Reposo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Reposo $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReposoType(), $entity, array(
            'action' => $this->generateUrl('reposo_create'),
            'method' => 'POST',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Reposo entity.
     *
     */
    public function newAction($cedula = null)
    {
        $entity = new Reposo();
        $em = $this->getDoctrine()->getManager();
        if($cedula !== null){
            $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
            $entity->setPersonal($personal);
        }
        
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:Reposo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'cedula' => $cedula,
        ));
    }

    /**
     * Finds and displays a Reposo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Reposo')->find($id);
        $cedula = 0;
        if($entity->getPersonal() !== null)
            $cedula = $entity->getPersonal()->getId();

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Reposo.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Reposo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'cedula'      => $cedula,
            ));
    }

    /**
     * Displays a form to edit an existing Reposo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Reposo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Reposo.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $cedula=0;
        if($entity->getPersonal() !== null)
            $cedula = $entity->getPersonal()->getId();
        
        return $this->render('CetNominaBundle:Reposo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'cedula' => $cedula,
        ));
    }

    /**
    * Creates a form to edit a Reposo entity.
    *
    * @param Reposo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Reposo $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ReposoType(), $entity, array(
            'action' => $this->generateUrl('reposo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing Reposo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Reposo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Reposo.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('reposo_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:Reposo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Reposo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Reposo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Reposo.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        $cedula=0;
        if($entity->getPersonal() !== null)
            $cedula = $entity->getPersonal()->getId();    
            
        return $this->redirect($this->generateUrl('reposo', array('cedula' => $cedula)));
    }

    /**
     * Creates a form to delete a Reposo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reposo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
