<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Archivo
 *
 * @ORM\Entity(repositoryClass="ArchivoRepository")
 * @ORM\Table(name="sis_nomina.archivo")
 */
class Archivo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     * @ORM\Column(name="ruta", type="text", nullable=true)
     */
    private $ruta;

    /**
     * @var string
     * @ORM\Column(name="tipo", type="text", nullable=true)
     */
    private $tipo;

    /**
     * @var float
     * @ORM\Column(name="tamanio", type="float", nullable=true)
     */
    private $tamanio;

    /**
     * @ORM\ManyToOne(targetEntity="Respaldo", inversedBy="archivos")
     * @ORM\JoinColumn(name="respaldo_id", referencedColumnName="id")
     */
    protected $respaldo;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Archivo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     * @return Archivo
     */
    public function setRuta($ruta)
    {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string 
     */
    public function getRuta()
    {
        return $this->ruta;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Archivo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set tamanio
     *
     * @param float $tamanio
     * @return Archivo
     */
    public function setTamanio($tamanio)
    {
        $this->tamanio = $tamanio;

        return $this;
    }

    /**
     * Get tamanio
     *
     * @return float 
     */
    public function getTamanio()
    {
        return $this->tamanio;
    }

    /**
     * Set respaldo
     *
     * @param \Cet\NominaBundle\Entity\Respaldo $respaldo
     * @return Archivo
     */
    public function setRespaldo(\Cet\NominaBundle\Entity\Respaldo $respaldo = null)
    {
        $this->respaldo = $respaldo;

        return $this;
    }

    /**
     * Get respaldo
     *
     * @return \Cet\NominaBundle\Entity\Respaldo 
     */
    public function getRespaldo()
    {
        return $this->respaldo;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
       $metadata->addPropertyConstraint('nombre', new Assert\NotBlank())
                ->addPropertyConstraint('nombre', new Assert\Type(array('type'=>'string')))
//                ->addPropertyConstraint('ruta', new Assert\Email(array('message' => 'El campo no tiene el formato de correo electrónico.')))
                ->addPropertyConstraint('ruta', new Assert\NotBlank())
                ->addPropertyConstraint('tipo', new Assert\NotBlank())
                ->addPropertyConstraint('tipo', new Assert\Type(array('type'=>'string')))                


                ->addPropertyConstraint('tamanio', new Assert\NotBlank())               
                ->addPropertyConstraint('tamanio', new Assert\Type(array('type'=>'numeric')))
            ;
        
    }
}
