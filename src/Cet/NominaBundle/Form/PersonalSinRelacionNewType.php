<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalSinRelacionNewType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null,array(
                'label' => 'Cédula',
                'attr' => array('help'=>'Ingrese el número de cédula. Ejemplo 10000000.')))
                
            ->add('nacionalidad', 'choice', array(
                'label'=> 'Nacionalidad',
                'attr' => array('help'=>'Seleccione alguna de las dos opciones: Venezolano(a) o Extranjero(a).'),
                'choices' => array('V' => 'Venezolano(a)', 'E' => 'Extranjero(a)'),
            ))
             ->add('primerNombre',null,array(
                 'label'=> 'Primer Nombre',
                 'attr' => array('help'=>'Ingrese el primer nombre, no debe tener números ni caracteres especiales.',
                     )))
            
            ->add('segundoNombre',null,array(
                'label'=> 'Segundo Nombre',
                'attr' => array('help'=>'Ingrese el segundo nombre, no debe tener números ni caracteres especiales. (En caso de que no tenga un segundo nombre el campo puede quedar vacio)',
                    )))
                
            ->add('primerApellido',null,array(
                'label'=> 'Primer Apellido',
                'attr' => array('help'=>'Ingrese el primer apellido, no debe tener números ni caracteres especiales.',
                    )))
                
            ->add('segundoApellido',null,array(
                'label'=> 'Segundo Apellido',
                'attr' => array('help'=>'Ingrese el segundo apellido, no debe tener números ni caracteres especiales. (En caso de que no tenga un segundo apellido el campo puede quedar vacio) ',
                    )))
                
            ->add('genero', 'choice', array(
                'label'=> 'Género',
                'attr' => array('help'=>'Seleccione alguna de las dos opciones: Masculino o Femenino.'),
                'choices' => array(''=>'Seleccione el Género', 'M' => 'Masculino', 'F' => 'Femenino'),
            ))
                
            ->add('estadoCivil', 'choice', array(
                'label'=> 'Estado Civil',
                'attr' => array('help'=>'Seleccione alguna de las opciones: Soltero(a), Casado(a), Divorciado(a) o Viudo(a).'),  
                'choices' => array(''=>'Seleccione el estado civil', 'S' => 'Soltero(a)', 'C' => 'Casado(a)', 'D' => 'Divorciado(a)', 'V' => 'Viudo(a)'),
            ))     
            ->add('fechaNacimiento','date', array(
                'widget' => 'single_text',
                'label'=> 'Fecha de Nacimiento',                
                'attr' => array('help'=>'Ingrese la fecha de nacimiento.'),
            ))
//            ->add('libretaMilitar',null,array(
//                'label'=> 'Libreta Militar',
//                'attr' => array('help'=>'Ingrese el número de carnet militar.',
//                    )))
            ->add('rif',null,array(
                'label'=> 'Número de RIF',
                'attr' => array('help'=>'El Nro de Rif debe empezar con la letra E o V seguido de un guión (-) luego 8 números seguidos, un guión (-) y un número. Ejemplos V-87654321-0    E-87654321-0',
                    )))
//            ->add('licencia','choice',array(
//                'label'=> 'Licencia',
//                'attr' => array('help'=>'Seleccione el tipo de Licencia de Conducir.'),
//                'choices' => array(''=>'Seleccione el tipo de Licencia de Conducir', '1' => 'Primera', '2' => 'Segunda', '3' => 'Tercera', '4' => 'Cuarta','5' => 'Quinta'),    
//                    ))
//            ->add('altura',null,array(
//                'label'=> 'Altura',
//                'attr' => array('help'=>'Ingrese la estatura en metros. Ejemplo: 1.80',
//                    )))
//            ->add('peso',null,array(
//                'label'=> 'Peso',
//                'attr' => array('help'=>'Ingrese el peso en kilogramos. Ejemplo: 80',
//                    )))
//            ->add('tallaCamisa','choice',array(
//                'label'=> 'Talla de Camisa',
//                'attr' => array('help'=>'Seleccione de la lista la talla que corresponda'),
//                'choices' => array(''=>'Seleccione la talla de Camisa', 'ss' => 'Muy Pequeña (SS)', 's' => 'Pequeña (S) ', 'm' => 'Mediana (M)' , 'l' => 'Grande (L)','xl' => 'Muy Grande (XL)', 'xxl' => 'Extra Grande (XXL)'),
//                ))
//            ->add('tallaPantalon','choice',array(
//                'label'=> 'Talla de Pantalón',
//                'attr' => array('help'=>'Ingrese la talla del pantalón. Ejemplo: 10 en caso de que sea dama o 30 si es caballero'),
//                'choices' => array(''=>'Seleccione la talla de Pantalón', '26' => '06--26', '28' => '08--28', '30' => '10--30' , '32' => '12--32','34' => '14--34', '36' => '16--36', '38' => '18--38', '38' => '18--38', '40' => '20--40', '42' => '22--42' ),
//                ))
//            
//             ->add('tallaCalzado','choice',array(
//                 'label'=> 'Talla de Calzado',
//                 'attr' => array('help'=>'Ingrese la talla del calzado. Ejemplo: 41'),
//                 'choices' => array(''=>'Seleccione la talla del Calzado','35'=>'35','36'=>'36','37'=>'37','38'=>'38','39'=>'39','40'=>'40','41'=>'41','42'=>'42','43'=>'43','44'=>'44','45'=>'45','46'=>'46','47'=>'47',),    
//                 ))
              ->add('foto', 'file', array(
                'data_class' => null,
                'required'   => false,
              ))   
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Personal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_personal';
    }
}
