<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\orgaFeriadosM;
use Cet\NominaBundle\Form\orgaFeriadosMType;

/**
 * orgaFeriadosM controller.
 *
 */
class orgaFeriadosMController extends Controller
{

    /**
     * Lists all orgaFeriadosM entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:orgaFeriadosM')->findAll();

        return $this->render('CetNominaBundle:orgaFeriadosM:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new orgaFeriadosM entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new orgaFeriadosM();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('orgaferiadosm_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:orgaFeriadosM:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a orgaFeriadosM entity.
    *
    * @param orgaFeriadosM $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(orgaFeriadosM $entity)
    {
        $form = $this->createForm(new orgaFeriadosMType(), $entity, array(
            'action' => $this->generateUrl('orgaferiadosm_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new orgaFeriadosM entity.
     *
     */
    public function newAction()
    {
        $entity = new orgaFeriadosM();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:orgaFeriadosM:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a orgaFeriadosM entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:orgaFeriadosM')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad orgaFeriadosM.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:orgaFeriadosM:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing orgaFeriadosM entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:orgaFeriadosM')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad orgaFeriadosM.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:orgaFeriadosM:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a orgaFeriadosM entity.
    *
    * @param orgaFeriadosM $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(orgaFeriadosM $entity)
    {
        $form = $this->createForm(new orgaFeriadosMType(), $entity, array(
            'action' => $this->generateUrl('orgaferiadosm_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing orgaFeriadosM entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:orgaFeriadosM')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad orgaFeriadosM.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('orgaferiadosm_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:orgaFeriadosM:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a orgaFeriadosM entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:orgaFeriadosM')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad orgaFeriadosM.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('orgaferiadosm'));
    }

    /**
     * Creates a form to delete a orgaFeriadosM entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('orgaferiadosm_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
