<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalInformacionMedicaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoSangre', 'choice', array(
                'label' => 'Tipo de Sangre',
                'attr' => array('help'=>'Seleccione el tipo de sangre del funcionario.'),
                'choices' => array('' => 'Seleccione','O-' => 'O-', 'O+' => 'O+', 'A' => 'A-', 'A+' => 'A+', 'B-' => 'B-', 'B+' => 'B+', 'AB-' => 'AB-', 'AB+' => 'AB+'),
            ))    
            ->add('usaLentes',null,array(
                'label' => 'Usa Lentes',
                'attr' => array('help'=>'Seleccione la opción en caso de que el funcionario utiliza lentes.')))
            ->add('esZurdo',null,array(
                'label' => 'Es Zurdo',
                'attr' => array('help'=>'Seleccione la opción en caso de que el funcionario sea zurdo.')))
            //->add('baseLegal')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\InformacionMedica'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_informacionmedica';
    }
}
