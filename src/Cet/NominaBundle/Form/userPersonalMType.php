<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Cet\NominaBundle\Form\userPersonalSistemasDType;
use Cet\NominaBundle\Entity\userPersonalSistemasD;

class userPersonalMType extends AbstractType
{  
    private $nuevo = true;
    private $vacio = false;
    private $id = null;
    
    public function __construct($valor, $vacio = false, $id = null){
        $this->nuevo = $valor;
        $this->vacio = $vacio;
        $this->id = $id;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\Personal");
        $userTransformer->setEntityRepository("CetNominaBundle:Personal");
        $userTransformer->setEntityType("Personal");
        
//        $entity1 = new userPersonalSistemasD();
//        $entity2 = $options['em']->getRepository('Cet\NominaBundle\Entity\userPersonalM')->findOneBy(array("id" => $this->id));
//        $entity1->setIdUsuario($entity2);
        
        if($this->nuevo || $this->vacio){
            $builder
                ->add('persNombr1','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persApell1','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persCedula','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persNacion','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persLoginu',null,array('label' => 'Usuario','attr' => array('help'=>'Ingrese el nombre de usuario deseado')))
                ->add('persPasswdmd5','password',array(
                    'always_empty' => false,
                    'label' => 'Contraseña',
                    'required' => false,
                    'attr' => array('help'=>'Ingrese la contraseña')
                ))
                //->add('personal',null,array('label' => 'Personal','attr' => array('help'=>'Asigne un funcionario')))
                ->add('personal',null,array('query_builder' => function($r) {
                        return $r->createQueryBuilder('personal')
                                   ->orderBy('personal.id','ASC');
                        },'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione el funcionario a asignar')))
//                ->add('personal',null)
                ->add('personalSistemas','collection',array(
                    'type' => new userPersonalSistemasDType(),
                    'allow_add'    => true,
                    'allow_delete'    => true,
                    'by_reference' => false,
                    'options' => array(
                        'em' => $options['em'],                        
//                        'data' => $entity1
                    ),
                ))
            ;       
        }
        else{
            $builder
                ->add('persNombr1','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persApell1','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persCedula','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persNacion','hidden', array(
                    'empty_data' => '-',
                ))
                ->add('persLoginu',null,array('label' => 'Usuario','attr' => array('help'=>'Ingrese el nombre de usuario deseado')))
                ->add('persPasswdmd5','password',array(
                    'always_empty' => false,
                    'label' => 'Contraseña',                                        
                    'required' => false,
                    'attr' => array('help'=>'Ingrese la contraseña')
                ))
                //->add('personal',null,array('label' => 'Personal','attr' => array('help'=>'Asigne un funcionario')))
                ->add($builder->create('personal','hidden')->addModelTransformer($userTransformer))
                ->add('personalSistemas','collection',array(
                    'type' => new userPersonalSistemasDType(),
                    'allow_add'    => true,
                    'allow_delete'    => true,
                    'by_reference' => false,
                    'options' => array(
                        'em' => $options['em'],                        
//                        'data' => $entity1
                    ),
                ))
            ; 
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\userPersonalM'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_userpersonalm';
    }
}
