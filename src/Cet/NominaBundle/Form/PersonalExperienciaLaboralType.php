<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalExperienciaLaboralType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('institucion',null,array(
                'label'=> 'Institución, Organismo o Empresa ',
                'attr' => array('help'=>'Ingrese el nombre de la institución, organismo o empresa donde laboraba.'),
            ))        
            ->add('tipo', 'choice', array(
                'choices' => array('' => 'Seleccione','A' => 'Administración Pública', 'E' => 'Empresa Privada'),
                'attr' => array('help'=>'Indique si es Admon. Pública o Privada.'),
            ))   
            ->add('fechaIngreso','date', array(
                'widget' => 'single_text',
                'attr' => array('help'=>'Indique la fecha de ingreso a la empresa o institución.'),
            ))    
            ->add('fechaEgreso','date', array(
                'widget' => 'single_text',
                'attr' => array('help'=>'Indique si es Admon. Pública o Privada.'),
                'attr' => array('help'=>'Indique la fecha de egreso de empresa o institución.'),
                'required' => false,
            ))    
            ->add('cargo',null,array('label'=> 'Cargo','attr' => array('help'=>'Ingrese el cargo que desempeñaba.')))
            ->add('sueldo',null,array('label' => 'Sueldo','attr' => array('help'=>'Ingrese el monto del sueldo. Ejemplo:1500,15')))
            //->add('baseLegal')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\ExperienciaLaboral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_experiencialaboral';
    }
}
