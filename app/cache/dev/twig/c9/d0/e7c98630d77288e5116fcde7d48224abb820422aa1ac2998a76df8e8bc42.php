<?php

/* CetNominaBundle:Default:index.html.twig */
class __TwigTemplate_c9d0e7c98630d77288e5116fcde7d48224abb820422aa1ac2998a76df8e8bc42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Hello <b>";
        echo twig_escape_filter($this->env, $this->getContext($context, "name"), "html", null, true);
        echo "<b>!
";
    }

    public function getTemplateName()
    {
        return "CetNominaBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
