<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa;
use Cet\NominaBundle\Form\HistoricoUnidadOrganizativaType;

/**
 * HistoricoUnidadOrganizativa controller.
 *
 */
class HistoricoUnidadOrganizativaController extends Controller
{

    /**
     * Lists all HistoricoUnidadOrganizativa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->findAll();

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new HistoricoUnidadOrganizativa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new HistoricoUnidadOrganizativa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('historicounidadorganizativa_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a HistoricoUnidadOrganizativa entity.
    *
    * @param HistoricoUnidadOrganizativa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(HistoricoUnidadOrganizativa $entity)
    {
        $form = $this->createForm(new HistoricoUnidadOrganizativaType(), $entity, array(
            'action' => $this->generateUrl('historicounidadorganizativa_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new HistoricoUnidadOrganizativa entity.
     *
     */
    public function newAction()
    {
        $entity = new HistoricoUnidadOrganizativa();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a HistoricoUnidadOrganizativa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing HistoricoUnidadOrganizativa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a HistoricoUnidadOrganizativa entity.
    *
    * @param HistoricoUnidadOrganizativa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(HistoricoUnidadOrganizativa $entity)
    {
        $form = $this->createForm(new HistoricoUnidadOrganizativaType(), $entity, array(
            'action' => $this->generateUrl('historicounidadorganizativa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing HistoricoUnidadOrganizativa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('historicounidadorganizativa_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a HistoricoUnidadOrganizativa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('historicounidadorganizativa'));
    }

    /**
     * Creates a form to delete a HistoricoUnidadOrganizativa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('historicounidadorganizativa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
