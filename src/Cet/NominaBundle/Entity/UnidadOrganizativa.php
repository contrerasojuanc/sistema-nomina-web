<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\UnidadOrganizativa
 *
 * @ORM\Entity(repositoryClass="UnidadOrganizativaRepository")
 * @ORM\Table(name="sis_nomina.unidad_organizativa", indexes={@ORM\Index(name="fk_unidad_organizativa_unidad_organizativa1_idx", columns={"unidad_organizativa_id"}), @ORM\Index(name="fk_unidad_organizativa_institucion1_idx", columns={"institucion_id"})})
 */
class UnidadOrganizativa
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * Solo valores:
     * 1 = Direccion
     * 2 = Division
     * 3 = Departamento
     *
     * @var string
     * @ORM\Column(name="nivel", type="text", nullable=true)
     */
    private $nivel;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoUnidadOrganizativa", mappedBy="fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1")
     * @ORM\JoinColumn(name="unidad_organizativa_id", referencedColumnName="id", nullable=false)
     */
    protected $historicoUnidadOrganizativas;

    /**
     * @ORM\OneToMany(targetEntity="UnidadOrganizativa", mappedBy="fk_unidad_organizativa_unidad_organizativa1")
     * @ORM\JoinColumn(name="unidad_organizativa_id", referencedColumnName="id")
     */
    protected $unidadOrganizativas;//Categoria hijos

    /**
     * @ORM\ManyToOne(targetEntity="UnidadOrganizativa", inversedBy="unidadOrganizativas")
     * @ORM\JoinColumn(name="unidad_organizativa_id", referencedColumnName="id")
     */
    protected $fk_unidad_organizativa_unidad_organizativa1;//Categoria padre

    /**
     * @ORM\ManyToOne(targetEntity="Institucion", inversedBy="unidadOrganizativas")
     * @ORM\JoinColumn(name="institucion_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_unidad_organizativa_institucion1;
    
    /**
     * @ORM\OneToMany(targetEntity="HistoricoCargo", mappedBy="fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1")
     * @ORM\JoinColumn(name="unidad_organizativa_id", referencedColumnName="id", nullable=false)
     */
    protected $historicoCargos;

    public function __construct()
    {
        $this->historicoUnidadOrganizativas = new ArrayCollection();
        $this->unidadOrganizativas = new ArrayCollection();
        $this->historicoCargos = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return UnidadOrganizativa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nivel
     *
     * @param integer $nivel
     * @return UnidadOrganizativa
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return integer 
     */
    public function getNivel()
    {
        return $this->nivel;
    }
    
    /**
     * Add historicoUnidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas
     * @return UnidadOrganizativa
     */
    public function addHistoricoUnidadOrganizativa(\Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas)
    {
        $this->historicoUnidadOrganizativas[] = $historicoUnidadOrganizativas;

        return $this;
    }

    /**
     * Remove historicoUnidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas
     */
    public function removeHistoricoUnidadOrganizativa(\Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas)
    {
        $this->historicoUnidadOrganizativas->removeElement($historicoUnidadOrganizativas);
    }

    /**
     * Get historicoUnidadOrganizativas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoUnidadOrganizativas()
    {
        return $this->historicoUnidadOrganizativas;
    }

    /**
     * Add unidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas
     * @return UnidadOrganizativa
     */
    public function addUnidadOrganizativa(\Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas)
    {
        $this->unidadOrganizativas[] = $unidadOrganizativas;

        return $this;
    }

    /**
     * Remove unidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas
     */
    public function removeUnidadOrganizativa(\Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas)
    {
        $this->unidadOrganizativas->removeElement($unidadOrganizativas);
    }

    /**
     * Get unidadOrganizativas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUnidadOrganizativas()
    {
        return $this->unidadOrganizativas;
    }

    /**
     * Set fk_unidad_organizativa_unidad_organizativa1
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $fkUnidadOrganizativaUnidadOrganizativa1
     * @return UnidadOrganizativa
     */
    public function setFkUnidadOrganizativaUnidadOrganizativa1(\Cet\NominaBundle\Entity\UnidadOrganizativa $fkUnidadOrganizativaUnidadOrganizativa1 = null)
    {
        $this->fk_unidad_organizativa_unidad_organizativa1 = $fkUnidadOrganizativaUnidadOrganizativa1;

        return $this;
    }

    /**
     * Get fk_unidad_organizativa_unidad_organizativa1
     *
     * @return \Cet\NominaBundle\Entity\UnidadOrganizativa 
     */
    public function getFkUnidadOrganizativaUnidadOrganizativa1()
    {
        return $this->fk_unidad_organizativa_unidad_organizativa1;
    }

    /**
     * Set fk_unidad_organizativa_institucion1
     *
     * @param \Cet\NominaBundle\Entity\Institucion $fkUnidadOrganizativaInstitucion1
     * @return UnidadOrganizativa
     */
    public function setFkUnidadOrganizativaInstitucion1(\Cet\NominaBundle\Entity\Institucion $fkUnidadOrganizativaInstitucion1)
    {
        $this->fk_unidad_organizativa_institucion1 = $fkUnidadOrganizativaInstitucion1;

        return $this;
    }

    /**
     * Get fk_unidad_organizativa_institucion1
     *
     * @return \Cet\NominaBundle\Entity\Institucion 
     */
    public function getFkUnidadOrganizativaInstitucion1()
    {
        return $this->fk_unidad_organizativa_institucion1;
    }
    
    /**
     * Add historicoCargos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos
     * @return UnidadOrganizativa
     */
    public function addHistoricoCargo(\Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos)
    {
        $this->historicoCargos[] = $historicoCargos;

        return $this;
    }

    /**
     * Remove historicoCargos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos
     */
    public function removeHistoricoCargo(\Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos)
    {
        $this->historicoCargos->removeElement($historicoCargos);
    }

    /**
     * Get historicoCargos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoCargos()
    {
        return $this->historicoCargos;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('nombre', new Assert\NotBlank())
                ->addPropertyConstraint ('nombre', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('nivel', new Assert\NotBlank())
            ;
    } 
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
