<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Usuario
 *
 * Esta tabla esta creada dentro del esquema gen_usuarios.
 *
 * @ORM\Entity(repositoryClass="UsuarioRepository")
 * @ORM\Table(name="sis_nomina.usuario")
 */
class Usuario
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     * @ORM\Column(name="apellido", type="text", nullable=true)
     */
    private $apellido;

    /**
     * @var string
     * @ORM\Column(name="cedula", type="text", nullable=true)
     */
    private $cedula;

    /**
     * @var string
     * @ORM\Column(name="usuario", type="text", nullable=true)
     */
    private $usuario;

    /**
     * Aplicar MD5
     *
     * @var string
     * @ORM\Column(name="clave", type="text", nullable=true, options={"comment"="Aplicar MD5"})
     */
    private $clave;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Usuario
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set cedula
     *
     * @param string $cedula
     * @return Usuario
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return string 
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return Usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Usuario
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }
    
     /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('nombre', new Assert\NotBlank())
//                ->addPropertyConstraint ('nombre', new Assert\Type(array('Type'=> 'string')))
             
//                ->addPropertyConstraint ('nombre', new Assert\Regex(array(
//                    'pattern' => '/\D+/',                  
//                    'pattern' => '/w/',
//                    'match'   => TRUE,
//                    'message' => 'El Nombre no puede contener números',
//                  )))
                ->addPropertyConstraint ('apellido', new Assert\NotBlank())
                ->addPropertyConstraint ('apellido', new Assert\Regex(array(
                    'pattern' => '/(\D+\w)/',                  
//                    'pattern' => '/^[A-Z]{1}+[a-zA-Z]/',
                    'match'   => True,
                    'message' => 'El Apellido no puede contener números',
                  )))            
                ->addPropertyConstraint ('cedula',  new Assert\NotBlank())   
                ->addPropertyConstraint ('cedula', new Assert\Type(array('type' => 'numeric')))
                ->addPropertyConstraint('cedula', new Assert\Length(array(
                    'min'=> 5,
                    'max'=> 10,
                    'minMessage'=> 'El campo no debe ser menor a 5 digitos.',
                    'maxMessage'=> 'El campo no debe ser mayor a 10 digitos.',
                )))  
                ->addPropertyConstraint ('usuario',  new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))             
                ->addPropertyConstraint ('clave',  new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))             
           
            ;
     
    } 
}
