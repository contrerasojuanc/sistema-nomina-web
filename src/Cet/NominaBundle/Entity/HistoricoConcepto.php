<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\HistoricoConcepto
 *
 * @ORM\Entity(repositoryClass="HistoricoConceptoRepository")
 * @ORM\Table(name="sis_nomina.historico_concepto", indexes={@ORM\Index(name="fk_historico_concepto_concepto1_idx", columns={"concepto_id"}), @ORM\Index(name="fk_historico_concepto_personal1_idx", columns={"personal_cedula"}), @ORM\Index(name="fk_historico_concepto_nomina1_idx", columns={"nomina_id"})})
 */
class HistoricoConcepto
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;
    
    /**
     * @var integer
     * @ORM\Column(name="concepto_id", type="integer", nullable=false)
     */
    private $conceptoId;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_fin", type="date", nullable=true)
     */
    private $fechaFin;

    /**
     * @var integer
     * @ORM\Column(name="personal_cedula", type="integer", nullable=false)
     */
    private $personalCedula;

    /**
     * @var integer
     * @ORM\Column(name="nomina_id", type="integer", nullable=false)
     */
    private $nominaId;

    /**
     * @var float
     * @ORM\Column(name="monto", type="float", nullable=true)
     */
    private $monto;

    /**
     * @var float
     * @ORM\Column(name="monto_patronal", type="float", nullable=true)
     */
    private $montoPatronal;

    /**
     * @var float
     * @ORM\Column(name="acumulado", type="float", nullable=true)
     */
    private $acumulado;

    /**
     * @ORM\ManyToOne(targetEntity="Concepto", inversedBy="historicoConceptos")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_historico_concepto_concepto1;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="historicoConceptos")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_historico_concepto_personal1;

    /**
     * @ORM\ManyToOne(targetEntity="Nomina", inversedBy="historicoConceptos")
     * @ORM\JoinColumn(name="nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_historico_concepto_nomina1;

    public function __construct()
    {
    }


    /**
     * Set conceptoId
     *
     * @param integer $conceptoId
     * @return HistoricoConcepto
     */
    public function setConceptoId($conceptoId)
    {
        $this->conceptoId = $conceptoId;

        return $this;
    }

    /**
     * Get conceptoId
     *
     * @return integer 
     */
    public function getConceptoId()
    {
        return $this->conceptoId;
    }
    
    
    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return HistoricoConcepto
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return HistoricoConcepto
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set personalCedula
     *
     * @param integer $personalCedula
     * @return HistoricoConcepto
     */
    public function setPersonalCedula($personalCedula)
    {
        $this->personalCedula = $personalCedula;

        return $this;
    }

    /**
     * Get personalCedula
     *
     * @return integer 
     */
    public function getPersonalCedula()
    {
        return $this->personalCedula;
    }

    /**
     * Set nominaId
     *
     * @param integer $nominaId
     * @return HistoricoConcepto
     */
    public function setNominaId($nominaId)
    {
        $this->nominaId = $nominaId;

        return $this;
    }

    /**
     * Get nominaId
     *
     * @return integer 
     */
    public function getNominaId()
    {
        return $this->nominaId;
    }

    /**
     * Set monto
     *
     * @param string $monto
     * @return HistoricoConcepto
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return string 
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set fk_historico_concepto_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkHistoricoConceptoConcepto1
     * @return HistoricoConcepto
     */
    public function setFkHistoricoConceptoConcepto1(\Cet\NominaBundle\Entity\Concepto $fkHistoricoConceptoConcepto1)
    {
        $this->fk_historico_concepto_concepto1 = $fkHistoricoConceptoConcepto1;

        return $this;
    }

    /**
     * Get fk_historico_concepto_concepto1
     *
     * @return \Cet\NominaBundle\Entity\Concepto 
     */
    public function getFkHistoricoConceptoConcepto1()
    {
        return $this->fk_historico_concepto_concepto1;
    }

    /**
     * Set fk_historico_concepto_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkHistoricoConceptoPersonal1
     * @return HistoricoConcepto
     */
    public function setFkHistoricoConceptoPersonal1(\Cet\NominaBundle\Entity\Personal $fkHistoricoConceptoPersonal1)
    {
        $this->fk_historico_concepto_personal1 = $fkHistoricoConceptoPersonal1;

        return $this;
    }

    /**
     * Get fk_historico_concepto_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkHistoricoConceptoPersonal1()
    {
        return $this->fk_historico_concepto_personal1;
    }

    /**
     * Set fk_historico_concepto_nomina1
     *
     * @param \Cet\NominaBundle\Entity\Nomina $fkHistoricoConceptoNomina1
     * @return HistoricoConcepto
     */
    public function setFkHistoricoConceptoNomina1(\Cet\NominaBundle\Entity\Nomina $fkHistoricoConceptoNomina1)
    {
        $this->fk_historico_concepto_nomina1 = $fkHistoricoConceptoNomina1;

        return $this;
    }

    /**
     * Get fk_historico_concepto_nomina1
     *
     * @return \Cet\NominaBundle\Entity\Nomina 
     */
    public function getFkHistoricoConceptoNomina1()
    {
        return $this->fk_historico_concepto_nomina1;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montoPatronal
     *
     * @param float $montoPatronal
     * @return HistoricoConcepto
     */
    public function setMontoPatronal($montoPatronal)
    {
        $this->montoPatronal = $montoPatronal;

        return $this;
    }

    /**
     * Get montoPatronal
     *
     * @return float 
     */
    public function getMontoPatronal()
    {
        return $this->montoPatronal;
    }

    /**
     * Set acumulado
     *
     * @param float $acumulado
     * @return HistoricoConcepto
     */
    public function setAcumulado($acumulado)
    {
        $this->acumulado = $acumulado;

        return $this;
    }

    /**
     * Get acumulado
     *
     * @return float 
     */
    public function getAcumulado()
    {
        return $this->acumulado;
    }
}
