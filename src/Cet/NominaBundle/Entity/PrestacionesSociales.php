<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\PrestacionesSociales
 *
 * @ORM\Entity(repositoryClass="PrestacionesSocialesRepository")
 * @ORM\Table(name="sis_nomina.prestaciones_sociales")
 */
class PrestacionesSociales
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var integer
     * @ORM\Column(name="dias", type="integer", nullable=true)
     */
    private $dias;

    /**
     * @var float
     * @ORM\Column(name="prestaciones", type="float", nullable=true)
     */
    private $prestaciones;

    /**
     * @var integer
     * @ORM\Column(name="dias_adicionales", type="integer", nullable=true)
     */
    private $diasAdicionales;

    /**
     * @var float
     * @ORM\Column(name="prestaciones_adicionales", type="float", nullable=true)
     */
    private $prestacionesAdicionales;

    /**
     * @var float
     * @ORM\Column(name="ingreso_diario", type="float", nullable=true)
     */
    private $ingresoDiario;
    
    /**
     * @var float
     * @ORM\Column(name="alicota_bono_vacacional", type="float", nullable=true)
     */
    private $alicotaBonoVacacional;
    
    /**
     * @var float
     * @ORM\Column(name="alicota_aguinaldos", type="float", nullable=true)
     */
    private $alicotaAguinaldos;
    
    /**
     * @var float
     * @ORM\Column(name="prestaciones_diaria", type="float", nullable=true)
     */
    private $prestacionesDiaria;
    
    /**
     * @var string
     * @ORM\Column(name="conceptos", type="text", nullable=true)
     */
    private $conceptos;
    
    /**
     * @var string
     * @ORM\Column(name="conceptosAdicionales", type="text", nullable=true)
     */
    private $conceptosAdicionales;
    
    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula")
     */
    protected $personal;
    
    /**
     * @ORM\ManyToOne(targetEntity="PrestacionesSocialesGeneral", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="id_general", referencedColumnName="id")
     */
    protected $general;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     * @return PrestacionesSociales
     */
    public function setDias($dias)
    {
        $this->dias = $dias;

        return $this;
    }

    /**
     * Get dias
     *
     * @return integer 
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set prestaciones
     *
     * @param float $prestaciones
     * @return PrestacionesSociales
     */
    public function setPrestaciones($prestaciones)
    {
        $this->prestaciones = $prestaciones;

        return $this;
    }

    /**
     * Get prestaciones
     *
     * @return float 
     */
    public function getPrestaciones()
    {
        return $this->prestaciones;
    }

    /**
     * Set diasAdicionales
     *
     * @param integer $diasAdicionales
     * @return PrestacionesSociales
     */
    public function setDiasAdicionales($diasAdicionales)
    {
        $this->diasAdicionales = $diasAdicionales;

        return $this;
    }

    /**
     * Get diasAdicionales
     *
     * @return integer 
     */
    public function getDiasAdicionales()
    {
        return $this->diasAdicionales;
    }

    /**
     * Set prestacionesAdicionales
     *
     * @param float $prestacionesAdicionales
     * @return PrestacionesSociales
     */
    public function setPrestacionesAdicionales($prestacionesAdicionales)
    {
        $this->prestacionesAdicionales = $prestacionesAdicionales;

        return $this;
    }

    /**
     * Get prestacionesAdicionales
     *
     * @return float 
     */
    public function getPrestacionesAdicionales()
    {
        return $this->prestacionesAdicionales;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return PrestacionesSociales
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return PrestacionesSociales
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }


    /**
     * Set general
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $general
     * @return PrestacionesSociales
     */
    public function setGeneral(\Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $general = null)
    {
        $this->general = $general;

        return $this;
    }

    /**
     * Get general
     *
     * @return \Cet\NominaBundle\Entity\PrestacionesSocialesGeneral 
     */
    public function getGeneral()
    {
        return $this->general;
    }

    /**
     * Set ingresoDiario
     *
     * @param float $ingresoDiario
     * @return PrestacionesSociales
     */
    public function setIngresoDiario($ingresoDiario)
    {
        $this->ingresoDiario = $ingresoDiario;

        return $this;
    }

    /**
     * Get ingresoDiario
     *
     * @return float 
     */
    public function getIngresoDiario()
    {
        return $this->ingresoDiario;
    }

    /**
     * Set alicotaBonoVacacional
     *
     * @param float $alicotaBonoVacacional
     * @return PrestacionesSociales
     */
    public function setAlicotaBonoVacacional($alicotaBonoVacacional)
    {
        $this->alicotaBonoVacacional = $alicotaBonoVacacional;

        return $this;
    }

    /**
     * Get alicotaBonoVacacional
     *
     * @return float 
     */
    public function getAlicotaBonoVacacional()
    {
        return $this->alicotaBonoVacacional;
    }

    /**
     * Set alicotaAguinaldos
     *
     * @param float $alicotaAguinaldos
     * @return PrestacionesSociales
     */
    public function setAlicotaAguinaldos($alicotaAguinaldos)
    {
        $this->alicotaAguinaldos = $alicotaAguinaldos;

        return $this;
    }

    /**
     * Get alicotaAguinaldos
     *
     * @return float 
     */
    public function getAlicotaAguinaldos()
    {
        return $this->alicotaAguinaldos;
    }

    /**
     * Set prestacionesDiaria
     *
     * @param float $prestacionesDiaria
     * @return PrestacionesSociales
     */
    public function setPrestacionesDiaria($prestacionesDiaria)
    {
        $this->prestacionesDiaria = $prestacionesDiaria;

        return $this;
    }

    /**
     * Get prestacionesDiaria
     *
     * @return float 
     */
    public function getPrestacionesDiaria()
    {
        return $this->prestacionesDiaria;
    }

    /**
     * Set conceptos
     *
     * @param string $conceptos
     * @return PrestacionesSociales
     */
    public function setConceptos($conceptos)
    {
        $this->conceptos = $conceptos;

        return $this;
    }

    /**
     * Get conceptos
     *
     * @return string 
     */
    public function getConceptos()
    {
        return $this->conceptos;
    }

    /**
     * Set conceptosAdicionales
     *
     * @param string $conceptosAdicionales
     * @return PrestacionesSociales
     */
    public function setConceptosAdicionales($conceptosAdicionales)
    {
        $this->conceptosAdicionales = $conceptosAdicionales;

        return $this;
    }

    /**
     * Get conceptosAdicionales
     *
     * @return string 
     */
    public function getConceptosAdicionales()
    {
        return $this->conceptosAdicionales;
    }
}
