<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Reposo
 *
 * @ORM\Entity(repositoryClass="ReposoRepository")
 * @ORM\Table(name="sis_nomina.reposo")
 */
class Reposo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=false)
     */
    private $fechaInicio;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_final", type="datetime", nullable=false)
     */
    private $fechaFinal;
    
    /**
     * @var integer
     * @ORM\Column(name="dias", type="integer", nullable=false)
     */
    private $dias;
    
    /**
     * @var string
     * @ORM\Column(name="diagnostico", type="text", nullable=true)
     */
    private $diagnostico;
    
    /**
     * @var string
     * @ORM\Column(name="interruptor", type="boolean", nullable=true)
     */
    private $interruptor;
    
    /**
     * @ORM\ManyToOne(targetEntity="Especialidad", inversedBy="reposos")
     * @ORM\JoinColumn(name="especialidad_id", referencedColumnName="id")
     */
    protected $especialidad;
    
    /**
     * @ORM\ManyToOne(targetEntity="Medico", inversedBy="reposos")
     * @ORM\JoinColumn(name="medico_id", referencedColumnName="id")
     */
    protected $medico;
    
    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="reposos")
     * @ORM\JoinColumn(name="personal_id", referencedColumnName="cedula")
     */
    protected $personal;

    public function __construct()
    {
        
    }

    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('fechaInicio', new Assert\NotBlank())
                ->addPropertyConstraint('fechaInicio', new Assert\Date()) 
                ->addPropertyConstraint('fechaFinal', new Assert\NotBlank())
                ->addPropertyConstraint('fechaFinal', new Assert\Date())
                ->addPropertyConstraint('dias', new Assert\NotBlank())
             ;          
    }

    public function __toString()
    {
        return $this->getDiagnostico();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Reposo
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFinal
     *
     * @param \DateTime $fechaFinal
     * @return Reposo
     */
    public function setFechaFinal($fechaFinal)
    {
        $this->fechaFinal = $fechaFinal;

        return $this;
    }

    /**
     * Get fechaFinal
     *
     * @return \DateTime 
     */
    public function getFechaFinal()
    {
        return $this->fechaFinal;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     * @return Reposo
     */
    public function setDias($dias)
    {
        $this->dias = $dias;

        return $this;
    }

    /**
     * Get dias
     *
     * @return integer 
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set diagnostico
     *
     * @param string $diagnostico
     * @return Reposo
     */
    public function setDiagnostico($diagnostico)
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return string 
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }

    /**
     * Set interruptor
     *
     * @param boolean $interruptor
     * @return Reposo
     */
    public function setInterruptor($interruptor)
    {
        $this->interruptor = $interruptor;

        return $this;
    }

    /**
     * Get interruptor
     *
     * @return boolean 
     */
    public function getInterruptor()
    {
        return $this->interruptor;
    }

    /**
     * Set especialidad
     *
     * @param \Cet\NominaBundle\Entity\Especialidad $especialidad
     * @return Reposo
     */
    public function setEspecialidad(\Cet\NominaBundle\Entity\Especialidad $especialidad = null)
    {
        $this->especialidad = $especialidad;

        return $this;
    }

    /**
     * Get especialidad
     *
     * @return \Cet\NominaBundle\Entity\Especialidad 
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }

    /**
     * Set medico
     *
     * @param \Cet\NominaBundle\Entity\Medico $medico
     * @return Reposo
     */
    public function setMedico(\Cet\NominaBundle\Entity\Medico $medico = null)
    {
        $this->medico = $medico;

        return $this;
    }

    /**
     * Get medico
     *
     * @return \Cet\NominaBundle\Entity\Medico 
     */
    public function getMedico()
    {
        return $this->medico;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return Reposo
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
}
