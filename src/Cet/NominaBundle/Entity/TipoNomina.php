<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cet\NominaBundle\Entity\TipoNomina
 *
 * @ORM\Entity(repositoryClass="TipoNominaRepository")
 * @ORM\Table(name="sis_nomina.tipo_nomina")
 */
class TipoNomina
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="PlantillaNomina", mappedBy="fk_plantilla_nomina_tipo_nomina1")
     * @ORM\JoinColumn(name="tipo_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $plantillaNominas;
    
    /**
     * @ORM\OneToMany(targetEntity="PrestacionesSocialesGeneral", mappedBy="fk_prestaciones_tipo_nomina1")
     * @ORM\JoinColumn(name="tipo_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $prestaciones;
    
    /**
     * @ORM\OneToMany(targetEntity="InformacionLaboral", mappedBy="fk_informacion_laboral_tipo_nomina1")
     * @ORM\JoinColumn(name="tipo_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $informacionLaborals;

    public function __construct()
    {
        $this->plantillaNominas = new ArrayCollection();
        $this->informacionLaborals = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\TipoNomina
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of nombre.
     *
     * @param string $nombre
     * @return \Entity\TipoNomina
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add PlantillaNomina entity to collection (one to many).
     *
     * @param PlantillaNomina $plantillaNomina
     * @return \Entity\TipoNomina
     */
    public function addPlantillaNomina(PlantillaNomina $plantillaNomina)
    {
        $this->plantillaNominas[] = $plantillaNomina;

        return $this;
    }

    /**
     * Remove PlantillaNomina entity from collection (one to many).
     *
     * @param PlantillaNomina $plantillaNomina
     * @return \Entity\TipoNomina
     */
    public function removePlantillaNomina(PlantillaNomina $plantillaNomina)
    {
        $this->plantillaNominas->removeElement($plantillaNomina);

        return $this;
    }

    /**
     * Get PlantillaNomina entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlantillaNominas()
    {
        return $this->plantillaNominas;
    }

    public function __sleep()
    {
        return array('id', 'nombre');
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Add informacionLaborals
     *
     * @param \Cet\NominaBundle\Entity\InformacionLaboral $informacionLaborals
     * @return TipoNomina
     */
    public function addInformacionLaboral(\Cet\NominaBundle\Entity\InformacionLaboral $informacionLaborals)
    {
        $this->informacionLaborals[] = $informacionLaborals;

        return $this;
    }

    /**
     * Remove informacionLaborals
     *
     * @param \Cet\NominaBundle\Entity\InformacionLaboral $informacionLaborals
     */
    public function removeInformacionLaboral(\Cet\NominaBundle\Entity\InformacionLaboral $informacionLaborals)
    {
        $this->informacionLaborals->removeElement($informacionLaborals);
    }

    /**
     * Get informacionLaborals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInformacionLaborals()
    {
        return $this->informacionLaborals;
    }

    /**
     * Add prestaciones
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $prestaciones
     * @return TipoNomina
     */
    public function addPrestacione(\Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $prestaciones)
    {
        $this->prestaciones[] = $prestaciones;

        return $this;
    }

    /**
     * Remove prestaciones
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $prestaciones
     */
    public function removePrestacione(\Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $prestaciones)
    {
        $this->prestaciones->removeElement($prestaciones);
    }

    /**
     * Get prestaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrestaciones()
    {
        return $this->prestaciones;
    }
}
