<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa
 *
 * @ORM\Entity(repositoryClass="HistoricoUnidadOrganizativaRepository")
 * @ORM\Table(name="sis_nomina.historico_unidad_organizativa", indexes={@ORM\Index(name="fk_unidad_organizativa_has_informacion_laboral_informacion__idx", columns={"informacion_laboral_id"}), @ORM\Index(name="fk_unidad_organizativa_has_informacion_laboral_unidad_organ_idx", columns={"unidad_organizativa_id"})})
 */
class HistoricoUnidadOrganizativa
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_fin", type="date", nullable=true)
     */
    private $fechaFin;

    /**
     * @ORM\ManyToOne(targetEntity="UnidadOrganizativa", inversedBy="historicoUnidadOrganizativas")
     * @ORM\JoinColumn(name="unidad_organizativa_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1;

    /**
     * @ORM\ManyToOne(targetEntity="InformacionLaboral", inversedBy="historicoUnidadOrganizativas")
     * @ORM\JoinColumn(name="informacion_laboral_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_unidad_organizativa_has_informacion_laboral_informacion_la1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return HistoricoUnidadOrganizativa
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return HistoricoUnidadOrganizativa
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $fkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1
     * @return HistoricoUnidadOrganizativa
     */
    public function setFkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1(\Cet\NominaBundle\Entity\UnidadOrganizativa $fkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1)
    {
        $this->fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 = $fkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1;

        return $this;
    }

    /**
     * Get fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1
     *
     * @return \Cet\NominaBundle\Entity\UnidadOrganizativa 
     */
    public function getFkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1()
    {
        return $this->fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1;
    }

    /**
     * Set fk_unidad_organizativa_has_informacion_laboral_informacion_la1
     *
     * @param \Cet\NominaBundle\Entity\InformacionLaboral $fkUnidadOrganizativaHasInformacionLaboralInformacionLa1
     * @return HistoricoUnidadOrganizativa
     */
    public function setFkUnidadOrganizativaHasInformacionLaboralInformacionLa1(\Cet\NominaBundle\Entity\InformacionLaboral $fkUnidadOrganizativaHasInformacionLaboralInformacionLa1)
    {
        $this->fk_unidad_organizativa_has_informacion_laboral_informacion_la1 = $fkUnidadOrganizativaHasInformacionLaboralInformacionLa1;

        return $this;
    }

    /**
     * Get fk_unidad_organizativa_has_informacion_laboral_informacion_la1
     *
     * @return \Cet\NominaBundle\Entity\InformacionLaboral 
     */
    public function getFkUnidadOrganizativaHasInformacionLaboralInformacionLa1()
    {
        return $this->fk_unidad_organizativa_has_informacion_laboral_informacion_la1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('fechaInicio', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('fechaInicio', new Assert\Date())
 
            ;         
    }
}
