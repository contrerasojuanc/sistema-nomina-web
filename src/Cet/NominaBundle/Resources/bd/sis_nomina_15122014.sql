--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: sis_nomina; Type: SCHEMA; Schema: -; Owner: eduardo
--

CREATE SCHEMA sis_nomina;


ALTER SCHEMA sis_nomina OWNER TO eduardo;

SET search_path = sis_nomina, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: archivo; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE archivo (
    id integer NOT NULL,
    respaldo_id integer,
    nombre text,
    ruta text,
    tipo text,
    tamanio double precision
);


ALTER TABLE sis_nomina.archivo OWNER TO eduardo;

--
-- Name: archivo_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE archivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.archivo_id_seq OWNER TO eduardo;

--
-- Name: archivo_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('archivo_id_seq', 1, true);


--
-- Name: base_legal; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE base_legal (
    id integer NOT NULL,
    tipo_base_legal_id integer NOT NULL,
    ruta_digital text,
    fecha_expedicion date,
    fecha_expiracion date,
    observaciones text,
    base_legal_id integer
);


ALTER TABLE sis_nomina.base_legal OWNER TO eduardo;

--
-- Name: base_legal_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE base_legal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.base_legal_id_seq OWNER TO eduardo;

--
-- Name: base_legal_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('base_legal_id_seq', 1, true);


--
-- Name: carga_familiar; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE carga_familiar (
    id integer NOT NULL,
    base_legal_id integer,
    personal_cedula integer NOT NULL,
    nombre text,
    cedula integer,
    parentesco character varying(1) NOT NULL,
    fecha_nacimiento date,
    genero character varying(1) DEFAULT NULL::character varying,
    estudia character varying(1) DEFAULT NULL::character varying,
    nivel_instruccion character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.carga_familiar OWNER TO eduardo;

--
-- Name: carga_familiar_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE carga_familiar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.carga_familiar_id_seq OWNER TO eduardo;

--
-- Name: carga_familiar_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('carga_familiar_id_seq', 1, true);


--
-- Name: cargo; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE cargo (
    id integer NOT NULL,
    nombre text,
    jerarquia integer,
    nivel character varying(5) DEFAULT NULL::character varying,
    sueldo double precision
);


ALTER TABLE sis_nomina.cargo OWNER TO eduardo;

--
-- Name: cargo_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE cargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.cargo_id_seq OWNER TO eduardo;

--
-- Name: cargo_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('cargo_id_seq', 287, true);


--
-- Name: concepto; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE concepto (
    id integer NOT NULL,
    codigo integer NOT NULL,
    identificador text NOT NULL,
    denominacion text NOT NULL,
    descripcion text,
    sql text,
    formula_patronal text,
    tipo text NOT NULL,
    fecha_inicia timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    fecha_fin timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    es_acumulado boolean
);


ALTER TABLE sis_nomina.concepto OWNER TO eduardo;

--
-- Name: concepto_has_concepto; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE concepto_has_concepto (
    concepto_id integer NOT NULL,
    concepto_id1 integer NOT NULL
);


ALTER TABLE sis_nomina.concepto_has_concepto OWNER TO eduardo;

--
-- Name: concepto_has_plantilla_nomina; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE concepto_has_plantilla_nomina (
    concepto_id integer NOT NULL,
    plantilla_nomina_id integer NOT NULL,
    personal_cedula integer NOT NULL,
    activo boolean
);


ALTER TABLE sis_nomina.concepto_has_plantilla_nomina OWNER TO eduardo;

--
-- Name: concepto_has_plantilla_nomina1; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE concepto_has_plantilla_nomina1 (
    plantilla_nomina_id integer NOT NULL,
    concepto_id integer NOT NULL
);


ALTER TABLE sis_nomina.concepto_has_plantilla_nomina1 OWNER TO eduardo;

--
-- Name: concepto_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE concepto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.concepto_id_seq OWNER TO eduardo;

--
-- Name: concepto_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('concepto_id_seq', 51, true);


--
-- Name: correo_personal; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE correo_personal (
    id integer NOT NULL,
    personal_cedula integer NOT NULL,
    correo character varying(45) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.correo_personal OWNER TO eduardo;

--
-- Name: correo_personal_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE correo_personal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.correo_personal_id_seq OWNER TO eduardo;

--
-- Name: correo_personal_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('correo_personal_id_seq', 1, true);


--
-- Name: datos_genericos; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE datos_genericos (
    id integer NOT NULL,
    personal_cedula integer NOT NULL,
    tipo_dato_generico_id integer NOT NULL,
    valor text NOT NULL
);


ALTER TABLE sis_nomina.datos_genericos OWNER TO eduardo;

--
-- Name: datos_genericos_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE datos_genericos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.datos_genericos_id_seq OWNER TO eduardo;

--
-- Name: datos_genericos_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('datos_genericos_id_seq', 1, true);


--
-- Name: datos_globales; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE datos_globales (
    id integer NOT NULL,
    nombre text,
    valor double precision
);


ALTER TABLE sis_nomina.datos_globales OWNER TO eduardo;

--
-- Name: datos_globales_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE datos_globales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.datos_globales_id_seq OWNER TO eduardo;

--
-- Name: datos_globales_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('datos_globales_id_seq', 2, true);


--
-- Name: datos_variables; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE datos_variables (
    id integer NOT NULL,
    personal_cedula integer NOT NULL,
    concepto_id integer NOT NULL,
    nombre text,
    valor double precision NOT NULL,
    modo_afectacion text,
    seccion_afectacion text
);


ALTER TABLE sis_nomina.datos_variables OWNER TO eduardo;

--
-- Name: datos_variables_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE datos_variables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.datos_variables_id_seq OWNER TO eduardo;

--
-- Name: datos_variables_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('datos_variables_id_seq', 1, true);


--
-- Name: enfermedad; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE enfermedad (
    id integer NOT NULL,
    informacion_medica_id integer NOT NULL,
    descripcion text,
    tratamiento text,
    costo_tratamiento double precision
);


ALTER TABLE sis_nomina.enfermedad OWNER TO eduardo;

--
-- Name: enfermedad_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE enfermedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.enfermedad_id_seq OWNER TO eduardo;

--
-- Name: enfermedad_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('enfermedad_id_seq', 1, true);


--
-- Name: estado; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE estado (
    id integer NOT NULL,
    nombre character varying(45) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.estado OWNER TO eduardo;

--
-- Name: estado_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE estado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.estado_id_seq OWNER TO eduardo;

--
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('estado_id_seq', 25, true);


--
-- Name: estudio; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE estudio (
    id integer NOT NULL,
    base_legal_id integer,
    personal_cedula integer NOT NULL,
    descripcion text,
    duracion text,
    tipo integer NOT NULL,
    fecha_inicio date,
    fecha_culminacion date,
    institucion text,
    estado character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.estudio OWNER TO eduardo;

--
-- Name: estudio_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE estudio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.estudio_id_seq OWNER TO eduardo;

--
-- Name: estudio_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('estudio_id_seq', 2, true);


--
-- Name: experiencia_laboral; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE experiencia_laboral (
    id integer NOT NULL,
    base_legal_id integer,
    personal_cedula integer NOT NULL,
    institucion text,
    fecha_ingreso date,
    fecha_egreso date,
    cargo text,
    sueldo double precision,
    tipo character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.experiencia_laboral OWNER TO eduardo;

--
-- Name: experiencia_laboral_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE experiencia_laboral_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.experiencia_laboral_id_seq OWNER TO eduardo;

--
-- Name: experiencia_laboral_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('experiencia_laboral_id_seq', 4, true);


--
-- Name: formula; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE formula (
    variable_id integer NOT NULL,
    concepto_id integer NOT NULL,
    posicion integer
);


ALTER TABLE sis_nomina.formula OWNER TO eduardo;

--
-- Name: historico_cargo; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE historico_cargo (
    id integer NOT NULL,
    informacion_laboral_id integer NOT NULL,
    cargo_id integer NOT NULL,
    unidad_organizativa_id integer,
    fecha_inicio date,
    fecha_fin date,
    es_encargado boolean,
    numero_resolucion integer NOT NULL,
    fecha_resolucion date NOT NULL
);


ALTER TABLE sis_nomina.historico_cargo OWNER TO eduardo;

--
-- Name: historico_cargo_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE historico_cargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.historico_cargo_id_seq OWNER TO eduardo;

--
-- Name: historico_cargo_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('historico_cargo_id_seq', 300, true);


--
-- Name: historico_concepto; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE historico_concepto (
    id integer NOT NULL,
    concepto_id integer NOT NULL,
    personal_cedula integer NOT NULL,
    nomina_id integer NOT NULL,
    fecha_inicio date,
    fecha_fin date,
    monto double precision,
    monto_patronal double precision,
    acumulado double precision
);


ALTER TABLE sis_nomina.historico_concepto OWNER TO eduardo;

--
-- Name: historico_concepto_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE historico_concepto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.historico_concepto_id_seq OWNER TO eduardo;

--
-- Name: historico_concepto_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('historico_concepto_id_seq', 1, true);


--
-- Name: historico_evento; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE historico_evento (
    id integer NOT NULL,
    esquema text,
    tabla text,
    usuarioconexion text,
    usuarioaplicacion text,
    ipconexion text,
    ipaplicacion text,
    sistema text,
    fecha timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    accion text,
    datosviejos text,
    datosnuevos text,
    sentencia text
);


ALTER TABLE sis_nomina.historico_evento OWNER TO eduardo;

--
-- Name: historico_evento_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE historico_evento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.historico_evento_id_seq OWNER TO eduardo;

--
-- Name: historico_evento_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('historico_evento_id_seq', 1, true);


--
-- Name: historico_unidad_organizativa; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE historico_unidad_organizativa (
    id integer NOT NULL,
    unidad_organizativa_id integer NOT NULL,
    informacion_laboral_id integer NOT NULL,
    fecha_inicio date,
    fecha_fin date
);


ALTER TABLE sis_nomina.historico_unidad_organizativa OWNER TO eduardo;

--
-- Name: historico_unidad_organizativa_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE historico_unidad_organizativa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.historico_unidad_organizativa_id_seq OWNER TO eduardo;

--
-- Name: historico_unidad_organizativa_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('historico_unidad_organizativa_id_seq', 300, true);


--
-- Name: historico_variable; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE historico_variable (
    id integer NOT NULL,
    variable_id integer NOT NULL,
    personal_cedula integer NOT NULL,
    fecha_inicio timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    fecha_fin timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    valor text,
    sql text
);


ALTER TABLE sis_nomina.historico_variable OWNER TO eduardo;

--
-- Name: historico_variable_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE historico_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.historico_variable_id_seq OWNER TO eduardo;

--
-- Name: historico_variable_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('historico_variable_id_seq', 1, true);


--
-- Name: informacion_cultural_deportiva; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE informacion_cultural_deportiva (
    id integer NOT NULL,
    personal_cedula integer NOT NULL,
    nombre text,
    tipo character varying(1) DEFAULT NULL::character varying,
    representa_institucion character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.informacion_cultural_deportiva OWNER TO eduardo;

--
-- Name: informacion_cultural_deportiva_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE informacion_cultural_deportiva_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.informacion_cultural_deportiva_id_seq OWNER TO eduardo;

--
-- Name: informacion_cultural_deportiva_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('informacion_cultural_deportiva_id_seq', 1, true);


--
-- Name: informacion_laboral; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE informacion_laboral (
    id integer NOT NULL,
    base_legal_id integer,
    personal_cedula integer NOT NULL,
    tipo_nomina_id integer NOT NULL,
    cuenta_nomina text,
    tipo_cuenta_nomina integer DEFAULT 0,
    cuenta_fideicomiso text,
    tipo_cuenta_fideicomiso integer DEFAULT 0,
    fecha_ingreso date,
    fecha_egreso date,
    condicion_egreso character varying(1) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.informacion_laboral OWNER TO eduardo;

--
-- Name: informacion_laboral_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE informacion_laboral_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.informacion_laboral_id_seq OWNER TO eduardo;

--
-- Name: informacion_laboral_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('informacion_laboral_id_seq', 300, true);


--
-- Name: informacion_medica; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE informacion_medica (
    id integer NOT NULL,
    base_legal_id integer,
    personal_cedula integer NOT NULL,
    tipo_sangre text,
    usa_lentes boolean,
    es_zurdo boolean
);


ALTER TABLE sis_nomina.informacion_medica OWNER TO eduardo;

--
-- Name: informacion_medica_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE informacion_medica_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.informacion_medica_id_seq OWNER TO eduardo;

--
-- Name: informacion_medica_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('informacion_medica_id_seq', 2, true);


--
-- Name: institucion; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE institucion (
    id integer NOT NULL,
    nombre text
);


ALTER TABLE sis_nomina.institucion OWNER TO eduardo;

--
-- Name: institucion_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE institucion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.institucion_id_seq OWNER TO eduardo;

--
-- Name: institucion_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('institucion_id_seq', 1, true);


--
-- Name: municipio; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE municipio (
    id integer NOT NULL,
    estado_id integer NOT NULL,
    nombre character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.municipio OWNER TO eduardo;

--
-- Name: municipio_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE municipio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.municipio_id_seq OWNER TO eduardo;

--
-- Name: municipio_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('municipio_id_seq', 462, true);


--
-- Name: nomina; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE nomina (
    id integer NOT NULL,
    plantilla_nomina_id integer NOT NULL,
    descripcion text,
    fecha date,
    desde date,
    hasta date,
    periodo integer,
    anio integer,
    tipo text
);


ALTER TABLE sis_nomina.nomina OWNER TO eduardo;

--
-- Name: nomina_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE nomina_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.nomina_id_seq OWNER TO eduardo;

--
-- Name: nomina_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('nomina_id_seq', 1, true);


--
-- Name: parroquia; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE parroquia (
    id integer NOT NULL,
    municipio_id integer NOT NULL,
    nombre character varying(125) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.parroquia OWNER TO eduardo;

--
-- Name: parroquia_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE parroquia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.parroquia_id_seq OWNER TO eduardo;

--
-- Name: parroquia_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('parroquia_id_seq', 1138, true);


--
-- Name: personal; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE personal (
    cedula integer NOT NULL,
    nacionalidad character varying(1) DEFAULT NULL::character varying,
    primer_nombre text,
    segundo_nombre text,
    primer_apellido text,
    segundo_apellido text,
    genero character varying(1) DEFAULT NULL::character varying,
    estado_civil character varying(1) DEFAULT NULL::character varying,
    fecha_nacimiento date,
    libreta_militar character varying(45) DEFAULT NULL::character varying,
    rif character varying(45) DEFAULT NULL::character varying,
    licencia character varying(45) DEFAULT NULL::character varying,
    foto text,
    ruta_foto text,
    altura character varying(10) DEFAULT NULL::character varying,
    peso character varying(10) DEFAULT NULL::character varying,
    talla_camisa character varying(10) DEFAULT NULL::character varying,
    talla_pantalon character varying(10) DEFAULT NULL::character varying,
    talla_calzado character varying(10) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.personal OWNER TO eduardo;

--
-- Name: plantilla_nomina; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE plantilla_nomina (
    id integer NOT NULL,
    tipo_nomina_id integer NOT NULL,
    nombre text,
    frecuencia text
);


ALTER TABLE sis_nomina.plantilla_nomina OWNER TO eduardo;

--
-- Name: plantilla_nomina_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE plantilla_nomina_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.plantilla_nomina_id_seq OWNER TO eduardo;

--
-- Name: plantilla_nomina_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('plantilla_nomina_id_seq', 1, true);


--
-- Name: reporte; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE reporte (
    id integer NOT NULL,
    nomina_id integer NOT NULL,
    descripcion text,
    fecha date
);


ALTER TABLE sis_nomina.reporte OWNER TO eduardo;

--
-- Name: reporte_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE reporte_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.reporte_id_seq OWNER TO eduardo;

--
-- Name: reporte_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('reporte_id_seq', 1, true);


--
-- Name: respaldo; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE respaldo (
    id integer NOT NULL,
    nomina_id integer NOT NULL,
    descripcion text,
    fecha timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE sis_nomina.respaldo OWNER TO eduardo;

--
-- Name: respaldo_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE respaldo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.respaldo_id_seq OWNER TO eduardo;

--
-- Name: respaldo_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('respaldo_id_seq', 1, true);


--
-- Name: telefono_personal; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE telefono_personal (
    id integer NOT NULL,
    personal_cedula integer NOT NULL,
    telefono character varying(45) DEFAULT NULL::character varying
);


ALTER TABLE sis_nomina.telefono_personal OWNER TO eduardo;

--
-- Name: telefono_personal_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE telefono_personal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.telefono_personal_id_seq OWNER TO eduardo;

--
-- Name: telefono_personal_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('telefono_personal_id_seq', 3, true);


--
-- Name: tipo_base_legal; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE tipo_base_legal (
    id integer NOT NULL,
    tipo text
);


ALTER TABLE sis_nomina.tipo_base_legal OWNER TO eduardo;

--
-- Name: tipo_base_legal_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE tipo_base_legal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.tipo_base_legal_id_seq OWNER TO eduardo;

--
-- Name: tipo_base_legal_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('tipo_base_legal_id_seq', 1, true);


--
-- Name: tipo_dato_generico; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE tipo_dato_generico (
    id integer NOT NULL,
    nombre text
);


ALTER TABLE sis_nomina.tipo_dato_generico OWNER TO eduardo;

--
-- Name: tipo_dato_generico_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE tipo_dato_generico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.tipo_dato_generico_id_seq OWNER TO eduardo;

--
-- Name: tipo_dato_generico_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('tipo_dato_generico_id_seq', 5, true);


--
-- Name: tipo_nomina; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE tipo_nomina (
    id integer NOT NULL,
    nombre text
);


ALTER TABLE sis_nomina.tipo_nomina OWNER TO eduardo;

--
-- Name: tipo_nomina_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE tipo_nomina_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.tipo_nomina_id_seq OWNER TO eduardo;

--
-- Name: tipo_nomina_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('tipo_nomina_id_seq', 7, true);


--
-- Name: unidad_organizativa; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE unidad_organizativa (
    id integer NOT NULL,
    unidad_organizativa_id integer,
    institucion_id integer NOT NULL,
    nombre text,
    nivel integer
);


ALTER TABLE sis_nomina.unidad_organizativa OWNER TO eduardo;

--
-- Name: unidad_organizativa_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE unidad_organizativa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.unidad_organizativa_id_seq OWNER TO eduardo;

--
-- Name: unidad_organizativa_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('unidad_organizativa_id_seq', 99, true);


--
-- Name: usuario; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE usuario (
    id integer NOT NULL,
    nombre text,
    apellido text,
    cedula text,
    usuario text,
    clave text
);


ALTER TABLE sis_nomina.usuario OWNER TO eduardo;

--
-- Name: COLUMN usuario.clave; Type: COMMENT; Schema: sis_nomina; Owner: eduardo
--

COMMENT ON COLUMN usuario.clave IS 'Aplicar MD5';


--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.usuario_id_seq OWNER TO eduardo;

--
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('usuario_id_seq', 1, true);


--
-- Name: variable; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE variable (
    id integer NOT NULL,
    nombre text NOT NULL,
    descripcion text,
    tipo character varying(1) DEFAULT NULL::character varying,
    sql text,
    tipo_gestor_sql character varying(1) DEFAULT NULL::character varying,
    fecha_inicia date,
    fecha_fin date
);


ALTER TABLE sis_nomina.variable OWNER TO eduardo;

--
-- Name: variable_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.variable_id_seq OWNER TO eduardo;

--
-- Name: variable_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('variable_id_seq', 20, true);


--
-- Name: vehiculo; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE vehiculo (
    id integer NOT NULL,
    personal_cedula integer NOT NULL,
    placa character varying(10) DEFAULT NULL::character varying,
    modelo character varying(45) DEFAULT NULL::character varying,
    anio integer
);


ALTER TABLE sis_nomina.vehiculo OWNER TO eduardo;

--
-- Name: vehiculo_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE vehiculo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.vehiculo_id_seq OWNER TO eduardo;

--
-- Name: vehiculo_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('vehiculo_id_seq', 2, true);


--
-- Name: vivienda; Type: TABLE; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE TABLE vivienda (
    id integer NOT NULL,
    parroquia_id integer NOT NULL,
    personal_cedula integer NOT NULL,
    condicion character varying(1) DEFAULT NULL::character varying,
    direccion text NOT NULL,
    codigo_postal character varying(10) DEFAULT NULL::character varying,
    referencia text
);


ALTER TABLE sis_nomina.vivienda OWNER TO eduardo;

--
-- Name: vivienda_id_seq; Type: SEQUENCE; Schema: sis_nomina; Owner: eduardo
--

CREATE SEQUENCE vivienda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sis_nomina.vivienda_id_seq OWNER TO eduardo;

--
-- Name: vivienda_id_seq; Type: SEQUENCE SET; Schema: sis_nomina; Owner: eduardo
--

SELECT pg_catalog.setval('vivienda_id_seq', 4, true);


--
-- Data for Name: archivo; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: base_legal; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: carga_familiar; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: cargo; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (1, 'ALTOS FUNCIONARIOS', 1, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (2, 'DIRECTIVO', 3, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (3, 'DIRECTIVO FISCAL', 3, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (4, 'JEFE DE OFICINA', 4, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (5, 'JEFE DE OFICINA FISCAL', 4, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (6, 'JEF.D/OFIC,JEF.D/DIV,SUPERVISO', 4, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (7, 'PROFESIONAL FISCAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (8, 'PROFESIONAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (9, 'TECNICO FISCAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (10, 'TECNICO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (11, 'ADMINISTRATIVO FISCAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (12, 'ADMINISTRATIVO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (13, 'OBREROS', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (14, 'CONTRATADO (ADMINISTRATIVO)', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (15, 'CONTRATADO (OBRERO)', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (16, 'PENSIONADO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (17, 'CARGO VACANTE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (18, 'SUPLENTE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (19, 'CONTRATADO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (20, 'CONTRATADO L.O.T.T.T.', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (21, 'JUBILADO/PENSIONADO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (22, 'CARGO VACANTE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (23, 'SUPLENTE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (24, 'CONTRATADO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (25, 'CONTRATADO L.O.T.', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (26, 'JUBILADO/PENSIONADO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (27, 'CONTRALOR DEL ESTADO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (28, 'DIRECTORA GENERAL', 2, '', 24484);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (29, 'DIRECTOR', 3, '', 21418);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (30, 'DIRECTOR (E)', 3, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (31, 'AUDITOR INTERNO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (32, 'AUDITOR INTERNO (E)', 7, '', 21418);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (33, 'COORDINADOR DEL DESPACHO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (34, 'COORD. D/RELAC. INTERINSTITUC.', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (35, 'COORDINADOR DE RELAC. PUBLICAS', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (36, 'COOR.DE COOPERACIONTECNICA', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (37, 'JEFE DE OFICINA', 4, '', 14321);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (38, 'JEFE DE DIV.GRAL.DEAUDITORIAS', 4, '', 13759);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (39, 'JEFE D/DIV.GRAL.D/AUDITORIA(E)', 4, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (40, 'JEFE D/DIV.GRAL D/POTEST,INVES', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (41, 'JEFE D/DIV.GRAL DEREC.HUMANOS', 4, '', 13759);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (42, 'JEFE DIV.GRL.D/INVESTIGACIONES', 4, '', 13759);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (43, 'SUPERV.DE SERVICIOSGENERALES', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (44, 'SUPERVISOR DE APOYO', 5, '', 12262);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (45, 'JEFE D/DIV. SEGUR.Y TRANSP.', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (46, 'ABOGADO JEFE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (47, 'ASISTENTE DEL DESPACHO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (48, 'JEFE DE DIVISION', 5, '', 12262);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (49, 'JEFE DE DIVISION LEGAL', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (50, 'SUPERVISOR DE AUDITORIA', 5, '', 12262);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (51, 'SUPERVISOR (E)', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (52, 'SUPERV.D/EXAMEN D/LA CUENTA(E)', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (53, 'SUPERV.DE ASUNTOS LEGALES (E)', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (54, 'SUPERVISOR DE AUDITORIA', 5, '', 12262);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (55, 'SUP. DE POTESTAD INVESTIGATIVA', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (56, 'SUPERV.DE EXAMEN DELA CUENTA', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (57, 'SUPERVISOR DE INVESTIGACIONES', 5, '', 12262);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (58, 'PERIODISTA', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (59, 'JEFE DE DEPARTAMENTO', 6, '', 9230);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (60, 'ABOGADO VI', 7, 'VI', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (61, 'AUDITOR VI', 7, 'VI', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (62, 'PLANIFICADOR VI', 7, 'VI', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (63, 'ABOGADO V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (64, 'ANAL.DE COMPRAS E INVENTAR. V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (65, 'ANALISTA DE EXAM.DELA CTA.V', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (66, 'ANAL.DE SEGURIDAD LABORAL V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (67, 'AUDITOR V', 7, 'V', 10390);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (68, 'PLANIFICADOR V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (69, 'ABOGADO IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (70, 'ANALISTA COMUNICACIONAL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (71, 'ANALISTA CONTABLE IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (72, 'ANAL.DE COMPRAS E INVENTAR.IV', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (73, 'ANALISTA DE EVALUACION IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (74, 'ANALISTA DE EXAM.DELA CTA. IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (75, 'ANAL.DE SEGURIDAD LABORAL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (76, 'AUDITOR IV', 7, 'IV', 9641);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (77, 'PLANIFICADOR IV', 7, 'IV', 9641);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (78, 'TRABAJADOR SOCIAL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (79, 'ABOGADO III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (80, 'ANALISTA COMUNICACIONAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (81, 'ANALISTA CONTABLE III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (82, 'ANAL.DE COMPRAS E INVENT.III', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (83, 'ANALISTA DE EVALUACION III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (84, 'ANALISTA DE EXAM.DELA CTA.III', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (85, 'ANAL.DE ORGANIZ.Y METODOS III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (86, 'ANAL.DE SEGURIDAD LABORAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (87, 'AUDITOR III', 7, 'III', 8892);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (88, 'CONTADOR', 7, '', 10390);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (89, 'PLANIFICADOR III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (90, 'PROMOTOR SOCIAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (91, 'TRABAJADOR SOCIAL III', 7, 'III', 8892);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (92, 'SEGURIDAD III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (93, 'ABOGADO II', 7, 'II', 8143);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (94, 'ANALISTA COMUNICACIONAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (95, 'ANALISTA CONTABLE II', 7, 'II', 8143);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (96, 'ANAL.DE COMPRAS E INVENTA. II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (97, 'ANALISTA DE EVALUACION II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (98, 'ANALISTA D/EXAMEN D/LA CTA. II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (99, 'ANAL.DE ORGANIZ.Y METODOS II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (100, 'ANAL.DE SEGURIDAD LABORAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (101, 'AUDITOR II', 7, 'II', 8143);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (102, 'MEDICO', 7, '', 6986.1000000000004);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (103, 'PLANIFICADOR II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (104, 'PROMOTOR SOCIAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (105, 'TRABAJADOR SOCIAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (106, 'MEDICO (1/2 TIEMPO)', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (107, 'ANALISTA DE SISTEMAS II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (108, 'ANALISTA DE PERSONAL II', 7, 'II', 8143);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (109, 'ANAL.D/RECLU.Y SELEC.D/PERS.II', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (110, 'ABOGADO I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (111, 'ANALISTA COMUNICACIONAL I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (112, 'ANALISTA CONTABLE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (113, 'ANALISTA DE COMPRASE INVENT.', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (114, 'ANALISTA DE EVALUACION', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (115, 'ANALIST.DE EXAMEN DE LA CTA. I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (116, 'ANALISTA D/ORG. Y METODOS I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (117, 'ANAL.DE SEGURIDAD LABORAL I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (118, 'AUDITOR I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (119, 'PLANIFICADOR I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (120, 'PROMOTOR SOCIAL I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (121, 'TRABAJADOR SOCIAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (122, 'ANALISTA DE PERSONAL I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (123, 'ANALISTA DE MANTENIM.Y SERV.GR', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (124, 'DOCENTE I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (125, 'ANALISTA DE SISTEMAS I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (126, 'ANAL.D/RECLU.Y SELEC.D/PERS. I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (127, 'SEGURIDAD I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (128, 'ANAL.D/ORGANIZACIONCULTURAL', 7, '', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (129, 'ANALISTA DE SOPORTETECNICO I', 7, 'I', 7394);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (130, 'ASISTENTE ADMINISTRATIVO V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (131, 'ASISTENTE DE AUDITORIA V', 7, 'V', 7283);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (132, 'ASISTENTE DE CONTROL V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (133, 'ASISTENTE DE PERSONAL V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (134, 'ASISTENTE DE PRESUPUESTO V', 7, 'V', 7283);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (135, 'ASISTENTE DE SISTEMAS V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (136, 'ASISTENTE PROMOTORSOCIAL V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (137, 'PROGRAMADOR V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (138, 'ASISTENTE ADMINISTRATIVO IV', 7, 'IV', 6815);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (139, 'ASISTENTE DE AUDITORIA IV', 7, 'IV', 6815);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (140, 'ASISTENTE DE CONTROL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (141, 'ASISTENTE DE PERSONAL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (142, 'ASISTENTE DE PRESUPUESTO IV', 7, 'IV', 6815);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (143, 'ASIST.DE SERVICIOSGRALES IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (144, 'ASISTENTE DE SISTEMAS IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (145, 'ASISTENTE PROMOTORSOCIAL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (146, 'PROGRAMADOR IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (147, 'ASISTENTE TRABAJADOR SOCIAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (148, 'ASISTENTE D/INVENTARIO Y ALMAC', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (149, 'ASISTENTE ADMINISTRATIVO III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (150, 'ASIT.D.ACTIV.DEPOR.Y RECRE.III', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (151, 'ASISTENTE DE ARCHIVO III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (152, 'ASIST.DE AUDITORIAIII', 7, '', 6347);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (153, 'ASISTENTE DE CONTROL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (154, 'ASISTENTE DE PERSONAL III', 7, 'III', 6347);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (155, 'ASISTENTE DE PRESUPUESTO III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (156, 'ASIST.DE SERVICIOSGRLES.III', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (157, 'ASISTENTE DE SISTEMAS III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (158, 'ASISTENTE PROMOTORSOCIAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (159, 'ASIST.TRABAJADOR SOCIAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (160, 'PROGRAMADOR III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (161, 'ASISTENTE DE EVALUACION III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (162, 'ASISTENTE DE REDESIII', 7, '', 6347);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (163, 'ASISTENTE ADMINISTRATIVO II', 7, 'II', 5879);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (164, 'ASIST.D.ACTIV.DEPORT.Y RECR.II', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (165, 'ASISTENTE DE ARCHIVO II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (166, 'ASISTENTE DE AUDITORIA II', 7, 'II', 5879);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (167, 'ASISTENTE DE COMPRAS II', 7, 'II', 5879);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (168, 'ASISTENTE DE PERSONAL II', 7, 'II', 5879);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (169, 'ASISTENTE DE PRESUPUESTO II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (170, 'ASIST.DE SERVICIOSGRALES II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (171, 'ASISTENTE DE SISTEMAS II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (172, 'ASISTENTE PROMOTORSOCIAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (173, 'ASISTENTE TRABAJADOR SOCIAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (174, 'PROGRAMADOR II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (175, 'ASISTENTE DE INVESTIGACIONES', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (176, 'ASISTENTE ADMINISTRATIVO I', 7, 'I', 5411);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (177, 'ASIST.DE ACTIV.DEPORT.Y RECRE.', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (178, 'ASISTENTE DE ARCHIVO I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (179, 'ASISTENTE DE AUDITORIA I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (180, 'BIBLIOTECARIO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (181, 'ASISTENTE DE PERSONAL I', 7, 'I', 5411);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (182, 'ASISTENTE DE PRESUPUESTO I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (183, 'ASIST.DE SERVICIOSGRALES.', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (184, 'ASISTENTE DE SISTEMAS', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (185, 'ASISTENTE PROMOTORSOCIAL I', 7, 'I', 5411);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (186, 'ASISTENTE TRABAJADOR SOCIAL I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (187, 'PROGRAMADOR I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (188, 'ASISTENTE DE EVALUACION I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (189, 'ASISTENTE DE COMPRAS I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (190, 'ENFERMERA I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (191, 'ASISTENTE DE SEGURIDAD I', 7, 'I', 5411);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (192, 'ASISTENTE DE INVESTIGACIONES I', 7, 'I', 5411);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (193, 'ASISTENTE DE SEG.LABORAL I', 7, 'I', 5411);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (194, 'ASISTENTE DE TRANSPORTE I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (195, 'SECRETARIA EJECUTIVA', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (196, 'SECRETARIA VI', 7, 'VI', 6646);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (197, 'SECRETARIA V', 7, 'V', 6195);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (198, 'SECRETARIA IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (199, 'SECRETARIA III', 7, 'III', 5633);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (200, 'SECRETARIA II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (201, 'SECRETARIA I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (202, 'SECRETARIA I (COMISIONES)', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (203, 'AUXILIAR DE OFICINAV', 7, '', 5775);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (204, 'AUXILIAR DE AUDITORIA V', 7, 'V', 5775);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (205, 'AUXILIAR DE CONTROLV', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (206, 'AUXIL.DE EVENTOS ESPECIALES V', 7, 'V', 5775);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (207, 'AUXILIAR DE PROGRAMACION V', 7, 'V', 5775);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (208, 'AUXILIAR DE SISTEMAS V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (209, 'AUXILIAR PROMOTOR SOCIAL V', 7, 'V', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (210, 'AUXILIAR DE SERV. GRALES. V', 7, 'V', 5775);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (211, 'OPERAD.DE EQUIP.DEREPRODUC.V', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (212, 'TECNIC.D REPARAC.YMANTENIMIEN', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (213, 'TELEFONISTA', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (214, 'ARCHIVISTA IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (215, 'AUXILIAR DE ARCHIVOIV', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (216, 'AUXILIAR DE AUDITORIA IV', 7, 'IV', 5551);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (217, 'AUXILIAR DE CONTROLIV', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (218, 'AUXIL.DE EVENTOS ESPECIALES IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (219, 'AUXILIAR DE PROGRAMACION IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (220, 'AUXIL.DE SERVICIOSGRALES. IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (221, 'AUXILIAR DE SISTEMAS IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (222, 'AUXILIAR PROMOTOR SOCIAL IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (223, 'NOTIFICADOR IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (224, 'OPERADOR D.EQUIP.DREPRODUC.IV', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (225, 'RECEPCIONISTA IV', 7, 'IV', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (226, 'AUXILIAR DE OFICINAIV', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (227, 'ALMACENISTA III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (228, 'ARCHIVISTA III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (229, 'AUXILIAR DE ARCHIVOIII', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (230, 'AUXILIAR DE AUDITORIA III', 7, 'III', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (231, 'AUXILIAR DE CONTROLIII', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (232, 'AUXIL.DE EVENTOS ESPEC.III', 7, '', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (233, 'AUXILIAR DE PROGRAMACION III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (234, 'AUXIL.DE SERVICIOSGRALES. III', 7, 'III', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (235, 'AUXILIAR DE SISTEMAS III', 7, 'III', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (236, 'AUXILIAR PROMOTOR SOCIAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (237, 'AUXILIAR TRABAJADORSOCIAL III', 7, 'III', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (238, 'NOTIFICADOR III', 7, 'III', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (239, 'OPERADOR D.EQUIP.D.REPROD.III', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (240, 'RECEPCIONISTA III', 7, 'III', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (241, 'REGISTRADOR D BIENES Y MAT.III', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (242, 'AUXILIAR DE OFICINAIII', 7, '', 5326);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (243, 'DISE¥ADOR WEB', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (244, 'ALMACENISTA II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (245, 'ARCHIVISTA II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (246, 'AUXILIAR DE ARCHIVOII', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (247, 'AUXILIAR DE AUDITORIA II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (248, 'AUXILIAR DE CONTROLII', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (249, 'AUXIL.DE EVENTOS ESPECIALES II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (250, 'AUXILIAR DE PROGRAMACION II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (251, 'AUXIL.DE SERVICIOSGRALES.II', 7, '', 5101);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (252, 'AUXILIAR DE SISTEMAS II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (253, 'AUXILIAR PROMOTOR SOCIAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (254, 'AUXILIAR TRABAJADORSOCIAL II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (255, 'NOTIFICADOR II', 7, 'II', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (256, 'OPERADOR D.EQUIP.DREPRODUC.II', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (257, 'RECEPCIONISTA II', 7, 'II', 5101);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (258, 'AUXILIAR DE ALMACENII', 7, '', 5101);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (259, 'AUXILIAR DE OFICINAII', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (260, 'AUXILIAR I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (261, 'ARCHIVISTA I', 7, 'I', 5071);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (262, 'AUXILIAR DE ARCHIVO', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (263, 'AUXILIAR DE AUDITORIA I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (264, 'AUXILIAR DE CONTROLI', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (265, 'AUXIL.DE EVENTOS ESPECIALES I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (266, 'AUXILIAR DE PROGRAMACION I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (267, 'AUXILIAR DE SERVICIOS GRALES.I', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (268, 'AUXILIAR DE SISTEMAS I', 7, 'I', 4901);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (269, 'AUXILIAR PROMOTOR SOCIAL I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (270, 'AUXILIAR TRABAJADORSOCIAL', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (271, 'NOTIFICADOR I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (272, 'OPERADOR DE EQUIP.D.REPRODUC.I', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (273, 'RECEPCIONISTA I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (274, 'REGISTRADOR D BIENES Y MATER.I', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (275, 'AUXILIAR DE ALMACEN', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (276, 'AUXIL.D/PROTOCOLO YACTIV.RECR', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (277, 'AUXILIAR DE OFICINAI', 7, '', 4901);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (278, 'AUXILIAR I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (279, 'ODONTOLOGO', 7, '', 4680);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (280, 'AUXILIAR DE ENFERMERIA I', 7, 'I', 4901);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (281, 'ENFERMERA', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (282, 'SUPERVISOR D/SEGURIDAD INDUST.', 5, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (283, 'PERSONAL DE SEGURIDAD I', 7, 'I', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (284, 'DOCENTE', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (285, 'HIGIENISTA', 7, '', 0);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (286, 'JUBILADO', 7, '', 4889.1099999999997);
INSERT INTO cargo (id, nombre, jerarquia, nivel, sueldo) VALUES (287, 'PENSIONADO', 7, '', 4889.1099999999997);


--
-- Data for Name: concepto; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO concepto (id, codigo, identificador, denominacion, descripcion, sql, formula_patronal, tipo, fecha_inicia, fecha_fin, es_acumulado) VALUES
  (28, 144, 'caja_ahorro', 'Caja de Ahorro', 'Deducción por caja de ahorro de la Contraloría del Estado Táchira', 'sueldo_basico * 0.1', 'sueldo_basico * 0.1', 'Deducción', NULL, NULL, '1'),
  (5, 40, 'dia_feriado', 'Dia feriado', 'Se cancela a vigilantes por día feriado laborado', '(sueldo_basico_mensual * 0.5 / 30) * dias_feriados_trabajados', '', 'Asignación', NULL, NULL, '0'),
  (24, 8, 'pension_jubilacion', 'Pensión por Jubilación', 'Jubilados', 'sueldo_basico_mensual / 2', '', 'Asignación', NULL, NULL, '0'),
  (25, 9, 'pension_incapacidad', 'Pensión por Incapacidad', 'Pensionados', 'sueldo_basico_mensual / 2', '', 'Asignación', NULL, NULL, '0'),
  (4, 19, 'bono_nocturno', 'Bono Nocturno', 'Se cancela a vigilantes por horas nocturnas laboradas', '((sueldo_basico_mensual * 0.3) / 30 / 10) * horas__nocturnas_trabajadas', '', 'Asignación', NULL, NULL, '0'),
  (29, 148, 'emporio_optico_lentes', 'Emporio Óptico (Lentes)', 'Deducción según monto establecido por el comercio (Ajustes de conceptos).', '0', '', 'Deducción', NULL, NULL, '0'),
  (2, 20, 'asignacion_transporte', 'Asignación de transporte', 'Monto fijo por concepto de transporte (Quincenal)', '40', NULL, 'Asignación', NULL, NULL, '0'),
  (30, 152, 'fundatachira', 'Fundatachira', 'Según relación de cuotas enviada por Fundatachira (Ajustes de Conceptos).', '0', '', 'Deducción', NULL, NULL, '0'),
  (32, 172, 'pension_alimentacion_menores', 'Pensión Alimentación de Menores', 'Monto deducido según Tribunal de Menores (Ajustes de Conceptos).', '0', '', 'Deducción', NULL, NULL, '0'),
  (33, 159, 'descuento_asocijupecet', 'Descuento ASOCIJUPECET', 'Descuento por asociado.', '1', '', 'Deducción', NULL, NULL, '0'),
  (36, 155, 'prestamo_especial', '03 Préstamo especial', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (35, 154, 'prestamo_mediano_plazo', '02 Préstamo a mediano plazo', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (34, 153, 'prestamo_corto_plazo', '01 Préstamo a corto plazo', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (8, 11, 'prima_coordinacion_ega', 'Prima Coordinación EGA', 'Asignación relacionada con el coordinador de la Escuela Gubernamental de Auditoría (Ajuste de Conceptos)', 'coordinacion_ega === "Verdadero" ? 500 : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (1, 1, 'sueldo_basico', 'Sueldo Básico', 'Sueldo Básico quincenal', 'sueldo_basico_mensual / 2', '', 'Asignación', NULL, NULL, '0'),
  (37, 156, 'aporte_voluntario_caprecet', 'Aporte voluntario CAPRECET', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (38, 157, 'descuento_locatel', '14 Descuento Locatel', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (39, 158, 'prestamo_comercial', '04 Préstamo comercial', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (31, 162, 'fondo_ahorro_obligatorio_vivienda', 'Fondo Ahorro Obligat. Vivienda', 'Ley de Vivienda y Habitat', 'sueldo_integral * 0.01', 'sueldo_integral * 0.02', 'Deducción', NULL, NULL, '1'),
  (13, 35, 'especializacion', 'Especialización', 'Compensación de acuerdo a nivel académico', 'estudio_funcionario === ''Especialización'' ? 150 : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (14, 37, 'maestria', 'Maestría', 'Compensación de acuerdo a nivel académico', 'estudio_funcionario === ''Maestría'' ? 200: 0', NULL, 'Asignación', NULL, NULL, '0'),
  (15, 39, 'doctorado', 'Doctorado', 'Compensación de acuerdo a nivel académico', 'estudio_funcionario === ''Doctorado'' ? 250 : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (12, 24, 'prima_nivelacion_profesional', 'Prima de Nivelación Profesional', 'Compensación de acuerdo al nivel académico', 'estudio_funcionario === ''T.S.U'' ? 36.5 :\r\n\r\nestudio_funcionario === ''Universitario'' || \r\n\r\nestudio_funcionario === ''Diplomado'' ||\r\n\r\nestudio_funcionario === ''Especialización'' ||\r\n\r\nestudio_funcionario === ''Maestría'' ||\r\n\r\nestudio_funcionario === ''Doctorado'' ? 50', NULL, 'Asignación', NULL, NULL, '0'),
  (9, 31, 'bono_eficiencia', 'Bono de Eficiencia', 'Se asignan: 45 Bs. para evaluación de desempeño individual: Muy bueno y Sobre lo esperado. 55 Bs. para eficiente.', '( calificacion_desempeno_individual === ''Muy bueno'' || \r\n\r\ncalificacion_desempeno_individual === ''Sobre lo esperado'' ) ?\r\n\r\n22.5:\r\n\r\n( calificacion_desempeno_individual === ''Excelente'' ? 27.5)', '0', 'Asignación', NULL, NULL, '0'),
  (21, 14, 'dif_sueldo_basico_supervisor_e', 'Diferencia Sueldo Básico Supervisor (E)', 'Diferencia de sueldo básico por encargaduría.', 'nombre_cargo_encargado === ''Supervisor'' ? \r\n( sueldo_basico_mensual_encargado / 2 ) - ( sueldo_basico_mensual / 2 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (10, 22, 'prima_jerarquia', 'Prima de Jerarquia', 'Director General: 300 Bs., Director: 200 Bs., Coordinador: 200 Bs., Jefe de División General: 150 Bs., Supervisor: 125 Bs.', '( nombre_cargo === ''DIRECTORA GENERAL''  || nombre_cargo === ''DIRECTOR GENERAL'' ) ? 300:\r\n( nombre_cargo === ''DIRECTOR'' ) ? 200 :\r\n( nombre_cargo === ''JEFE D/DIV.GRAL DEREC.HUMANOS'' || nombre_cargo === ''JEFE D/DIV.GRAL D/POTEST,INVES'' || nombre_cargo === ''JEFE D/DIV. SEGUR.Y TRANSP.'' || nombre_cargo === ''JEFE DE DIV.GRAL.DEAUDITORIAS'' || nombre_cargo === ''JEFE DIV.GRL.D/INVESTIGACIONES'' || nombre_cargo === ''JEFE DE OFICINA'' ) ? 150 :\r\n( nombre_cargo === ''SUPERVISOR DE AUDITORIA'' || nombre_cargo === ''SUPERVISOR DE INVESTIGACIONES'' || nombre_cargo === ''SUPERVISOR D/SEGURIDAD INDUST.''  || nombre_cargo === ''JEFE DE DIVISION'' ) ? 125 :\r\n0', NULL, 'Asignación', NULL, NULL, '0'),
  (22, 7, 'dif_sueldo_basico_contralor_e', 'Diferencia Sueldo Básico Contralor (E)', 'Diferencia de sueldo básico por encargaduría.', 'nombre_cargo_encargado === ''Contralor''\r\n? ( sueldo_basico_mensual_encargado / 2 ) - ( sueldo_basico_mensual / 2 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (18, 18, 'dif_sueldo_basico_director_e', 'Diferencia Sueldo Básico Director (E)', 'Diferencia de sueldo básico por encargaduría.', 'nombre_cargo_encargado === ''Director'' ? ( sueldo_basico_mensual_encargado / 2 ) - ( sueldo_basico_mensual / 2 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (23, 13, 'dif_sueldo_basico_jefe_oficina_e', 'Diferencia Sueldo Básico Jefe de Oficina (E)', 'Diferencia de sueldo básico por encargaduría.', 'nombre_cargo_encargado === ''Jefe de Oficina''\r\n? ( sueldo_basico_mensual_encargado / 2 ) - ( sueldo_basico_mensual / 2 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (7, 25, 'complemento_sobretiempo', 'Complemento por sobretiempo', 'Secretaria VI adscrita a la Dirección de la Secretaría del Despacho Contralor (Ajuste de Conceptos)', 'unidad_organizativa_empleado === \"DIR.D/LA SECRETARIA D/DESPACHO CONTRALOR\" ?\r\n( nombre_cargo === ''Secretaria'' && nivel_cargo === ''VI'' ? 375 : 0 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (40, 163, 'desc_lacor_dia_madre_padre', '12 Desc. Lacor Dia Madre/Padre', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (41, 165, 'corporativo_celular_movistar', '16 Corporativo Tel. Cel. Movistar', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (42, 166, 'credito_corp_navideno', '13 Crédito Corp. Navideño', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (43, 168, 'colectivo_seguro', '09 Colectivo Seguro', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (44, 170, 'cuentas_cobrar_ca', '10 Cuentas por cobrar (C. A.)', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (45, 171, 'desceunto_seguro_sovenpfa', '15 Descuento Seguro Sovenpfa', 'Deducción según CAPRECET (Ajustes de Conceptos)', '0', '', 'Deducción', NULL, NULL, '0'),
  (46, 2, 'sueldo_basico_encargado', 'Sueldo Básico Encargado', 'Sueldo Básico Quincenal Encargado', 'sueldo_basico_mensual_encargado / 2', '', 'Asignación', NULL, NULL, '0'),
  (48, 161, 'regimen_prestacional_empleo_pf', 'Régimen Prestacional de Empleo/P. F.', 'Régimen Prestacional de Empleo/P. F.', '(sueldo_integral * 2) > ( 10 * sueldo_minimo ) ? ( ( sueldo_minimo * 12 / 52 ) * 0.005 ) * cantidad_lunes :\r\n\r\n( ( sueldo_integral  * 2 * 12 / 52 ) * 0.005 ) * cantidad_lunes', '( ( sueldo_integral  * 2 * 12 / 52 ) * 0.02 ) * cantidad_lunes', 'Deducción', NULL, NULL, '1'),
  (27, 141, 'seguro_social_obligatorio', 'Seguro Social Obligatorio', 'Retención por seguro social obligatorio (Sueldo sin prima por hijos * 12 / 52 * 4% * cantidad de lunes de la quincena) (Se hace el descuento si la encargaduría supera un mes).', '(sueldo_integral * 2) > ( 5 * sueldo_minimo ) ? ( ( sueldo_minimo * 12 / 52 ) * 0.04 ) * cantidad_lunes :\r\n\r\n( ( sueldo_integral  * 2 * 12 / 52 ) * 0.04 ) * cantidad_lunes', '( ( sueldo_integral  * 2 * 12 / 52 ) * 0.09 ) * cantidad_lunes', 'Deducción', NULL, NULL, '1'),
  (11, 17, 'dif_prima_jerarquia_e', 'Dif. Prima de Jerarquia (E)', 'Diferencia de Prima de Jerarquia por Encargadurias', '( nombre_cargo_encargado === ''Director General'' ) ? 300:\r\n\r\n( nombre_cargo_encargado === ''Director'' || nombre_cargo_encargado === ''Coordinador'' ) ? 200 :\r\n\r\n( nombre_cargo_encargado === ''Jefe de División General'' || nombre_cargo_encargado === ''Jefe de Oficina'' ) ? 150 :\r\n\r\n( nombre_cargo_encargado === ''Supervisor'' ) ? 125 :\r\n\r\n0', NULL, 'Asignación', NULL, NULL, '0'),
  (6, 28, 'dif_prima_antiguedad_e', 'Dif. Prima de Antigüedad (E)', 'Es la diferencia entre las primas por antigüedad del cargo como encargado y del cargo propio.', 'sueldo_basico_mensual_encargado>0? (( Variable.setVariable(''años'',( dias_otros_organismos + dias_servicio_contraloria )  / 365 )  +\r\n\r\n(Variable.getVariable(''años'')>=2 && Variable.getVariable(''años'')<4?sueldo_basico_mensual_encargado/2*0.03\r\n\r\n:Variable.getVariable(''años'')>=4 && Variable.getVariable(''años'')<6?sueldo_basico_mensual_encargado/2*0.04\r\n\r\n:Variable.getVariable(''años'')>=6 && Variable.getVariable(''años'')<8?sueldo_basico_mensual_encargado/2*0.05\r\n\r\n:Variable.getVariable(''años'')>=8 && Variable.getVariable(''años'')<10?sueldo_basico_mensual_encargado/2*0.06\r\n\r\n:Variable.getVariable(''años'')>=10 && Variable.getVariable(''años'')<12?sueldo_basico_mensual_encargado/2*0.07 \r\n\r\n:Variable.getVariable(''años'')>=12 && Variable.getVariable(''años'')<14?sueldo_basico_mensual_encargado/2*0.08\r\n\r\n:Variable.getVariable(''años'')>=14 && Variable.getVariable(''años'')<16?sueldo_basico_mensual_encargado/2*0.09 \r\n\r\n:Variable.getVariable(''años'')>=16 && Variable.getVariable(''años'')<18?sueldo_basico_mensual_encargado/2*0.10 \r\n\r\n:Variable.getVariable(''años'')>=18 && Variable.getVariable(''años'')<20?sueldo_basico_mensual_encargado/2*0.11\r\n\r\n:Variable.getVariable(''años'')>=20 && Variable.getVariable(''años'')<22?sueldo_basico_mensual_encargado/2*0.12 \r\n\r\n:Variable.getVariable(''años'')>=22 && Variable.getVariable(''años'')<24?sueldo_basico_mensual_encargado/2*0.13\r\n\r\n:Variable.getVariable(''años'')>=24 ? sueldo_basico_mensual_encargado/2*0.14) )\r\n\r\n-\r\n\r\nprima_antiguedad)', NULL, 'Asignación', NULL, NULL, '0'),
  (47, 3, 'sueldo_integral', 'Sueldo Integral', 'Sueldo Integral (Todas las asignaciones) menos prima por hijos', 'sueldo_basico + \r\n\r\nbono_nocturno + \r\n\r\ndia_feriado + \r\n\r\nasignacion_transporte + \r\n\r\nprima_antiguedad + \r\n\r\ndif_prima_antiguedad_e + \r\n\r\ncomplemento_sobretiempo + \r\n\r\nbono_eficiencia + \r\n\r\nprima_jerarquia + \r\n\r\ndif_prima_jerarquia_e + \r\n\r\nprima_nivelacion_profesional + \r\n\r\nespecializacion + \r\n\r\nmaestria + \r\n\r\ndoctorado + \r\n\r\ngastos_movilizacion + \r\n\r\ndif_sueldo_basico_jefe_div_gral_e + \r\n\r\ndif_sueldo_basico_jefe_div_e +\r\n\r\ndif_sueldo_basico_jefe_oficina_e + \r\n\r\ndif_sueldo_basico_director_e + \r\n\r\ndif_sueldo_basico_supervisor_e + \r\n\r\ndif_sueldo_basico_contralor_e', NULL, 'Asignación', NULL, NULL, '0'),
  (20, 15, 'dif_sueldo_basico_jefe_div_e', 'Diferencia Sueldo Básico Jefe Div (E)', 'Diferencia de sueldo básico por encargaduría.', 'nombre_cargo_encargado === ''Jefe de División'' \r\n? ( sueldo_basico_mensual_encargado / 2 ) - ( sueldo_basico_mensual / 2 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (19, 16, 'dif_sueldo_basico_jefe_div_gral_e', 'Diferencia Sueldo Básico Jefe Div. Gral (E)', 'Diferencia de sueldo básico por encargaduría.', 'nombre_cargo_encargado === ''Jefe de División General'' \r\n? ( sueldo_basico_mensual_encargado / 2 ) - ( sueldo_basico_mensual / 2 ) : 0', NULL, 'Asignación', NULL, NULL, '0'),
  (26, 143, 'impuesto_sobre_renta', 'Impuesto sobre la renta', 'Porcentaje de retención según AR-I presentada por el Funcionario (Ajustes de Conceptos)', '( sueldo_integral) * ( porcentaje_retencion_islr / 100 )', NULL, 'Deducción', NULL, NULL, '1'),
  (3, 34, 'prima_antiguedad', 'Prima de Antigüedad', 'Prima por concepto de antigüedad, la misma varia según años de servicios.', 'Variable.setVariable(''años'',( dias_otros_organismos + dias_servicio_contraloria )  / 365 )  +\r\n\r\n(Variable.getVariable(''años'')>=2 && Variable.getVariable(''años'')<4?sueldo_basico*0.03\r\n\r\n:Variable.getVariable(''años'')>=4 && Variable.getVariable(''años'')<6?sueldo_basico*0.04\r\n\r\n:Variable.getVariable(''años'')>=6 && Variable.getVariable(''años'')<8?sueldo_basico*0.05\r\n\r\n:Variable.getVariable(''años'')>=8 && Variable.getVariable(''años'')<10?sueldo_basico*0.06\r\n\r\n:Variable.getVariable(''años'')>=10 && Variable.getVariable(''años'')<12?sueldo_basico*0.07 \r\n\r\n:Variable.getVariable(''años'')>=12 && Variable.getVariable(''años'')<14?sueldo_basico*0.08\r\n\r\n:Variable.getVariable(''años'')>=14 && Variable.getVariable(''años'')<16?sueldo_basico*0.09 \r\n\r\n:Variable.getVariable(''años'')>=16 && Variable.getVariable(''años'')<18?sueldo_basico*0.10 \r\n\r\n:Variable.getVariable(''años'')>=18 && Variable.getVariable(''años'')<20?sueldo_basico*0.11\r\n\r\n:Variable.getVariable(''años'')>=20 && Variable.getVariable(''años'')<22?sueldo_basico*0.12 \r\n\r\n:Variable.getVariable(''años'')>=22 && Variable.getVariable(''años'')<24?sueldo_basico*0.13\r\n\r\n:Variable.getVariable(''años'')>=24 ? sueldo_basico*0.14)', NULL, 'Asignación', NULL, NULL, '0'),
  (49, 4, 'aguinaldos', 'Aguinaldos', 'Aguinaldos', 'sueldo_integral * 2 * 12 / 365 * 90', NULL, 'Asignación', NULL, NULL, '0'),
  (50, 5, 'bono_vacacional', 'Bono Vacacional', 'Bono vacacional', 'Variable.setVariable( ''años'',( dias_otros_organismos + dias_servicio_contraloria )  / 365 ) + \r\n\r\n( \r\n\r\nVariable.getVariable(''años'') < 6 ? 35 * ( sueldo_integral * 24 / 365 ) : \r\n\r\nVariable.getVariable(''años'') < 10 ? 40 * ( sueldo_integral * 24 / 365 ) : \r\n\r\nVariable.getVariable(''años'') < 15 ? 43 * ( sueldo_integral * 24 / 365 ) :\r\n\r\n 48 * ( sueldo_integral * 24 / 365 )\r\n\r\n)', NULL, 'Asignación', NULL, NULL, '0'),
  (51, 6, 'garantia_prestaciones_sociales', 'Garantía Prestaciones Sociales', 'Depósito de la Garantía de las Prestaciones Sociales Art. 143 LOTTT', 'Variable.setVariable(''años'',( dias_otros_organismos + dias_servicio_contraloria )  / 365 ) +\r\n\r\n( fecha_ingreso.format(''m'')  - hoy().format(''m'') ) % 3 == 0 ?\r\n\r\nsueldo_integral * 24 / 365 * ( 15 + (Variable.getVariable(''años'') - 1) * 2 )', NULL, 'Asignación', NULL, NULL, '1'),
  (17, 36, 'prima_hijos', 'Prima por Hijos', 'Pago por cada hijo del empleado.', 'cantidad_hijos * 10', NULL, 'Asignación', NULL, NULL, '0'),
  (16, 38, 'gastos_movilizacion', 'Gastos de Movilización', 'Compensación por gastos de movilización de auxiliares, asistentes y auditores', 'unidad_organizativa_raiz_empleado === ''DIR.D/CONTROL D/LA ADMON.DESCENTRALIZADA'' ||\r\nunidad_organizativa_raiz_empleado === ''DIR.D/CONTROL D/LA ADMON.CENT.Y POD.EST.'' ||\r\nunidad_organizativa_raiz_empleado === ''DIRECCION GENERAL'' ?\r\n( nombre_cargo === ''Auxiliar'' ||\r\nnombre_cargo === ''Asistente'' ||\r\nnombre_cargo === ''Auditor'' ? 27 : 0 )', NULL, 'Asignación', NULL, NULL, '0');


--
-- Data for Name: concepto_has_concepto; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: concepto_has_plantilla_nomina; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: concepto_has_plantilla_nomina1; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: correo_personal; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: datos_genericos; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: datos_globales; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO datos_globales (id, nombre, valor) VALUES (1, 'Sueldo Mínimo', 4889.1099999999997);
INSERT INTO datos_globales (id, nombre, valor) VALUES (2, 'Valor Unidad Tributaria', 127);


--
-- Data for Name: datos_variables; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: enfermedad; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: estado; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO estado (id, nombre) VALUES (1, 'Amazonas');
INSERT INTO estado (id, nombre) VALUES (2, 'Anzoátegui');
INSERT INTO estado (id, nombre) VALUES (3, 'Apure');
INSERT INTO estado (id, nombre) VALUES (4, 'Aragua');
INSERT INTO estado (id, nombre) VALUES (5, 'Barinas');
INSERT INTO estado (id, nombre) VALUES (6, 'Bolívar');
INSERT INTO estado (id, nombre) VALUES (7, 'Carabobo');
INSERT INTO estado (id, nombre) VALUES (8, 'Cojedes');
INSERT INTO estado (id, nombre) VALUES (9, 'Delta Amacuro');
INSERT INTO estado (id, nombre) VALUES (10, 'Falcón');
INSERT INTO estado (id, nombre) VALUES (11, 'Guárico');
INSERT INTO estado (id, nombre) VALUES (12, 'Lara');
INSERT INTO estado (id, nombre) VALUES (13, 'Mérida');
INSERT INTO estado (id, nombre) VALUES (14, 'Miranda');
INSERT INTO estado (id, nombre) VALUES (15, 'Monagas');
INSERT INTO estado (id, nombre) VALUES (16, 'Nueva Esparta');
INSERT INTO estado (id, nombre) VALUES (17, 'Portuguesa');
INSERT INTO estado (id, nombre) VALUES (18, 'Sucre');
INSERT INTO estado (id, nombre) VALUES (19, 'Táchira');
INSERT INTO estado (id, nombre) VALUES (20, 'Trujillo');
INSERT INTO estado (id, nombre) VALUES (21, 'Vargas');
INSERT INTO estado (id, nombre) VALUES (22, 'Yaracuy');
INSERT INTO estado (id, nombre) VALUES (23, 'Zulia');
INSERT INTO estado (id, nombre) VALUES (24, 'Distrito Capital');
INSERT INTO estado (id, nombre) VALUES (25, 'Dependencias Federales');


--
-- Data for Name: estudio; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO estudio (id, base_legal_id, personal_cedula, descripcion, duracion, tipo, fecha_inicio, fecha_culminacion, institucion, estado) VALUES (1, NULL, 14417996, 'ADMINISTRACION DE EMPRESAS MENCION: RECURSOS MATERIALES Y FINANCIEROS', '5 AÑOS', 4, '2006-01-09', '2009-07-30', 'UNIVERSIDAD NACIONAL EXPERIMENTAL SIMON RODRIGUEZ (UNESR)', 'T');
INSERT INTO estudio (id, base_legal_id, personal_cedula, descripcion, duracion, tipo, fecha_inicio, fecha_culminacion, institucion, estado) VALUES (2, NULL, 14417996, 'TSU CONTADURIA PUBLICA', '3 AÑOS', 3, '1998-07-01', '2001-07-30', 'UNIVERSIDAD EXPERIMENTAL DE LOS LLANOS EZEQUIEL ZAMORA (UNELLEZ)', 'T');


--
-- Data for Name: experiencia_laboral; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO experiencia_laboral (id, base_legal_id, personal_cedula, institucion, fecha_ingreso, fecha_egreso, cargo, sueldo, tipo) VALUES (1, NULL, 14417996, 'SEGUROS LOS ANDES, C.A.', '1995-07-15', '2004-04-23', 'ANALISTA DE CONTABILIDAD III', 224, 'E');
INSERT INTO experiencia_laboral (id, base_legal_id, personal_cedula, institucion, fecha_ingreso, fecha_egreso, cargo, sueldo, tipo) VALUES (2, NULL, 14417996, 'BANFOANDES BANCO UNIVERSAL, C.A.', '2004-06-29', '2009-12-19', 'ANALISTA DE RECURSOS HUMANOS I', 2868, 'A');
INSERT INTO experiencia_laboral (id, base_legal_id, personal_cedula, institucion, fecha_ingreso, fecha_egreso, cargo, sueldo, tipo) VALUES (3, NULL, 14417996, 'BANCO BICENTENARIO, BANCO UNIVERSAL', '2009-12-19', '2012-11-30', 'ANALISTA DE RECURSOS HUMANOS II', 3289, 'A');
INSERT INTO experiencia_laboral (id, base_legal_id, personal_cedula, institucion, fecha_ingreso, fecha_egreso, cargo, sueldo, tipo) VALUES (4, NULL, 9247506, 'IUTEPAL', '1996-09-21', '2009-04-15', 'Secretaria', 1000, 'E');


--
-- Data for Name: formula; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: historico_cargo; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (1, 1, 48, NULL, '2006-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (2, 2, 76, NULL, '1997-02-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (3, 3, 242, NULL, '1998-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (4, 4, 280, NULL, '2011-03-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (5, 5, 238, NULL, '1996-08-30', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (6, 6, 152, NULL, '1995-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (7, 7, 197, NULL, '1990-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (8, 8, 234, NULL, '2006-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (9, 9, 76, NULL, '1997-02-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (10, 10, 181, NULL, '1998-12-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (11, 11, 207, NULL, '1991-02-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (12, 12, 67, NULL, '1991-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (13, 13, 197, NULL, '1991-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (14, 14, 67, NULL, '1991-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (15, 15, 67, NULL, '1989-04-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (16, 16, 238, NULL, '1998-12-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (17, 17, 32, NULL, '2013-08-05', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (18, 18, 129, NULL, '1990-05-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (19, 19, 67, NULL, '1985-03-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (20, 20, 67, NULL, '1993-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (21, 21, 210, NULL, '1988-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (22, 22, 197, NULL, '1981-02-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (23, 23, 234, NULL, '1990-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (24, 24, 29, NULL, '2002-06-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (25, 25, 67, NULL, '1995-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (26, 26, 203, NULL, '1991-06-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (27, 27, 206, NULL, '1991-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (28, 28, 268, NULL, '2007-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (29, 29, 101, NULL, '1997-06-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (30, 30, 29, NULL, '2003-05-19', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (31, 31, 204, NULL, '1994-01-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (32, 32, 203, NULL, '1989-02-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (33, 33, 38, NULL, '1988-08-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (34, 34, 67, NULL, '1997-02-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (35, 35, 77, NULL, '1994-10-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (36, 36, 91, NULL, '1990-03-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (37, 37, 118, NULL, '1995-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (38, 38, 139, NULL, '1991-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (39, 39, 235, NULL, '2007-06-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (40, 40, 196, NULL, '1999-02-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (41, 41, 38, NULL, '1991-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (42, 42, 199, NULL, '1998-05-11', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (43, 43, 37, NULL, '1996-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (44, 44, 54, NULL, '2010-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (45, 45, 139, NULL, '1995-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (46, 46, 216, NULL, '1995-03-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (47, 47, 139, NULL, '1994-09-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (48, 48, 168, NULL, '2009-03-04', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (49, 49, 76, NULL, '1998-07-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (50, 50, 242, NULL, '1996-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (51, 51, 88, NULL, '2005-04-06', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (52, 52, 44, NULL, '2014-10-06', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (53, 53, 118, NULL, '2013-02-25', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (54, 54, 48, NULL, '2007-07-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (55, 55, 76, NULL, '1991-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (56, 56, 118, NULL, '1995-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (57, 57, 102, NULL, '2008-06-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (58, 58, 59, NULL, '2013-08-07', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (59, 59, 29, NULL, '2010-05-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (60, 60, 101, NULL, '2011-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (61, 61, 48, NULL, '1997-06-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (62, 62, 118, NULL, '2008-02-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (63, 63, 120, NULL, '1996-04-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (64, 64, 139, NULL, '1998-06-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (65, 65, 38, NULL, '1995-09-05', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (66, 66, 131, NULL, '1997-02-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (67, 67, 240, NULL, '1994-01-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (68, 68, 139, NULL, '1994-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (69, 69, 93, NULL, '2009-06-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (70, 70, 59, NULL, '1999-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (71, 71, 125, NULL, '2012-03-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (72, 72, 197, NULL, '1994-01-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (73, 73, 118, NULL, '2000-11-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (74, 74, 29, NULL, '2008-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (75, 75, 232, NULL, '2013-09-24', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (76, 76, 59, NULL, '1996-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (77, 77, 152, NULL, '1992-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (78, 78, 118, NULL, '2008-03-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (79, 79, 230, NULL, '1998-01-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (80, 80, 197, NULL, '1995-08-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (81, 81, 142, NULL, '2003-06-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (82, 82, 139, NULL, '1994-10-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (83, 83, 57, NULL, '2008-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (84, 84, 29, NULL, '2010-07-12', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (85, 85, 50, NULL, '2000-05-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (86, 86, 28, NULL, '2011-10-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (87, 87, 128, NULL, '2011-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (88, 88, 134, NULL, '1996-07-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (89, 89, 251, NULL, '2006-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (90, 90, 101, NULL, '2009-03-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (91, 91, 166, NULL, '1996-09-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (92, 92, 163, NULL, '2007-11-06', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (93, 93, 111, NULL, '2012-08-29', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (94, 94, 138, NULL, '1996-08-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (95, 95, 101, NULL, '2009-03-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (96, 96, 118, NULL, '2013-02-25', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (97, 97, 93, NULL, '2010-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (98, 98, 125, NULL, '2009-02-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (99, 99, 48, NULL, '2005-08-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (100, 100, 93, NULL, '2009-03-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (101, 101, 193, NULL, '2013-02-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (102, 102, 95, NULL, '2007-07-04', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (103, 103, 29, NULL, '2012-03-12', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (104, 104, 108, NULL, '2006-10-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (105, 105, 257, NULL, '2005-06-30', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (106, 106, 176, NULL, '2014-10-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (107, 107, 122, NULL, '2006-01-30', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (108, 108, 110, NULL, '2013-04-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (109, 109, 29, NULL, '2014-09-22', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (110, 110, 87, NULL, '2007-11-19', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (111, 111, 162, NULL, '2010-05-19', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (112, 112, 57, NULL, '2008-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (113, 113, 118, NULL, '2013-07-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (114, 114, 122, NULL, '2013-08-08', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (115, 115, 29, NULL, '2005-05-23', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (116, 116, 118, NULL, '2010-03-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (117, 117, 48, NULL, '2012-08-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (118, 118, 277, NULL, '2012-12-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (119, 119, 118, NULL, '2013-05-27', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (120, 120, 176, NULL, '2013-02-25', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (121, 121, 110, NULL, '2012-03-21', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (122, 122, 101, NULL, '2009-03-04', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (123, 123, 87, NULL, '2006-01-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (124, 124, 176, NULL, '2014-04-24', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (125, 125, 118, NULL, '2007-10-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (126, 126, 42, NULL, '2006-10-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (127, 127, 118, NULL, '2013-05-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (128, 128, 116, NULL, '2012-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (129, 129, 191, NULL, '2014-04-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (130, 130, 87, NULL, '2004-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (131, 131, 41, NULL, '2008-06-09', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (132, 132, 87, NULL, '2007-07-23', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (133, 133, 59, NULL, '2008-06-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (134, 134, 29, NULL, '2007-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (135, 135, 154, NULL, '2008-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (136, 136, 50, NULL, '2009-03-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (137, 137, 50, NULL, '2010-06-07', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (138, 138, 110, NULL, '2013-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (139, 139, 258, NULL, '2008-06-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (140, 140, 118, NULL, '2013-07-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (141, 141, 110, NULL, '2013-02-26', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (142, 142, 176, NULL, '2014-08-07', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (143, 143, 125, NULL, '2009-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (144, 144, 101, NULL, '2008-12-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (145, 145, 110, NULL, '2014-10-06', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (146, 146, 110, NULL, '2013-09-19', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (147, 147, 118, NULL, '2010-03-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (148, 148, 110, NULL, '2013-09-19', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (149, 149, 118, NULL, '2012-03-06', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (150, 150, 110, NULL, '2014-02-10', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (151, 151, 126, NULL, '2008-02-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (152, 152, 93, NULL, '2011-07-14', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (153, 153, 37, NULL, '2009-03-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (154, 154, 110, NULL, '2014-05-14', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (155, 155, 110, NULL, '2014-04-29', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (156, 156, 163, NULL, '2010-04-26', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (157, 157, 118, NULL, '2012-08-22', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (158, 158, 101, NULL, '2011-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (159, 159, 110, NULL, '2013-04-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (160, 160, 118, NULL, '2011-07-26', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (161, 161, 261, NULL, '2012-07-04', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (162, 162, 185, NULL, '2011-05-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (163, 163, 176, NULL, '2012-05-23', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (164, 164, 167, NULL, '2011-04-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (165, 165, 111, NULL, '2013-10-07', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (166, 166, 118, NULL, '2014-08-26', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (167, 167, 118, NULL, '2012-09-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (168, 168, 118, NULL, '2014-05-23', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (169, 169, 101, NULL, '2014-09-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (170, 170, 110, NULL, '2014-11-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (171, 171, 176, NULL, '2012-09-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (172, 172, 192, NULL, '2013-01-21', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (173, 173, 116, NULL, '2012-11-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (174, 174, 110, NULL, '2014-04-29', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (175, 175, 110, NULL, '2014-11-10', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (176, 176, 176, NULL, '2013-02-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (177, 177, 118, NULL, '2014-08-11', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (197, 197, 279, NULL, '2013-07-03', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (208, 208, 286, NULL, '1981-09-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (209, 209, 287, NULL, '2007-05-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (210, 210, 286, NULL, '1990-05-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (211, 211, 287, NULL, '2005-06-07', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (212, 212, 286, NULL, '1996-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (213, 213, 286, NULL, '1993-10-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (214, 214, 286, NULL, '2013-08-05', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (215, 215, 287, NULL, '2010-10-05', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (216, 216, 286, NULL, '1982-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (217, 217, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (218, 218, 286, NULL, '1992-09-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (219, 219, 286, NULL, '2013-06-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (220, 220, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (221, 221, 286, NULL, '2011-10-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (222, 222, 286, NULL, '2013-05-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (223, 223, 286, NULL, '2012-08-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (224, 224, 287, NULL, '2009-08-14', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (225, 225, 286, NULL, '1999-05-10', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (226, 226, 286, NULL, '1982-06-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (227, 227, 286, NULL, '1998-06-30', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (228, 228, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (229, 229, 287, NULL, '2010-06-21', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (230, 230, 286, NULL, '1980-07-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (231, 231, 286, NULL, '1976-06-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (232, 232, 286, NULL, '1977-04-25', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (233, 233, 286, NULL, '1998-06-30', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (234, 234, 286, NULL, '1976-04-26', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (235, 235, 286, NULL, '1976-01-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (236, 236, 287, NULL, '1991-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (237, 237, 287, NULL, '1996-07-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (238, 238, 286, NULL, '1980-01-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (239, 239, 286, NULL, '1981-02-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (240, 240, 286, NULL, '1986-07-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (241, 241, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (242, 242, 286, NULL, '1998-05-31', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (243, 243, 286, NULL, '2014-04-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (244, 244, 287, NULL, '2006-07-12', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (245, 245, 286, NULL, '1986-06-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (246, 246, 286, NULL, '2014-05-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (247, 247, 286, NULL, '1972-06-06', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (248, 248, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (249, 249, 286, NULL, '2004-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (250, 250, 286, NULL, '1980-01-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (251, 251, 286, NULL, '1982-02-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (252, 252, 286, NULL, '2004-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (253, 253, 287, NULL, '2007-11-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (254, 254, 286, NULL, '1998-06-30', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (255, 255, 286, NULL, '1986-02-17', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (256, 256, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (257, 257, 286, NULL, '1978-01-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (258, 258, 286, NULL, '2014-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (259, 259, 287, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (260, 260, 286, NULL, '1978-01-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (261, 261, 286, NULL, '1982-07-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (262, 262, 286, NULL, '1977-05-23', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (263, 263, 287, NULL, '2007-08-31', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (264, 264, 286, NULL, '1981-09-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (265, 265, 287, NULL, '2008-09-19', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (266, 266, 287, NULL, '2003-12-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (267, 267, 287, NULL, '2009-08-21', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (268, 268, 286, NULL, '1980-10-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (269, 269, 286, NULL, '1982-08-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (270, 270, 287, NULL, '2003-12-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (271, 271, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (272, 272, 286, NULL, '1982-03-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (273, 273, 287, NULL, '2009-07-23', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (274, 274, 286, NULL, '1981-04-22', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (275, 275, 287, NULL, '2013-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (276, 276, 286, NULL, '1983-05-02', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (277, 277, 287, NULL, '2010-06-18', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (278, 278, 286, NULL, '1982-03-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (279, 279, 287, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (280, 280, 287, NULL, '2010-05-11', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (281, 281, 287, NULL, '2006-03-21', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (282, 282, 286, NULL, '1980-01-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (283, 283, 287, NULL, '2009-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (284, 284, 286, NULL, '1996-09-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (285, 285, 286, NULL, '2014-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (286, 286, 286, NULL, '2014-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (287, 287, 287, NULL, '2007-11-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (288, 288, 287, NULL, '1988-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (289, 289, 286, NULL, '2014-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (290, 290, 287, NULL, '2006-11-10', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (291, 291, 287, NULL, '2007-11-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (292, 292, 287, NULL, '2007-05-15', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (293, 293, 287, NULL, '1991-01-16', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (294, 294, 287, NULL, '2006-11-10', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (295, 295, 286, NULL, '1982-03-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (296, 296, 286, NULL, '2004-02-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (297, 297, 287, NULL, '2005-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (298, 298, 286, NULL, '2011-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (299, 299, 286, NULL, '2014-01-01', NULL, NULL, 1, '2014-12-15');
INSERT INTO historico_cargo (id, informacion_laboral_id, cargo_id, unidad_organizativa_id, fecha_inicio, fecha_fin, es_encargado, numero_resolucion, fecha_resolucion) VALUES (300, 300, 287, NULL, '1996-09-02', NULL, NULL, 1, '2014-12-15');


--
-- Data for Name: historico_concepto; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: historico_evento; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: historico_unidad_organizativa; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (1, 40, 1, '2006-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (2, 21, 2, '1997-02-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (3, 31, 3, '1998-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (4, 46, 4, '2011-03-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (5, 11, 5, '1996-08-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (6, 31, 6, '1995-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (7, 8, 7, '1990-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (8, 42, 8, '2006-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (9, 30, 9, '1997-02-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (10, 44, 10, '1998-12-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (11, 51, 11, '1991-02-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (12, 31, 12, '1991-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (13, 30, 13, '1991-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (14, 29, 14, '1991-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (15, 30, 15, '1989-04-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (16, 32, 16, '1998-12-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (17, 2, 17, '2013-08-05', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (18, 51, 18, '1990-05-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (19, 29, 19, '1985-03-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (20, 30, 20, '1993-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (21, 42, 21, '1988-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (22, 29, 22, '1981-02-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (23, 42, 23, '1990-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (24, 8, 24, '2002-06-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (25, 30, 25, '1995-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (26, 47, 26, '1991-06-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (27, 47, 27, '1991-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (28, 51, 28, '2007-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (29, 31, 29, '1997-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (30, 20, 30, '2003-05-19', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (31, 30, 31, '1994-01-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (32, 2, 32, '1989-02-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (33, 29, 33, '1988-08-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (34, 30, 34, '1997-02-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (35, 28, 35, '1994-10-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (36, 46, 36, '1990-03-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (37, 29, 37, '1995-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (38, 30, 38, '1991-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (39, 51, 39, '2007-06-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (40, 20, 40, '1999-02-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (41, 31, 41, '1991-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (42, 40, 42, '1998-05-11', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (43, 22, 43, '1996-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (44, 29, 44, '2010-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (45, 30, 45, '1995-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (46, 21, 46, '1995-03-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (47, 29, 47, '1994-09-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (48, 45, 48, '2009-03-04', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (49, 29, 49, '1998-07-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (50, 42, 50, '1996-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (51, 49, 51, '2005-04-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (52, 20, 52, '2014-10-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (53, 29, 53, '2013-02-25', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (54, 28, 54, '2007-07-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (55, 29, 55, '1991-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (56, 31, 56, '1995-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (57, 46, 57, '2008-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (58, 45, 58, '2013-08-07', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (59, 10, 59, '2010-05-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (60, 31, 60, '2011-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (61, 42, 61, '1997-06-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (62, 31, 62, '2008-02-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (63, 40, 63, '1996-04-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (64, 30, 64, '1998-06-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (65, 30, 65, '1995-09-05', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (66, 31, 66, '1997-02-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (67, 42, 67, '1994-01-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (68, 30, 68, '1994-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (69, 32, 69, '2009-06-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (70, 46, 70, '1999-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (71, 27, 71, '2012-03-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (72, 6, 72, '1994-01-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (73, 29, 73, '2000-11-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (74, 11, 74, '2008-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (75, 47, 75, '2013-09-24', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (76, 48, 76, '1996-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (77, 31, 77, '1992-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (78, 2, 78, '2008-03-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (79, 21, 79, '1998-01-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (80, 3, 80, '1995-08-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (81, 50, 81, '2003-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (82, 29, 82, '1994-10-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (83, 32, 83, '2008-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (84, 9, 84, '2010-07-12', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (85, 29, 85, '2000-05-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (86, 3, 86, '2011-10-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (87, 47, 87, '2011-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (88, 50, 88, '1996-07-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (89, 42, 89, '2006-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (90, 30, 90, '2009-03-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (91, 30, 91, '1996-09-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (92, 11, 92, '2007-11-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (93, 47, 93, '2012-08-29', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (94, 49, 94, '1996-08-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (95, 31, 95, '2009-03-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (96, 29, 96, '2013-02-25', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (97, 32, 97, '2010-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (98, 27, 98, '2009-02-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (99, 51, 99, '2005-08-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (100, 32, 100, '2009-03-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (101, 46, 101, '2013-02-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (102, 49, 102, '2007-07-04', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (103, 5, 103, '2012-03-12', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (104, 45, 104, '2006-10-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (105, 42, 105, '2005-06-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (106, 48, 106, '2014-10-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (107, 45, 107, '2006-01-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (108, 5, 108, '2013-04-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (109, 4, 109, '2014-09-22', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (110, 30, 110, '2007-11-19', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (111, 27, 111, '2010-05-19', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (112, 32, 112, '2008-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (113, 29, 113, '2013-07-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (114, 45, 114, '2013-08-08', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (115, 7, 115, '2005-05-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (116, 29, 116, '2010-03-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (117, 26, 117, '2012-08-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (118, 48, 118, '2012-12-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (119, 31, 119, '2013-05-27', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (120, 7, 120, '2013-02-25', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (121, 4, 121, '2012-03-21', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (122, 30, 122, '2009-03-04', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (123, 29, 123, '2006-01-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (124, 20, 124, '2014-04-24', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (125, 30, 125, '2007-10-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (126, 32, 126, '2006-10-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (127, 31, 127, '2013-05-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (128, 28, 128, '2012-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (129, 41, 129, '2014-04-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (130, 29, 130, '2004-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (131, 24, 131, '2008-06-09', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (132, 31, 132, '2007-07-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (133, 44, 133, '2008-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (134, 6, 134, '2007-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (135, 45, 135, '2008-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (136, 30, 136, '2009-03-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (137, 31, 137, '2010-06-07', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (138, 32, 138, '2013-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (139, 48, 139, '2008-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (140, 30, 140, '2013-07-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (141, 8, 141, '2013-02-26', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (142, 45, 142, '2014-08-07', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (143, 27, 143, '2009-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (144, 30, 144, '2008-12-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (145, 7, 145, '2014-10-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (146, 32, 146, '2013-09-19', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (147, 29, 147, '2010-03-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (148, 11, 148, '2013-09-19', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (149, 30, 149, '2012-03-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (150, 7, 150, '2014-02-10', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (151, 44, 151, '2008-02-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (152, 11, 152, '2011-07-14', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (153, 27, 153, '2009-03-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (154, 9, 154, '2014-05-14', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (155, 32, 155, '2014-04-29', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (156, 30, 156, '2010-04-26', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (157, 31, 157, '2012-08-22', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (158, 31, 158, '2011-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (159, 21, 159, '2013-04-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (160, 2, 160, '2011-07-26', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (161, 43, 161, '2012-07-04', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (162, 40, 162, '2011-05-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (163, 9, 163, '2012-05-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (164, 48, 164, '2011-04-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (165, 47, 165, '2013-10-07', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (166, 30, 166, '2014-08-26', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (167, 31, 167, '2012-09-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (168, 31, 168, '2014-05-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (169, 30, 169, '2014-09-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (170, 11, 170, '2014-11-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (171, 5, 171, '2012-09-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (172, 10, 172, '2013-01-21', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (173, 28, 173, '2012-11-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (174, 32, 174, '2014-04-29', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (175, 32, 175, '2014-11-10', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (176, 43, 176, '2013-02-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (177, 30, 177, '2014-08-11', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (178, 99, 178, '2010-06-25', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (179, 25, 179, '2003-11-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (180, 23, 180, '2009-07-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (181, 23, 181, '2003-12-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (182, 23, 182, '2003-10-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (183, 99, 183, '2013-04-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (184, 99, 184, '2006-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (185, 25, 185, '2012-05-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (186, 25, 186, '2003-05-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (187, 99, 187, '2014-03-31', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (188, 23, 188, '2013-06-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (189, 23, 189, '2012-07-11', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (190, 25, 190, '2004-03-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (191, 23, 191, '2007-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (192, 23, 192, '2013-10-21', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (193, 99, 193, '2013-10-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (194, 99, 194, '2014-06-11', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (195, 99, 195, '2014-09-24', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (196, 23, 196, '2007-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (197, 99, 197, '2013-07-03', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (198, 25, 198, '2003-06-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (199, 25, 199, '2008-06-09', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (200, 25, 200, '2005-05-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (201, 99, 201, '2014-05-08', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (202, 99, 202, '2014-03-10', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (203, 25, 203, '2006-01-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (204, 25, 204, '2008-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (205, 99, 205, '2013-10-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (206, 25, 206, '2008-12-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (207, 25, 207, '2012-11-26', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (208, 20, 208, '1981-09-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (209, 99, 209, '2007-05-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (210, 20, 210, '1990-05-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (211, 99, 211, '2005-06-07', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (212, 20, 212, '1996-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (213, 20, 213, '1993-10-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (214, 20, 214, '2013-08-05', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (215, 99, 215, '2010-10-05', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (216, 20, 216, '1982-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (217, 20, 217, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (218, 20, 218, '1992-09-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (219, 20, 219, '2013-06-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (220, 20, 220, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (221, 20, 221, '2011-10-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (222, 20, 222, '2013-05-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (223, 20, 223, '2012-08-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (224, 99, 224, '2009-08-14', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (225, 20, 225, '1999-05-10', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (226, 20, 226, '1982-06-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (227, 20, 227, '1998-06-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (228, 20, 228, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (229, 99, 229, '2010-06-21', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (230, 20, 230, '1980-07-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (231, 20, 231, '1976-06-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (232, 20, 232, '1977-04-25', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (233, 20, 233, '1998-06-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (234, 20, 234, '1976-04-26', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (235, 20, 235, '1976-01-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (236, 99, 236, '1991-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (237, 99, 237, '1996-07-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (238, 20, 238, '1980-01-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (239, 20, 239, '1981-02-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (240, 20, 240, '1986-07-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (241, 20, 241, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (242, 20, 242, '1998-05-31', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (243, 20, 243, '2014-04-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (244, 99, 244, '2006-07-12', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (245, 20, 245, '1986-06-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (246, 20, 246, '2014-05-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (247, 20, 247, '1972-06-06', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (248, 20, 248, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (249, 20, 249, '2004-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (250, 20, 250, '1980-01-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (251, 20, 251, '1982-02-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (252, 20, 252, '2004-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (253, 99, 253, '2007-11-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (254, 20, 254, '1998-06-30', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (255, 20, 255, '1986-02-17', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (256, 20, 256, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (257, 20, 257, '1978-01-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (258, 20, 258, '2014-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (259, 99, 259, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (260, 20, 260, '1978-01-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (261, 20, 261, '1982-07-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (262, 20, 262, '1977-05-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (263, 99, 263, '2007-08-31', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (264, 20, 264, '1981-09-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (265, 99, 265, '2008-09-19', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (266, 99, 266, '2003-12-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (267, 99, 267, '2009-08-21', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (268, 20, 268, '1980-10-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (269, 20, 269, '1982-08-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (270, 99, 270, '2003-12-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (271, 20, 271, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (272, 20, 272, '1982-03-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (273, 99, 273, '2009-07-23', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (274, 20, 274, '1981-04-22', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (275, 99, 275, '2013-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (276, 20, 276, '1983-05-02', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (277, 99, 277, '2010-06-18', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (278, 20, 278, '1982-03-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (279, 99, 279, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (280, 99, 280, '2010-05-11', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (281, 99, 281, '2006-03-21', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (282, 20, 282, '1980-01-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (283, 99, 283, '2009-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (284, 20, 284, '1996-09-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (285, 20, 285, '2014-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (286, 20, 286, '2014-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (287, 99, 287, '2007-11-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (288, 99, 288, '1988-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (289, 20, 289, '2014-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (290, 99, 290, '2006-11-10', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (291, 99, 291, '2007-11-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (292, 99, 292, '2007-05-15', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (293, 99, 293, '1991-01-16', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (294, 99, 294, '2006-11-10', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (295, 20, 295, '1982-03-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (296, 20, 296, '2004-02-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (297, 99, 297, '2005-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (298, 20, 298, '2011-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (299, 20, 299, '2014-01-01', NULL);
INSERT INTO historico_unidad_organizativa (id, unidad_organizativa_id, informacion_laboral_id, fecha_inicio, fecha_fin) VALUES (300, 99, 300, '1996-09-02', NULL);


--
-- Data for Name: historico_variable; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: informacion_cultural_deportiva; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: informacion_laboral; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (1, NULL, 1551555, 2, '01020501830003032211', 0, '01160122760203563948', 1, '2006-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (2, NULL, 3193103, 2, '01020501860003034031', 0, '01160122770203609298', 1, '1997-02-03', '1997-02-12', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (3, NULL, 3618098, 2, '01020501850003034099', 0, '01160122740203463544', 1, '1998-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (4, NULL, 3644111, 2, '01020501810003034549', 0, '01160122750203462319', 1, '2011-03-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (5, NULL, 3716598, 2, '01020501810003030721', 0, '01160122720203431730', 1, '1996-08-30', '1996-12-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (6, NULL, 4209398, 2, '01020501860003033980', 0, '01160122770203570227', 1, '1995-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (7, NULL, 4212434, 2, '01020501840003034015', 0, '01160122790203545620', 1, '1990-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (8, NULL, 4434487, 2, '01020501870003033906', 0, '01160122730203603052', 1, '2006-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (9, NULL, 4493876, 2, '01020501860003034028', 0, '01160122770203481810', 1, '1997-02-03', '1997-02-12', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (10, NULL, 4630076, 2, '01020501890003033838', 0, '01160122790203576690', 1, '1998-12-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (11, NULL, 4634588, 2, '01020501840003034905', 0, '01160122760203621492', 1, '1991-02-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (12, NULL, 5030837, 2, '01020501810003034963', 0, '01160122790203448790', 1, '1991-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (13, NULL, 5034618, 2, '01020501850003034992', 0, '01160122750203536614', 1, '1991-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (14, NULL, 5283541, 2, '01020501810003033045', 0, '01160122730203528000', 1, '1991-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (15, NULL, 5644414, 2, '01020501870003033100', 0, '01160122730203527879', 1, '1989-04-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (16, NULL, 5646127, 2, '01020501890003033113', 0, '01160122710203570189', 1, '1998-12-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (17, NULL, 5652245, 2, '01020501810003033126', 0, '01160223160207256462', 1, '2013-08-05', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (18, NULL, 5654242, 2, '01020501820003033139', 0, '01160122740203577205', 1, '1990-05-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (19, NULL, 5654282, 2, '01020501820003033142', 0, '01160122750203457170', 1, '1985-03-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (20, NULL, 5655835, 2, '01020501840003033155', 0, '01160122700203609310', 1, '1993-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (21, NULL, 5658730, 2, '01020501860003033171', 0, '01160122700203460073', 1, '1988-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (22, NULL, 5671752, 2, '01020501850003032868', 0, '01160122710203576632', 1, '1981-02-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (23, NULL, 5676979, 2, '01020501870003032884', 0, '01160122710203539184', 1, '1990-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (24, NULL, 5682926, 2, '01020501810003032923', 0, '01160122730203597656', 1, '2002-06-03', '2011-01-01', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (25, NULL, 5685532, 2, '01020501850003032952', 0, '01160122760203544137', 1, '1995-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (26, NULL, 5688197, 2, '01020501810003032994', 0, '01160122760203597842', 1, '1991-06-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (27, NULL, 5688684, 2, '01020501850003033003', 0, '01160122790203570243', 1, '1991-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (28, NULL, 6432041, 2, '01020501870003034523', 0, '01160122710203600680', 1, '2007-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (29, NULL, 7362601, 2, '01020501840003034497', 0, '01160122790203560019', 1, '1997-06-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (31, NULL, 8708654, 2, '01020501850003033566', 0, '01160122760203570110', 1, '1994-01-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (32, NULL, 8993128, 2, '01020501870003033579', 0, '01160122790203454900', 1, '1989-02-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (33, NULL, 9127453, 2, '01020501850003033650', 0, '01160122770203563018', 1, '1988-08-17', '1995-09-19', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (34, NULL, 9146962, 2, '01020501890003033676', 0, '01160122760203448900', 1, '1997-02-03', '1997-02-12', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (35, NULL, 9210236, 2, '01020501870003033498', 0, '01160122740203562770', 1, '1994-10-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (36, NULL, 9211472, 2, '01020501880003033508', 0, '01160122760203433068', 1, '1990-03-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (37, NULL, 9217620, 2, '01020501860003032392', 0, '01160122700203577779', 1, '1995-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (38, NULL, 9218696, 2, '01020501890003032415', 0, '01160122750203474082', 1, '1991-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (39, NULL, 9220083, 2, '01020501810003032428', 0, '01160122700203501799', 1, '2007-06-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (40, NULL, 9223480, 2, '01020501840003032460', 0, '01160122720203474805', 1, '1999-02-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (41, NULL, 9225202, 2, '01020501800003032499', 0, '01160122750203603079', 1, '1991-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (42, NULL, 9226981, 2, '01020501820003032525', 0, '01160122710203449606', 1, '1998-05-11', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (43, NULL, 9229274, 2, '01020501840003032538', 0, '01160122710203834186', 1, '1996-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (44, NULL, 9229973, 2, '01020501840003032541', 0, '01160122790203544595', 1, '2010-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (45, NULL, 9233104, 2, '01020501880003032567', 0, '01160122710203457544', 1, '1995-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (46, NULL, 9233318, 2, '01020501880003032570', 0, '01160122720203597567', 1, '1995-03-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (47, NULL, 9240774, 2, '01020501820003032606', 0, '01160122700203484860', 1, '1994-09-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (48, NULL, 9247506, 2, '01020501880003032648', 0, '01160122700203570430', 1, '2009-03-04', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (49, NULL, 9248679, 2, '01020501800003032664', 0, '01160122760203563352', 1, '1998-07-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (50, NULL, 9249105, 2, '01020501810003032677', 0, '01160122750203545958', 1, '1996-02-01', '1996-11-06', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (51, NULL, 9343250, 2, '01020501830003032693', 0, '01160122730203431685', 1, '2005-04-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (52, NULL, 9343546, 2, '01020501840003032703', 0, NULL, 1, '2014-10-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (53, NULL, 9345491, 2, '01020501860003032716', 0, '01160122700206625499', 1, '2013-02-25', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (54, NULL, 9349833, 2, '01020501880003032729', 0, '01160122780203459880', 1, '2007-07-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (55, NULL, 9364657, 2, '01020501880003032732', 0, '01160122780203602536', 1, '1991-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (56, NULL, 9461641, 2, '01020501810003032431', 0, '01160122710203577840', 1, '1995-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (57, NULL, 9729246, 2, '01020501800003034141', 0, '01160122770203530357', 1, '2008-06-02', '2008-04-21', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (58, NULL, 9882350, 2, '01020501880003034125', 0, '01160223110207239835', 1, '2013-08-07', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (60, NULL, 10152376, 2, '01020501830003034167', 0, '01160122770203475330', 1, '2011-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (61, NULL, 10154041, 2, '01020501830003034170', 0, '01160122700203455827', 1, '1997-06-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (62, NULL, 10159422, 2, '01020501870003034196', 0, '01160122710203494920', 1, '2008-02-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (63, NULL, 10165529, 2, '01020501880003034206', 0, '01160122730203444663', 1, '1996-04-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (64, NULL, 10168182, 2, '01020501800003034219', 0, '01160122750203598725', 1, '1998-06-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (65, NULL, 10169417, 2, '01020501800003034222', 0, '01160122760203562976', 1, '1995-09-05', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (66, NULL, 10174245, 2, '01020501810003034235', 0, '01160122700203556852', 1, '1997-02-03', '1998-02-12', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (67, NULL, 10176223, 2, '01020501830003034251', 0, '01160122770203459709', 1, '1994-01-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (68, NULL, 10176587, 2, '01020501850003034264', 0, '01160122780203442903', 1, '1994-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (69, NULL, 10178175, 2, '01020501870003034277', 0, '01160122780203446062', 1, '2009-06-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (70, NULL, 10748849, 2, '01020501870003029774', 0, '01160122710203600606', 1, '1999-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (71, NULL, 10749497, 2, '01020501820003030242', 0, '01160122710205579620', 1, '2012-03-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (72, NULL, 11109820, 2, '01020501800003027996', 0, '01160122710203598610', 1, '1994-01-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (73, NULL, 11109944, 2, '01020501870003028429', 0, '01160122720203576640', 1, '2000-11-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (74, NULL, 11114194, 2, '01020501800003028856', 0, '01160122770203485548', 1, '2008-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (75, NULL, 11114976, 2, '01020501810003029321', 0, '01160122710207264112', 1, '2013-09-24', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (30, NULL, 8104194, 2, '01020501830003033634', 0, '01160122780203597788', 1, '2003-05-19', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (76, NULL, 11370746, 2, '01020501810003034879', 0, '01160122710203597389', 1, '1996-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (77, NULL, 11373818, 2, '01020501810003034882', 0, '01160122790203576608', 1, '1992-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (78, NULL, 11497586, 2, '01020501890003034701', 0, '01160122740203547380', 1, '2008-03-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (79, NULL, 11498234, 2, '01020501810003034714', 0, '01160122740203570383', 1, '1998-01-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (80, NULL, 11498513, 2, '01020501880003034769', 0, '01160122700203527755', 1, '1995-08-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (81, NULL, 11502597, 2, '01020501800003034785', 0, '01160122710203562844', 1, '2003-06-02', '2004-12-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (82, NULL, 11505190, 2, '01020501820003034811', 0, '01160122700203459814', 1, '1994-10-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (83, NULL, 11505221, 2, '01020501860003034837', 0, '01160122710203529987', 1, '2008-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (84, NULL, 11924357, 2, '01020501830003034895', 0, '01160122790203450345', 1, '2010-07-12', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (85, NULL, 11997518, 2, '01020501850003035205', 0, '01160122710203570359', 1, '2000-05-16', '2001-12-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (86, NULL, 12226494, 2, '01020501860003034840', 0, '01160122780203576780', 1, '2011-10-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (87, NULL, 12228449, 2, '01020501800003034866', 0, '01160122710203499026', 1, '2011-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (88, NULL, 12229282, 2, '01020501870003035218', 0, '01160122730203485424', 1, '1996-07-01', '1997-12-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (89, NULL, 12231796, 2, '01020501870003035221', 0, '01160122700203576616', 1, '2006-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (90, NULL, 12232430, 2, NULL, 0, '01160122710203609239', 1, '2009-03-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (91, NULL, 12620335, 2, '01020501860003035292', 0, '01160122790203563440', 1, '1996-09-02', '1997-12-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (92, NULL, 12633840, 2, '01020501870003035302', 0, '01160122750203687680', 1, '2007-11-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (93, NULL, 12784982, 2, '01020501810003034633', 0, '01160122700205996566', 1, '2012-08-29', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (94, NULL, 12813266, 2, '01020501820003034646', 0, '01160122750203576594', 1, '1996-08-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (95, NULL, 12889478, 2, '01020501810003035250', 0, '01160122730203570375', 1, '2009-03-02', '2011-10-24', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (96, NULL, 12890352, 2, '01020501840003035276', 0, '01160122720206628960', 1, '2013-02-25', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (97, NULL, 12970105, 2, '01020501860003035289', 0, '01160122750203434455', 1, '2010-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (98, NULL, 13304199, 2, '01020501830003033391', 0, '01160122770203535669', 1, '2009-02-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (99, NULL, 13351695, 2, '01020501840003033401', 0, '01160122710203563212', 1, '2005-08-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (100, NULL, 13486092, 2, '01020501880003033427', 0, '01160122720203445708', 1, '2009-03-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (101, NULL, 13505874, 2, '01020501800003034057', 0, '01160122760206628552', 1, '2013-02-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (102, NULL, 13549685, 2, '01020501800003033362', 0, '01160122710203792190', 1, '2007-07-04', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (103, NULL, 13587082, 2, '01020501860003033414', 0, '01160122740205580165', 1, '2012-03-12', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (104, NULL, 13821735, 2, '01020501880003033430', 0, '01160122780203460110', 1, '2006-10-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (105, NULL, 13891549, 2, '01020501880003034044', 0, '01160122700203564952', 1, '2005-06-30', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (106, NULL, 13939279, 2, '01020501880003033346', 0, NULL, 1, '2014-10-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (107, NULL, 13973905, 2, '01020501800003033359', 0, '01160122700203535502', 1, '2006-01-30', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (108, NULL, 14041929, 2, '01020501810003033692', 0, '01160122700207016976', 1, '2013-04-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (109, NULL, 14179006, 2, '01020501870003033744', 0, NULL, 1, '2014-09-22', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (110, NULL, 14180037, 2, '01020501870003033825', 0, '01160122720203460340', 1, '2007-11-19', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (111, NULL, 14180431, 2, '01020501850003032790', 0, '01160122720203570448', 1, '2010-05-19', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (112, NULL, 14180445, 2, '01020501880003032813', 0, '01160122780203432193', 1, '2008-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (113, NULL, 14348752, 2, '01020501830003033715', 0, '01160122710207222614', 1, '2013-07-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (114, NULL, 14417996, 2, '01020501810003033773', 0, '01160223120207251703', 1, '2013-08-08', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (115, NULL, 14460143, 2, '01020501840003033799', 0, '01160122750203563182', 1, '2005-05-23', '2011-01-01', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (116, NULL, 14502282, 2, '01020501850003033809', 0, '01160122750203601459', 1, '2010-03-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (117, NULL, 14605656, 2, '01020501800003032826', 0, '01160122730205866042', 1, '2012-08-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (118, NULL, 14607236, 2, '01020501810003032839', 0, '01160122730206617836', 1, '2012-12-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (119, NULL, 14885039, 2, '01020501850003033731', 0, '01160223100207048983', 1, '2013-05-27', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (120, NULL, 15027498, 2, '01020501890003033760', 0, '01160122720206626738', 1, '2013-02-25', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (121, NULL, 15079059, 2, '01020501840003032295', 0, '01160122740205576567', 1, '2012-03-21', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (122, NULL, 15079183, 2, '01020501850003032305', 0, '01160122780203551524', 1, '2009-03-04', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (123, NULL, 15157202, 2, '01020501810003032347', 0, '01160122780203460219', 1, '2006-01-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (124, NULL, 15159174, 2, '01020501840003033236', 0, '01160479060019591070', 1, '2014-04-24', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (125, NULL, 15242383, 2, '01020501860003033252', 0, '01160122710203455258', 1, '2007-10-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (126, NULL, 15242873, 2, '01020501800003033281', 0, '01160122710203577795', 1, '2006-10-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (127, NULL, 15242977, 2, '01020501810003033294', 0, '01160122740207221900', 1, '2013-05-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (128, NULL, 15503666, 2, '01020501820003033223', 0, '01160122710205580483', 1, '2012-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (129, NULL, 15567505, 2, '01020501820003032282', 0, '01160223140207665613', 1, '2014-04-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (130, NULL, 15639083, 2, '01020501810003032350', 0, '01160122710203449878', 1, '2004-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (131, NULL, 15989057, 2, '01020501810003033210', 0, '01160122790203475097', 1, '2008-06-09', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (132, NULL, 16122635, 2, '01020501810003031898', 0, '01160122710203440056', 1, '2007-07-23', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (133, NULL, 16409244, 2, '01020501880003031869', 0, '01160122710203535880', 1, '2008-06-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (134, NULL, 16409249, 2, '01020501880003031872', 0, '01160122770203455479', 1, '2007-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (135, NULL, 16540489, 2, '01020501820003031911', 0, '01160122730203544455', 1, '2008-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (136, NULL, 16540588, 2, '01020501840003031924', 0, '01160122770203609270', 1, '2009-03-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (137, NULL, 16575616, 2, '01020501860003031937', 0, '01160122770203555260', 1, '2010-06-07', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (138, NULL, 16611146, 2, '01020501810003032758', 0, '01160122710206600240', 1, '2013-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (139, NULL, 16611330, 2, '01020501830003032774', 0, '01160122750203535995', 1, '2008-06-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (140, NULL, 16745514, 2, '01020501820003031908', 0, '01160122700207222797', 1, '2013-07-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (141, NULL, 16960418, 2, '01020501830003035030', 0, '01160122760206628986', 1, '2013-02-26', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (142, NULL, 17107267, 2, '01020501830003035111', 0, '01160223160207753768', 1, '2014-08-07', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (143, NULL, 17179288, 2, '01020501870003035137', 0, '01160122770203488997', 1, '2009-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (144, NULL, 17188574, 2, '01020501820003035098', 0, '01160122710203577272', 1, '2008-12-01', '2009-04-30', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (145, NULL, 17310239, 2, '01020501830003034086', 0, NULL, 1, '2014-10-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (146, NULL, 17368905, 2, '01020501890003035072', 0, '01160053120207241376', 1, '2013-09-19', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (147, NULL, 17466696, 2, '01020501850003035043', 0, '01160122770203463226', 1, '2010-03-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (148, NULL, 17467886, 2, '01020501870003035056', 0, '01160053100207247480', 1, '2013-09-19', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (149, NULL, 17527174, 2, '01020501890003035069', 0, '01160122730205589375', 1, '2012-03-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (150, NULL, 17644919, 2, '01020501810003035085', 0, '01160223100207524343', 1, '2014-02-10', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (151, NULL, 17677488, 2, '01020501830003035108', 0, '01160122710203542576', 1, '2008-02-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (152, NULL, 17810911, 2, '01020501890003035153', 0, '01160122750203449134', 1, '2011-07-14', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (153, NULL, 17811842, 2, '01020501810003035166', 0, '01160122720203562690', 1, '2009-03-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (154, NULL, 17831294, 2, '01020501810003034073', 0, '01160223160207645876', 1, '2014-05-14', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (155, NULL, 17931413, 2, '01020501830003034332', 0, '01160223150207616175', 1, '2014-04-29', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (156, NULL, 18089366, 2, '01020501810003034390', 0, '01160122750203457668', 1, '2010-04-26', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (157, NULL, 18091927, 2, '01020501850003034426', 0, '01160122740205803806', 1, '2012-08-22', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (158, NULL, 18255360, 2, '01020501890003034293', 0, '01160122750203432169', 1, '2011-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (159, NULL, 18256660, 2, '01020501800003034303', 0, '01160122760207023719', 1, '2013-04-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (160, NULL, 18392581, 2, '01020501870003034361', 0, '01160122700203474619', 1, '2011-07-26', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (161, NULL, 18393100, 2, '01020501890003034374', 0, '01160122790205613380', 1, '2012-07-04', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (162, NULL, 18565488, 2, '01020501840003034581', 0, '01160122740203459784', 1, '2011-05-16', '2013-01-28', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (163, NULL, 18762218, 2, '01020501810003034552', 0, '01160122710205641792', 1, '2012-05-23', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (164, NULL, 18790377, 2, '01020501860003034594', 0, '01160122790203570405', 1, '2011-04-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (165, NULL, 18990427, 2, '01020501890003034617', 0, '01160122760207273316', 1, '2013-10-07', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (166, NULL, 19134197, 2, '01020501860003025930', 0, '01160122760207799946', 1, '2014-08-26', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (167, NULL, 19135479, 2, '01020501820003026272', 0, '01160122740205806480', 1, '2012-09-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (168, NULL, 19194827, 2, '01020501820003027132', 0, '01160053140207648743', 1, '2014-05-23', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (169, NULL, 19384392, 2, '01020501840003034578', 0, '01160122720207274193', 1, '2014-09-16', '2014-08-01', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (170, NULL, 19599113, 2, NULL, 0, NULL, 1, '2014-11-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (171, NULL, 19777625, 2, '01020501880003031788', 0, '01160122700205814204', 1, '2012-09-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (172, NULL, 19877410, 2, '01020501870003031704', 0, '01160122730206601832', 1, '2013-01-21', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (173, NULL, 19878267, 2, '01020501890003031717', 0, '01160122740206087004', 1, '2012-11-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (174, NULL, 20123329, 2, '01020501890003031720', 0, '01160223110207616132', 1, '2014-04-29', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (175, NULL, 20413389, 2, NULL, 0, NULL, 1, '2014-11-10', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (176, NULL, 20607943, 2, '01020501810003031254', 0, '01160122740206626916', 1, '2013-02-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (177, NULL, 21003940, 2, '01020501840003035195', 0, '01160122700207761310', 1, '2014-08-11', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (178, NULL, 1584405, 4, '01020501850003032224', 0, '01160122770203544470', 1, '2010-06-25', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (179, NULL, 4722610, 4, '01020501800003035001', 0, '01160122740203570464', 1, '2003-11-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (180, NULL, 5044324, 4, '01020501810003035014', 0, '01160122700203529456', 1, '2009-07-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (181, NULL, 6029508, 4, '01020501850003034507', 0, '01160122770203441095', 1, '2003-12-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (182, NULL, 7217325, 4, '01020501820003034484', 0, '01160122740203562798', 1, '2003-10-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (183, NULL, 9206397, 4, '01020501830003033469', 0, '01160122700207062307', 1, '2013-04-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (184, NULL, 9212052, 4, '01020501810003033540', 0, '01160122700203576860', 1, '2006-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (185, NULL, 9217928, 4, '01020501870003032402', 0, '01160122710205706860', 1, '2012-05-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (186, NULL, 9226358, 4, '01020501810003032509', 0, '01160122700203536746', 1, '2003-05-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (187, NULL, 9226823, 4, '01020501810003032512', 0, '01160223190207626430', 1, '2014-03-31', '2010-06-19', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (188, NULL, 9238021, 4, '01020501800003032583', 0, '01160122720207218013', 1, '2013-06-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (189, NULL, 9242382, 4, '01020501860003032635', 0, '01160122720205732267', 1, '2012-07-11', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (190, NULL, 10175817, 4, '01020501830003034248', 0, '01160122760203471660', 1, '2004-03-23', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (191, NULL, 12815249, 4, '01020501840003034659', 0, '01160122770203464800', 1, '2007-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (192, NULL, 13145307, 4, '01020501860003034675', 0, '01160122770206722664', 1, '2013-10-21', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (193, NULL, 13303380, 4, '01020501810003033375', 0, '01160122700207435006', 1, '2013-10-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (194, NULL, 13509191, 4, '01020501800003034060', 0, '01160053170207644410', 1, '2014-06-11', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (195, NULL, 14100292, 4, '01020501820003033786', 0, '01160122700207841934', 1, '2014-09-24', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (196, NULL, 14504701, 4, '01020501850003033812', 0, '01160122760203537947', 1, '2007-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (197, NULL, 14782183, 4, '01020501810003033702', 0, '01160223120207263396', 1, '2013-07-03', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (198, NULL, 15027132, 4, '01020501890003033757', 0, '01160122790203609204', 1, '2003-06-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (199, NULL, 15079871, 4, '01020501890003032334', 0, '01160122710203577540', 1, '2008-06-09', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (200, NULL, 15232905, 4, '01020501860003033249', 0, '01160122770203542363', 1, '2005-05-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (201, NULL, 15565847, 4, '01020501820003032279', 0, '01160223180207649022', 1, '2014-05-08', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (202, NULL, 16122426, 4, '01020501800003031885', 0, '01160122700207519455', 1, '2014-03-10', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (203, NULL, 16228364, 4, '01020501860003031940', 0, '01160122750203544048', 1, '2006-01-30', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (204, NULL, 16230882, 4, '01020501880003031953', 0, '01160122710203531019', 1, '2008-06-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (205, NULL, 17877234, 4, '01020501810003034316', 0, '01160122720207428174', 1, '2013-10-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (206, NULL, 19135703, 4, '01020501860003026706', 0, '01160122720203602668', 1, '2008-12-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (207, NULL, 20999679, 4, '01020501860003031775', 0, '01160122780206690029', 1, '2012-11-26', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (208, NULL, 1511005, 5, NULL, 0, NULL, 1, '1981-09-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (209, NULL, 1585168, 5, '01020501870003032237', 0, NULL, 1, '2007-05-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (210, NULL, 1856950, 5, NULL, 0, NULL, 1, '1990-05-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (211, NULL, 1871671, 5, '01020501810003032185', 0, NULL, 1, '2005-06-07', '2012-07-09', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (212, NULL, 2114930, 5, NULL, 0, NULL, 1, '1996-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (213, NULL, 2553023, 5, NULL, 0, NULL, 1, '1993-10-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (214, NULL, 2814218, 5, '01020501820003035182', 0, NULL, 1, '2013-08-05', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (215, NULL, 2892695, 5, NULL, 0, NULL, 1, '2010-10-05', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (216, NULL, 2893520, 5, '01020501830003032127', 0, NULL, 1, '1982-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (217, NULL, 3036971, 5, '01020501850003032143', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (218, NULL, 3070162, 5, '01020501870003032156', 0, NULL, 1, '1992-09-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (219, NULL, 3076373, 5, NULL, 0, NULL, 1, '2013-06-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (220, NULL, 3193180, 5, NULL, 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (221, NULL, 3195539, 5, NULL, 0, NULL, 1, '2011-10-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (222, NULL, 3196551, 5, NULL, 0, NULL, 1, '2013-05-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (223, NULL, 3313556, 5, NULL, 0, NULL, 1, '2012-08-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (224, NULL, 3467743, 5, '01020501800003032017', 0, NULL, 1, '2009-08-14', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (225, NULL, 3622075, 5, '01020501870003034280', 0, NULL, 1, '1999-05-10', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (226, NULL, 3622778, 5, '01020501870003034442', 0, NULL, 1, '1982-06-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (227, NULL, 3723633, 5, NULL, 0, NULL, 1, '1998-06-30', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (228, NULL, 3789237, 5, '01020501810003033207', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (229, NULL, 3791240, 5, '01020501820003032363', 0, NULL, 1, '2010-06-21', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (230, NULL, 3794368, 5, '01020501800003032745', 0, NULL, 1, '1980-07-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (231, NULL, 3794866, 5, NULL, 0, NULL, 1, '1976-06-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (232, NULL, 3795309, 5, NULL, 0, NULL, 1, '1977-04-25', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (233, NULL, 3795747, 5, NULL, 0, NULL, 1, '1998-06-30', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (234, NULL, 3795914, 5, NULL, 0, NULL, 1, '1976-04-26', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (235, NULL, 3916517, 5, NULL, 0, NULL, 1, '1976-01-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (236, NULL, 3997860, 5, '01020501800003032020', 0, NULL, 1, '1991-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (237, NULL, 3998558, 5, NULL, 0, NULL, 1, '1996-07-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (238, NULL, 3999232, 5, NULL, 0, NULL, 1, '1980-01-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (239, NULL, 3999334, 5, '01020501850003032062', 0, NULL, 1, '1981-02-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (240, NULL, 4000117, 5, NULL, 0, NULL, 1, '1986-07-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (241, NULL, 4000865, 5, '01020501890003032088', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (242, NULL, 4001028, 5, NULL, 0, NULL, 1, '1998-05-31', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (243, NULL, 4001588, 5, '01020501860003031856', 0, NULL, 1, '2014-04-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (244, NULL, 4001953, 5, '01020501810003032266', 0, NULL, 1, '2006-07-12', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (245, NULL, 4094271, 5, NULL, 0, NULL, 1, '1986-06-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (246, NULL, 4113764, 5, '01020501820003033304', 0, NULL, 1, '2014-05-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (247, NULL, 4203418, 5, NULL, 0, NULL, 1, '1972-06-06', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (248, NULL, 4203769, 5, '01020501810003033935', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (249, NULL, 4205929, 5, '01020501820003033948', 0, NULL, 1, '2004-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (250, NULL, 4207171, 5, NULL, 0, NULL, 1, '1980-01-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (251, NULL, 4211712, 5, NULL, 0, NULL, 1, '1982-02-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (252, NULL, 4630210, 5, '01020501890003033841', 0, NULL, 1, '2004-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (253, NULL, 4630373, 5, '01020501820003033867', 0, NULL, 1, '2007-11-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (254, NULL, 4630395, 5, NULL, 0, NULL, 1, '1998-06-30', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (255, NULL, 4633493, 5, '01020501840003033883', 0, NULL, 1, '1986-02-17', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (256, NULL, 4633817, 5, '01020501860003033896', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (257, NULL, 5022440, 5, '01020501860003034918', 0, NULL, 1, '1978-01-15', '2012-07-09', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (258, NULL, 5022569, 5, '01020501860003034921', 0, '01160122770203558952', 1, '2014-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (259, NULL, 5023877, 5, '01020501880003034934', 0, NULL, 1, '2011-01-01', '2012-07-09', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (260, NULL, 5027947, 5, NULL, 0, NULL, 1, '1978-01-02', '2012-07-19', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (261, NULL, 5028172, 5, '01020501800003034950', 0, NULL, 1, '1982-07-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (262, NULL, 5030905, 5, NULL, 0, NULL, 1, '1977-05-23', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (263, NULL, 5032111, 5, '01020501850003034989', 0, NULL, 1, '2007-08-31', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (264, NULL, 5326532, 5, '01020501870003033016', 0, NULL, 1, '1981-09-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (265, NULL, 5424001, 5, '01020501890003033029', 0, NULL, 1, '2008-09-19', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (266, NULL, 5449866, 5, '01020501890003033032', 0, NULL, 1, '2003-12-01', '1992-12-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (267, NULL, 5639446, 5, NULL, 0, NULL, 1, '2009-08-21', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (268, NULL, 5641138, 5, NULL, 0, NULL, 1, '1980-10-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (269, NULL, 5641857, 5, '01020501840003033074', 0, NULL, 1, '1982-08-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (270, NULL, 5643128, 5, '01020501860003033087', 0, NULL, 1, '2003-12-01', '2012-07-09', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (271, NULL, 5643923, 5, '01020501860003033090', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (272, NULL, 5661357, 5, '01020501880003033184', 0, NULL, 1, '1982-03-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (273, NULL, 5665179, 5, '01020501830003032855', 0, NULL, 1, '2009-07-23', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (274, NULL, 5671953, 5, NULL, 0, NULL, 1, '1981-04-22', '2012-07-09', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (275, NULL, 5679731, 5, '01020501800003032910', 0, NULL, 1, '2013-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (276, NULL, 5683136, 5, '01020501830003032936', 0, NULL, 1, '1983-05-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (277, NULL, 5683390, 5, '01020501850003032949', 0, NULL, 1, '2010-06-18', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (278, NULL, 5687942, 5, NULL, 0, NULL, 1, '1982-03-16', '2007-05-31', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (279, NULL, 6182857, 5, '01020501810003034471', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (280, NULL, 6447501, 5, '01020501890003034455', 0, NULL, 1, '2010-05-11', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (281, NULL, 6526362, 5, '01020501810003034468', 0, NULL, 1, '2006-03-21', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (282, NULL, 8072313, 5, NULL, 0, NULL, 1, '1980-01-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (283, NULL, 8073700, 5, '01020501890003033595', 0, NULL, 1, '2009-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (284, NULL, 8091914, 5, '01020501800003033605', 0, NULL, 1, '1996-09-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (285, NULL, 8093698, 5, '01020501810003033618', 0, '01160122710203482069', 1, '2014-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (286, NULL, 8101734, 5, '01020501810003033621', 0, '01160122730203535626', 1, '2014-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (287, NULL, 9121338, 5, '01020501850003033647', 0, NULL, 1, '2007-11-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (288, NULL, 9142656, 5, NULL, 0, NULL, 1, '1988-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (289, NULL, 9206182, 5, '01020501810003033456', 0, '01160122710203496949', 1, '2014-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (290, NULL, 9211978, 5, NULL, 0, NULL, 1, '2006-11-10', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (291, NULL, 9214700, 5, '01020501860003032389', 0, NULL, 1, '2007-11-15', '2012-07-09', NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (292, NULL, 9221875, 5, '01020501820003032444', 0, NULL, 1, '2007-05-15', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (293, NULL, 9222463, 5, NULL, 0, NULL, 1, '1991-01-16', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (294, NULL, 9225056, 5, '01020501880003032486', 0, NULL, 1, '2006-11-10', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (295, NULL, 9230572, 5, '01020501860003032554', 0, NULL, 1, '1982-03-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (296, NULL, 9330404, 5, NULL, 0, NULL, 1, '2004-02-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (297, NULL, 10154136, 5, NULL, 0, NULL, 1, '2005-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (298, NULL, 12070672, 5, '01020501880003034691', 0, NULL, 1, '2011-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (299, NULL, 12235308, 5, NULL, 0, NULL, 1, '2014-01-01', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (300, NULL, 12817519, 5, '01020501840003034662', 0, NULL, 1, '1996-09-02', NULL, NULL);
INSERT INTO informacion_laboral (id, base_legal_id, personal_cedula, tipo_nomina_id, cuenta_nomina, tipo_cuenta_nomina, cuenta_fideicomiso, tipo_cuenta_fideicomiso, fecha_ingreso, fecha_egreso, condicion_egreso) VALUES (59, NULL, 10147218, 2, '01020501810003034154', 0, '01160122710203476409', 1, '2010-05-17', NULL, NULL);


--
-- Data for Name: informacion_medica; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO informacion_medica (id, base_legal_id, personal_cedula, tipo_sangre, usa_lentes, es_zurdo) VALUES (1, NULL, 14417996, 'O+', false, false);
INSERT INTO informacion_medica (id, base_legal_id, personal_cedula, tipo_sangre, usa_lentes, es_zurdo) VALUES (2, NULL, 17179288, 'AB+', true, false);


--
-- Data for Name: institucion; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO institucion (id, nombre) VALUES (1, 'Contraloría del Estado Táchira');


--
-- Data for Name: municipio; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO municipio (id, estado_id, nombre) VALUES (1, 1, 'Alto Orinoco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (2, 1, 'Atabapo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (3, 1, 'Atures');
INSERT INTO municipio (id, estado_id, nombre) VALUES (4, 1, 'Autana');
INSERT INTO municipio (id, estado_id, nombre) VALUES (5, 1, 'Manapiare');
INSERT INTO municipio (id, estado_id, nombre) VALUES (6, 1, 'Maroa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (7, 1, 'Río Negro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (8, 2, 'Anaco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (9, 2, 'Aragua');
INSERT INTO municipio (id, estado_id, nombre) VALUES (10, 2, 'Manuel Ezequiel Bruzual');
INSERT INTO municipio (id, estado_id, nombre) VALUES (11, 2, 'Diego Bautista Urbaneja');
INSERT INTO municipio (id, estado_id, nombre) VALUES (12, 2, 'Fernando Peñalver');
INSERT INTO municipio (id, estado_id, nombre) VALUES (13, 2, 'Francisco Del Carmen Carvajal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (14, 2, 'General Sir Arthur McGregor');
INSERT INTO municipio (id, estado_id, nombre) VALUES (15, 2, 'Guanta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (16, 2, 'Independencia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (17, 2, 'José Gregorio Monagas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (18, 2, 'Juan Antonio Sotillo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (19, 2, 'Juan Manuel Cajigal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (20, 2, 'Libertad');
INSERT INTO municipio (id, estado_id, nombre) VALUES (21, 2, 'Francisco de Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (22, 2, 'Pedro María Freites');
INSERT INTO municipio (id, estado_id, nombre) VALUES (23, 2, 'Píritu');
INSERT INTO municipio (id, estado_id, nombre) VALUES (24, 2, 'San José de Guanipa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (25, 2, 'San Juan de Capistrano');
INSERT INTO municipio (id, estado_id, nombre) VALUES (26, 2, 'Santa Ana');
INSERT INTO municipio (id, estado_id, nombre) VALUES (27, 2, 'Simón Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (28, 2, 'Simón Rodríguez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (29, 3, 'Achaguas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (30, 3, 'Biruaca');
INSERT INTO municipio (id, estado_id, nombre) VALUES (31, 3, 'Muñóz');
INSERT INTO municipio (id, estado_id, nombre) VALUES (32, 3, 'Páez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (33, 3, 'Pedro Camejo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (34, 3, 'Rómulo Gallegos');
INSERT INTO municipio (id, estado_id, nombre) VALUES (35, 3, 'San Fernando');
INSERT INTO municipio (id, estado_id, nombre) VALUES (36, 4, 'Atanasio Girardot');
INSERT INTO municipio (id, estado_id, nombre) VALUES (37, 4, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (38, 4, 'Camatagua');
INSERT INTO municipio (id, estado_id, nombre) VALUES (39, 4, 'Francisco Linares Alcántara');
INSERT INTO municipio (id, estado_id, nombre) VALUES (40, 4, 'José Ángel Lamas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (41, 4, 'José Félix Ribas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (42, 4, 'José Rafael Revenga');
INSERT INTO municipio (id, estado_id, nombre) VALUES (43, 4, 'Libertador');
INSERT INTO municipio (id, estado_id, nombre) VALUES (44, 4, 'Mario Briceño Iragorry');
INSERT INTO municipio (id, estado_id, nombre) VALUES (45, 4, 'Ocumare de la Costa de Oro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (46, 4, 'San Casimiro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (47, 4, 'San Sebastián');
INSERT INTO municipio (id, estado_id, nombre) VALUES (48, 4, 'Santiago Mariño');
INSERT INTO municipio (id, estado_id, nombre) VALUES (49, 4, 'Santos Michelena');
INSERT INTO municipio (id, estado_id, nombre) VALUES (50, 4, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (51, 4, 'Tovar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (52, 4, 'Urdaneta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (53, 4, 'Zamora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (54, 5, 'Alberto Arvelo Torrealba');
INSERT INTO municipio (id, estado_id, nombre) VALUES (55, 5, 'Andrés Eloy Blanco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (56, 5, 'Antonio José de Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (57, 5, 'Arismendi');
INSERT INTO municipio (id, estado_id, nombre) VALUES (58, 5, 'Barinas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (59, 5, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (60, 5, 'Cruz Paredes');
INSERT INTO municipio (id, estado_id, nombre) VALUES (61, 5, 'Ezequiel Zamora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (62, 5, 'Obispos');
INSERT INTO municipio (id, estado_id, nombre) VALUES (63, 5, 'Pedraza');
INSERT INTO municipio (id, estado_id, nombre) VALUES (64, 5, 'Rojas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (65, 5, 'Sosa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (66, 6, 'Caroní');
INSERT INTO municipio (id, estado_id, nombre) VALUES (67, 6, 'Cedeño');
INSERT INTO municipio (id, estado_id, nombre) VALUES (68, 6, 'El Callao');
INSERT INTO municipio (id, estado_id, nombre) VALUES (69, 6, 'Gran Sabana');
INSERT INTO municipio (id, estado_id, nombre) VALUES (70, 6, 'Heres');
INSERT INTO municipio (id, estado_id, nombre) VALUES (71, 6, 'Piar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (72, 6, 'Angostura (Raúl Leoni)');
INSERT INTO municipio (id, estado_id, nombre) VALUES (73, 6, 'Roscio');
INSERT INTO municipio (id, estado_id, nombre) VALUES (74, 6, 'Sifontes');
INSERT INTO municipio (id, estado_id, nombre) VALUES (75, 6, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (76, 6, 'Padre Pedro Chien');
INSERT INTO municipio (id, estado_id, nombre) VALUES (77, 7, 'Bejuma');
INSERT INTO municipio (id, estado_id, nombre) VALUES (78, 7, 'Carlos Arvelo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (79, 7, 'Diego Ibarra');
INSERT INTO municipio (id, estado_id, nombre) VALUES (80, 7, 'Guacara');
INSERT INTO municipio (id, estado_id, nombre) VALUES (81, 7, 'Juan José Mora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (82, 7, 'Libertador');
INSERT INTO municipio (id, estado_id, nombre) VALUES (83, 7, 'Los Guayos');
INSERT INTO municipio (id, estado_id, nombre) VALUES (84, 7, 'Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (85, 7, 'Montalbán');
INSERT INTO municipio (id, estado_id, nombre) VALUES (86, 7, 'Naguanagua');
INSERT INTO municipio (id, estado_id, nombre) VALUES (87, 7, 'Puerto Cabello');
INSERT INTO municipio (id, estado_id, nombre) VALUES (88, 7, 'San Diego');
INSERT INTO municipio (id, estado_id, nombre) VALUES (89, 7, 'San Joaquín');
INSERT INTO municipio (id, estado_id, nombre) VALUES (90, 7, 'Valencia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (91, 8, 'Anzoátegui');
INSERT INTO municipio (id, estado_id, nombre) VALUES (92, 8, 'Tinaquillo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (93, 8, 'Girardot');
INSERT INTO municipio (id, estado_id, nombre) VALUES (94, 8, 'Lima Blanco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (95, 8, 'Pao de San Juan Bautista');
INSERT INTO municipio (id, estado_id, nombre) VALUES (96, 8, 'Ricaurte');
INSERT INTO municipio (id, estado_id, nombre) VALUES (97, 8, 'Rómulo Gallegos');
INSERT INTO municipio (id, estado_id, nombre) VALUES (98, 8, 'San Carlos');
INSERT INTO municipio (id, estado_id, nombre) VALUES (99, 8, 'Tinaco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (100, 9, 'Antonio Díaz');
INSERT INTO municipio (id, estado_id, nombre) VALUES (101, 9, 'Casacoima');
INSERT INTO municipio (id, estado_id, nombre) VALUES (102, 9, 'Pedernales');
INSERT INTO municipio (id, estado_id, nombre) VALUES (103, 9, 'Tucupita');
INSERT INTO municipio (id, estado_id, nombre) VALUES (104, 10, 'Acosta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (105, 10, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (106, 10, 'Buchivacoa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (107, 10, 'Cacique Manaure');
INSERT INTO municipio (id, estado_id, nombre) VALUES (108, 10, 'Carirubana');
INSERT INTO municipio (id, estado_id, nombre) VALUES (109, 10, 'Colina');
INSERT INTO municipio (id, estado_id, nombre) VALUES (110, 10, 'Dabajuro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (111, 10, 'Democracia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (112, 10, 'Falcón');
INSERT INTO municipio (id, estado_id, nombre) VALUES (113, 10, 'Federación');
INSERT INTO municipio (id, estado_id, nombre) VALUES (114, 10, 'Jacura');
INSERT INTO municipio (id, estado_id, nombre) VALUES (115, 10, 'José Laurencio Silva');
INSERT INTO municipio (id, estado_id, nombre) VALUES (116, 10, 'Los Taques');
INSERT INTO municipio (id, estado_id, nombre) VALUES (117, 10, 'Mauroa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (118, 10, 'Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (119, 10, 'Monseñor Iturriza');
INSERT INTO municipio (id, estado_id, nombre) VALUES (120, 10, 'Palmasola');
INSERT INTO municipio (id, estado_id, nombre) VALUES (121, 10, 'Petit');
INSERT INTO municipio (id, estado_id, nombre) VALUES (122, 10, 'Píritu');
INSERT INTO municipio (id, estado_id, nombre) VALUES (123, 10, 'San Francisco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (124, 10, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (125, 10, 'Tocópero');
INSERT INTO municipio (id, estado_id, nombre) VALUES (126, 10, 'Unión');
INSERT INTO municipio (id, estado_id, nombre) VALUES (127, 10, 'Urumaco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (128, 10, 'Zamora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (129, 11, 'Camaguán');
INSERT INTO municipio (id, estado_id, nombre) VALUES (130, 11, 'Chaguaramas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (131, 11, 'El Socorro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (132, 11, 'José Félix Ribas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (133, 11, 'José Tadeo Monagas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (134, 11, 'Juan Germán Roscio');
INSERT INTO municipio (id, estado_id, nombre) VALUES (135, 11, 'Julián Mellado');
INSERT INTO municipio (id, estado_id, nombre) VALUES (136, 11, 'Las Mercedes');
INSERT INTO municipio (id, estado_id, nombre) VALUES (137, 11, 'Leonardo Infante');
INSERT INTO municipio (id, estado_id, nombre) VALUES (138, 11, 'Pedro Zaraza');
INSERT INTO municipio (id, estado_id, nombre) VALUES (139, 11, 'Ortíz');
INSERT INTO municipio (id, estado_id, nombre) VALUES (140, 11, 'San Gerónimo de Guayabal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (141, 11, 'San José de Guaribe');
INSERT INTO municipio (id, estado_id, nombre) VALUES (142, 11, 'Santa María de Ipire');
INSERT INTO municipio (id, estado_id, nombre) VALUES (143, 11, 'Sebastián Francisco de Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (144, 12, 'Andrés Eloy Blanco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (145, 12, 'Crespo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (146, 12, 'Iribarren');
INSERT INTO municipio (id, estado_id, nombre) VALUES (147, 12, 'Jiménez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (148, 12, 'Morán');
INSERT INTO municipio (id, estado_id, nombre) VALUES (149, 12, 'Palavecino');
INSERT INTO municipio (id, estado_id, nombre) VALUES (150, 12, 'Simón Planas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (151, 12, 'Torres');
INSERT INTO municipio (id, estado_id, nombre) VALUES (152, 12, 'Urdaneta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (179, 13, 'Alberto Adriani');
INSERT INTO municipio (id, estado_id, nombre) VALUES (180, 13, 'Andrés Bello');
INSERT INTO municipio (id, estado_id, nombre) VALUES (181, 13, 'Antonio Pinto Salinas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (182, 13, 'Aricagua');
INSERT INTO municipio (id, estado_id, nombre) VALUES (183, 13, 'Arzobispo Chacón');
INSERT INTO municipio (id, estado_id, nombre) VALUES (184, 13, 'Campo Elías');
INSERT INTO municipio (id, estado_id, nombre) VALUES (185, 13, 'Caracciolo Parra Olmedo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (186, 13, 'Cardenal Quintero');
INSERT INTO municipio (id, estado_id, nombre) VALUES (187, 13, 'Guaraque');
INSERT INTO municipio (id, estado_id, nombre) VALUES (188, 13, 'Julio César Salas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (189, 13, 'Justo Briceño');
INSERT INTO municipio (id, estado_id, nombre) VALUES (190, 13, 'Libertador');
INSERT INTO municipio (id, estado_id, nombre) VALUES (191, 13, 'Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (192, 13, 'Obispo Ramos de Lora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (193, 13, 'Padre Noguera');
INSERT INTO municipio (id, estado_id, nombre) VALUES (194, 13, 'Pueblo Llano');
INSERT INTO municipio (id, estado_id, nombre) VALUES (195, 13, 'Rangel');
INSERT INTO municipio (id, estado_id, nombre) VALUES (196, 13, 'Rivas Dávila');
INSERT INTO municipio (id, estado_id, nombre) VALUES (197, 13, 'Santos Marquina');
INSERT INTO municipio (id, estado_id, nombre) VALUES (198, 13, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (199, 13, 'Tovar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (200, 13, 'Tulio Febres Cordero');
INSERT INTO municipio (id, estado_id, nombre) VALUES (201, 13, 'Zea');
INSERT INTO municipio (id, estado_id, nombre) VALUES (223, 14, 'Acevedo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (224, 14, 'Andrés Bello');
INSERT INTO municipio (id, estado_id, nombre) VALUES (225, 14, 'Baruta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (226, 14, 'Brión');
INSERT INTO municipio (id, estado_id, nombre) VALUES (227, 14, 'Buroz');
INSERT INTO municipio (id, estado_id, nombre) VALUES (228, 14, 'Carrizal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (229, 14, 'Chacao');
INSERT INTO municipio (id, estado_id, nombre) VALUES (230, 14, 'Cristóbal Rojas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (231, 14, 'El Hatillo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (232, 14, 'Guaicaipuro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (233, 14, 'Independencia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (234, 14, 'Lander');
INSERT INTO municipio (id, estado_id, nombre) VALUES (235, 14, 'Los Salias');
INSERT INTO municipio (id, estado_id, nombre) VALUES (236, 14, 'Páez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (237, 14, 'Paz Castillo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (238, 14, 'Pedro Gual');
INSERT INTO municipio (id, estado_id, nombre) VALUES (239, 14, 'Plaza');
INSERT INTO municipio (id, estado_id, nombre) VALUES (240, 14, 'Simón Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (241, 14, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (242, 14, 'Urdaneta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (243, 14, 'Zamora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (258, 15, 'Acosta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (259, 15, 'Aguasay');
INSERT INTO municipio (id, estado_id, nombre) VALUES (260, 15, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (261, 15, 'Caripe');
INSERT INTO municipio (id, estado_id, nombre) VALUES (262, 15, 'Cedeño');
INSERT INTO municipio (id, estado_id, nombre) VALUES (263, 15, 'Ezequiel Zamora');
INSERT INTO municipio (id, estado_id, nombre) VALUES (264, 15, 'Libertador');
INSERT INTO municipio (id, estado_id, nombre) VALUES (265, 15, 'Maturín');
INSERT INTO municipio (id, estado_id, nombre) VALUES (266, 15, 'Piar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (267, 15, 'Punceres');
INSERT INTO municipio (id, estado_id, nombre) VALUES (268, 15, 'Santa Bárbara');
INSERT INTO municipio (id, estado_id, nombre) VALUES (269, 15, 'Sotillo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (270, 15, 'Uracoa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (271, 16, 'Antolín del Campo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (272, 16, 'Arismendi');
INSERT INTO municipio (id, estado_id, nombre) VALUES (273, 16, 'García');
INSERT INTO municipio (id, estado_id, nombre) VALUES (274, 16, 'Gómez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (275, 16, 'Maneiro');
INSERT INTO municipio (id, estado_id, nombre) VALUES (276, 16, 'Marcano');
INSERT INTO municipio (id, estado_id, nombre) VALUES (277, 16, 'Mariño');
INSERT INTO municipio (id, estado_id, nombre) VALUES (278, 16, 'Península de Macanao');
INSERT INTO municipio (id, estado_id, nombre) VALUES (279, 16, 'Tubores');
INSERT INTO municipio (id, estado_id, nombre) VALUES (280, 16, 'Villalba');
INSERT INTO municipio (id, estado_id, nombre) VALUES (281, 16, 'Díaz');
INSERT INTO municipio (id, estado_id, nombre) VALUES (282, 17, 'Agua Blanca');
INSERT INTO municipio (id, estado_id, nombre) VALUES (283, 17, 'Araure');
INSERT INTO municipio (id, estado_id, nombre) VALUES (284, 17, 'Esteller');
INSERT INTO municipio (id, estado_id, nombre) VALUES (285, 17, 'Guanare');
INSERT INTO municipio (id, estado_id, nombre) VALUES (286, 17, 'Guanarito');
INSERT INTO municipio (id, estado_id, nombre) VALUES (287, 17, 'Monseñor José Vicente de Unda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (288, 17, 'Ospino');
INSERT INTO municipio (id, estado_id, nombre) VALUES (289, 17, 'Páez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (290, 17, 'Papelón');
INSERT INTO municipio (id, estado_id, nombre) VALUES (291, 17, 'San Genaro de Boconoíto');
INSERT INTO municipio (id, estado_id, nombre) VALUES (292, 17, 'San Rafael de Onoto');
INSERT INTO municipio (id, estado_id, nombre) VALUES (293, 17, 'Santa Rosalía');
INSERT INTO municipio (id, estado_id, nombre) VALUES (294, 17, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (295, 17, 'Turén');
INSERT INTO municipio (id, estado_id, nombre) VALUES (296, 18, 'Andrés Eloy Blanco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (297, 18, 'Andrés Mata');
INSERT INTO municipio (id, estado_id, nombre) VALUES (298, 18, 'Arismendi');
INSERT INTO municipio (id, estado_id, nombre) VALUES (299, 18, 'Benítez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (300, 18, 'Bermúdez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (301, 18, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (302, 18, 'Cajigal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (303, 18, 'Cruz Salmerón Acosta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (304, 18, 'Libertador');
INSERT INTO municipio (id, estado_id, nombre) VALUES (305, 18, 'Mariño');
INSERT INTO municipio (id, estado_id, nombre) VALUES (306, 18, 'Mejía');
INSERT INTO municipio (id, estado_id, nombre) VALUES (307, 18, 'Montes');
INSERT INTO municipio (id, estado_id, nombre) VALUES (308, 18, 'Ribero');
INSERT INTO municipio (id, estado_id, nombre) VALUES (309, 18, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (310, 18, 'Valdéz');
INSERT INTO municipio (id, estado_id, nombre) VALUES (341, 19, 'Andrés Bello');
INSERT INTO municipio (id, estado_id, nombre) VALUES (342, 19, 'Antonio Rómulo Costa');
INSERT INTO municipio (id, estado_id, nombre) VALUES (343, 19, 'Ayacucho');
INSERT INTO municipio (id, estado_id, nombre) VALUES (344, 19, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (345, 19, 'Cárdenas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (346, 19, 'Córdoba');
INSERT INTO municipio (id, estado_id, nombre) VALUES (347, 19, 'Fernández Feo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (348, 19, 'Francisco de Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (349, 19, 'García de Hevia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (350, 19, 'Guásimos');
INSERT INTO municipio (id, estado_id, nombre) VALUES (351, 19, 'Independencia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (352, 19, 'Jáuregui');
INSERT INTO municipio (id, estado_id, nombre) VALUES (353, 19, 'José María Vargas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (354, 19, 'Junín');
INSERT INTO municipio (id, estado_id, nombre) VALUES (355, 19, 'Libertad');
INSERT INTO municipio (id, estado_id, nombre) VALUES (356, 19, 'Libertador');
INSERT INTO municipio (id, estado_id, nombre) VALUES (357, 19, 'Lobatera');
INSERT INTO municipio (id, estado_id, nombre) VALUES (358, 19, 'Michelena');
INSERT INTO municipio (id, estado_id, nombre) VALUES (359, 19, 'Panamericano');
INSERT INTO municipio (id, estado_id, nombre) VALUES (360, 19, 'Pedro María Ureña');
INSERT INTO municipio (id, estado_id, nombre) VALUES (361, 19, 'Rafael Urdaneta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (362, 19, 'Samuel Darío Maldonado');
INSERT INTO municipio (id, estado_id, nombre) VALUES (363, 19, 'San Cristóbal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (364, 19, 'Seboruco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (365, 19, 'Simón Rodríguez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (366, 19, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (367, 19, 'Torbes');
INSERT INTO municipio (id, estado_id, nombre) VALUES (368, 19, 'Uribante');
INSERT INTO municipio (id, estado_id, nombre) VALUES (369, 19, 'San Judas Tadeo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (370, 20, 'Andrés Bello');
INSERT INTO municipio (id, estado_id, nombre) VALUES (371, 20, 'Boconó');
INSERT INTO municipio (id, estado_id, nombre) VALUES (372, 20, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (373, 20, 'Candelaria');
INSERT INTO municipio (id, estado_id, nombre) VALUES (374, 20, 'Carache');
INSERT INTO municipio (id, estado_id, nombre) VALUES (375, 20, 'Escuque');
INSERT INTO municipio (id, estado_id, nombre) VALUES (376, 20, 'José Felipe Márquez Cañizalez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (377, 20, 'Juan Vicente Campos Elías');
INSERT INTO municipio (id, estado_id, nombre) VALUES (378, 20, 'La Ceiba');
INSERT INTO municipio (id, estado_id, nombre) VALUES (379, 20, 'Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (380, 20, 'Monte Carmelo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (381, 20, 'Motatán');
INSERT INTO municipio (id, estado_id, nombre) VALUES (382, 20, 'Pampán');
INSERT INTO municipio (id, estado_id, nombre) VALUES (383, 20, 'Pampanito');
INSERT INTO municipio (id, estado_id, nombre) VALUES (384, 20, 'Rafael Rangel');
INSERT INTO municipio (id, estado_id, nombre) VALUES (385, 20, 'San Rafael de Carvajal');
INSERT INTO municipio (id, estado_id, nombre) VALUES (386, 20, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (387, 20, 'Trujillo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (388, 20, 'Urdaneta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (389, 20, 'Valera');
INSERT INTO municipio (id, estado_id, nombre) VALUES (390, 21, 'Vargas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (391, 22, 'Arístides Bastidas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (392, 22, 'Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (407, 22, 'Bruzual');
INSERT INTO municipio (id, estado_id, nombre) VALUES (408, 22, 'Cocorote');
INSERT INTO municipio (id, estado_id, nombre) VALUES (409, 22, 'Independencia');
INSERT INTO municipio (id, estado_id, nombre) VALUES (410, 22, 'José Antonio Páez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (411, 22, 'La Trinidad');
INSERT INTO municipio (id, estado_id, nombre) VALUES (412, 22, 'Manuel Monge');
INSERT INTO municipio (id, estado_id, nombre) VALUES (413, 22, 'Nirgua');
INSERT INTO municipio (id, estado_id, nombre) VALUES (414, 22, 'Peña');
INSERT INTO municipio (id, estado_id, nombre) VALUES (415, 22, 'San Felipe');
INSERT INTO municipio (id, estado_id, nombre) VALUES (416, 22, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (417, 22, 'Urachiche');
INSERT INTO municipio (id, estado_id, nombre) VALUES (418, 22, 'José Joaquín Veroes');
INSERT INTO municipio (id, estado_id, nombre) VALUES (441, 23, 'Almirante Padilla');
INSERT INTO municipio (id, estado_id, nombre) VALUES (442, 23, 'Baralt');
INSERT INTO municipio (id, estado_id, nombre) VALUES (443, 23, 'Cabimas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (444, 23, 'Catatumbo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (445, 23, 'Colón');
INSERT INTO municipio (id, estado_id, nombre) VALUES (446, 23, 'Francisco Javier Pulgar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (447, 23, 'Páez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (448, 23, 'Jesús Enrique Losada');
INSERT INTO municipio (id, estado_id, nombre) VALUES (449, 23, 'Jesús María Semprún');
INSERT INTO municipio (id, estado_id, nombre) VALUES (450, 23, 'La Cañada de Urdaneta');
INSERT INTO municipio (id, estado_id, nombre) VALUES (451, 23, 'Lagunillas');
INSERT INTO municipio (id, estado_id, nombre) VALUES (452, 23, 'Machiques de Perijá');
INSERT INTO municipio (id, estado_id, nombre) VALUES (453, 23, 'Mara');
INSERT INTO municipio (id, estado_id, nombre) VALUES (454, 23, 'Maracaibo');
INSERT INTO municipio (id, estado_id, nombre) VALUES (455, 23, 'Miranda');
INSERT INTO municipio (id, estado_id, nombre) VALUES (456, 23, 'Rosario de Perijá');
INSERT INTO municipio (id, estado_id, nombre) VALUES (457, 23, 'San Francisco');
INSERT INTO municipio (id, estado_id, nombre) VALUES (458, 23, 'Santa Rita');
INSERT INTO municipio (id, estado_id, nombre) VALUES (459, 23, 'Simón Bolívar');
INSERT INTO municipio (id, estado_id, nombre) VALUES (460, 23, 'Sucre');
INSERT INTO municipio (id, estado_id, nombre) VALUES (461, 23, 'Valmore Rodríguez');
INSERT INTO municipio (id, estado_id, nombre) VALUES (462, 24, 'Libertador');


--
-- Data for Name: nomina; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: parroquia; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1, 1, 'Alto Orinoco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (2, 1, 'Huachamacare Acanaña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (3, 1, 'Marawaka Toky Shamanaña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (4, 1, 'Mavaka Mavaka');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (5, 1, 'Sierra Parima Parimabé');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (6, 2, 'Ucata Laja Lisa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (7, 2, 'Yapacana Macuruco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (8, 2, 'Caname Guarinuma');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (9, 3, 'Fernando Girón Tovar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (10, 3, 'Luis Alberto Gómez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (11, 3, 'Pahueña Limón de Parhueña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (12, 3, 'Platanillal Platanillal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (13, 4, 'Samariapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (14, 4, 'Sipapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (15, 4, 'Munduapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (16, 4, 'Guayapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (17, 5, 'Alto Ventuari');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (18, 5, 'Medio Ventuari');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (19, 5, 'Bajo Ventuari');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (20, 6, 'Victorino');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (21, 6, 'Comunidad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (22, 7, 'Casiquiare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (23, 7, 'Cocuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (24, 7, 'San Carlos de Río Negro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (25, 7, 'Solano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (26, 8, 'Anaco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (27, 8, 'San Joaquín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (28, 9, 'Cachipo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (29, 9, 'Aragua de Barcelona');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (30, 11, 'Lechería');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (31, 11, 'El Morro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (32, 12, 'Puerto Píritu');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (33, 12, 'San Miguel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (34, 12, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (35, 13, 'Valle de Guanape');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (36, 13, 'Santa Bárbara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (37, 14, 'El Chaparro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (38, 14, 'Tomás Alfaro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (39, 14, 'Calatrava');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (40, 15, 'Guanta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (41, 15, 'Chorrerón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (42, 16, 'Mamo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (43, 16, 'Soledad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (44, 17, 'Mapire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (45, 17, 'Piar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (46, 17, 'Santa Clara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (47, 17, 'San Diego de Cabrutica');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (48, 17, 'Uverito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (49, 17, 'Zuata');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (50, 18, 'Puerto La Cruz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (51, 18, 'Pozuelos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (52, 19, 'Onoto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (53, 19, 'San Pablo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (54, 20, 'San Mateo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (55, 20, 'El Carito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (56, 20, 'Santa Inés');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (57, 20, 'La Romereña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (58, 21, 'Atapirire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (59, 21, 'Boca del Pao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (60, 21, 'El Pao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (61, 21, 'Pariaguán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (62, 22, 'Cantaura');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (63, 22, 'Libertador');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (64, 22, 'Santa Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (65, 22, 'Urica');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (66, 23, 'Píritu');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (67, 23, 'San Francisco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (68, 24, 'San José de Guanipa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (69, 25, 'Boca de Uchire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (70, 25, 'Boca de Chávez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (71, 26, 'Pueblo Nuevo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (72, 26, 'Santa Ana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (73, 27, 'Bergatín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (74, 27, 'Caigua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (75, 27, 'El Carmen');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (76, 27, 'El Pilar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (77, 27, 'Naricual');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (78, 27, 'San Crsitóbal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (79, 28, 'Edmundo Barrios');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (80, 28, 'Miguel Otero Silva');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (81, 29, 'Achaguas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (82, 29, 'Apurito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (83, 29, 'El Yagual');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (84, 29, 'Guachara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (85, 29, 'Mucuritas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (86, 29, 'Queseras del medio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (87, 30, 'Biruaca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (88, 31, 'Bruzual');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (89, 31, 'Mantecal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (90, 31, 'Quintero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (91, 31, 'Rincón Hondo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (92, 31, 'San Vicente');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (93, 32, 'Guasdualito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (94, 32, 'Aramendi');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (95, 32, 'El Amparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (96, 32, 'San Camilo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (97, 32, 'Urdaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (98, 33, 'San Juan de Payara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (99, 33, 'Codazzi');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (100, 33, 'Cunaviche');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (101, 34, 'Elorza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (102, 34, 'La Trinidad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (103, 35, 'San Fernando');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (104, 35, 'El Recreo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (105, 35, 'Peñalver');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (106, 35, 'San Rafael de Atamaica');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (107, 36, 'Pedro José Ovalles');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (108, 36, 'Joaquín Crespo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (109, 36, 'José Casanova Godoy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (110, 36, 'Madre María de San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (111, 36, 'Andrés Eloy Blanco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (112, 36, 'Los Tacarigua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (113, 36, 'Las Delicias');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (114, 36, 'Choroní');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (115, 37, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (116, 38, 'Camatagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (117, 38, 'Carmen de Cura');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (118, 39, 'Santa Rita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (119, 39, 'Francisco de Miranda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (120, 39, 'Moseñor Feliciano González');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (121, 40, 'Santa Cruz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (122, 41, 'José Félix Ribas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (123, 41, 'Castor Nieves Ríos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (124, 41, 'Las Guacamayas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (125, 41, 'Pao de Zárate');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (126, 41, 'Zuata');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (127, 42, 'José Rafael Revenga');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (128, 43, 'Palo Negro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (129, 43, 'San Martín de Porres');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (130, 44, 'El Limón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (131, 44, 'Caña de Azúcar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (132, 45, 'Ocumare de la Costa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (133, 46, 'San Casimiro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (134, 46, 'Güiripa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (135, 46, 'Ollas de Caramacate');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (136, 46, 'Valle Morín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (137, 47, 'San Sebastían');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (138, 48, 'Turmero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (139, 48, 'Arevalo Aponte');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (140, 48, 'Chuao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (141, 48, 'Samán de Güere');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (142, 48, 'Alfredo Pacheco Miranda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (143, 49, 'Santos Michelena');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (144, 49, 'Tiara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (145, 50, 'Cagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (146, 50, 'Bella Vista');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (147, 51, 'Tovar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (148, 52, 'Urdaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (149, 52, 'Las Peñitas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (150, 52, 'San Francisco de Cara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (151, 52, 'Taguay');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (152, 53, 'Zamora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (153, 53, 'Magdaleno');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (154, 53, 'San Francisco de Asís');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (155, 53, 'Valles de Tucutunemo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (156, 53, 'Augusto Mijares');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (157, 54, 'Sabaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (158, 54, 'Juan Antonio Rodríguez Domínguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (159, 55, 'El Cantón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (160, 55, 'Santa Cruz de Guacas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (161, 55, 'Puerto Vivas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (162, 56, 'Ticoporo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (163, 56, 'Nicolás Pulido');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (164, 56, 'Andrés Bello');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (165, 57, 'Arismendi');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (166, 57, 'Guadarrama');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (167, 57, 'La Unión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (168, 57, 'San Antonio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (169, 58, 'Barinas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (170, 58, 'Alberto Arvelo Larriva');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (171, 58, 'San Silvestre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (172, 58, 'Santa Inés');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (173, 58, 'Santa Lucía');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (174, 58, 'Torumos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (175, 58, 'El Carmen');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (176, 58, 'Rómulo Betancourt');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (177, 58, 'Corazón de Jesús');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (178, 58, 'Ramón Ignacio Méndez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (179, 58, 'Alto Barinas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (180, 58, 'Manuel Palacio Fajardo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (181, 58, 'Juan Antonio Rodríguez Domínguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (182, 58, 'Dominga Ortiz de Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (183, 59, 'Barinitas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (184, 59, 'Altamira de Cáceres');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (185, 59, 'Calderas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (186, 60, 'Barrancas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (187, 60, 'El Socorro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (188, 60, 'Mazparrito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (189, 61, 'Santa Bárbara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (190, 61, 'Pedro Briceño Méndez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (191, 61, 'Ramón Ignacio Méndez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (192, 61, 'José Ignacio del Pumar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (193, 62, 'Obispos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (194, 62, 'Guasimitos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (195, 62, 'El Real');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (196, 62, 'La Luz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (197, 63, 'Ciudad Bolívia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (198, 63, 'José Ignacio Briceño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (199, 63, 'José Félix Ribas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (200, 63, 'Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (201, 64, 'Libertad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (202, 64, 'Dolores');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (203, 64, 'Santa Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (204, 64, 'Palacio Fajardo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (205, 65, 'Ciudad de Nutrias');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (206, 65, 'El Regalo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (207, 65, 'Puerto Nutrias');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (208, 65, 'Santa Catalina');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (209, 66, 'Cachamay');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (210, 66, 'Chirica');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (211, 66, 'Dalla Costa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (212, 66, 'Once de Abril');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (213, 66, 'Simón Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (214, 66, 'Unare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (215, 66, 'Universidad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (216, 66, 'Vista al Sol');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (217, 66, 'Pozo Verde');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (218, 66, 'Yocoima');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (219, 66, '5 de Julio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (220, 67, 'Cedeño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (221, 67, 'Altagracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (222, 67, 'Ascensión Farreras');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (223, 67, 'Guaniamo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (224, 67, 'La Urbana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (225, 67, 'Pijiguaos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (226, 68, 'El Callao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (227, 69, 'Gran Sabana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (228, 69, 'Ikabarú');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (229, 70, 'Catedral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (230, 70, 'Zea');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (231, 70, 'Orinoco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (232, 70, 'José Antonio Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (233, 70, 'Marhuanta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (234, 70, 'Agua Salada');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (235, 70, 'Vista Hermosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (236, 70, 'La Sabanita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (237, 70, 'Panapana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (238, 71, 'Andrés Eloy Blanco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (239, 71, 'Pedro Cova');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (240, 72, 'Raúl Leoni');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (241, 72, 'Barceloneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (242, 72, 'Santa Bárbara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (243, 72, 'San Francisco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (244, 73, 'Roscio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (245, 73, 'Salóm');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (246, 74, 'Sifontes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (247, 74, 'Dalla Costa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (248, 74, 'San Isidro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (249, 75, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (250, 75, 'Aripao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (251, 75, 'Guarataro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (252, 75, 'Las Majadas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (253, 75, 'Moitaco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (254, 76, 'Padre Pedro Chien');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (255, 76, 'Río Grande');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (256, 77, 'Bejuma');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (257, 77, 'Canoabo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (258, 77, 'Simón Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (259, 78, 'Güigüe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (260, 78, 'Carabobo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (261, 78, 'Tacarigua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (262, 79, 'Mariara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (263, 79, 'Aguas Calientes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (264, 80, 'Ciudad Alianza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (265, 80, 'Guacara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (266, 80, 'Yagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (267, 81, 'Morón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (268, 81, 'Yagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (269, 82, 'Tocuyito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (270, 82, 'Independencia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (271, 83, 'Los Guayos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (272, 84, 'Miranda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (273, 85, 'Montalbán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (274, 86, 'Naguanagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (275, 87, 'Bartolomé Salóm');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (276, 87, 'Democracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (277, 87, 'Fraternidad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (278, 87, 'Goaigoaza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (279, 87, 'Juan José Flores');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (280, 87, 'Unión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (281, 87, 'Borburata');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (282, 87, 'Patanemo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (283, 88, 'San Diego');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (284, 89, 'San Joaquín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (285, 90, 'Candelaria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (286, 90, 'Catedral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (287, 90, 'El Socorro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (288, 90, 'Miguel Peña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (289, 90, 'Rafael Urdaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (290, 90, 'San Blas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (291, 90, 'San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (292, 90, 'Santa Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (293, 90, 'Negro Primero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (294, 91, 'Cojedes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (295, 91, 'Juan de Mata Suárez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (296, 92, 'Tinaquillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (297, 93, 'El Baúl');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (298, 93, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (299, 94, 'La Aguadita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (300, 94, 'Macapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (301, 95, 'El Pao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (302, 96, 'El Amparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (303, 96, 'Libertad de Cojedes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (304, 97, 'Rómulo Gallegos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (305, 98, 'San Carlos de Austria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (306, 98, 'Juan Ángel Bravo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (307, 98, 'Manuel Manrique');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (308, 99, 'General en Jefe José Laurencio Silva');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (309, 100, 'Curiapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (310, 100, 'Almirante Luis Brión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (311, 100, 'Francisco Aniceto Lugo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (312, 100, 'Manuel Renaud');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (313, 100, 'Padre Barral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (314, 100, 'Santos de Abelgas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (315, 101, 'Imataca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (316, 101, 'Cinco de Julio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (317, 101, 'Juan Bautista Arismendi');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (318, 101, 'Manuel Piar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (319, 101, 'Rómulo Gallegos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (320, 102, 'Pedernales');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (321, 102, 'Luis Beltrán Prieto Figueroa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (322, 103, 'San José (Delta Amacuro)');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (323, 103, 'José Vidal Marcano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (324, 103, 'Juan Millán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (325, 103, 'Leonardo Ruíz Pineda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (326, 103, 'Mariscal Antonio José de Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (327, 103, 'Monseñor Argimiro García');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (328, 103, 'San Rafael (Delta Amacuro)');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (329, 103, 'Virgen del Valle');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (330, 10, 'Clarines');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (331, 10, 'Guanape');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (332, 10, 'Sabana de Uchire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (333, 104, 'Capadare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (334, 104, 'La Pastora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (335, 104, 'Libertador');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (336, 104, 'San Juan de los Cayos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (337, 105, 'Aracua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (338, 105, 'La Peña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (339, 105, 'San Luis');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (340, 106, 'Bariro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (341, 106, 'Borojó');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (342, 106, 'Capatárida');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (343, 106, 'Guajiro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (344, 106, 'Seque');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (345, 106, 'Zazárida');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (346, 106, 'Valle de Eroa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (347, 107, 'Cacique Manaure');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (348, 108, 'Norte');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (349, 108, 'Carirubana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (350, 108, 'Santa Ana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (351, 108, 'Urbana Punta Cardón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (352, 109, 'La Vela de Coro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (353, 109, 'Acurigua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (354, 109, 'Guaibacoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (355, 109, 'Las Calderas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (356, 109, 'Macoruca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (357, 110, 'Dabajuro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (358, 111, 'Agua Clara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (359, 111, 'Avaria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (360, 111, 'Pedregal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (361, 111, 'Piedra Grande');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (362, 111, 'Purureche');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (363, 112, 'Adaure');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (364, 112, 'Adícora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (365, 112, 'Baraived');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (366, 112, 'Buena Vista');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (367, 112, 'Jadacaquiva');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (368, 112, 'El Vínculo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (369, 112, 'El Hato');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (370, 112, 'Moruy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (371, 112, 'Pueblo Nuevo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (372, 113, 'Agua Larga');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (373, 113, 'El Paují');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (374, 113, 'Independencia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (375, 113, 'Mapararí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (376, 114, 'Agua Linda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (377, 114, 'Araurima');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (378, 114, 'Jacura');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (379, 115, 'Tucacas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (380, 115, 'Boca de Aroa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (381, 116, 'Los Taques');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (382, 116, 'Judibana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (383, 117, 'Mene de Mauroa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (384, 117, 'San Félix');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (385, 117, 'Casigua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (386, 118, 'Guzmán Guillermo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (387, 118, 'Mitare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (388, 118, 'Río Seco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (389, 118, 'Sabaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (390, 118, 'San Antonio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (391, 118, 'San Gabriel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (392, 118, 'Santa Ana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (393, 119, 'Boca del Tocuyo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (394, 119, 'Chichiriviche');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (395, 119, 'Tocuyo de la Costa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (396, 120, 'Palmasola');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (397, 121, 'Cabure');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (398, 121, 'Colina');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (399, 121, 'Curimagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (400, 122, 'San José de la Costa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (401, 122, 'Píritu');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (402, 123, 'San Francisco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (403, 124, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (404, 124, 'Pecaya');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (405, 125, 'Tocópero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (406, 126, 'El Charal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (407, 126, 'Las Vegas del Tuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (408, 126, 'Santa Cruz de Bucaral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (409, 127, 'Bruzual');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (410, 127, 'Urumaco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (411, 128, 'Puerto Cumarebo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (412, 128, 'La Ciénaga');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (413, 128, 'La Soledad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (414, 128, 'Pueblo Cumarebo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (415, 128, 'Zazárida');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (416, 113, 'Churuguara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (417, 129, 'Camaguán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (418, 129, 'Puerto Miranda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (419, 129, 'Uverito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (420, 130, 'Chaguaramas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (421, 131, 'El Socorro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (422, 132, 'Tucupido');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (423, 132, 'San Rafael de Laya');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (424, 133, 'Altagracia de Orituco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (425, 133, 'San Rafael de Orituco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (426, 133, 'San Francisco Javier de Lezama');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (427, 133, 'Paso Real de Macaira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (428, 133, 'Carlos Soublette');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (429, 133, 'San Francisco de Macaira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (430, 133, 'Libertad de Orituco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (431, 134, 'Cantaclaro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (432, 134, 'San Juan de los Morros');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (433, 134, 'Parapara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (434, 135, 'El Sombrero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (435, 135, 'Sosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (436, 136, 'Las Mercedes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (437, 136, 'Cabruta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (438, 136, 'Santa Rita de Manapire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (439, 137, 'Valle de la Pascua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (440, 137, 'Espino');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (441, 138, 'San José de Unare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (442, 138, 'Zaraza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (443, 139, 'San José de Tiznados');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (444, 139, 'San Francisco de Tiznados');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (445, 139, 'San Lorenzo de Tiznados');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (446, 139, 'Ortiz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (447, 140, 'Guayabal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (448, 140, 'Cazorla');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (449, 141, 'San José de Guaribe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (450, 141, 'Uveral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (451, 142, 'Santa María de Ipire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (452, 142, 'Altamira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (453, 143, 'El Calvario');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (454, 143, 'El Rastro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (455, 143, 'Guardatinajas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (456, 143, 'Capital Urbana Calabozo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (457, 144, 'Quebrada Honda de Guache');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (458, 144, 'Pío Tamayo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (459, 144, 'Yacambú');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (460, 145, 'Fréitez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (461, 145, 'José María Blanco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (462, 146, 'Catedral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (463, 146, 'Concepción');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (464, 146, 'El Cují');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (465, 146, 'Juan de Villegas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (466, 146, 'Santa Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (467, 146, 'Tamaca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (468, 146, 'Unión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (469, 146, 'Aguedo Felipe Alvarado');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (470, 146, 'Buena Vista');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (471, 146, 'Juárez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (472, 147, 'Juan Bautista Rodríguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (473, 147, 'Cuara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (474, 147, 'Diego de Lozada');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (475, 147, 'Paraíso de San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (476, 147, 'San Miguel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (477, 147, 'Tintorero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (478, 147, 'José Bernardo Dorante');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (479, 147, 'Coronel Mariano Peraza ');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (480, 148, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (481, 148, 'Anzoátegui');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (482, 148, 'Guarico');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (483, 148, 'Hilario Luna y Luna');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (484, 148, 'Humocaro Alto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (485, 148, 'Humocaro Bajo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (486, 148, 'La Candelaria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (487, 148, 'Morán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (488, 149, 'Cabudare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (489, 149, 'José Gregorio Bastidas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (490, 149, 'Agua Viva');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (491, 150, 'Sarare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (492, 150, 'Buría');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (493, 150, 'Gustavo Vegas León');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (494, 151, 'Trinidad Samuel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (495, 151, 'Antonio Díaz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (496, 151, 'Camacaro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (497, 151, 'Castañeda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (498, 151, 'Cecilio Zubillaga');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (499, 151, 'Chiquinquirá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (500, 151, 'El Blanco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (501, 151, 'Espinoza de los Monteros');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (502, 151, 'Lara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (503, 151, 'Las Mercedes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (504, 151, 'Manuel Morillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (505, 151, 'Montaña Verde');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (506, 151, 'Montes de Oca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (507, 151, 'Torres');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (508, 151, 'Heriberto Arroyo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (509, 151, 'Reyes Vargas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (510, 151, 'Altagracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (511, 152, 'Siquisique');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (512, 152, 'Moroturo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (513, 152, 'San Miguel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (514, 152, 'Xaguas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (515, 179, 'Presidente Betancourt');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (516, 179, 'Presidente Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (517, 179, 'Presidente Rómulo Gallegos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (518, 179, 'Gabriel Picón González');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (519, 179, 'Héctor Amable Mora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (520, 179, 'José Nucete Sardi');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (521, 179, 'Pulido Méndez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (522, 180, 'La Azulita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (523, 181, 'Santa Cruz de Mora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (524, 181, 'Mesa Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (525, 181, 'Mesa de Las Palmas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (526, 182, 'Aricagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (527, 182, 'San Antonio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (528, 183, 'Canagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (529, 183, 'Capurí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (530, 183, 'Chacantá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (531, 183, 'El Molino');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (532, 183, 'Guaimaral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (533, 183, 'Mucutuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (534, 183, 'Mucuchachí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (535, 184, 'Fernández Peña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (536, 184, 'Matriz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (537, 184, 'Montalbán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (538, 184, 'Acequias');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (539, 184, 'Jají');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (540, 184, 'La Mesa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (541, 184, 'San José del Sur');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (542, 185, 'Tucaní');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (543, 185, 'Florencio Ramírez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (544, 186, 'Santo Domingo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (545, 186, 'Las Piedras');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (546, 187, 'Guaraque');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (547, 187, 'Mesa de Quintero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (548, 187, 'Río Negro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (549, 188, 'Arapuey');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (550, 188, 'Palmira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (551, 189, 'San Cristóbal de Torondoy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (552, 189, 'Torondoy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (553, 190, 'Antonio Spinetti Dini');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (554, 190, 'Arias');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (555, 190, 'Caracciolo Parra Pérez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (556, 190, 'Domingo Peña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (557, 190, 'El Llano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (558, 190, 'Gonzalo Picón Febres');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (559, 190, 'Jacinto Plaza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (560, 190, 'Juan Rodríguez Suárez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (561, 190, 'Lasso de la Vega');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (562, 190, 'Mariano Picón Salas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (563, 190, 'Milla');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (564, 190, 'Osuna Rodríguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (565, 190, 'Sagrario');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (566, 190, 'El Morro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (567, 190, 'Los Nevados');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (568, 191, 'Andrés Eloy Blanco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (569, 191, 'La Venta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (570, 191, 'Piñango');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (571, 191, 'Timotes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (572, 192, 'Eloy Paredes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (573, 192, 'San Rafael de Alcázar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (574, 192, 'Santa Elena de Arenales');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (575, 193, 'Santa María de Caparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (576, 194, 'Pueblo Llano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (577, 195, 'Cacute');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (578, 195, 'La Toma');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (579, 195, 'Mucuchíes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (580, 195, 'Mucurubá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (581, 195, 'San Rafael');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (582, 196, 'Gerónimo Maldonado');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (583, 196, 'Bailadores');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (584, 197, 'Tabay');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (585, 198, 'Chiguará');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (586, 198, 'Estánquez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (587, 198, 'Lagunillas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (588, 198, 'La Trampa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (589, 198, 'Pueblo Nuevo del Sur');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (590, 198, 'San Juan');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (591, 199, 'El Amparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (592, 199, 'El Llano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (593, 199, 'San Francisco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (594, 199, 'Tovar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (595, 200, 'Independencia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (596, 200, 'María de la Concepción Palacios Blanco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (597, 200, 'Nueva Bolivia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (598, 200, 'Santa Apolonia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (599, 201, 'Caño El Tigre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (600, 201, 'Zea');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (601, 223, 'Aragüita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (602, 223, 'Arévalo González');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (603, 223, 'Capaya');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (604, 223, 'Caucagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (605, 223, 'Panaquire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (606, 223, 'Ribas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (607, 223, 'El Café');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (608, 223, 'Marizapa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (609, 224, 'Cumbo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (610, 224, 'San José de Barlovento');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (611, 225, 'El Cafetal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (612, 225, 'Las Minas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (613, 225, 'Nuestra Señora del Rosario');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (614, 226, 'Higuerote');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (615, 226, 'Curiepe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (616, 226, 'Tacarigua de Brión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (617, 227, 'Mamporal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (618, 228, 'Carrizal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (619, 229, 'Chacao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (620, 230, 'Charallave');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (621, 230, 'Las Brisas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (622, 231, 'El Hatillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (623, 232, 'Altagracia de la Montaña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (624, 232, 'Cecilio Acosta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (625, 232, 'Los Teques');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (626, 232, 'El Jarillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (627, 232, 'San Pedro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (628, 232, 'Tácata');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (629, 232, 'Paracotos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (630, 233, 'Cartanal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (631, 233, 'Santa Teresa del Tuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (632, 234, 'La Democracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (633, 234, 'Ocumare del Tuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (634, 234, 'Santa Bárbara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (635, 235, 'San Antonio de los Altos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (636, 236, 'Río Chico');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (637, 236, 'El Guapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (638, 236, 'Tacarigua de la Laguna');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (639, 236, 'Paparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (640, 236, 'San Fernando del Guapo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (641, 237, 'Santa Lucía del Tuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (642, 238, 'Cúpira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (643, 238, 'Machurucuto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (644, 239, 'Guarenas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (645, 240, 'San Antonio de Yare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (646, 240, 'San Francisco de Yare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (647, 241, 'Leoncio Martínez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (648, 241, 'Petare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (649, 241, 'Caucagüita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (650, 241, 'Filas de Mariche');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (651, 241, 'La Dolorita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (652, 242, 'Cúa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (653, 242, 'Nueva Cúa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (654, 243, 'Guatire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (655, 243, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (656, 258, 'San Antonio de Maturín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (657, 258, 'San Francisco de Maturín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (658, 259, 'Aguasay');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (659, 260, 'Caripito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (660, 261, 'El Guácharo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (661, 261, 'La Guanota');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (662, 261, 'Sabana de Piedra');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (663, 261, 'San Agustín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (664, 261, 'Teresen');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (665, 261, 'Caripe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (666, 262, 'Areo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (667, 262, 'Capital Cedeño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (668, 262, 'San Félix de Cantalicio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (669, 262, 'Viento Fresco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (670, 263, 'El Tejero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (671, 263, 'Punta de Mata');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (672, 264, 'Chaguaramas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (673, 264, 'Las Alhuacas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (674, 264, 'Tabasca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (675, 264, 'Temblador');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (676, 265, 'Alto de los Godos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (677, 265, 'Boquerón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (678, 265, 'Las Cocuizas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (679, 265, 'La Cruz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (680, 265, 'San Simón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (681, 265, 'El Corozo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (682, 265, 'El Furrial');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (683, 265, 'Jusepín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (684, 265, 'La Pica');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (685, 265, 'San Vicente');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (686, 266, 'Aparicio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (687, 266, 'Aragua de Maturín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (688, 266, 'Chaguamal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (689, 266, 'El Pinto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (690, 266, 'Guanaguana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (691, 266, 'La Toscana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (692, 266, 'Taguaya');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (693, 267, 'Cachipo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (694, 267, 'Quiriquire');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (695, 268, 'Santa Bárbara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (696, 269, 'Barrancas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (697, 269, 'Los Barrancos de Fajardo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (698, 270, 'Uracoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (699, 271, 'Antolín del Campo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (700, 272, 'Arismendi');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (701, 273, 'García');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (702, 273, 'Francisco Fajardo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (703, 274, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (704, 274, 'Guevara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (705, 274, 'Matasiete');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (706, 274, 'Santa Ana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (707, 274, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (708, 275, 'Aguirre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (709, 275, 'Maneiro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (710, 276, 'Adrián');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (711, 276, 'Juan Griego');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (712, 276, 'Yaguaraparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (713, 277, 'Porlamar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (714, 278, 'San Francisco de Macanao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (715, 278, 'Boca de Río');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (716, 279, 'Tubores');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (717, 279, 'Los Baleales');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (718, 280, 'Vicente Fuentes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (719, 280, 'Villalba');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (720, 281, 'San Juan Bautista');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (721, 281, 'Zabala');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (722, 283, 'Capital Araure');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (723, 283, 'Río Acarigua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (724, 284, 'Capital Esteller');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (725, 284, 'Uveral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (726, 285, 'Guanare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (727, 285, 'Córdoba');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (728, 285, 'San José de la Montaña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (729, 285, 'San Juan de Guanaguanare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (730, 285, 'Virgen de la Coromoto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (731, 286, 'Guanarito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (732, 286, 'Trinidad de la Capilla');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (733, 286, 'Divina Pastora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (734, 287, 'Monseñor José Vicente de Unda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (735, 287, 'Peña Blanca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (736, 288, 'Capital Ospino');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (737, 288, 'Aparición');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (738, 288, 'La Estación');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (739, 289, 'Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (740, 289, 'Payara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (741, 289, 'Pimpinela');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (742, 289, 'Ramón Peraza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (743, 290, 'Papelón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (744, 290, 'Caño Delgadito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (745, 291, 'San Genaro de Boconoito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (746, 291, 'Antolín Tovar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (747, 292, 'San Rafael de Onoto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (748, 292, 'Santa Fe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (749, 292, 'Thermo Morles');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (750, 293, 'Santa Rosalía');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (751, 293, 'Florida');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (752, 294, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (753, 294, 'Concepción');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (754, 294, 'San Rafael de Palo Alzado');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (755, 294, 'Uvencio Antonio Velásquez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (756, 294, 'San José de Saguaz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (757, 294, 'Villa Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (758, 295, 'Turén');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (759, 295, 'Canelones');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (760, 295, 'Santa Cruz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (761, 295, 'San Isidro Labrador');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (762, 296, 'Mariño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (763, 296, 'Rómulo Gallegos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (764, 297, 'San José de Aerocuar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (765, 297, 'Tavera Acosta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (766, 298, 'Río Caribe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (767, 298, 'Antonio José de Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (768, 298, 'El Morro de Puerto Santo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (769, 298, 'Puerto Santo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (770, 298, 'San Juan de las Galdonas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (771, 299, 'El Pilar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (772, 299, 'El Rincón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (773, 299, 'General Francisco Antonio Váquez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (774, 299, 'Guaraúnos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (775, 299, 'Tunapuicito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (776, 299, 'Unión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (777, 300, 'Santa Catalina');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (778, 300, 'Santa Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (779, 300, 'Santa Teresa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (780, 300, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (781, 300, 'Maracapana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (782, 302, 'Libertad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (783, 302, 'El Paujil');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (784, 302, 'Yaguaraparo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (785, 303, 'Cruz Salmerón Acosta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (786, 303, 'Chacopata');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (787, 303, 'Manicuare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (788, 304, 'Tunapuy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (789, 304, 'Campo Elías');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (790, 305, 'Irapa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (791, 305, 'Campo Claro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (792, 305, 'Maraval');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (793, 305, 'San Antonio de Irapa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (794, 305, 'Soro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (795, 306, 'Mejía');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (796, 307, 'Cumanacoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (797, 307, 'Arenas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (798, 307, 'Aricagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (799, 307, 'Cogollar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (800, 307, 'San Fernando');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (801, 307, 'San Lorenzo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (802, 308, 'Villa Frontado (Muelle de Cariaco)');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (803, 308, 'Catuaro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (804, 308, 'Rendón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (805, 308, 'San Cruz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (806, 308, 'Santa María');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (807, 309, 'Altagracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (808, 309, 'Santa Inés');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (809, 309, 'Valentín Valiente');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (810, 309, 'Ayacucho');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (811, 309, 'San Juan');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (812, 309, 'Raúl Leoni');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (813, 309, 'Gran Mariscal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (814, 310, 'Cristóbal Colón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (815, 310, 'Bideau');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (816, 310, 'Punta de Piedras');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (817, 310, 'Güiria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (818, 341, 'Andrés Bello');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (819, 342, 'Antonio Rómulo Costa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (820, 343, 'Ayacucho');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (821, 343, 'Rivas Berti');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (822, 343, 'San Pedro del Río');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (823, 344, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (824, 344, 'Palotal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (825, 344, 'General Juan Vicente Gómez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (826, 344, 'Isaías Medina Angarita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (827, 345, 'Cárdenas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (828, 345, 'Amenodoro Ángel Lamus');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (829, 345, 'La Florida');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (830, 346, 'Córdoba');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (831, 347, 'Fernández Feo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (832, 347, 'Alberto Adriani');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (833, 347, 'Santo Domingo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (834, 348, 'Francisco de Miranda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (835, 349, 'García de Hevia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (836, 349, 'Boca de Grita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (837, 349, 'José Antonio Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (838, 350, 'Guásimos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (839, 351, 'Independencia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (840, 351, 'Juan Germán Roscio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (841, 351, 'Román Cárdenas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (842, 352, 'Jáuregui');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (843, 352, 'Emilio Constantino Guerrero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (844, 352, 'Monseñor Miguel Antonio Salas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (845, 353, 'José María Vargas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (846, 354, 'Junín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (847, 354, 'La Petrólea');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (848, 354, 'Quinimarí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (849, 354, 'Bramón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (850, 355, 'Libertad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (851, 355, 'Cipriano Castro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (852, 355, 'Manuel Felipe Rugeles');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (853, 356, 'Libertador');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (854, 356, 'Doradas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (855, 356, 'Emeterio Ochoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (856, 356, 'San Joaquín de Navay');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (857, 357, 'Lobatera');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (858, 357, 'Constitución');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (859, 358, 'Michelena');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (860, 359, 'Panamericano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (861, 359, 'La Palmita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (862, 360, 'Pedro María Ureña');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (863, 360, 'Nueva Arcadia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (864, 361, 'Delicias');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (865, 361, 'Pecaya');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (866, 362, 'Samuel Darío Maldonado');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (867, 362, 'Boconó');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (868, 362, 'Hernández');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (869, 363, 'La Concordia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (870, 363, 'San Juan Bautista');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (871, 363, 'Pedro María Morantes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (872, 363, 'San Sebastián');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (873, 363, 'Dr. Francisco Romero Lobo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (874, 364, 'Seboruco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (875, 365, 'Simón Rodríguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (876, 366, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (877, 366, 'Eleazar López Contreras');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (878, 366, 'San Pablo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (879, 367, 'Torbes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (880, 368, 'Uribante');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (881, 368, 'Cárdenas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (882, 368, 'Juan Pablo Peñalosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (883, 368, 'Potosí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (884, 369, 'San Judas Tadeo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (885, 370, 'Araguaney');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (886, 370, 'El Jaguito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (887, 370, 'La Esperanza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (888, 370, 'Santa Isabel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (889, 371, 'Boconó');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (890, 371, 'El Carmen');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (891, 371, 'Mosquey');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (892, 371, 'Ayacucho');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (893, 371, 'Burbusay');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (894, 371, 'General Ribas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (895, 371, 'Guaramacal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (896, 371, 'Vega de Guaramacal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (897, 371, 'Monseñor Jáuregui');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (898, 371, 'Rafael Rangel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (899, 371, 'San Miguel');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (900, 371, 'San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (901, 372, 'Sabana Grande');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (902, 372, 'Cheregüé');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (903, 372, 'Granados');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (904, 373, 'Arnoldo Gabaldón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (905, 373, 'Bolivia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (906, 373, 'Carrillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (907, 373, 'Cegarra');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (908, 373, 'Chejendé');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (909, 373, 'Manuel Salvador Ulloa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (910, 373, 'San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (911, 374, 'Carache');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (912, 374, 'La Concepción');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (913, 374, 'Cuicas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (914, 374, 'Panamericana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (915, 374, 'Santa Cruz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (916, 375, 'Escuque');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (917, 375, 'La Unión');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (918, 375, 'Santa Rita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (919, 375, 'Sabana Libre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (920, 376, 'El Socorro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (921, 376, 'Los Caprichos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (922, 376, 'Antonio José de Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (923, 377, 'Campo Elías');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (924, 377, 'Arnoldo Gabaldón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (925, 378, 'Santa Apolonia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (926, 378, 'El Progreso');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (927, 378, 'La Ceiba');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (928, 378, 'Tres de Febrero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (929, 379, 'El Dividive');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (930, 379, 'Agua Santa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (931, 379, 'Agua Caliente');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (932, 379, 'El Cenizo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (933, 379, 'Valerita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (934, 380, 'Monte Carmelo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (935, 380, 'Buena Vista');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (936, 380, 'Santa María del Horcón');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (937, 381, 'Motatán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (938, 381, 'El Baño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (939, 381, 'Jalisco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (940, 382, 'Pampán');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (941, 382, 'Flor de Patria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (942, 382, 'La Paz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (943, 382, 'Santa Ana');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (944, 383, 'Pampanito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (945, 383, 'La Concepción');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (946, 383, 'Pampanito II');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (947, 384, 'Betijoque');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (948, 384, 'José Gregorio Hernández');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (949, 384, 'La Pueblita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (950, 384, 'Los Cedros');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (951, 385, 'Carvajal');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (952, 385, 'Campo Alegre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (953, 385, 'Antonio Nicolás Briceño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (954, 385, 'José Leonardo Suárez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (955, 386, 'Sabana de Mendoza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (956, 386, 'Junín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (957, 386, 'Valmore Rodríguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (958, 386, 'El Paraíso');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (959, 387, 'Andrés Linares');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (960, 387, 'Chiquinquirá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (961, 387, 'Cristóbal Mendoza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (962, 387, 'Cruz Carrillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (963, 387, 'Matriz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (964, 387, 'Monseñor Carrillo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (965, 387, 'Tres Esquinas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (966, 388, 'Cabimbú');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (967, 388, 'Jajó');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (968, 388, 'La Mesa de Esnujaque');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (969, 388, 'Santiago');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (970, 388, 'Tuñame');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (971, 388, 'La Quebrada');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (972, 389, 'Juan Ignacio Montilla');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (973, 389, 'La Beatriz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (974, 389, 'La Puerta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (975, 389, 'Mendoza del Valle de Momboy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (976, 389, 'Mercedes Díaz');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (977, 389, 'San Luis');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (978, 390, 'Caraballeda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (979, 390, 'Carayaca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (980, 390, 'Carlos Soublette');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (981, 390, 'Caruao Chuspa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (982, 390, 'Catia La Mar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (983, 390, 'El Junko');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (984, 390, 'La Guaira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (985, 390, 'Macuto');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (986, 390, 'Maiquetía');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (987, 390, 'Naiguatá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (988, 390, 'Urimare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (989, 391, 'Arístides Bastidas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (990, 392, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (991, 407, 'Chivacoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (992, 407, 'Campo Elías');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (993, 408, 'Cocorote');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (994, 409, 'Independencia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (995, 410, 'José Antonio Páez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (996, 411, 'La Trinidad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (997, 412, 'Manuel Monge');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (998, 413, 'Salóm');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (999, 413, 'Temerla');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1000, 413, 'Nirgua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1001, 414, 'San Andrés');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1002, 414, 'Yaritagua');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1003, 415, 'San Javier');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1004, 415, 'Albarico');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1005, 415, 'San Felipe');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1006, 416, 'Sucre');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1007, 417, 'Urachiche');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1008, 418, 'El Guayabo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1009, 418, 'Farriar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1010, 441, 'Isla de Toas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1011, 441, 'Monagas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1012, 442, 'San Timoteo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1013, 442, 'General Urdaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1014, 442, 'Libertador');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1015, 442, 'Marcelino Briceño');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1016, 442, 'Pueblo Nuevo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1017, 442, 'Manuel Guanipa Matos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1018, 443, 'Ambrosio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1019, 443, 'Carmen Herrera');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1020, 443, 'La Rosa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1021, 443, 'Germán Ríos Linares');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1022, 443, 'San Benito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1023, 443, 'Rómulo Betancourt');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1024, 443, 'Jorge Hernández');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1025, 443, 'Punta Gorda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1026, 443, 'Arístides Calvani');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1027, 444, 'Encontrados');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1028, 444, 'Udón Pérez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1029, 445, 'Moralito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1030, 445, 'San Carlos del Zulia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1031, 445, 'Santa Cruz del Zulia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1032, 445, 'Santa Bárbara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1033, 445, 'Urribarrí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1034, 446, 'Carlos Quevedo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1035, 446, 'Francisco Javier Pulgar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1036, 446, 'Simón Rodríguez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1037, 446, 'Guamo-Gavilanes');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1038, 448, 'La Concepción');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1039, 448, 'San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1040, 448, 'Mariano Parra León');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1041, 448, 'José Ramón Yépez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1042, 449, 'Jesús María Semprún');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1043, 449, 'Barí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1044, 450, 'Concepción');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1045, 450, 'Andrés Bello');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1046, 450, 'Chiquinquirá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1047, 450, 'El Carmelo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1048, 450, 'Potreritos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1049, 451, 'Libertad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1050, 451, 'Alonso de Ojeda');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1051, 451, 'Venezuela');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1052, 451, 'Eleazar López Contreras');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1053, 451, 'Campo Lara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1054, 452, 'Bartolomé de las Casas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1055, 452, 'Libertad');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1056, 452, 'Río Negro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1057, 452, 'San José de Perijá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1058, 453, 'San Rafael');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1059, 453, 'La Sierrita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1060, 453, 'Las Parcelas');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1061, 453, 'Luis de Vicente');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1062, 453, 'Monseñor Marcos Sergio Godoy');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1063, 453, 'Ricaurte');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1064, 453, 'Tamare');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1065, 454, 'Antonio Borjas Romero');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1066, 454, 'Bolívar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1067, 454, 'Cacique Mara');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1068, 454, 'Carracciolo Parra Pérez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1069, 454, 'Cecilio Acosta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1070, 454, 'Cristo de Aranza');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1071, 454, 'Coquivacoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1072, 454, 'Chiquinquirá');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1073, 454, 'Francisco Eugenio Bustamante');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1074, 454, 'Idelfonzo Vásquez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1075, 454, 'Juana de Ávila');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1076, 454, 'Luis Hurtado Higuera');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1077, 454, 'Manuel Dagnino');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1078, 454, 'Olegario Villalobos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1079, 454, 'Raúl Leoni');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1080, 454, 'Santa Lucía');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1081, 454, 'Venancio Pulgar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1082, 454, 'San Isidro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1083, 455, 'Altagracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1084, 455, 'Faría');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1085, 455, 'Ana María Campos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1086, 455, 'San Antonio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1087, 455, 'San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1088, 456, 'Donaldo García');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1089, 456, 'El Rosario');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1090, 456, 'Sixto Zambrano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1091, 457, 'San Francisco');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1092, 457, 'El Bajo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1093, 457, 'Domitila Flores');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1094, 457, 'Francisco Ochoa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1095, 457, 'Los Cortijos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1096, 457, 'Marcial Hernández');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1097, 458, 'Santa Rita');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1098, 458, 'El Mene');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1099, 458, 'Pedro Lucas Urribarrí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1100, 458, 'José Cenobio Urribarrí');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1101, 459, 'Rafael Maria Baralt');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1102, 459, 'Manuel Manrique');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1103, 459, 'Rafael Urdaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1104, 460, 'Bobures');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1105, 460, 'Gibraltar');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1106, 460, 'Heras');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1107, 460, 'Monseñor Arturo Álvarez');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1108, 460, 'Rómulo Gallegos');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1109, 460, 'El Batey');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1110, 461, 'Rafael Urdaneta');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1111, 461, 'La Victoria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1112, 461, 'Raúl Cuenca');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1113, 447, 'Sinamaica');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1114, 447, 'Alta Guajira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1115, 447, 'Elías Sánchez Rubio');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1116, 447, 'Guajira');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1117, 462, 'Altagracia');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1118, 462, 'Antímano');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1119, 462, 'Caricuao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1120, 462, 'Catedral');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1121, 462, 'Coche');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1122, 462, 'El Junquito');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1123, 462, 'El Paraíso');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1124, 462, 'El Recreo');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1125, 462, 'El Valle');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1126, 462, 'La Candelaria');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1127, 462, 'La Pastora');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1128, 462, 'La Vega');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1129, 462, 'Macarao');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1130, 462, 'San Agustín');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1131, 462, 'San Bernardino');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1132, 462, 'San José');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1133, 462, 'San Juan');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1134, 462, 'San Pedro');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1135, 462, 'Santa Rosalía');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1136, 462, 'Santa Teresa');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1137, 462, 'Sucre (Catia)');
INSERT INTO parroquia (id, municipio_id, nombre) VALUES (1138, 462, '23 de enero');


--
-- Data for Name: personal; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10749497, 'V', 'RAFAEL', 'ALBERTO', 'SANCHEZ', 'MONTILLA', 'M', 'D', '1974-11-08', NULL, 'V-10749497-5', NULL, NULL, '10749497.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3795747, 'V', 'BETTY', '', 'QUIÑONEZ', 'DE CARDONA', 'F', 'C', '1946-06-26', NULL, NULL, NULL, NULL, '3795747.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3196551, 'V', 'JOSE', 'ANDRES', 'ROSALES', 'ZAMUDIO', 'M', 'C', '1945-11-19', NULL, NULL, NULL, NULL, '3196551.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3313556, 'V', 'RAMON', '', 'URIBE', 'DIAZ', 'M', 'C', '1953-03-10', NULL, NULL, NULL, NULL, '3313556.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3467743, 'V', 'JOSE', '', 'OSORIO', 'GARCIAHERNANDO', 'M', 'C', '1952-08-15', NULL, NULL, NULL, NULL, '3467743.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3622075, 'V', 'JOSE', 'EMILIO', 'DE ', 'LEONOSORIO', 'M', 'C', '1952-07-23', NULL, NULL, NULL, NULL, '3622075.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3622778, 'V', 'MARIA', 'ELENA', 'FERNANDEZ', 'GARCIA', 'F', 'S', '1953-02-13', NULL, NULL, NULL, NULL, '3622778.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3723633, 'V', 'ALI', 'FILIBERTO', 'CASTEJON', 'ARAQUE', 'M', 'C', '1952-11-14', NULL, NULL, NULL, NULL, '3723633.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3789237, 'V', 'CESAR', 'DE ', 'PEREZ', 'RAMIREZ', 'M', 'C', '1948-12-01', NULL, NULL, NULL, NULL, '3789237.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3791240, 'V', 'SULLY', 'O.', 'CHACON', 'DE GUARDIA', 'F', 'C', '1949-11-05', NULL, NULL, NULL, NULL, '3791240.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3794368, 'V', 'FANNY', 'E.', 'JAIMES', 'ANTOLINEZ', 'F', 'C', '1953-02-14', NULL, NULL, NULL, NULL, '3794368.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3794866, 'V', 'CARMEN', 'ALICIA', 'MORA', 'ACEVEDO', 'F', 'C', '1952-05-31', NULL, NULL, NULL, NULL, '3794866.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3795309, 'V', 'ISABEL', '', 'CARRERO', 'MURILLO', 'F', 'C', '1953-11-19', NULL, NULL, NULL, NULL, '3795309.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3795914, 'V', 'ANA', 'GRISELDA', 'CASTELLANOS', '', 'F', 'S', '1952-11-20', NULL, NULL, NULL, NULL, '3795914.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3916517, 'V', 'JESUS', 'A.', 'CASTILLO', 'HEREDIA', 'M', 'C', '1947-12-18', NULL, NULL, NULL, NULL, '3916517.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3997860, 'V', 'GIL', 'ANTONIO', 'USECHE', 'USECHE', 'M', 'D', '1950-09-01', NULL, NULL, NULL, NULL, '3997860.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3998558, 'V', 'ALBA', 'MARIA', 'PARRA', 'DE SANCHEZ', 'F', 'C', '1948-11-20', NULL, NULL, NULL, NULL, '3998558.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3999232, 'V', 'BLANCA', '', 'MENDEZ', 'DE CASTILLO', 'F', 'C', '1952-05-31', NULL, NULL, NULL, NULL, '3999232.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3999334, 'V', 'LEOPOLDINA', '', 'ROJAS', 'MARTINEZ', 'F', 'S', '1954-04-27', NULL, NULL, NULL, NULL, '3999334.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4000117, 'V', 'IDA', 'CECILIA', 'BECERRA', '', 'F', 'C', '1956-12-29', NULL, NULL, NULL, NULL, '4000117.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4000865, 'V', 'ALIX', 'C.', 'HERNANDEZ', 'SANCHEZ', 'F', 'C', '1952-10-20', NULL, NULL, NULL, NULL, '4000865.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4001028, 'V', 'LADY', 'E.', 'SALCEDO', 'DE NARVAEZ', 'F', 'C', '1950-05-05', NULL, NULL, NULL, NULL, '4001028.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4001588, 'V', 'ZULLY', 'CLARITZA', 'VALECILLOS', 'PAZ', 'F', 'C', '1954-02-12', NULL, NULL, NULL, NULL, '4001588.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4001953, 'V', 'NANCY', 'CELINA', 'HINOJOSA', 'PEREZ', 'F', '', '1950-04-07', NULL, NULL, NULL, NULL, '4001953.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4094271, 'V', 'JOSE', 'GONZALO', 'MARQUEZ', 'CONTRERAS', 'M', 'C', '1956-07-30', NULL, NULL, NULL, NULL, '4094271.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4113764, 'V', 'EDDYLUZ', 'C.', 'GONZALEZ', 'GARCIA', 'F', 'S', '1958-02-17', NULL, NULL, NULL, NULL, '4113764.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4203418, 'V', 'SONIA', '', 'DAVILA', 'CASTRO', 'F', 'C', '1952-03-08', NULL, NULL, NULL, NULL, '4203418.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4203769, 'V', 'CARMEN', 'R.', 'DIAZ', 'DE COLMENARES', 'F', 'C', '1952-06-06', NULL, NULL, NULL, NULL, '4203769.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4205929, 'V', 'MAXIMIANO', '', 'CARRERO', 'VIVAS', 'M', 'S', '1950-02-13', NULL, NULL, NULL, NULL, '4205929.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4207171, 'V', 'ANDRES', 'ELOY', 'PARADA', 'VIVAS', 'M', 'C', '1953-10-27', NULL, NULL, NULL, NULL, '4207171.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4211712, 'V', 'GERSON', '', 'QUINTERO', 'CASTELLANOS', 'M', 'C', '1956-02-16', NULL, NULL, NULL, NULL, '4211712.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4630373, 'V', 'CARLOS', 'ALBERTO', 'VARELA', 'OROZCO', 'F', 'C', '1957-05-20', NULL, NULL, NULL, NULL, '4630373.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4630395, 'V', 'CARMEN', '', 'BAEZ', 'DE ZAMBRANO', 'F', 'C', '1951-07-27', NULL, NULL, NULL, NULL, '4630395.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4633493, 'V', 'JULIO', 'CESAR', 'ROSALES', 'CARDENAS', 'M', 'C', '1956-02-05', NULL, NULL, NULL, NULL, '4633493.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4633817, 'V', 'OLGA', '', 'RANGEL', 'DE CARDENAS', 'F', 'C', '1954-07-05', NULL, NULL, NULL, NULL, '4633817.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5022440, 'V', 'JOSE', 'LUIS', 'DEVIA', 'MEDINA', 'M', 'C', '1956-09-18', NULL, NULL, NULL, NULL, '5022440.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5022569, 'V', 'DORIS', 'MARIA', 'MORA', 'DE PEREZ', 'F', 'C', '1956-06-13', NULL, NULL, NULL, NULL, '5022569.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5023877, 'V', 'ROSA', 'ELENA', 'ACEVEDO', 'DE SANCHEZ', 'F', 'C', '1955-08-30', NULL, NULL, NULL, NULL, '5023877.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5027947, 'V', 'OMAR', '', 'ROJAS', 'URBINA', 'M', 'C', '1956-06-09', NULL, NULL, NULL, NULL, '5027947.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5028172, 'V', 'EDITH', 'MARIA', 'HERNANDEZ', 'VEZGA', 'F', 'C', '1959-05-09', NULL, NULL, NULL, NULL, '5028172.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5030905, 'V', 'JOSEFINA', '', 'RAMIREZ', 'VIVAS', 'F', 'S', '1957-12-30', NULL, NULL, NULL, NULL, '5030905.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5032111, 'V', 'EL', '', 'CANDIALES', 'CABALLEROEZEQU', 'M', 'C', '1955-10-03', NULL, NULL, NULL, NULL, '5032111.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5326532, 'V', 'MIRIAM', 'DEL', 'VIVAS', 'MORA', 'F', 'S', '1959-07-18', NULL, NULL, NULL, NULL, '5326532.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5424001, 'V', 'ELSY', 'CAROLINA', 'BONILLA', 'MONSALVE', 'F', 'S', '1961-07-01', NULL, NULL, NULL, NULL, '5424001.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5449866, 'V', 'DORA', 'INES', 'MOLINA', '', 'F', 'S', '1954-09-19', NULL, NULL, NULL, NULL, '5449866.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5639446, 'V', 'NINFA', 'CECILIA', 'SEGURA', 'SANCHEZ', 'F', 'S', '1957-07-25', NULL, NULL, NULL, NULL, '5639446.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5641138, 'V', 'CESAR', 'AUGUSTO', 'PABON', 'BONILLA', 'M', 'C', '1957-08-10', NULL, NULL, NULL, NULL, '5641138.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5641857, 'V', 'IVAN', 'HENRY', 'DELGADO', 'MONCADA', 'M', 'C', '1957-12-26', NULL, NULL, NULL, NULL, '5641857.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5643128, 'V', 'IRIS', 'J.', 'MARQUEZ', 'DE BOLIVAR', 'F', 'D', '1958-06-22', NULL, NULL, NULL, NULL, '5643128.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5643923, 'V', 'TRINIDAD', '', 'QUINTERO', 'USECHE', 'F', 'S', '1958-11-04', NULL, NULL, NULL, NULL, '5643923.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5661357, 'V', 'DORIS', 'GISELA', 'OROZCO', 'GONZALEZ', 'F', 'C', '1961-11-20', NULL, NULL, NULL, NULL, '5661357.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5665179, 'V', 'NELLY', 'A.', 'CASTRO', 'GUERRERO', 'F', 'S', '1960-09-19', NULL, NULL, NULL, NULL, '5665179.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5671953, 'V', 'NANCY', 'C.', 'CHACON', 'GUERRERO', 'F', 'C', '1961-02-05', NULL, NULL, NULL, NULL, '5671953.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5679731, 'V', 'CRUZ', 'EMELIA', 'GOLON', 'BERNAL', 'F', 'S', '1961-05-03', NULL, NULL, NULL, NULL, '5679731.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5683136, 'V', 'GLORIA', 'ZULAY', 'VELASCO', 'DE JAIMES', 'F', 'C', '1960-02-20', NULL, NULL, NULL, NULL, '5683136.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5683390, 'V', 'RAFAEL', 'GERARDO', 'SALAS', 'GARCIA', 'M', 'S', '1961-12-30', NULL, NULL, NULL, NULL, '5683390.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5687942, 'V', 'MARCO', 'AURELIO', 'MORA', 'BAEZ', 'M', 'S', '1954-03-14', NULL, NULL, NULL, NULL, '5687942.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (6182857, 'V', 'ZULAY', 'MARIA', 'CACERES', 'CHACON', 'F', 'C', '1966-04-12', NULL, NULL, NULL, NULL, '6182857.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (6526362, 'V', 'EINA', '', 'OLMOS', 'LEALMARVELYAD', 'F', 'D', '1962-04-21', NULL, NULL, NULL, NULL, '6526362.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8072313, 'V', 'ANTONIO', 'ABAD', 'REINA', 'MARQUEZ', 'M', 'S', '1948-01-17', NULL, NULL, NULL, NULL, '8072313.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8073700, 'V', 'JOSE', 'SALVADOR', 'ROJAS', '', 'M', 'C', '1959-10-04', NULL, NULL, NULL, NULL, '8073700.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8091914, 'V', 'HERBERTH', 'HERNANDO', 'RUIZ', 'ROJAS', 'M', 'C', '1958-06-11', NULL, NULL, NULL, NULL, '8091914.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8093698, 'V', 'ANA', 'MARIA', 'SANCHEZ', 'CHACON', 'F', '', '1960-10-20', NULL, NULL, NULL, NULL, '8093698.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8101734, 'V', 'BELEN', '', 'ZAMBRANO', 'PABON', 'F', 'C', '1962-05-07', NULL, NULL, NULL, NULL, '8101734.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9121338, 'V', 'CARMEN', 'COROMOTO', 'GARCIA', 'MORA', 'F', 'S', '1961-07-14', NULL, NULL, NULL, NULL, '9121338.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9142656, 'V', 'CARMEN', 'ALICIA', 'SANTANDER', '', 'F', 'C', '1962-06-23', NULL, NULL, NULL, NULL, '9142656.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9206182, 'V', 'GLADYS', 'YOLEIDA', 'MOLINA', 'ARIAS', 'F', 'S', '1963-07-25', NULL, NULL, NULL, NULL, '9206182.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9211978, 'V', 'FANNY', '', 'MARTINEZ', 'QUINTERO', 'F', 'D', '1964-12-28', NULL, NULL, NULL, NULL, '9211978.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9221875, 'V', 'ORNOLDO', 'DEL', 'GUERRERO', 'ARAQUE', 'M', 'S', '1962-03-23', NULL, NULL, NULL, NULL, '9221875.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9222463, 'V', 'CARMEN', 'T.', 'RODRIGUEZ', 'MARTINEZ', 'F', 'C', '1964-07-12', NULL, NULL, NULL, NULL, '9222463.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9225056, 'V', 'CAROLINA', '', 'PARCESEPE', 'SARMIENTO', 'F', 'S', '1966-05-06', NULL, NULL, NULL, NULL, '9225056.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9230572, 'V', 'RAMONA', '', 'MONCADA', 'MONCADA', 'F', 'C', '1966-12-15', NULL, NULL, NULL, NULL, '9230572.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9330404, 'V', 'ANA', 'LOURDIS', 'MORALES', 'RAMIREZ', 'F', 'C', '1964-12-31', NULL, NULL, NULL, NULL, '9330404.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10154136, 'V', 'OROMOTO', '', 'LEON', 'TORRESLOURDES', 'F', 'S', '1968-01-20', NULL, NULL, NULL, NULL, '10154136.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12070672, 'V', 'DIOLINDA', '', 'CACERES', 'DE VELASCO', 'F', 'C', '1952-07-20', NULL, NULL, NULL, NULL, '12070672.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12235308, 'V', 'NEILA', '', 'ARDILA', 'DE VILLA', 'F', 'C', '1947-07-06', NULL, NULL, NULL, NULL, '12235308.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12817519, 'V', 'UZ', '', 'DIAZ', 'DE ALTUVEMARY', 'F', 'C', '1947-03-10', NULL, NULL, NULL, NULL, '12817519.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (6447501, 'V', 'MARTHA', 'S.', 'URUEÑA', 'DE CONTRERAS', 'F', 'C', '1963-06-18', NULL, NULL, NULL, NULL, '6447501.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9233318, 'V', 'DOUGLAS', 'MANUEL', 'PEÑA', 'PAREDES', 'M', 'S', '1966-09-02', NULL, NULL, NULL, NULL, '9233318.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11505221, 'V', 'ENZO', 'ALI', 'CAÑIZALES', 'DELGADO', 'M', 'C', '1973-01-19', NULL, NULL, NULL, NULL, '11505221.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13304199, 'V', 'JUAN', 'GABRIEL', 'CARREÑO', 'JAIMES', 'M', 'S', '1979-03-05', NULL, NULL, NULL, NULL, '13304199.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4630210, 'V', 'BENJAMIN', '', 'OCHOA', 'MARIÑO', 'M', 'C', '1957-09-16', NULL, NULL, NULL, NULL, '4630210.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9214700, 'V', 'TERESA', '', 'URBINA', 'UREÑA', 'F', 'C', '1963-01-02', NULL, NULL, NULL, NULL, '9214700.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11502597, 'V', 'GLENIS', 'DAYANNA', 'FERRER', 'OMAÑA', 'F', 'S', '1973-08-27', NULL, NULL, NULL, NULL, '11502597.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11997518, 'V', 'JOAQUIN', 'F.', 'ECKSTEIN', 'ABDALA', 'M', 'S', '1973-12-27', NULL, NULL, NULL, NULL, '11997518.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17179288, 'V', 'EDUARDO', 'JOSE', 'MENDOZA', 'GUTIERREZ', 'M', 'S', '1985-07-09', NULL, 'V-17179288-2', NULL, NULL, '17179288.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14180445, 'V', 'ANGGIE', 'M.', 'RIVERO', 'ESTUPIÑAN', 'F', 'S', '1980-02-22', NULL, NULL, NULL, NULL, '14180445.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3036971, 'V', 'CESAR', 'AUGUSTO', 'LOPEZ', 'AÑEZ', 'M', 'C', '1949-01-28', NULL, NULL, NULL, NULL, '3036971.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9220083, 'V', 'JOSE', 'GREGORIO', 'SOLER', 'GARCIA', 'M', 'C', '1966-03-20', NULL, NULL, NULL, NULL, '9220083.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9223480, 'V', 'MARIA', 'CLARITZA', 'MARQUEZ', 'ROMERO', 'F', 'S', '1966-03-10', NULL, NULL, NULL, NULL, '9223480.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9225202, 'V', 'LIVIA', 'T.', 'QUINTERO', 'ONTIVEROS', 'F', 'X', '1965-08-04', NULL, NULL, NULL, NULL, '9225202.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9226981, 'V', 'NINA', 'M.', 'RUGELES', 'CASANOVA', 'F', 'S', '1967-06-26', NULL, NULL, NULL, NULL, '9226981.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9229274, 'V', 'NORAIMA', 'C.', 'DELGADO', 'AYALA', 'F', 'C', '1966-03-12', NULL, NULL, NULL, NULL, '9229274.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9229973, 'V', 'VICKY', 'JOSEFINA', 'CHACON', 'VEZGA', 'F', 'D', '1966-06-22', NULL, NULL, NULL, NULL, '9229973.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9233104, 'V', 'JOSE', 'CRISPULO', 'VIVAS', 'ROSALES', 'M', 'S', '1966-12-29', NULL, NULL, NULL, NULL, '9233104.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9240774, 'V', 'OTILIA', '', 'SANTOS', 'MONSALVE', 'F', 'C', '1967-12-13', NULL, NULL, NULL, NULL, '9240774.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9247506, 'V', 'CARMEN', 'MAGERLY', 'GARCIA', 'ESTEPA', 'F', 'S', '1967-04-22', NULL, NULL, NULL, NULL, '9247506.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9248679, 'V', 'IVAN', 'ADOLFO', 'ESPINEL', 'VIVAS', 'M', 'C', '1968-03-20', NULL, NULL, NULL, NULL, '9248679.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9249105, 'V', 'BLANCA', 'NORIS', 'ZAMORA', 'SANCHEZ', 'F', 'S', '1970-07-01', NULL, NULL, NULL, NULL, '9249105.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9343250, 'V', 'KARINA', 'ELIZABETH', 'REY', 'MARTINEZ', 'F', 'S', '1971-08-25', NULL, NULL, NULL, NULL, '9343250.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9343546, 'V', 'KALININ', 'A.', 'CASTILLO', 'BORRERO', 'M', 'S', '1972-03-15', NULL, NULL, NULL, NULL, '9343546.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9345491, 'V', 'CAROL', 'Y.', 'ARCINIEGAS', 'REYES', 'F', 'S', '1975-06-24', NULL, NULL, NULL, NULL, '9345491.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9349833, 'V', 'DURBI', 'ALEJANDRA', 'ARAQUE', 'MENDOZA', 'F', 'S', '1976-10-09', NULL, NULL, NULL, NULL, '9349833.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9364657, 'V', 'BLANCA', 'GLADYS', 'RAMIREZ', 'MENDOZA', 'F', 'C', '1968-03-13', NULL, NULL, NULL, NULL, '9364657.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9461641, 'V', 'ALEXANDER', '', 'PEREZ', 'FERNANDEZ', 'M', 'C', '1968-02-06', NULL, NULL, NULL, NULL, '9461641.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9729246, 'V', 'JUAN', 'CARLOS', 'ZAMBRANO', 'BRAVO', 'M', '', '1968-04-18', NULL, NULL, NULL, NULL, '9729246.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9882350, 'V', 'MARIELA', 'E.', 'RANGEL', 'PACHECO', 'F', 'S', '1969-03-30', NULL, NULL, NULL, NULL, '9882350.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10147218, 'V', 'DINORAH', 'ASTRID', 'MARQUEZ', 'VIVAS', 'F', 'S', '1970-03-12', NULL, NULL, NULL, NULL, '10147218.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10152376, 'V', 'RAYCELIN', 'T.', 'SALCEDO', 'SANCHEZ', 'F', 'C', '1971-01-11', NULL, NULL, NULL, NULL, '10152376.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10154041, 'V', 'DETSSY', 'YAJAIRA', 'RUIZ', 'OROZCO', 'F', 'S', '1969-04-14', NULL, NULL, NULL, NULL, '10154041.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10159422, 'V', 'LINDA', 'KATHERINE', 'MEDINA', 'MEDINA', 'F', 'S', '1971-03-18', NULL, NULL, NULL, NULL, '10159422.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10165529, 'V', 'SARA', 'YALINE', 'VELASCO', 'CHACON', 'F', 'D', '1970-11-18', NULL, NULL, NULL, NULL, '10165529.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10168182, 'V', 'EDGAR', 'ANTONIO', 'PEREZ', 'MORALES', 'M', 'C', '1971-04-10', NULL, NULL, NULL, NULL, '10168182.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10169417, 'V', 'YOLANDA', '', 'FERNANDEZ', 'URBINA', 'F', 'S', '1971-08-27', NULL, NULL, NULL, NULL, '10169417.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10174245, 'V', 'JOSE', 'EDUARDO', 'COLMENAREZ', 'AUCEDO', 'M', 'S', '1973-07-05', NULL, NULL, NULL, NULL, '10174245.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10176223, 'V', 'BELKYS', 'JEANETH', 'ACEVEDO', '', 'F', 'S', '1972-07-02', NULL, NULL, NULL, NULL, '10176223.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10176587, 'V', 'INRRYT', 'ZULAY', 'VARGAS', 'SEPULVEDA', 'F', 'S', '1973-03-07', NULL, NULL, NULL, NULL, '10176587.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10178175, 'V', 'RUTH', 'CAROLINA', 'RIVERO', 'ROA', 'F', 'S', '1973-02-14', NULL, NULL, NULL, NULL, '10178175.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10748849, 'V', 'MARIA', 'E.', 'PULIDO', 'RAMIREZ', 'F', 'S', '1975-06-12', NULL, NULL, NULL, NULL, '10748849.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11109820, 'V', 'LEYDES', 'DEL', 'PULIDO', 'ROSALES', 'F', 'S', '1972-09-21', NULL, NULL, NULL, NULL, '11109820.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11109944, 'V', 'THANIA', '', 'OVALLES', 'ROMERO', 'F', 'S', '1973-03-15', NULL, NULL, NULL, NULL, '11109944.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11114194, 'V', 'JAVIER', 'ALEXIS', 'MARTINEZ', 'SOTO', 'M', 'C', '1975-01-15', NULL, NULL, NULL, NULL, '11114194.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11114976, 'V', 'JUAN', 'CARLOS', 'MORA', 'GOYENECHE', 'M', 'S', '1975-07-18', NULL, NULL, NULL, NULL, '11114976.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11370746, 'V', 'NANCY', 'COROMOTO', 'PEREIRA', 'PERNIA', 'F', 'S', '1972-02-08', NULL, NULL, NULL, NULL, '11370746.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11373818, 'V', 'YOUCELIANE', 'RAYBEL', 'OROZCO', '', 'F', 'S', '1973-12-25', NULL, NULL, NULL, NULL, '11373818.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11497586, 'V', 'PEDRO', 'A.', 'CHAVEZ', 'COLMENARES', 'M', 'S', '1971-11-05', NULL, NULL, NULL, NULL, '11497586.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11498234, 'V', 'JACKSON', 'L.', 'GARCIA', 'CONTRERAS', 'M', 'S', '1973-01-22', NULL, NULL, NULL, NULL, '11498234.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11498513, 'V', 'EUGENIA', 'C.', 'NARVAEZ', 'DE VARELA', 'F', 'S', '1974-07-13', NULL, NULL, NULL, NULL, '11498513.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11505190, 'V', 'DUBRASKA', 'K.', 'ARANGUREN', 'VIVAS', 'F', 'S', '1973-11-08', NULL, NULL, NULL, NULL, '11505190.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (11924357, 'V', 'MABEL', 'BAUTISTA', 'JIMENEZ', '', 'F', 'S', '1962-06-24', NULL, NULL, NULL, NULL, '11924357.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12226494, 'V', 'NEIDA', 'C.', 'MORENO', 'ZAMBRANO', 'F', 'C', '1974-03-05', NULL, NULL, NULL, NULL, '12226494.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12228449, 'V', 'MARCO', 'JAVIER', 'SAYAGO', 'MEDINA', 'M', 'C', '1974-01-24', NULL, NULL, NULL, NULL, '12228449.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12229282, 'V', 'TANIA', 'YANETH', 'CAMARGO', 'PARADA', 'F', 'S', '1975-09-26', NULL, NULL, NULL, NULL, '12229282.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12231796, 'V', 'OMAR', 'ALFONSO', 'OSORIO', 'LABRADOR', 'M', 'C', '1975-02-28', NULL, NULL, NULL, NULL, '12231796.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12232430, 'V', 'GILBERTO', 'N.', 'GUERRERO', 'ROSALES', 'M', 'S', '1975-07-21', NULL, NULL, NULL, NULL, '12232430.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12620335, 'V', 'LEYDEN', 'M.', 'COLMENARES', 'DEPABLOS', 'F', 'S', '1976-10-14', NULL, NULL, NULL, NULL, '12620335.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12633840, 'V', 'GLEISY', 'Y.', 'ROSALES', 'GONZALEZ', 'F', 'S', '1976-05-08', NULL, NULL, NULL, NULL, '12633840.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12784982, 'V', 'RICHARD', 'A.', 'JAIMES', 'FLORES', 'M', 'C', '1977-11-01', NULL, NULL, NULL, NULL, '12784982.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12813266, 'V', 'KARINA', 'DEL', 'OCARIZ', 'MURILLO', 'F', 'S', '1976-07-16', NULL, NULL, NULL, NULL, '12813266.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12889478, 'V', 'SAIR', 'RAROSMAR', 'GAMEZ', 'OROZCO', 'F', 'S', '1977-12-27', NULL, NULL, NULL, NULL, '12889478.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12890352, 'V', 'NEYRA', 'CECILIA', 'VEGA', 'DUQUE', 'F', 'S', '1976-11-05', NULL, NULL, NULL, NULL, '12890352.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12970105, 'V', 'FRANKLIN', '', 'HERNANDEZ', 'BLANCO', 'M', 'C', '1977-04-29', NULL, NULL, NULL, NULL, '12970105.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13351695, 'V', 'MEIRY', 'DAYANA', 'FARIA', 'ARCINIEGAS', 'F', 'S', '1979-05-12', NULL, NULL, NULL, NULL, '13351695.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13486092, 'V', 'VIANNEY', 'J.', 'RIVERO', 'GOMEZ', 'F', 'S', '1979-01-02', NULL, NULL, NULL, NULL, '13486092.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13505874, 'V', 'RONMHEL', '', 'GONZALEZ', 'COLMENARES', 'M', 'S', '1978-11-02', NULL, NULL, NULL, NULL, '13505874.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13549685, 'V', 'KARINA', '', 'FERNANDEZ', 'GONZALEZ', 'F', 'S', '1979-02-09', NULL, NULL, NULL, NULL, '13549685.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13587082, 'V', 'JEANETTE', 'M.', 'MARTINEZ', 'LAGOS', 'F', 'S', '1978-05-30', NULL, NULL, NULL, NULL, '13587082.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13821735, 'V', 'ZAIDA', 'LIZETH', 'AVILA', 'MOLINA', 'F', 'S', '1979-08-27', NULL, NULL, NULL, NULL, '13821735.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13891549, 'V', 'LUZ', 'A.', 'COLMENARES', 'DE ARAQUE', 'F', 'C', '1978-05-22', NULL, NULL, NULL, NULL, '13891549.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13939279, 'V', 'LERKYS', 'YADIRA', 'PACHECO', 'DAZA', 'F', 'S', '1980-06-13', NULL, NULL, NULL, NULL, '13939279.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13973905, 'V', 'HEYLIN', 'JOHANNA', 'SOTO', 'CONTRERAS', 'F', 'S', '1979-08-16', NULL, NULL, NULL, NULL, '13973905.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14041929, 'V', 'ALFREDO', 'J.', 'SERRANO', 'CONTRERAS', 'M', 'S', '1978-06-21', NULL, NULL, NULL, NULL, '14041929.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14179006, 'V', 'MIGUEL', 'A.', 'ESCALANTE', 'VELASCO', 'M', 'S', '1979-11-27', NULL, NULL, NULL, NULL, '14179006.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14180037, 'V', 'FRANCISCO', 'J.', 'AYALA', 'RODRIGUEZ', 'M', 'S', '1980-05-21', NULL, NULL, NULL, NULL, '14180037.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14180431, 'V', 'ANGEL', 'JESUS', 'GARCIA', 'MONCADA', 'M', 'S', '1979-04-12', NULL, NULL, NULL, NULL, '14180431.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14348752, 'V', 'GIPSSY', 'YURLEY', 'CHACON', 'LEON', 'F', 'S', '1979-07-02', NULL, NULL, NULL, NULL, '14348752.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14460143, 'V', 'ATOS', '', 'ZAPPI', 'MORILLO', 'M', 'S', '1980-11-08', NULL, NULL, NULL, NULL, '14460143.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14502282, 'V', 'MARTHA', 'EDILIA', 'RAIZ', 'VERA', 'F', 'S', '1980-04-23', NULL, NULL, NULL, NULL, '14502282.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14605656, 'V', 'KAREN', 'SCARLET', 'RIVERA', 'CONTRERAS', 'F', 'S', '1980-10-03', NULL, NULL, NULL, NULL, '14605656.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14607236, 'V', 'DOUGLAS', 'JOSE', 'CHACON', 'SUAREZ', 'M', 'S', '1980-05-07', NULL, NULL, NULL, NULL, '14607236.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14885039, 'V', 'MERCEDES', 'E.', 'BLANCO', 'MUNDARAY', 'F', 'S', '1979-02-28', NULL, NULL, NULL, NULL, '14885039.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15027498, 'V', 'IVIS', 'OFIR', 'PARADA', 'MARQUEZ', 'F', 'S', '1980-04-25', NULL, NULL, NULL, NULL, '15027498.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15079059, 'V', 'ANGEL', 'DAVID', 'CASTRO', 'SILVA', 'M', 'S', '1981-09-09', NULL, NULL, NULL, NULL, '15079059.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15079183, 'V', 'ALEIDY', 'MARITZA', 'MORA', 'ACEVEDO', 'F', 'S', '1982-07-11', NULL, NULL, NULL, NULL, '15079183.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15157202, 'V', 'ERNESTO', 'FIDEL', 'AVILA', 'URBINA', 'M', 'S', '1980-06-29', NULL, NULL, NULL, NULL, '15157202.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15159174, 'V', 'YESLANY', 'CLARET', 'SOSA', 'GUERRERO', 'F', 'S', '1981-03-03', NULL, NULL, NULL, NULL, '15159174.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15242383, 'V', 'LEYMER', 'G.', 'LABRADOR', 'BOLIVAR', 'M', 'S', '1982-08-13', NULL, NULL, NULL, NULL, '15242383.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (1551555, 'V', 'MARIA', 'M.', 'COLMENARES', 'DE PERNIA', 'F', 'C', '1942-01-06', NULL, NULL, NULL, NULL, '1551555.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3193103, 'V', 'ARMANDO', '', 'GUERRERO', 'GOMEZ', 'M', 'C', '1947-11-19', NULL, NULL, NULL, NULL, '3193103.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3618098, 'V', 'EDICTO', 'JOSE', 'VIVAS', '', 'M', 'C', '1948-05-26', NULL, NULL, NULL, NULL, '3618098.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3716598, 'V', 'PEDRO', 'RAFAEL', 'RICO', 'PEREZ', 'M', 'S', '1951-04-22', NULL, NULL, NULL, NULL, '3716598.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4209398, 'V', 'NANCY', 'YUDITH', 'CORZO', 'DE CORDERO', 'F', 'C', '1954-12-02', NULL, NULL, NULL, NULL, '4209398.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4212434, 'V', 'MARGARITA', '', 'MONSALVE', 'DE CALDERON', 'F', 'C', '1950-03-25', NULL, NULL, NULL, NULL, '4212434.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4434487, 'V', 'ALBERTO', 'JOSE', 'REINOSA', 'MONSALVE', 'M', 'D', '1953-04-17', NULL, NULL, NULL, NULL, '4434487.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4493876, 'V', 'RAFAEL', 'ANGEL', 'SANCHEZ', 'CARRILLO', 'M', 'C', '1957-05-07', NULL, NULL, NULL, NULL, '4493876.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4630076, 'V', 'ANA', 'CARLOTA', 'GOMEZ', '', 'F', 'C', '1953-05-07', NULL, NULL, NULL, NULL, '4630076.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4634588, 'V', 'HORACIO', '', 'SUAREZ', '', 'M', 'C', '1955-10-17', NULL, NULL, NULL, NULL, '4634588.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5030837, 'V', 'JORGE', '', 'RODRIGUEZ', 'SAYAGO', 'M', 'C', '1957-10-16', NULL, NULL, NULL, NULL, '5030837.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5034618, 'V', 'YADIRA', '', 'TOSCANO', 'DIAZ', 'F', 'S', '1958-12-25', NULL, NULL, NULL, NULL, '5034618.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5644414, 'V', 'EVA', 'MARIA', 'NAVA', 'RAMIREZ', 'F', 'S', '1961-05-20', NULL, NULL, NULL, NULL, '5644414.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5646127, 'V', 'MIGUEL', 'ANGEL', 'CORZO', 'CONTRERAS', 'M', 'C', '1957-11-27', NULL, NULL, NULL, NULL, '5646127.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5652245, 'V', 'NACARID', 'C.', 'SANABRIA', 'CARRERO', 'F', '', '1963-01-11', NULL, NULL, NULL, NULL, '5652245.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5654242, 'V', 'ROMULO', 'S.', 'GONZALEZ', 'ACEVEDO', 'M', 'C', '1959-06-09', NULL, NULL, NULL, NULL, '5654242.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5654282, 'V', 'JOSE', 'ALBERTO', 'VELAZCO', 'CHACON', 'M', 'C', '1961-01-04', NULL, NULL, NULL, NULL, '5654282.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5655835, 'V', 'OLGA', 'MARIA', 'GRANADOS', 'DELGADO', 'F', 'S', '1963-04-13', NULL, NULL, NULL, NULL, '5655835.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5658730, 'V', 'JOSE', 'WILLIAM', 'ARMAS', 'ZAMBRANO', 'M', '', '1960-10-14', NULL, NULL, NULL, NULL, '5658730.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5671752, 'V', 'LIDIA', 'DEL', 'OVALLES', 'ROMERO', 'F', 'S', '1962-08-03', NULL, NULL, NULL, NULL, '5671752.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5676979, 'V', 'ROMAN', 'WILFRIDO', 'MONCADA', 'MORENO', 'M', 'X', '1961-06-23', NULL, NULL, NULL, NULL, '5676979.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5685532, 'V', 'MARIANELA', '', 'CARRILLO', 'CHACON', 'F', 'S', '1963-08-19', NULL, NULL, NULL, NULL, '5685532.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5688197, 'V', 'GLORIA', 'M.', 'PASTRAN', 'MENDEZ', 'F', 'S', '1962-08-25', NULL, NULL, NULL, NULL, '5688197.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5688684, 'V', 'JAIRO', 'ALBERTO', 'CRUZ', 'FERNANDEZ', 'M', 'C', '1963-09-20', NULL, NULL, NULL, NULL, '5688684.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (7362601, 'V', 'MILEXA', '', 'MORLES', 'DE CALDERON', 'F', 'C', '1963-03-15', NULL, NULL, NULL, NULL, '7362601.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8708654, 'V', 'OSCAR', 'ADOLFO', 'CONTRERAS', '', 'M', 'S', '1967-11-28', NULL, NULL, NULL, NULL, '8708654.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8993128, 'V', 'CIRIA', 'CENAIDA', 'RUIZ', 'DEPABLOS', 'F', 'S', '1970-03-01', NULL, NULL, NULL, NULL, '8993128.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9127453, 'V', 'LUIS', 'ALBERTO', 'COLMENARES', 'MORA', 'M', '', '1964-04-15', NULL, NULL, NULL, NULL, '9127453.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9146962, 'V', 'BELKIS', 'C.', 'RONDON', 'DE GONZALEZ', 'F', 'S', '1966-05-31', NULL, NULL, NULL, NULL, '9146962.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9210236, 'V', 'BLANCA', 'FABIOLA', 'COLMENARES', '', 'F', 'S', '1963-05-30', NULL, NULL, NULL, NULL, '9210236.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9211472, 'V', 'MARIBEL', '', 'USECHE', 'DE MOLINA', 'F', 'C', '1964-08-10', NULL, NULL, NULL, NULL, '9211472.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15242873, 'V', 'GABRIELA', 'K.', 'PARADA', 'QUINTERO', 'F', 'S', '1981-11-28', NULL, NULL, NULL, NULL, '15242873.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15242977, 'V', 'CLAUDIA', 'M.', 'URBINA', 'ZAMBRANO', 'F', 'S', '1981-10-25', NULL, NULL, NULL, NULL, '15242977.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15503666, 'V', 'KARLA', 'A.', 'CASTILLO', 'VALECILLO', 'F', 'S', '1982-11-15', NULL, NULL, NULL, NULL, '15503666.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15567505, 'V', 'ERTO', '', 'DUQUE', 'CHACONJESUSAL', 'M', 'S', '1983-07-22', NULL, NULL, NULL, NULL, '15567505.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15639083, 'V', 'DEXY', 'ESPERANZA', 'VELASCO', 'JAIMES', 'F', 'S', '1981-09-04', NULL, NULL, NULL, NULL, '15639083.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15989057, 'V', 'JOHANA', 'CONSUELO', 'CAIRES', 'GOMEZ', 'F', 'S', '1983-08-15', NULL, NULL, NULL, NULL, '15989057.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16122635, 'V', 'JOSE', 'VICENTE', 'VARELA', 'GOMEZ', 'M', 'S', '1983-04-30', NULL, NULL, NULL, NULL, '16122635.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16409244, 'V', 'EDUARDO', 'JOSE', 'CARRERO', 'MONCADA', 'M', 'S', '1983-04-28', NULL, NULL, NULL, NULL, '16409244.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16409249, 'V', 'WALTER', 'ENRIQUE', 'RUIZ', 'ESCALANTE', 'M', 'S', '1984-11-02', NULL, NULL, NULL, NULL, '16409249.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16540489, 'V', 'LEIDY', 'A.', 'CASTELLANOS', 'CONDE ', 'F', 'S', '1983-01-13', NULL, NULL, NULL, NULL, '16540489.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16575616, 'V', 'JACKSON', 'E.', 'CONTRERAS', 'STERLING', 'M', 'S', '1985-01-31', NULL, NULL, NULL, NULL, '16575616.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16611146, 'V', 'NATASHA', 'SHARYKK', 'PANZA', 'GOMEZ', 'F', 'S', '1985-08-08', NULL, NULL, NULL, NULL, '16611146.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16611330, 'V', 'RONALD', 'A.', 'TORRES', 'CARRIASCO', 'M', 'S', '1984-02-28', NULL, NULL, NULL, NULL, '16611330.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16745514, 'V', 'MARIA', 'FERNANDA', 'MORA', 'CASIQUE', 'F', 'S', '1985-12-20', NULL, NULL, NULL, NULL, '16745514.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16960418, 'V', 'ANGELICA', 'M.', 'ROJAS', 'PAREDES', 'F', 'S', '1985-04-19', NULL, NULL, NULL, NULL, '16960418.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3644111, 'V', 'LESBIA', '', 'LIÑAN', '', 'F', 'S', '1947-07-03', NULL, NULL, NULL, NULL, '3644111.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5682926, 'V', 'ANA', 'COROMOTO', 'PEÑA', 'DE GOMEZ', 'F', 'C', '1964-06-26', NULL, NULL, NULL, NULL, '5682926.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (8104194, 'V', 'HILDA', 'INES', 'PATIÑO', 'CHACON', 'F', 'S', '1968-05-03', NULL, NULL, NULL, NULL, '8104194.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (6432041, 'V', 'JHONY', 'ALFONSO', 'PEREZ', 'MUÑOZ', 'M', 'S', '1962-09-02', NULL, NULL, NULL, NULL, '6432041.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16540588, 'V', 'ANDREA', 'PATRICIA', 'GUERRERO', 'PEÑA', 'F', 'S', '1984-08-26', NULL, NULL, NULL, NULL, '16540588.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5283541, 'V', 'JUANITA', '', 'NIÑO', 'DE PEÑA', 'F', 'C', '1957-07-25', NULL, NULL, NULL, NULL, '5283541.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9217620, 'V', 'MIGUEL', 'ARCANGEL', 'PEREZ', '', 'M', 'S', '1964-03-18', NULL, NULL, NULL, NULL, '9217620.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9218696, 'V', 'BETTY', 'R.', 'MALDONADO', 'VELASCO', 'F', 'S', '1964-01-31', NULL, NULL, NULL, NULL, '9218696.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14417996, 'V', 'NELSON', 'EPIFANIO', 'MORA', 'SANDIA', 'M', 'S', '1978-05-13', NULL, 'v-14417996-6', NULL, NULL, '14417996.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18392581, 'V', 'JENNIFER', 'M.', 'BOLAÑOS', 'MORALES', 'F', 'S', '1987-10-23', NULL, NULL, NULL, NULL, '18392581.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17811842, 'V', 'JUAN', 'CARLOS', 'CONTRERAS', 'ONTIVEROS', 'M', 'S', '1985-08-23', NULL, 'V-17811841-7', NULL, NULL, '17811842.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (20999679, 'V', 'DUARDO', '', 'ORDUÑO', 'ARELLANOANGEL', 'M', 'S', '1991-04-06', NULL, NULL, NULL, NULL, '20999679.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3070162, 'V', 'RAMON', 'GERARDO', 'PEÑA', 'LOPEZ', 'M', 'C', '1945-08-31', NULL, NULL, NULL, NULL, '3070162.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3076373, 'V', 'INIA', '', 'CASTAÑEDA', 'D.VALENCIAVIR', 'F', 'C', '1948-03-21', NULL, NULL, NULL, NULL, '3076373.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17107267, 'V', 'YIGLYBEY', 'J.', 'JIMENEZ', 'MARQUINA', 'F', 'S', '1985-02-11', NULL, NULL, NULL, NULL, '17107267.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17188574, 'V', 'KELLYS', 'P.', 'GONZALEZ', 'ESPINOZA', 'F', 'S', '1985-01-16', NULL, NULL, NULL, NULL, '17188574.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17310239, 'V', 'ESTHER', 'MARIA', 'DUQUE', 'DUQUE', 'F', 'S', '1986-12-17', NULL, NULL, NULL, NULL, '17310239.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17368905, 'V', 'CHRISTY', 'YOHANA', 'SALAS', '', 'F', 'S', '1984-11-13', NULL, NULL, NULL, NULL, '17368905.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17466696, 'V', 'JEAN', 'CARLOS', 'BERMUDEZ', 'DIAZ', 'M', 'S', '1987-07-16', NULL, NULL, NULL, NULL, '17466696.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17467886, 'V', 'ERIKA', 'MILENA', 'MURILLO', 'MURILLO', 'F', 'S', '1988-04-04', NULL, NULL, NULL, NULL, '17467886.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17527174, 'V', 'NEILA', 'YOHANA', 'TORRES', 'GARCIA', 'F', 'S', '1985-07-25', NULL, NULL, NULL, NULL, '17527174.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17644919, 'V', 'YOAMA', '', 'LASA', 'ELJURI', 'F', 'S', '1985-10-11', NULL, NULL, NULL, NULL, '17644919.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17677488, 'V', 'MARIA', 'TERESA', 'ZAMBRANO', 'RUEDA', 'F', 'S', '1987-07-18', NULL, NULL, NULL, NULL, '17677488.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17810911, 'V', 'LISBETH', 'Y.', 'JIMENEZ', 'GOMEZ', 'F', 'S', '1985-08-30', NULL, NULL, NULL, NULL, '17810911.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14100292, 'V', 'NILEY', 'CAROLINA', 'ARIAS', 'NIÑO', 'F', 'S', '1978-12-03', NULL, NULL, NULL, NULL, '14100292.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17831294, 'V', 'AURA', 'D.', 'VILLASMIL', 'CASTRO', 'F', 'S', '1986-03-25', NULL, NULL, NULL, NULL, '17831294.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17931413, 'V', 'KARLEN', 'DEL', 'ZAMBRANO', 'VARELA', 'F', 'S', '1987-02-07', NULL, NULL, NULL, NULL, '17931413.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18089366, 'V', 'KARIN', 'DAYANA', 'RUIZ', 'TORRES', 'F', 'S', '1986-08-24', NULL, NULL, NULL, NULL, '18089366.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18091927, 'V', 'MARIA', 'M.', 'CONTRERAS', 'RODRIGUEZ', 'F', 'S', '1987-05-01', NULL, NULL, NULL, NULL, '18091927.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18255360, 'V', 'JOSE', 'OMAR', 'RINCON', 'SEQUEDA', 'M', 'S', '1988-02-14', NULL, NULL, NULL, NULL, '18255360.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18256660, 'V', 'JULIROSSES', '', 'PEREIRA', 'MARQUEZ', 'F', 'S', '1986-06-03', NULL, NULL, NULL, NULL, '18256660.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18393100, 'V', 'KEILA', 'YAJAIR', 'DUARTE', 'MONTERREY', 'F', 'S', '1988-07-07', NULL, NULL, NULL, NULL, '18393100.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18565488, 'V', 'YORKIS', 'ODALI', 'ANGARITA', 'CAMARGO', 'F', 'S', '1988-02-14', NULL, NULL, NULL, NULL, '18565488.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18762218, 'V', 'MELITZA', 'O.', 'USECHE', 'RAMIREZ', 'F', 'S', '1988-11-12', NULL, NULL, NULL, NULL, '18762218.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18790377, 'V', 'YURKY', 'M.', 'GARCIA', 'CONTRERAS', 'F', 'S', '1984-10-22', NULL, NULL, NULL, NULL, '18790377.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (18990427, 'V', 'MARIA', 'GABRIELA', 'GOMEZ', 'RINCON', 'F', 'S', '1988-08-23', NULL, NULL, NULL, NULL, '18990427.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19134197, 'V', 'LUISANA', 'CAROLIN', 'ARIAS', 'CARRERO', 'F', 'S', '1989-03-05', NULL, NULL, NULL, NULL, '19134197.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19135479, 'V', 'ANYURY', 'COROMOTO', 'CRUZ', 'ESTEVES', 'F', 'S', '1989-02-19', NULL, NULL, NULL, NULL, '19135479.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19194827, 'V', 'JOSE', 'G.', 'MENDOZA', 'CARDENAS', 'M', 'S', '1990-03-15', NULL, NULL, NULL, NULL, '19194827.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19384392, 'V', 'GLORIAY.', '', 'DA', 'CUNHACONTRERAS', 'F', 'C', '1988-10-09', NULL, NULL, NULL, NULL, '19384392.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19599113, 'V', 'OMARY', 'ANDREINA', 'RINCON', 'BENACHI', 'F', 'S', '1990-12-04', NULL, NULL, NULL, NULL, '19599113.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19777625, 'V', 'GLORIA', 'M.', 'GUALDRON', 'MENDOZA', 'F', 'S', '1989-05-03', NULL, NULL, NULL, NULL, '19777625.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19877410, 'V', 'JENNIRE', 'C.', 'SANGUINO', 'DE DIAZ', 'F', 'C', '1990-02-11', NULL, NULL, NULL, NULL, '19877410.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19878267, 'V', 'MARIA', 'ALEJANDR', 'SUAREZ', 'FUENTES', 'F', 'S', '1990-08-03', NULL, NULL, NULL, NULL, '19878267.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (20123329, 'V', 'MILEIDY', 'CRISTIN', 'SANCHEZ', 'PARRA', 'F', 'S', '1991-08-23', NULL, NULL, NULL, NULL, '20123329.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (20413389, 'V', 'GONZALEZ', '', 'ROSA', 'AMERICASULBARA', 'F', 'S', '1992-09-24', NULL, NULL, NULL, NULL, '20413389.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (20607943, 'V', 'MARIELBY', 'K.', 'ZAMBRANO', 'VARELA', 'F', 'S', '1991-06-18', NULL, NULL, NULL, NULL, '20607943.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (21003940, 'V', 'EDGAR', 'L.', 'CORONEL', 'CASIQUE', 'M', 'S', '1991-09-14', NULL, NULL, NULL, NULL, '21003940.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (1584405, 'V', 'ROGER', 'ALBERTO', 'CASTELLANOS', '', 'M', 'S', '1945-12-22', NULL, NULL, NULL, NULL, '1584405.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (4722610, 'V', 'OSMIN', 'S.', 'GIMENEZ', 'GIMENEZ', 'M', 'S', '1956-04-16', NULL, NULL, NULL, NULL, '4722610.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (5044324, 'V', 'EDUARDO', 'EMILI', 'CANCHICA', 'RAMIREZ', 'M', 'S', '1952-10-17', NULL, NULL, NULL, NULL, '5044324.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (6029508, 'V', 'CARLOS', 'JOSE', 'JAIMES', 'CASTRO', 'M', 'C', '1959-12-06', NULL, NULL, NULL, NULL, '6029508.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (7217325, 'V', 'CARLOS', '', 'FRANCESCHINI', 'CONTRERAS', 'M', 'C', '1962-08-25', NULL, NULL, NULL, NULL, '7217325.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9206397, 'V', 'JOSE', 'GREGORIO', 'SUAREZ', 'ROMERO', 'M', 'S', '1963-04-23', NULL, NULL, NULL, NULL, '9206397.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9212052, 'V', 'JAIRO', '', 'GOMEZ', 'COTE', 'M', 'S', '1959-02-14', NULL, NULL, NULL, NULL, '9212052.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9217928, 'V', 'EDIZ', '', 'MARQUEZ', 'DE JIMENEZ', 'F', 'V', '1963-10-28', NULL, NULL, NULL, NULL, '9217928.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9226358, 'V', 'ROSA', 'ELENA', 'USECHE', 'DE OSORIO', 'F', 'C', '1964-11-14', NULL, NULL, NULL, NULL, '9226358.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9226823, 'V', 'RICARDO', 'ANTONIO', 'RAMIREZ', '', 'M', 'S', '1960-12-20', NULL, NULL, NULL, NULL, '9226823.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9238021, 'V', 'RAFAEL', '', 'MARQUEZ', 'GARCIA', 'M', 'D', '1965-03-07', NULL, NULL, NULL, NULL, '9238021.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (9242382, 'V', 'JOSE', 'N.', 'GALLANTI', 'BETANCOURT', 'M', 'S', '1967-01-31', NULL, NULL, NULL, NULL, '9242382.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (10175817, 'V', 'ELIZABETH', '', 'LIZARAZO', '', 'F', 'S', '1969-06-21', NULL, NULL, NULL, NULL, '10175817.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (12815249, 'V', 'RONNY', '', 'SALCEDO', 'LOPEZ', 'M', '', '1977-09-23', NULL, NULL, NULL, NULL, '12815249.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13145307, 'V', 'JESUS', 'OCTAVIO', 'RODRIGUEZ', '', 'M', 'C', '1977-03-14', NULL, NULL, NULL, NULL, '13145307.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13303380, 'V', 'EDISSON', 'O.', 'GARCIA', 'PACHECO', 'M', 'S', '1975-04-28', NULL, NULL, NULL, NULL, '13303380.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (13509191, 'V', '', '', 'SEPULVEDA', 'JOHANNAROSALYN', 'F', 'S', '1979-03-30', NULL, NULL, NULL, NULL, '13509191.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14504701, 'V', 'JEAN', 'CARLOS', 'CARRERO', 'TARAZONA', 'M', 'C', '1980-12-16', NULL, NULL, NULL, NULL, '14504701.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (14782183, 'V', 'LAURA', 'A.', 'SANCHEZ', 'CUELLAR', 'F', 'S', '1981-03-22', NULL, NULL, NULL, NULL, '14782183.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15027132, 'V', 'HEMBER', 'JESUS', 'GUTIERREZ', 'SILVA', 'M', 'S', '1979-12-29', NULL, NULL, NULL, NULL, '15027132.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15079871, 'V', 'NANCY', 'JUDITH', 'COLMENARES', 'CASTRO', 'F', 'S', '1981-03-26', NULL, NULL, NULL, NULL, '15079871.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15232905, 'V', 'MARY', 'ZULAY', 'CARRILLO', 'CAMARGO', 'F', 'S', '1981-10-07', NULL, NULL, NULL, NULL, '15232905.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (15565847, 'V', 'JOSE', 'RAUL', 'CASTRO', 'MENDEZ', 'M', 'S', '1982-09-10', NULL, NULL, NULL, NULL, '15565847.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16122426, 'V', 'G.', '', 'RODRIGUEZ', 'MURILLOMILANYE', 'M', 'S', '1984-02-13', NULL, NULL, NULL, NULL, '16122426.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16228364, 'V', 'NEREIDA', 'J.', 'CARRILLO', 'CAMARGO', 'F', 'S', '1984-04-21', NULL, NULL, NULL, NULL, '16228364.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (17877234, 'V', 'EDGAR', 'ALFONSO', 'RINCON', 'APONTE', 'M', 'S', '1984-07-20', NULL, NULL, NULL, NULL, '17877234.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (19135703, 'V', 'JOSE', 'ANTONIO', 'RANGEL', 'MARTINEZ', 'M', 'S', '1989-12-19', NULL, NULL, NULL, NULL, '19135703.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (1511005, 'V', 'HERIBERTO', '', 'SUAREZ', 'SOSA', 'M', 'C', '1934-03-04', NULL, NULL, NULL, NULL, '1511005.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (1585168, 'V', 'BLANCA', 'MARLENE', 'RAMIREZ', 'LOPEZ', 'F', 'S', '1951-03-12', NULL, NULL, NULL, NULL, '1585168.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (1856950, 'V', 'JOSE', 'TEODARDO', 'HERNANDEZ', '', 'M', 'C', '1939-03-17', NULL, NULL, NULL, NULL, '1856950.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (1871671, 'V', 'TEODARDO', '', 'MOLINA', 'TORRES', 'M', 'C', '1937-10-10', NULL, NULL, NULL, NULL, '1871671.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (2114930, 'V', 'ALVARO', '', 'SANCHEZ', 'NIETO', 'M', 'C', '1941-07-26', NULL, NULL, NULL, NULL, '2114930.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (2553023, 'V', 'GIOVANNI', '', 'RAMIREZ', 'ALVIAREZ', 'M', 'C', '1951-09-22', NULL, NULL, NULL, NULL, '2553023.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (2814218, 'V', 'CHINCO', 'I.', 'FRANCESCHINI', 'GARCIA', 'M', 'C', '1949-11-18', NULL, NULL, NULL, NULL, '2814218.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (2892695, 'V', 'MARIA', 'E.', 'GALLANTI', 'DE MEDINA', 'F', 'C', '1938-08-14', NULL, NULL, NULL, NULL, '2892695.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (2893520, 'V', 'CONSOLACION', '', 'GRANADOS', 'ZAMBRANO', 'F', 'S', '1945-02-06', NULL, NULL, NULL, NULL, '2893520.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3193180, 'V', 'ROSA', 'MIREYA', 'PORRAS', 'DE DUQUE', 'F', 'C', '1950-11-08', NULL, NULL, NULL, NULL, '3193180.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (3195539, 'V', 'WILERMA', 'DEL', 'GUERRERO', 'MORA', 'F', 'S', '1947-09-20', NULL, NULL, NULL, NULL, '3195539.jpg', NULL, NULL, NULL, NULL, NULL);
INSERT INTO personal (cedula, nacionalidad, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, genero, estado_civil, fecha_nacimiento, libreta_militar, rif, licencia, foto, ruta_foto, altura, peso, talla_camisa, talla_pantalon, talla_calzado) VALUES (16230882, 'V', 'OSCAR', 'EDUARDO', 'ZAMBRANO', 'LONDOÑO', 'M', 'S', '1984-07-21', NULL, NULL, NULL, NULL, '16230882.jpg', NULL, NULL, NULL, NULL, NULL);


--
-- Data for Name: plantilla_nomina; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: reporte; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: respaldo; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: telefono_personal; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO telefono_personal (id, personal_cedula, telefono) VALUES (1, 14417996, '04247153972');
INSERT INTO telefono_personal (id, personal_cedula, telefono) VALUES (2, 17179288, '04147052754');
INSERT INTO telefono_personal (id, personal_cedula, telefono) VALUES (3, 9247506, '04247004627');


--
-- Data for Name: tipo_base_legal; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: tipo_dato_generico; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO tipo_dato_generico (id, nombre) VALUES (1, 'Horas Nocturnas Trabajadas');
INSERT INTO tipo_dato_generico (id, nombre) VALUES (2, 'Días Feriados Trabajados');
INSERT INTO tipo_dato_generico (id, nombre) VALUES (3, 'Calificación de Desempeño Individual');
INSERT INTO tipo_dato_generico (id, nombre) VALUES (4, 'Porcentaje de Retención de ISLR');
INSERT INTO tipo_dato_generico (id, nombre) VALUES (5, 'Cantidad de lunes en quincena');
INSERT INTO tipo_dato_generico (id, nombre) VALUES (6, 'Coordinación EGA');


--
-- Data for Name: tipo_nomina; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO tipo_nomina (id, nombre) VALUES (1, 'Directores');
INSERT INTO tipo_nomina (id, nombre) VALUES (2, 'Funcionarios');
INSERT INTO tipo_nomina (id, nombre) VALUES (3, 'Obreros');
INSERT INTO tipo_nomina (id, nombre) VALUES (4, 'Contratados');
INSERT INTO tipo_nomina (id, nombre) VALUES (5, 'Jubilados y Pensionados');
INSERT INTO tipo_nomina (id, nombre) VALUES (6, 'Alto Nivel');
INSERT INTO tipo_nomina (id, nombre) VALUES (7, 'Otros');


--
-- Data for Name: unidad_organizativa; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (99, NULL, 1, 'SIN UNIDAD ORGANIZATIVA', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (1, NULL, 1, 'DESPACHO CONTRALOR', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (2, NULL, 1, 'UNIDAD DE AUDITORIA INTERNA', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (3, NULL, 1, 'DIRECCION GENERAL', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (4, NULL, 1, 'DIRECCION DE RECURSOS HUMANOS', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (5, NULL, 1, 'DIRECCION DE ADMINISTRACION', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (6, NULL, 1, 'DIR.D/PLANIFICACION,ORGANIZAC.Y SISTEMAS', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (7, NULL, 1, 'DIRECCION DE CONSULTORIA JURIDICA', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (8, NULL, 1, 'DIR.D/CONTROL D/LA ADMON.CENT.Y POD.EST.', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (9, NULL, 1, 'DIR.D/CONTROL D/LA ADMON.DESCENTRALIZADA', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (10, NULL, 1, 'DIRECCION DE INVESTIGACIONES', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (11, NULL, 1, 'DIR.D/DETERMINACION DE RESPONSABILIDADES', 1);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (20, 1, 1, 'DIR.D/LA SECRETARIA D/DESPACHO CONTRALOR', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (21, 3, 1, 'OFICINA D/ATENC.AL PUBLICO Y PARTIC.CIUD', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (22, 3, 1, 'OFIC. D/SEGUR., TRANSP. Y SERV. GRALES.', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (23, 3, 1, 'OBREROS FIJOS', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (24, 4, 1, 'DIVISION GENERAL DE RECURSOS HUMANOS', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (25, 4, 1, 'OBREROS FIJOS', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (26, 5, 1, 'DIVISION DE ADMINISTRACION', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (27, 6, 1, 'OFIC.DE SISTEMAS E INFORMACION DIGITAL', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (28, 6, 1, 'DIV.D/ CONTROL DE GESTION Y ORGANIZACION', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (29, 8, 1, 'DIVISION GENERAL DE AUDITORIA DE ESTADO', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (30, 9, 1, 'DIV.GRAL.DEL SECTOR DE DES.ECON.E INFRA', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (31, 9, 1, 'DIV.GRAL.DEL SECTOR DE DESARROLLO SOCIAL', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (32, 10, 1, 'DIVISION GENERAL DE INVESTIGACIONES', 2);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (40, 21, 1, 'DIV.D/PARTICIPACION CIUDADANA Y CAPACIT.', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (41, 22, 1, 'DIVISION DE SEGURIDAD Y TRANSPORTE', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (42, 22, 1, 'DIVISION DE SERVICIOS GENERALES', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (43, 24, 1, 'DIVISION DE ADMINISTRACION DE PERSONAL', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (44, 24, 1, 'DPTO.D/RECLUT.SELEC.EVAL.Y CAPAC.D/PERSO', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (45, 24, 1, 'DEPARTAMENTO DE REMUNERACIONES', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (46, 24, 1, 'DPTO.D/SEGURID,PREVENC.Y CONTROL D/RIESG', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (47, 24, 1, 'DIV.D/IMAGEN CORPORATIVA Y EVENTOS ESPEC', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (48, 26, 1, 'DEPARTAMENTO DE COMPRAS E INVENTARIOS', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (49, 26, 1, 'DEPARTAMENTO D/SERVICIOS ADMINISTRATIVOS', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (50, 26, 1, 'UNIDAD DE CONTROL PRESUPUESTARIO', 3);
INSERT INTO unidad_organizativa (id, unidad_organizativa_id, institucion_id, nombre, nivel) VALUES (51, 27, 1, 'DIVISION DE DIGITALIZACION Y SOPORTE', 3);


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--



--
-- Data for Name: variable; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO "variable" (id, nombre, descripcion, tipo, sql, tipo_gestor_sql, fecha_inicia, fecha_fin) VALUES
  (2, 'fecha_ingreso', 'Fecha de ingreso de la persona a la institución como empleado.', 'f', 'SELECT il.fechaIngreso \r\n\r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nWHERE il.personal=:personal_cedula', NULL, NULL, NULL),
  (1, 'sueldo_basico_mensual', 'Sueldo básico devengado por el empleado', 'f', 'SELECT c.sueldo \r\n\r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nJOIN il.historicoCargos hc\r\n\r\nJOIN hc.fk_informacion_laboral_has_cargo_cargo1 c\r\n\r\nWHERE hc.fechaFin IS null\r\n\r\nAND ( hc.esEncargado = ''f'' OR hc.esEncargado is null)\r\n\r\nAND il.personal=:personal_cedula', NULL, NULL, NULL),
  (4, 'horas__nocturnas_trabajadas', 'Cantidad de horas nocturnas trabajadas', NULL, 'SELECT dg.valor\r\n\r\nFROM Cet\\NominaBundle\\Entity\\DatosGenericos  dg \r\n\r\nJOIN dg.fk_datosGenericos_personal1 p \r\n\r\nJOIN dg.fk_dato_generico_tipo_dato_generico1 tdg\r\n\r\nWHERE tdg.nombre like ''Horas Nocturnas Trabajadas''\r\n\r\nAND dg.fk_datosGenericos_personal1=:personal_cedula', NULL, NULL, NULL),
  (5, 'dias_feriados_trabajados', 'Cantidad de días feriados trabajados', NULL, 'SELECT dg.valor\r\n\r\nFROM Cet\\NominaBundle\\Entity\\DatosGenericos  dg \r\n\r\nJOIN dg.fk_datosGenericos_personal1 p \r\n\r\nJOIN dg.fk_dato_generico_tipo_dato_generico1 tdg\r\n\r\nWHERE tdg.nombre like ''Días Feriados Trabajados''\r\n\r\nAND dg.fk_datosGenericos_personal1=:personal_cedula', NULL, NULL, NULL),
  (6, 'dias_otros_organismos', 'Dias de servicio en otros Organismos Públicos', NULL, 'SELECT sum(el.fechaEgreso-el.fechaIngreso)\r\n\r\nFROM Cet\\NominaBundle\\Entity\\ExperienciaLaboral el\r\n\r\nJOIN el.fk_experiencia_laboral_personal1 p\r\n\r\nWHERE el.tipo = ''A''\r\n\r\nAND p.id=:personal_cedula', NULL, NULL, NULL),
  (7, 'dias_servicio_contraloria', 'Días de servicio en la Contraloría del Estado Táchira', NULL, 'SELECT CURRENT_DATE()-il.fechaIngreso \r\n\r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nWHERE il.personal=:personal_cedula', NULL, NULL, NULL),
  (3, 'sueldo_basico_mensual_encargado', 'Sueldo Básico mensual por el mayor cargo encargado', NULL, 'SELECT max(c.sueldo) \r\n\r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nJOIN il.historicoCargos hc \r\n\r\nJOIN hc.fk_informacion_laboral_has_cargo_cargo1 c \r\n\r\nWHERE (hc.fechaFin IS null \r\n\r\nOR hc.fechaFin > CURRENT_DATE())\r\n\r\nAND hc.esEncargado = ''t'' \r\n\r\nAND il.personal=:personal_cedula\r\n\r\nGROUP BY c.sueldo', NULL, NULL, NULL),
  (8, 'calificacion_desempeno_individual', 'Mantiene el resultado de la Evaluación de Desempeño Individual', NULL, 'SELECT dg.valor\r\n\r\nFROM Cet\\NominaBundle\\Entity\\DatosGenericos dg\r\n\r\nJOIN dg.fk_datosGenericos_personal1 p\r\n\r\nJOIN dg.fk_dato_generico_tipo_dato_generico1 tdg\r\n\r\nWHERE tdg.nombre LIKE ''Calificación de Desempeño Individual''\r\n\r\nAND p.id = :personal_cedula', NULL, NULL, NULL),
  (11, 'nombre_cargo_encargado', 'Define el nombre del cargo actual del empleado como encargado', NULL, 'SELECT c.nombre FROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\nJOIN il.historicoCargos hc \r\nJOIN hc.fk_informacion_laboral_has_cargo_cargo1 c \r\nWHERE hc.fechaFin IS null AND hc.esEncargado = ''t'' \r\nAND il.personal=:personal_cedula\r\nAND c.sueldo IN\r\n( SELECT max(c2.sueldo) \r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il2\r\nJOIN il2.historicoCargos hc2 \r\nJOIN hc2.fk_informacion_laboral_has_cargo_cargo1 c2 \r\nWHERE hc2.fechaFin IS null \r\nOR hc2.fechaFin > CURRENT_DATE()\r\nAND hc2.esEncargado = ''t'' \r\nAND il2.personal=:personal_cedula\r\nGROUP BY c2.sueldo )', NULL, NULL, NULL),
  (9, 'nombre_cargo', 'Define el nombre del cargo actual del empleado', NULL, 'SELECT c.nombre FROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nJOIN il.historicoCargos hc \r\n\r\nJOIN hc.fk_informacion_laboral_has_cargo_cargo1 c \r\n\r\nWHERE hc.fechaFin IS null AND ( hc.esEncargado = ''f'' OR hc.esEncargado IS null ) \r\n\r\nAND il.personal=:personal_cedula', NULL, NULL, NULL),
  (12, 'unidad_organizativa_empleado', 'Define la unidad organizativa actual del empleado.', NULL, 'SELECT uo.nombre \r\n\r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nJOIN il.historicoUnidadOrganizativas huo\r\n\r\nJOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo \r\n\r\nWHERE huo.fechaFin IS null \r\n\r\nAND il.personal=:personal_cedula', NULL, NULL, NULL),
  (13, 'estudio_funcionario', 'Nivel académico del funcionario', NULL, 'SELECT CASE e.tipo \r\n\r\nWHEN ''3'' THEN ''TSU''\r\n\r\nWHEN ''4'' THEN ''Universitario''\r\n\r\nWHEN ''5'' THEN ''Diplomado''\r\n\r\nWHEN ''6'' THEN ''Especialización''\r\n\r\nWHEN ''7'' THEN ''Maestría''\r\n\r\nWHEN ''8'' THEN ''Doctorado''\r\n\r\nELSE ''Sin informacion'' END\r\n\r\nFROM Cet\\NominaBundle\\Entity\\Estudio e\r\n\r\nWHERE e.tipo IN \r\n\r\n(\r\n\r\nSELECT MAX(e2.tipo)\r\n\r\nFROM Cet\\NominaBundle\\Entity\\Estudio e2\r\n\r\nWHERE e2.estado = ''T''\r\n\r\nAND e2.fk_estudios_personal1 = :personal_cedula\r\n\r\n)\r\n\r\nAND e.fk_estudios_personal1 = :personal_cedula', NULL, NULL, NULL),
  (15, 'unidad_organizativa_raiz_empleado', 'Define la unidad organizativa raíz del empleado', NULL, 'SELECT uo2.nombre\r\n\r\nFROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nJOIN il.historicoUnidadOrganizativas huo \r\n\r\nJOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo \r\n\r\nLEFT JOIN uo.fk_unidad_organizativa_unidad_organizativa1 uo1\r\n\r\nLEFT JOIN uo1.fk_unidad_organizativa_unidad_organizativa1 uo2\r\n\r\nLEFT JOIN uo2.fk_unidad_organizativa_unidad_organizativa1 uo3\r\n\r\nLEFT JOIN uo3.fk_unidad_organizativa_unidad_organizativa1 uo4\r\n\r\nWHERE huo.fechaFin IS NULL \r\n\r\nAND il.personal=:personal_cedula', NULL, NULL, NULL),
  (16, 'nivel_cargo', 'Escalafón del cargo actual del funcionario', NULL, 'SELECT c.nivel FROM Cet\\NominaBundle\\Entity\\InformacionLaboral il \r\n\r\nJOIN il.historicoCargos hc \r\n\r\nJOIN hc.fk_informacion_laboral_has_cargo_cargo1 c \r\n\r\nWHERE hc.fechaFin IS null AND hc.esEncargado = ''f'' \r\n\r\nAND il.personal=:personal_cedula', NULL, NULL, NULL),
  (17, 'porcentaje_retencion_islr', 'Porcentaje de retención sobre el sueldo básico por funcionario.', NULL, 'SELECT dg.valor\r\n\r\nFROM Cet\\NominaBundle\\Entity\\DatosGenericos  dg \r\n\r\nJOIN dg.fk_datosGenericos_personal1 p \r\n\r\nJOIN dg.fk_dato_generico_tipo_dato_generico1 tdg\r\n\r\nWHERE tdg.nombre like ''Porcentaje de Retención de ISLR''\r\n\r\nAND dg.fk_datosGenericos_personal1=:personal_cedula', NULL, NULL, NULL),
  (18, 'cantidad_lunes', 'Cantidad de lunes', NULL, 'SELECT 0 FROM Cet\\NominaBundle\\Entity\\CargaFamiliar cf WHERE cf.fk_carga_familiar_personal1 = :personal_cedula', NULL, NULL, NULL),
  (19, 'sueldo_minimo', 'Sueldo mínimo vigente', NULL, 'SELECT sm.valor\r\n\r\nFROM Cet\\NominaBundle\\Entity\\DatosGlobales sm\r\n\r\nWHERE sm.nombre LIKE ''Sueldo Mínimo'' \r\n\r\nAND :personal_cedula = :personal_cedula', NULL, NULL, NULL),
  (20, 'valor_unidad_tributaria', 'Valor en bolívares de la unidad tribuitaria', NULL, 'SELECT sm.valor\r\n\r\nFROM Cet\\NominaBundle\\Entity\\DatosGlobales sm\r\n\r\nWHERE sm.nombre LIKE ''Valor Unidad Tributaria'' \r\n\r\nAND :personal_cedula = :personal_cedula', NULL, NULL, NULL),
  (14, 'cantidad_hijos', 'Cantidad de hijos por funcionario', NULL, 'SELECT COUNT(cf.id)\r\nFROM Cet\\NominaBundle\\Entity\\CargaFamiliar cf\r\nWHERE cf.parentesco = ''H''\r\nAND \r\n(\r\ncf.fechaNacimiento > DATE_SUB( CURRENT_DATE() , 216 , ''month'' )\r\nOR \r\n(cf.fechaNacimiento > DATE_SUB( CURRENT_DATE() , 252 , ''month'' ) AND cf.estudia = ''S'' )\r\n)\r\nAND cf.fk_carga_familiar_personal1 = :personal_cedula', NULL, NULL, NULL),
  (21, 'coordinacion_ega', 'Devuelve "Verdadero" si el funcionario tiene la prima por coordinación EGA.', NULL, 'SELECT dg.valor FROM Cet\\NominaBundle\\Entity\\DatosGenericos dg JOIN dg.fk_datosGenericos_personal1 p JOIN dg.fk_dato_generico_tipo_dato_generico1 tdg WHERE tdg.nombre like ''Coordinación EGA'' AND dg.fk_datosGenericos_personal1=:personal_cedula', NULL, NULL, NULL);

  
--
-- Data for Name: vehiculo; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO vehiculo (id, personal_cedula, placa, modelo, anio) VALUES (1, 14417996, 'AB255KS', 'FIESTA POWER', 2008);
INSERT INTO vehiculo (id, personal_cedula, placa, modelo, anio) VALUES (2, 17179288, 'AD4133', 'Turpial', 2013);


--
-- Data for Name: vivienda; Type: TABLE DATA; Schema: sis_nomina; Owner: eduardo
--

INSERT INTO vivienda (id, parroquia_id, personal_cedula, condicion, direccion, codigo_postal, referencia) VALUES (3, 827, 17179288, 'P', 'Carrera 1 con calle 0 casa 0-129 San rafael de Cordero Parte Baja', '5001', 'Detras de la estación de servicio.');
INSERT INTO vivienda (id, parroquia_id, personal_cedula, condicion, direccion, codigo_postal, referencia) VALUES (2, 838, 14417996, 'P', 'CALLE SUCRE, CON CALLE PABLO SANTO PALMIRA', '5001', 'ASILO HERMANAS NAZARETH / LICEO MAICA');
INSERT INTO vivienda (id, parroquia_id, personal_cedula, condicion, direccion, codigo_postal, referencia) VALUES (4, 827, 9247506, 'P', 'Capachito via el Junco Urb. Sueño Dorado Casa No. 70', '5001', 'Urbanización de los Guardias');


--
-- Name: archivo_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY archivo
    ADD CONSTRAINT archivo_pkey PRIMARY KEY (id);


--
-- Name: base_legal_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY base_legal
    ADD CONSTRAINT base_legal_pkey PRIMARY KEY (id);


--
-- Name: carga_familiar_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY carga_familiar
    ADD CONSTRAINT carga_familiar_pkey PRIMARY KEY (id);


--
-- Name: cargo_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY (id);


--
-- Name: concepto_has_concepto_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY concepto_has_concepto
    ADD CONSTRAINT concepto_has_concepto_pkey PRIMARY KEY (concepto_id, concepto_id1);


--
-- Name: concepto_has_plantilla_nomina1_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY concepto_has_plantilla_nomina1
    ADD CONSTRAINT concepto_has_plantilla_nomina1_pkey PRIMARY KEY (plantilla_nomina_id, concepto_id);


--
-- Name: concepto_has_plantilla_nomina_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY concepto_has_plantilla_nomina
    ADD CONSTRAINT concepto_has_plantilla_nomina_pkey PRIMARY KEY (concepto_id, plantilla_nomina_id, personal_cedula);


--
-- Name: concepto_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY concepto
    ADD CONSTRAINT concepto_pkey PRIMARY KEY (id);


--
-- Name: correo_personal_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY correo_personal
    ADD CONSTRAINT correo_personal_pkey PRIMARY KEY (id);


--
-- Name: datos_genericos_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY datos_genericos
    ADD CONSTRAINT datos_genericos_pkey PRIMARY KEY (id);


--
-- Name: datos_globales_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY datos_globales
    ADD CONSTRAINT datos_globales_pkey PRIMARY KEY (id);


--
-- Name: datos_variables_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY datos_variables
    ADD CONSTRAINT datos_variables_pkey PRIMARY KEY (id);


--
-- Name: enfermedad_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY enfermedad
    ADD CONSTRAINT enfermedad_pkey PRIMARY KEY (id);


--
-- Name: estado_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- Name: estudio_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY estudio
    ADD CONSTRAINT estudio_pkey PRIMARY KEY (id);


--
-- Name: experiencia_laboral_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY experiencia_laboral
    ADD CONSTRAINT experiencia_laboral_pkey PRIMARY KEY (id);


--
-- Name: formula_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY formula
    ADD CONSTRAINT formula_pkey PRIMARY KEY (variable_id, concepto_id);


--
-- Name: historico_cargo_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY historico_cargo
    ADD CONSTRAINT historico_cargo_pkey PRIMARY KEY (id);


--
-- Name: historico_concepto_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY historico_concepto
    ADD CONSTRAINT historico_concepto_pkey PRIMARY KEY (id);


--
-- Name: historico_evento_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY historico_evento
    ADD CONSTRAINT historico_evento_pkey PRIMARY KEY (id);


--
-- Name: historico_unidad_organizativa_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY historico_unidad_organizativa
    ADD CONSTRAINT historico_unidad_organizativa_pkey PRIMARY KEY (id);


--
-- Name: historico_variable_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY historico_variable
    ADD CONSTRAINT historico_variable_pkey PRIMARY KEY (id);


--
-- Name: informacion_cultural_deportiva_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY informacion_cultural_deportiva
    ADD CONSTRAINT informacion_cultural_deportiva_pkey PRIMARY KEY (id);


--
-- Name: informacion_laboral_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT informacion_laboral_pkey PRIMARY KEY (id);


--
-- Name: informacion_medica_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY informacion_medica
    ADD CONSTRAINT informacion_medica_pkey PRIMARY KEY (id);


--
-- Name: institucion_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY institucion
    ADD CONSTRAINT institucion_pkey PRIMARY KEY (id);


--
-- Name: municipio_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (id);


--
-- Name: nomina_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY nomina
    ADD CONSTRAINT nomina_pkey PRIMARY KEY (id);


--
-- Name: parroquia_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY parroquia
    ADD CONSTRAINT parroquia_pkey PRIMARY KEY (id);


--
-- Name: personal_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY personal
    ADD CONSTRAINT personal_pkey PRIMARY KEY (cedula);


--
-- Name: plantilla_nomina_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY plantilla_nomina
    ADD CONSTRAINT plantilla_nomina_pkey PRIMARY KEY (id);


--
-- Name: reporte_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY reporte
    ADD CONSTRAINT reporte_pkey PRIMARY KEY (id);


--
-- Name: respaldo_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY respaldo
    ADD CONSTRAINT respaldo_pkey PRIMARY KEY (id);


--
-- Name: telefono_personal_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY telefono_personal
    ADD CONSTRAINT telefono_personal_pkey PRIMARY KEY (id);


--
-- Name: tipo_base_legal_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY tipo_base_legal
    ADD CONSTRAINT tipo_base_legal_pkey PRIMARY KEY (id);


--
-- Name: tipo_dato_generico_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY tipo_dato_generico
    ADD CONSTRAINT tipo_dato_generico_pkey PRIMARY KEY (id);


--
-- Name: tipo_nomina_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY tipo_nomina
    ADD CONSTRAINT tipo_nomina_pkey PRIMARY KEY (id);


--
-- Name: unidad_organizativa_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY unidad_organizativa
    ADD CONSTRAINT unidad_organizativa_pkey PRIMARY KEY (id);


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- Name: variable_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY variable
    ADD CONSTRAINT variable_pkey PRIMARY KEY (id);


--
-- Name: vehiculo_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY vehiculo
    ADD CONSTRAINT vehiculo_pkey PRIMARY KEY (id);


--
-- Name: vivienda_pkey; Type: CONSTRAINT; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

ALTER TABLE ONLY vivienda
    ADD CONSTRAINT vivienda_pkey PRIMARY KEY (id);


--
-- Name: id_unique; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX id_unique ON informacion_laboral USING btree (id);


--
-- Name: idx_155b074d1ee3f92b; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_155b074d1ee3f92b ON nomina USING btree (plantilla_nomina_id);


--
-- Name: idx_1b2f33d66c2330bd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_1b2f33d66c2330bd ON formula USING btree (concepto_id);


--
-- Name: idx_1b2f33d6f3037e8e; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_1b2f33d6f3037e8e ON formula USING btree (variable_id);


--
-- Name: idx_3c89cc3f8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_3c89cc3f8f7f77f7 ON vehiculo USING btree (personal_cedula);


--
-- Name: idx_3e5093c98f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_3e5093c98f7f77f7 ON informacion_cultural_deportiva USING btree (personal_cedula);


--
-- Name: idx_406735f48f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_406735f48f7f77f7 ON historico_variable USING btree (personal_cedula);


--
-- Name: idx_406735f4f3037e8e; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_406735f4f3037e8e ON historico_variable USING btree (variable_id);


--
-- Name: idx_455ca6878f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_455ca6878f7f77f7 ON carga_familiar USING btree (personal_cedula);


--
-- Name: idx_4937dfd55b7959e0; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_4937dfd55b7959e0 ON archivo USING btree (respaldo_id);


--
-- Name: idx_4bbce6c428e3ccff; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_4bbce6c428e3ccff ON base_legal USING btree (tipo_base_legal_id);


--
-- Name: idx_55c60fd38f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_55c60fd38f7f77f7 ON datos_genericos USING btree (personal_cedula);


--
-- Name: idx_55c60fd3f92ddc11; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_55c60fd3f92ddc11 ON datos_genericos USING btree (tipo_dato_generico_id);


--
-- Name: idx_5ed99b3d1ee3f92b; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_5ed99b3d1ee3f92b ON concepto_has_plantilla_nomina1 USING btree (plantilla_nomina_id);


--
-- Name: idx_5ed99b3d6c2330bd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_5ed99b3d6c2330bd ON concepto_has_plantilla_nomina1 USING btree (concepto_id);


--
-- Name: idx_69ef5d705a5ada57; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_69ef5d705a5ada57 ON historico_cargo USING btree (informacion_laboral_id);


--
-- Name: idx_69ef5d707373b779; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_69ef5d707373b779 ON historico_cargo USING btree (unidad_organizativa_id);


--
-- Name: idx_69ef5d70813ac380; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_69ef5d70813ac380 ON historico_cargo USING btree (cargo_id);


--
-- Name: idx_6c7716d7366003b6; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_6c7716d7366003b6 ON concepto_has_concepto USING btree (concepto_id1);


--
-- Name: idx_6c7716d76c2330bd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_6c7716d76c2330bd ON concepto_has_concepto USING btree (concepto_id);


--
-- Name: idx_7064efd45a5ada57; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_7064efd45a5ada57 ON historico_unidad_organizativa USING btree (informacion_laboral_id);


--
-- Name: idx_7064efd47373b779; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_7064efd47373b779 ON historico_unidad_organizativa USING btree (unidad_organizativa_id);


--
-- Name: idx_78a5636b7373b779; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_78a5636b7373b779 ON unidad_organizativa USING btree (unidad_organizativa_id);


--
-- Name: idx_78a5636bb239fbc6; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_78a5636bb239fbc6 ON unidad_organizativa USING btree (institucion_id);


--
-- Name: idx_79d579438261ca50; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_79d579438261ca50 ON reporte USING btree (nomina_id);


--
-- Name: idx_7fd7cd968f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_7fd7cd968f7f77f7 ON experiencia_laboral USING btree (personal_cedula);


--
-- Name: idx_808436f1ee3f92b; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_808436f1ee3f92b ON concepto_has_plantilla_nomina USING btree (plantilla_nomina_id);


--
-- Name: idx_808436f6c2330bd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_808436f6c2330bd ON concepto_has_plantilla_nomina USING btree (concepto_id);


--
-- Name: idx_808436f8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_808436f8f7f77f7 ON concepto_has_plantilla_nomina USING btree (personal_cedula);


--
-- Name: idx_8ca5aa44600f751e; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_8ca5aa44600f751e ON informacion_laboral USING btree (tipo_nomina_id);


--
-- Name: idx_a780142e8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_a780142e8f7f77f7 ON correo_personal USING btree (personal_cedula);


--
-- Name: idx_ba2476ae8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_ba2476ae8f7f77f7 ON telefono_personal USING btree (personal_cedula);


--
-- Name: idx_bc79a43e6c2330bd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_bc79a43e6c2330bd ON datos_variables USING btree (concepto_id);


--
-- Name: idx_bc79a43e8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_bc79a43e8f7f77f7 ON datos_variables USING btree (personal_cedula);


--
-- Name: idx_c315717e8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_c315717e8f7f77f7 ON estudio USING btree (personal_cedula);


--
-- Name: idx_c3d193558bc1be0; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_c3d193558bc1be0 ON parroquia USING btree (municipio_id);


--
-- Name: idx_c84d0410fad7f811; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_c84d0410fad7f811 ON enfermedad USING btree (informacion_medica_id);


--
-- Name: idx_d102fabd9f5a440b; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_d102fabd9f5a440b ON municipio USING btree (estado_id);


--
-- Name: idx_d3faeda2600f751e; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_d3faeda2600f751e ON plantilla_nomina USING btree (tipo_nomina_id);


--
-- Name: idx_d88c71e274afdc17; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_d88c71e274afdc17 ON vivienda USING btree (parroquia_id);


--
-- Name: idx_e8a93aa96c2330bd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_e8a93aa96c2330bd ON historico_concepto USING btree (concepto_id);


--
-- Name: idx_e8a93aa98261ca50; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_e8a93aa98261ca50 ON historico_concepto USING btree (nomina_id);


--
-- Name: idx_e8a93aa98f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE INDEX idx_e8a93aa98f7f77f7 ON historico_concepto USING btree (personal_cedula);


--
-- Name: uniq_355fb20f28509fa1; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_355fb20f28509fa1 ON informacion_medica USING btree (base_legal_id);


--
-- Name: uniq_355fb20f8f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_355fb20f8f7f77f7 ON informacion_medica USING btree (personal_cedula);


--
-- Name: uniq_393e5db13a909126; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_393e5db13a909126 ON variable USING btree (nombre);


--
-- Name: uniq_393e5db1a02a2f00; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_393e5db1a02a2f00 ON variable USING btree (descripcion);


--
-- Name: uniq_3c89cc3f737097d4; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_3c89cc3f737097d4 ON vehiculo USING btree (placa);


--
-- Name: uniq_455ca68728509fa1; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_455ca68728509fa1 ON carga_familiar USING btree (base_legal_id);


--
-- Name: uniq_4bbce6c428509fa1; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_4bbce6c428509fa1 ON base_legal USING btree (base_legal_id);


--
-- Name: uniq_7fd7cd9628509fa1; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_7fd7cd9628509fa1 ON experiencia_laboral USING btree (base_legal_id);


--
-- Name: uniq_8ca5aa4428509fa1; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8ca5aa4428509fa1 ON informacion_laboral USING btree (base_legal_id);


--
-- Name: uniq_8ca5aa448f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8ca5aa448f7f77f7 ON informacion_laboral USING btree (personal_cedula);


--
-- Name: uniq_8ca5aa44a83e1f1a; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8ca5aa44a83e1f1a ON informacion_laboral USING btree (cuenta_fideicomiso);


--
-- Name: uniq_8ca5aa44ec87e5cd; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_8ca5aa44ec87e5cd ON informacion_laboral USING btree (cuenta_nomina);


--
-- Name: uniq_91f052eca8255881; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_91f052eca8255881 ON concepto USING btree (identificador);


--
-- Name: uniq_c315717e28509fa1; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_c315717e28509fa1 ON estudio USING btree (base_legal_id);


--
-- Name: uniq_cd051f128261ca50; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_cd051f128261ca50 ON respaldo USING btree (nomina_id);


--
-- Name: uniq_d88c71e28f7f77f7; Type: INDEX; Schema: sis_nomina; Owner: eduardo; Tablespace: 
--

CREATE UNIQUE INDEX uniq_d88c71e28f7f77f7 ON vivienda USING btree (personal_cedula);


--
-- Name: fk_155b074d1ee3f92b; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY nomina
    ADD CONSTRAINT fk_155b074d1ee3f92b FOREIGN KEY (plantilla_nomina_id) REFERENCES plantilla_nomina(id);


--
-- Name: fk_1b2f33d66c2330bd; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY formula
    ADD CONSTRAINT fk_1b2f33d66c2330bd FOREIGN KEY (concepto_id) REFERENCES concepto(id);


--
-- Name: fk_1b2f33d6f3037e8e; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY formula
    ADD CONSTRAINT fk_1b2f33d6f3037e8e FOREIGN KEY (variable_id) REFERENCES variable(id);


--
-- Name: fk_355fb20f28509fa1; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY informacion_medica
    ADD CONSTRAINT fk_355fb20f28509fa1 FOREIGN KEY (base_legal_id) REFERENCES base_legal(id);


--
-- Name: fk_355fb20f8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY informacion_medica
    ADD CONSTRAINT fk_355fb20f8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_3c89cc3f8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY vehiculo
    ADD CONSTRAINT fk_3c89cc3f8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_3e5093c98f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY informacion_cultural_deportiva
    ADD CONSTRAINT fk_3e5093c98f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_406735f48f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_variable
    ADD CONSTRAINT fk_406735f48f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_406735f4f3037e8e; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_variable
    ADD CONSTRAINT fk_406735f4f3037e8e FOREIGN KEY (variable_id) REFERENCES variable(id);


--
-- Name: fk_455ca68728509fa1; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY carga_familiar
    ADD CONSTRAINT fk_455ca68728509fa1 FOREIGN KEY (base_legal_id) REFERENCES base_legal(id);


--
-- Name: fk_455ca6878f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY carga_familiar
    ADD CONSTRAINT fk_455ca6878f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_4937dfd55b7959e0; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY archivo
    ADD CONSTRAINT fk_4937dfd55b7959e0 FOREIGN KEY (respaldo_id) REFERENCES respaldo(id);


--
-- Name: fk_4bbce6c428e3ccff; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY base_legal
    ADD CONSTRAINT fk_4bbce6c428e3ccff FOREIGN KEY (tipo_base_legal_id) REFERENCES tipo_base_legal(id);


--
-- Name: fk_55c60fd38f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY datos_genericos
    ADD CONSTRAINT fk_55c60fd38f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_55c60fd3f92ddc11; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY datos_genericos
    ADD CONSTRAINT fk_55c60fd3f92ddc11 FOREIGN KEY (tipo_dato_generico_id) REFERENCES tipo_dato_generico(id);


--
-- Name: fk_5ed99b3d1ee3f92b; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_plantilla_nomina1
    ADD CONSTRAINT fk_5ed99b3d1ee3f92b FOREIGN KEY (plantilla_nomina_id) REFERENCES plantilla_nomina(id);


--
-- Name: fk_5ed99b3d6c2330bd; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_plantilla_nomina1
    ADD CONSTRAINT fk_5ed99b3d6c2330bd FOREIGN KEY (concepto_id) REFERENCES concepto(id);


--
-- Name: fk_69ef5d705a5ada57; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_cargo
    ADD CONSTRAINT fk_69ef5d705a5ada57 FOREIGN KEY (informacion_laboral_id) REFERENCES informacion_laboral(id);


--
-- Name: fk_69ef5d707373b779; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_cargo
    ADD CONSTRAINT fk_69ef5d707373b779 FOREIGN KEY (unidad_organizativa_id) REFERENCES unidad_organizativa(id);


--
-- Name: fk_69ef5d70813ac380; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_cargo
    ADD CONSTRAINT fk_69ef5d70813ac380 FOREIGN KEY (cargo_id) REFERENCES cargo(id);


--
-- Name: fk_6c7716d7366003b6; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_concepto
    ADD CONSTRAINT fk_6c7716d7366003b6 FOREIGN KEY (concepto_id1) REFERENCES concepto(id);


--
-- Name: fk_6c7716d76c2330bd; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_concepto
    ADD CONSTRAINT fk_6c7716d76c2330bd FOREIGN KEY (concepto_id) REFERENCES concepto(id);


--
-- Name: fk_7064efd45a5ada57; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_unidad_organizativa
    ADD CONSTRAINT fk_7064efd45a5ada57 FOREIGN KEY (informacion_laboral_id) REFERENCES informacion_laboral(id);


--
-- Name: fk_7064efd47373b779; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_unidad_organizativa
    ADD CONSTRAINT fk_7064efd47373b779 FOREIGN KEY (unidad_organizativa_id) REFERENCES unidad_organizativa(id);


--
-- Name: fk_78a5636b7373b779; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY unidad_organizativa
    ADD CONSTRAINT fk_78a5636b7373b779 FOREIGN KEY (unidad_organizativa_id) REFERENCES unidad_organizativa(id);


--
-- Name: fk_78a5636bb239fbc6; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY unidad_organizativa
    ADD CONSTRAINT fk_78a5636bb239fbc6 FOREIGN KEY (institucion_id) REFERENCES institucion(id);


--
-- Name: fk_79d579438261ca50; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY reporte
    ADD CONSTRAINT fk_79d579438261ca50 FOREIGN KEY (nomina_id) REFERENCES nomina(id);


--
-- Name: fk_7fd7cd9628509fa1; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY experiencia_laboral
    ADD CONSTRAINT fk_7fd7cd9628509fa1 FOREIGN KEY (base_legal_id) REFERENCES base_legal(id);


--
-- Name: fk_7fd7cd968f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY experiencia_laboral
    ADD CONSTRAINT fk_7fd7cd968f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_808436f1ee3f92b; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_plantilla_nomina
    ADD CONSTRAINT fk_808436f1ee3f92b FOREIGN KEY (plantilla_nomina_id) REFERENCES plantilla_nomina(id);


--
-- Name: fk_808436f6c2330bd; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_plantilla_nomina
    ADD CONSTRAINT fk_808436f6c2330bd FOREIGN KEY (concepto_id) REFERENCES concepto(id);


--
-- Name: fk_808436f8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY concepto_has_plantilla_nomina
    ADD CONSTRAINT fk_808436f8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_8ca5aa4428509fa1; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT fk_8ca5aa4428509fa1 FOREIGN KEY (base_legal_id) REFERENCES base_legal(id);


--
-- Name: fk_8ca5aa44600f751e; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT fk_8ca5aa44600f751e FOREIGN KEY (tipo_nomina_id) REFERENCES tipo_nomina(id);


--
-- Name: fk_8ca5aa448f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY informacion_laboral
    ADD CONSTRAINT fk_8ca5aa448f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_a780142e8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY correo_personal
    ADD CONSTRAINT fk_a780142e8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_ba2476ae8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY telefono_personal
    ADD CONSTRAINT fk_ba2476ae8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_bc79a43e6c2330bd; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY datos_variables
    ADD CONSTRAINT fk_bc79a43e6c2330bd FOREIGN KEY (concepto_id) REFERENCES concepto(id) ON DELETE CASCADE;


--
-- Name: fk_bc79a43e8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY datos_variables
    ADD CONSTRAINT fk_bc79a43e8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_c315717e28509fa1; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY estudio
    ADD CONSTRAINT fk_c315717e28509fa1 FOREIGN KEY (base_legal_id) REFERENCES base_legal(id);


--
-- Name: fk_c315717e8f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY estudio
    ADD CONSTRAINT fk_c315717e8f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_c3d193558bc1be0; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY parroquia
    ADD CONSTRAINT fk_c3d193558bc1be0 FOREIGN KEY (municipio_id) REFERENCES municipio(id);


--
-- Name: fk_c84d0410fad7f811; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY enfermedad
    ADD CONSTRAINT fk_c84d0410fad7f811 FOREIGN KEY (informacion_medica_id) REFERENCES informacion_medica(id);


--
-- Name: fk_cd051f128261ca50; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY respaldo
    ADD CONSTRAINT fk_cd051f128261ca50 FOREIGN KEY (nomina_id) REFERENCES nomina(id);


--
-- Name: fk_d102fabd9f5a440b; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY municipio
    ADD CONSTRAINT fk_d102fabd9f5a440b FOREIGN KEY (estado_id) REFERENCES estado(id);


--
-- Name: fk_d3faeda2600f751e; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY plantilla_nomina
    ADD CONSTRAINT fk_d3faeda2600f751e FOREIGN KEY (tipo_nomina_id) REFERENCES tipo_nomina(id);


--
-- Name: fk_d88c71e274afdc17; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY vivienda
    ADD CONSTRAINT fk_d88c71e274afdc17 FOREIGN KEY (parroquia_id) REFERENCES parroquia(id);


--
-- Name: fk_d88c71e28f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY vivienda
    ADD CONSTRAINT fk_d88c71e28f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- Name: fk_e8a93aa96c2330bd; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_concepto
    ADD CONSTRAINT fk_e8a93aa96c2330bd FOREIGN KEY (concepto_id) REFERENCES concepto(id);


--
-- Name: fk_e8a93aa98261ca50; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_concepto
    ADD CONSTRAINT fk_e8a93aa98261ca50 FOREIGN KEY (nomina_id) REFERENCES nomina(id);


--
-- Name: fk_e8a93aa98f7f77f7; Type: FK CONSTRAINT; Schema: sis_nomina; Owner: eduardo
--

ALTER TABLE ONLY historico_concepto
    ADD CONSTRAINT fk_e8a93aa98f7f77f7 FOREIGN KEY (personal_cedula) REFERENCES personal(cedula);


--
-- PostgreSQL database dump complete
--

