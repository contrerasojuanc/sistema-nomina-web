<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HistoricoUnidadOrganizativaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaInicio','date', array(
                'widget' => 'single_text',
            ))
            ->add('fechaFin','date', array(
                'widget' => 'single_text',
            ))
            ->add('fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1')
            ->add('fk_unidad_organizativa_has_informacion_laboral_informacion_la1')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_historicounidadorganizativa';
    }
}
