<?php

namespace Cet\NominaBundle\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\PrestacionesSocialesGeneral
 *
 * @ORM\Entity(repositoryClass="PrestacionesSocialesGeneralRepository")
 * @ORM\Table(name="sis_nomina.prestaciones_sociales_general")
 */
class PrestacionesSocialesGeneral
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="TipoNomina", inversedBy="prestaciones")
     * @ORM\JoinColumn(name="tipo_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_prestaciones_tipo_nomina1;
    
    /**
     * @ORM\OneToMany(targetEntity="PrestacionesSociales", mappedBy="general")
     * @ORM\JoinColumn(name="id", referencedColumnName="id", nullable=false)
     */
    protected $prestaciones;
    
     /**
     * @ORM\OneToOne(targetEntity="Nomina", inversedBy="prestacionesSocialesGeneral")
     * @ORM\JoinColumn(name="nomina_id", referencedColumnName="id", nullable=true)
     */
    protected $fk_nomina_id;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return PrestacionesSocialesGeneral
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return PrestacionesSocialesGeneral
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->prestaciones = new \Doctrine\Common\Collections\ArrayCollection();
    }
 
    public function __toString()
    {
        return $this->getDescripcion();
    }

    /**
     * Add prestaciones
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones
     * @return PrestacionesSocialesGeneral
     */
    public function addPrestacione(\Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones)
    {
        $this->prestaciones[] = $prestaciones;

        return $this;
    }

    /**
     * Remove prestaciones
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones
     */
    public function removePrestacione(\Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones)
    {
        $this->prestaciones->removeElement($prestaciones);
    }

    /**
     * Get prestaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrestaciones()
    {
        return $this->prestaciones;
    }

    /**
     * Set fk_prestaciones_tipo_nomina1
     *
     * @param \Cet\NominaBundle\Entity\TipoNomina $fkPrestacionesTipoNomina1
     * @return PrestacionesSocialesGeneral
     */
    public function setFkPrestacionesTipoNomina1(\Cet\NominaBundle\Entity\TipoNomina $fkPrestacionesTipoNomina1)
    {
        $this->fk_prestaciones_tipo_nomina1 = $fkPrestacionesTipoNomina1;

        return $this;
    }

    /**
     * Get fk_prestaciones_tipo_nomina1
     *
     * @return \Cet\NominaBundle\Entity\TipoNomina 
     */
    public function getFkPrestacionesTipoNomina1()
    {
        return $this->fk_prestaciones_tipo_nomina1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
       $metadata->addPropertyConstraint('fk_prestaciones_tipo_nomina1', new Assert\NotBlank());
        
    }
    
    /**
     * Set fk_nomina_id
     *
     * @param \Cet\NominaBundle\Entity\Nomina $fkNominaId
     * @return PrestacionesSocialesGeneral
     */
    public function setFkNominaId(\Cet\NominaBundle\Entity\Nomina $fkNominaId = null)
    {
        $this->fk_nomina_id = $fkNominaId;

        return $this;
    }

    /**
     * Get fk_nomina_id
     *
     * @return \Cet\NominaBundle\Entity\Nomina 
     */
    public function getFkNominaId()
    {
        return $this->fk_nomina_id;
    }
}
