<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\TelefonoPersonal
 *
 * @ORM\Entity(repositoryClass="TelefonoPersonalRepository")
 * @ORM\Table(name="sis_nomina.telefono_personal", indexes={@ORM\Index(name="fk_telefono_personal_personal_idx", columns={"personal_cedula"})})
 * @UniqueEntity(
 *     fields={"telefono"},
 *     message="Número de telefono ya registrado, verifique"
 * )
 */
class TelefonoPersonal
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="telefono", type="string", length=45, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="telefonoPersonals")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_telefono_personal_personal;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return TelefonoPersonal
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set fk_telefono_personal_personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkTelefonoPersonalPersonal
     * @return TelefonoPersonal
     */
    public function setFkTelefonoPersonalPersonal(\Cet\NominaBundle\Entity\Personal $fkTelefonoPersonalPersonal)
    {
        $this->fk_telefono_personal_personal = $fkTelefonoPersonalPersonal;

        return $this;
    }

    /**
     * Get fk_telefono_personal_personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkTelefonoPersonalPersonal()
    {
        return $this->fk_telefono_personal_personal;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('telefono', new Assert\NotBlank())
                ->addPropertyConstraint ('telefono', new Assert\Type(array('type'=>'digit')))
                ->addPropertyConstraint ('telefono', new Assert\Length(array(
                    'min'=> 11,
                    'max'=> 11,
                    'minMessage'=> 'El Nro de Telefónico debe contener por lo menos 11 digitos.',
                    'maxMessage'=> 'El Nro de Telefónico debe contener por lo menos 11 digitos.',
                    )))             
                
            ;
    }  
}
