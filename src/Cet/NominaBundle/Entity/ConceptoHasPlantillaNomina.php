<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina
 *
 * @ORM\Entity(repositoryClass="ConceptoHasPlantillaNominaRepository")
 * @ORM\Table(name="sis_nomina.concepto_has_plantilla_nomina", indexes={@ORM\Index(name="fk_concepto_has_plantilla_nomina_plantilla_nomina1_idx", columns={"plantilla_nomina_id"}), @ORM\Index(name="fk_concepto_has_plantilla_nomina_concepto1_idx", columns={"concepto_id"}), @ORM\Index(name="fk_concepto_has_plantilla_nomina_personal1_idx", columns={"personal_cedula"})})
 */
class ConceptoHasPlantillaNomina
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="concepto_id", type="integer", nullable=false)
     */
    private $conceptoId;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="plantilla_nomina_id", type="integer", nullable=false)
     */
    private $plantillaNominaId;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="personal_cedula", type="integer", nullable=false)
     */
    private $personalCedula;

    /**
     * @var boolean
     * @ORM\Column(name="activo", type="boolean", nullable=true)
     */
    private $activo;

    /**
     * @ORM\ManyToOne(targetEntity="Concepto", inversedBy="conceptoHasPlantillaNominas")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_concepto_has_plantilla_nomina_concepto1;

    /**
     * @ORM\ManyToOne(targetEntity="PlantillaNomina", inversedBy="conceptoHasPlantillaNominas")
     * @ORM\JoinColumn(name="plantilla_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_concepto_has_plantilla_nomina_plantilla_nomina1;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="conceptoHasPlantillaNominas")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_concepto_has_plantilla_nomina_personal1;

    public function __construct()
    {
    }


    /**
     * Set conceptoId
     *
     * @param integer $conceptoId
     * @return ConceptoHasPlantillaNomina
     */
    public function setConceptoId($conceptoId)
    {
        $this->conceptoId = $conceptoId;

        return $this;
    }

    /**
     * Get conceptoId
     *
     * @return integer 
     */
    public function getConceptoId()
    {
        return $this->conceptoId;
    }

    /**
     * Set plantillaNominaId
     *
     * @param integer $plantillaNominaId
     * @return ConceptoHasPlantillaNomina
     */
    public function setPlantillaNominaId($plantillaNominaId)
    {
        $this->plantillaNominaId = $plantillaNominaId;

        return $this;
    }

    /**
     * Get plantillaNominaId
     *
     * @return integer 
     */
    public function getPlantillaNominaId()
    {
        return $this->plantillaNominaId;
    }

    /**
     * Set personalCedula
     *
     * @param integer $personalCedula
     * @return ConceptoHasPlantillaNomina
     */
    public function setPersonalCedula($personalCedula)
    {
        $this->personalCedula = $personalCedula;

        return $this;
    }

    /**
     * Get personalCedula
     *
     * @return integer 
     */
    public function getPersonalCedula()
    {
        return $this->personalCedula;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     * @return ConceptoHasPlantillaNomina
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fk_concepto_has_plantilla_nomina_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkConceptoHasPlantillaNominaConcepto1
     * @return ConceptoHasPlantillaNomina
     */
    public function setFkConceptoHasPlantillaNominaConcepto1(\Cet\NominaBundle\Entity\Concepto $fkConceptoHasPlantillaNominaConcepto1)
    {
        $this->fk_concepto_has_plantilla_nomina_concepto1 = $fkConceptoHasPlantillaNominaConcepto1;

        return $this;
    }

    /**
     * Get fk_concepto_has_plantilla_nomina_concepto1
     *
     * @return \Cet\NominaBundle\Entity\Concepto 
     */
    public function getFkConceptoHasPlantillaNominaConcepto1()
    {
        return $this->fk_concepto_has_plantilla_nomina_concepto1;
    }

    /**
     * Set fk_concepto_has_plantilla_nomina_plantilla_nomina1
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $fkConceptoHasPlantillaNominaPlantillaNomina1
     * @return ConceptoHasPlantillaNomina
     */
    public function setFkConceptoHasPlantillaNominaPlantillaNomina1(\Cet\NominaBundle\Entity\PlantillaNomina $fkConceptoHasPlantillaNominaPlantillaNomina1)
    {
        $this->fk_concepto_has_plantilla_nomina_plantilla_nomina1 = $fkConceptoHasPlantillaNominaPlantillaNomina1;

        return $this;
    }

    /**
     * Get fk_concepto_has_plantilla_nomina_plantilla_nomina1
     *
     * @return \Cet\NominaBundle\Entity\PlantillaNomina 
     */
    public function getFkConceptoHasPlantillaNominaPlantillaNomina1()
    {
        return $this->fk_concepto_has_plantilla_nomina_plantilla_nomina1;
    }

    /**
     * Set fk_concepto_has_plantilla_nomina_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkConceptoHasPlantillaNominaPersonal1
     * @return ConceptoHasPlantillaNomina
     */
    public function setFkConceptoHasPlantillaNominaPersonal1(\Cet\NominaBundle\Entity\Personal $fkConceptoHasPlantillaNominaPersonal1)
    {
        $this->fk_concepto_has_plantilla_nomina_personal1 = $fkConceptoHasPlantillaNominaPersonal1;

        return $this;
    }

    /**
     * Get fk_concepto_has_plantilla_nomina_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkConceptoHasPlantillaNominaPersonal1()
    {
        return $this->fk_concepto_has_plantilla_nomina_personal1;
    }
    
}
