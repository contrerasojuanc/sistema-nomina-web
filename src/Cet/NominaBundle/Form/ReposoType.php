<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReposoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\Personal");
        $userTransformer->setEntityRepository("CetNominaBundle:Personal");
        $userTransformer->setEntityType("Personal");
        
        $builder
            ->add('fechaInicio', 'date',array('label' => 'Fecha de Inicio','widget' => 'single_text','attr' => array('help'=>'Fecha de inicio de reposo médico')))
            ->add('fechaFinal', 'date',array('label' => 'Fecha de Culminación','widget' => 'single_text','attr' => array('help'=>'Fecha de culminación de reposo médico')))
            ->add('dias')
            ->add('diagnostico')
            //->add('interruptor')
            ->add('medico',null,array('attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione el médico tratante')))
            ->add('especialidad',null,array('attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione la especialidad')))            
//            ->add('personal')
            ->add($builder->create('personal','hidden',array(
                'required'  => false,
            ))->addModelTransformer($userTransformer))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Reposo'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_reposo';
    }
}
