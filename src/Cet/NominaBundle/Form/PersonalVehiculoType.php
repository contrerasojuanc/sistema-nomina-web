<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalVehiculoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('placa',null,array(
                'label'=> 'Placa del vehículo',
                'attr' => array('help'=>'Ingrese el número de la placa del vehículo.'),
                ))
            ->add('modelo',null,array(
                'label'=> 'Modelo del Vehículo',
                'attr' => array('help'=>'Ingrese el modelo del vehículo. Ejemplo:  Aveo.'),
                ))
            ->add('anio',null,array(
                'label'=> 'Año del Vehículo',
                'attr' => array('help'=>'Ingrese el año del vehículo. Ejemplo: 2010.'),
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Vehiculo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_vehiculo';
    }
}
