<?php

namespace Cet\NominaBundle\Helper;

use Doctrine\ORM\EntityManager;

class FuncionesHelper {
    
    private $entityManager;

    public function __construct(EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }
    
    /** 
    * Counts the number occurrences of a certain day of the week between a start and end date
    * The $start and $end variables must be in UTC format or you will get the wrong number 
    * of days  when crossing daylight savings time
    * @param - $day - the day of the week such as "Monday", "Tuesday"...
    * @param - $start - a UTC timestamp representing the start date
    * @param - $end - a UTC timestamp representing the end date
    * @return Number of occurences of $day between $start and $end
    */
    public function countDays($day, $start, $end)
    {        
        //get the day of the week for start and end dates (0-6)
        $w = array(date('w', $start), date('w', $end));

        //get partial week day count
        if ($w[0] < $w[1])
        {            
            $partialWeekCount = ($day >= $w[0] && $day <= $w[1]);
        }else if ($w[0] == $w[1])
        {
            $partialWeekCount = $w[0] == $day;
        }else
        {
            $partialWeekCount = ($day >= $w[0] || $day <= $w[1]);
        }

        //first count the number of complete weeks, then add 1 if $day falls in a partial week.
        return floor( ( $end-$start )/60/60/24/7) + $partialWeekCount;
    }
 
    /** 
    * Obtiene la información del realizador, revisores y aprobador    
    * @param - $em - Entity manager
    * @param - $usuario - El objeto del usuario que genera el documento
    * @return Arreglo contentivo de toda la cadena de revision
    */
    public function obtenerRevisores($em,$usuario)
    { 
        //Bloque para determinar funcionarios y titulos profesionales del Realizador, Revisor y Aprobador        
        
        $tituloProfesionalDirector[0][1] = "";
        $datosDirector['primerNombre'] = "";
        $datosDirector['segundoNombre'] = "";
        $datosDirector['primerApellido'] = "";
        $datosDirector['segundoApellido'] = "";
        $datosDirector['esEncargado'] = false;
        $tituloProfesionalJefe[0][1] = "";
        $datosJefe['primerNombre'] = "";
        $datosJefe['segundoNombre'] = "";
        $datosJefe['primerApellido'] = "";
        $datosJefe['segundoApellido'] = "";
        $datosJefe['esEncargado'] = false;
        $tituloProfesionalJefe2[0][1] = "";
        $datosJefe2['primerNombre'] = "";
        $datosJefe2['segundoNombre'] = "";
        $datosJefe2['primerApellido'] = "";
        $datosJefe2['segundoApellido'] = "";
        $datosJefe2['esEncargado'] = false;
        
        //Realizador
        $query = "SELECT CASE e.tipo "
                . "WHEN '0' THEN 'Br.' "
                . "WHEN '3' THEN 'T.S.U.' "
                . "WHEN '4' THEN ( CASE WHEN "
                    . "LOWER(e.descripcion) LIKE '%ingenier%' THEN 'Ing.' "
                    . "ELSE ( CASE WHEN LOWER(e.descripcion) LIKE '%aboga%' THEN 'Abg.' "
                    . "ELSE ( CASE WHEN LOWER(e.descripcion) LIKE '%licencia%' THEN ( CASE p.genero WHEN 'M' THEN 'Lcdo.' ELSE 'Lcda.' END) "
                    . " ELSE '' END ) END ) END ) "
                . "WHEN '5' THEN 'Dip.' "
                . "WHEN '6' THEN 'Esp.' "
                . "WHEN '7' THEN 'M. Sc.' "
                . "WHEN '8' THEN ( CASE p.genero WHEN 'M' THEN 'Dr.' ELSE 'Dra.' END) "
                . "ELSE '' END "
                . "FROM Cet\NominaBundle\Entity\Estudio e "
                . "JOIN e.fk_estudios_personal1 p "
                . "WHERE e.tipo IN( "
                    . "SELECT MAX(e2.tipo) "
                    . "FROM Cet\NominaBundle\Entity\Estudio e2 "
                    . "WHERE e2.estado = 'T' "
                    . "AND e2.fk_estudios_personal1 = :personal_cedula "
                    . "AND e2.tipo NOT IN (5,6) ) "
                . "AND e.fk_estudios_personal1 = :personal_cedula";
        
        $consulta = $em->createQuery($query);
        $consulta->setMaxResults(1);
        $consulta->setParameter('personal_cedula', $usuario->getPersonal()->getId());
        $tituloProfesional = $consulta->getResult();
        
        if(!isset($tituloProfesional[0][1])){
            $tituloProfesional[0][1] = "";
        }
        $nombreUsuario = "";
        $nombreUsuario = $usuario->getPersonal()->getPrimerNombre()." ".$usuario->getPersonal()->getSegundoNombre()." ".$usuario->getPersonal()->getPrimerApellido()." ".$usuario->getPersonal()->getSegundoApellido();            
        if($usuario->getPersonal()->getInformacionLaboral()->getTituloProfesional())
            $tituloProfesional[0][1] = $usuario->getPersonal()->getInformacionLaboral()->getTituloProfesional();
                   
        //Aprobador
        $queryDirector = "SELECT p.id, p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido, il.tituloProfesional, hc.esEncargado, hc.fechaInicio, hc.fechaFin "
            . "FROM CetNominaBundle:HistoricoCargo hc "
            . "JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c "
            . "JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
            . "JOIN il.personal p "
            . "JOIN il.historicoUnidadOrganizativas huo "
            . "JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
            . "WHERE c.nombre LIKE :cargo "
            . "AND uo.nombre LIKE :direccion "
            . "AND ( CURRENT_DATE() BETWEEN hc.fechaInicio AND (CASE WHEN hc.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc.fechaFin END) ) "
            . "ORDER BY hc.fechaInicio DESC";
        
        $consultaDirector = $em->createQuery($queryDirector);
        $consultaDirector->setParameter('cargo','%DIRECTOR(A)%');
        $consultaDirector->setParameter('direccion','%RECURSOS HUMANOS%');
        $consultaDirector->setMaxResults(1);
        try {
            $datosDirectorv = $consultaDirector->getResult(); 
        } catch (\Exception $exc) {
            throw new \Exception('No se encontró un único resultado para Director de Recursos Humanos. '.$exc);
        }

        foreach($datosDirectorv as $datosDirectoru){
            $datosDirector = $datosDirectoru;
            if(isset($datosDirector['id'])){
                $consultaDirector = $em->createQuery($query);
                $consultaDirector->setMaxResults(1);
                $consultaDirector->setParameter('personal_cedula', $datosDirector['id']);
                $tituloProfesionalDirector = $consultaDirector->getResult();
                if(!isset($tituloProfesionalDirector[0][1]))
                    $tituloProfesionalDirector[0][1]="";
                if($datosDirector['tituloProfesional'])
                    $tituloProfesionalDirector[0][1] = $datosDirector['tituloProfesional'];
            }
        }
        //Revisor 1   
        $queryJefe = "SELECT p.id, p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido, il.tituloProfesional, hc.esEncargado, hc.fechaInicio, hc.fechaFin "
            . "FROM CetNominaBundle:HistoricoCargo hc "
            . "JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c "
            . "JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
            . "JOIN il.personal p "
            . "JOIN il.historicoUnidadOrganizativas huo "
            . "JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
            . "WHERE c.nombre LIKE :cargo "
            . "AND uo.nombre LIKE :direccion "
            . "AND ( CURRENT_DATE() BETWEEN hc.fechaInicio AND (CASE WHEN hc.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc.fechaFin END) ) "
            . "ORDER BY hc.fechaInicio DESC";
                
        $consultaJefe = $em->createQuery($queryJefe);
        $consultaJefe->setParameter('cargo','%JEFE(A) DE DIVISION%');
        $consultaJefe->setParameter('direccion','%RECURSOS HUMANOS%');
        $consultaJefe->setMaxResults(1);
        try {
           $datosJefev = $consultaJefe->getResult();  
        } catch (\Exception $exc) {
            throw new \Exception('No se encontró un único resultado para Jefe de Divisiòn General de Recursos Humanos. '.$exc);
        }

        foreach($datosJefev as $datosJefeu){
            $datosJefe = $datosJefeu;
            if(isset($datosJefe['id'])){
                $consultaJefe = $em->createQuery($query);
                $consultaJefe->setMaxResults(1);
                $consultaJefe->setParameter('personal_cedula', $datosJefe['id']);
                $tituloProfesionalJefe = $consultaJefe->getResult();
                if(!isset($tituloProfesionalJefe[0][1]))
                    $tituloProfesionalJefe[0][1]="";
                if($datosJefe['tituloProfesional'])
                    $tituloProfesionalJefe[0][1] = $datosJefe['tituloProfesional'];
            }
        }
        
        //Revisor 2 (Debe cambiar constantemente)
        $queryJefe2 = "SELECT p.id, p.primerNombre, p.segundoNombre, p.primerApellido, p.segundoApellido, il.tituloProfesional, hc.esEncargado, hc.fechaInicio, hc.fechaFin "
            . "FROM CetNominaBundle:HistoricoCargo hc "
            . "JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c "
            . "JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
            . "JOIN il.personal p "
            . "JOIN il.historicoUnidadOrganizativas huo "
            . "JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
            . "WHERE c.nombre LIKE :cargo "
            . "AND uo.nombre LIKE :direccion "
            . "AND c.nivel = '3' "
            . "AND ( CURRENT_DATE() BETWEEN hc.fechaInicio AND (CASE WHEN hc.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc.fechaFin END) ) "
            . "ORDER BY hc.fechaInicio DESC";
        
        $consultaJefe2 = $em->createQuery($queryJefe2);
        $consultaJefe2->setParameter('cargo','%ANALISTA DE PERSONAL%');
        $consultaJefe2->setParameter('direccion','%DEPARTAMENTO DE REMUNERACIONES%');
        $consultaJefe2->setMaxResults(1);
        try {
            $datosJefe2v = $consultaJefe2->getResult();
        } catch (\Exception $exc) {
            throw new \Exception('No se encontró un único resultado para Jefe de Departamento de Remuneraciones de Recursos Humanos. '.$exc);
        }

        foreach($datosJefe2v as $datosJefe2u){
            $datosJefe2 = $datosJefe2u;
            if(isset($datosJefe2['id'])){           
                $consultaJefe2 = $em->createQuery($query);
                $consultaJefe2->setMaxResults(1);
                $consultaJefe2->setParameter('personal_cedula', $datosJefe2['id']);
                $tituloProfesionalJefe2 = $consultaJefe2->getResult();
                if(!isset($tituloProfesionalJefe2[0][1]))
                    $tituloProfesionalJefe2[0][1]="";
                if($datosJefe2['tituloProfesional'])
                    $tituloProfesionalJefe2[0][1] = $datosJefe2['tituloProfesional'];
            }
        }
        
        $nombreDirector = "";
        $nombreJefe = "";
        $nombreJefe2 = "";
        $nombreDirector = $datosDirector['primerNombre']." ".$datosDirector['segundoNombre']." ".$datosDirector['primerApellido']." ".$datosDirector['segundoApellido'];
        $nombreJefe = $datosJefe['primerNombre']." ".$datosJefe['segundoNombre']." ".$datosJefe['primerApellido']." ".$datosJefe['segundoApellido'];
        $nombreJefe2 = $datosJefe2['primerNombre']." ".$datosJefe2['segundoNombre']." ".$datosJefe2['primerApellido']." ".$datosJefe2['segundoApellido'];
        
        $revision['usuario'] = $tituloProfesional[0][1]." ".ucwords(strtolower($nombreUsuario));
        $revision['director'] = $tituloProfesionalDirector[0][1]." ".ucwords(strtolower($nombreDirector));
        $revision['directorEncargado'] = $datosDirector['esEncargado'];
        $revision['jefe'] = $tituloProfesionalJefe[0][1]." ".ucwords(strtolower($nombreJefe));
        $revision['jefeEncargado'] = $datosJefe['esEncargado'];
        $revision['jefe2'] = $tituloProfesionalJefe2[0][1]." ".ucwords(strtolower($nombreJefe2));
        $revision['jefe2Encargado'] = $datosJefe2['esEncargado'];
        $revision['iniciales'] = $this->iniciales($nombreUsuario)." / ".$this->iniciales($nombreJefe2)." / ".$this->iniciales($nombreJefe)." / ".$this->iniciales($nombreDirector);
        
        return $revision;
    }
    
    /** 
    * Determina iniciales de nombres propios
    * @param - $nombre - Cadena nombre propio
    */
    function iniciales($nombre) {
	$notocar = Array('Del','De','del','de','Y','y','La','la','Los','los');
	$trozos = explode(' ',$nombre);
	$iniciales = '';
	for($i=0;$i<count($trozos);$i++){
		if(in_array($trozos[$i],$notocar)) $iniciales .= $trozos[$i]." ";
		else $iniciales .= substr($trozos[$i],0,1)."";
	}
	return $iniciales;
    }
    
    /** 
    * Agrega cantidad de dias habiles a un fecha
    * @param - $date - Fecha inicial
    * @param - $numDays - Cantidad de dias habiles
    * @param - $holidays - Vector de dias feriados
    * @return Fecha luego de la cantidad de dias feriados
    */
    function addBusinessDays( $date , $numDays=1 , $holidays = array() ) {
//        $current_date = strtotime($date);
        $current_date = clone $date;
        $business_days = intval($numDays); // Decrement does not work on strings
        while ($business_days > 0) {
            if ($current_date->format('N') < 6 && !in_array($current_date, $holidays)) {
                $business_days--;
            }
            if ($business_days > 0) {
                $current_date = $current_date->modify('+1 day');
            }
        }
        return $current_date;
    }
    
    public function llenar_matriz(){
        $em = $this->entityManager;
        $metadata = $em->getMetadataFactory()->getAllMetadata();
        $entidades = array();
        $matriz = array();
        
        //Lista de entidades que no se mostrarán
        $patrones = array(
            "userPersonalM",
            "userPersonalSistemasD",
            "sistSistemasM",
            "Usuario",
        );
        $i=0;
        foreach($metadata as $entidad){
            $bandera=0;
            foreach ($patrones as $patron) {
                if (preg_match('/' . $patron . '/', $entidad->getName())) {
                    $bandera=1;
                    continue;
                }
            }
            if($bandera==1)continue;
            $entidades[]=$entidad->getName();
            $nombre=$entidad->getName();
            $columnas_array[] = $em->getClassMetadata($nombre)->getFieldNames();
            $columnas = $em->getClassMetadata($nombre)->getFieldNames();
            $relaciones = $em->getClassMetadata($nombre)->getAssociationNames();
            $a=1;
                $tipos_campos_tabla = array();
                //Campos
                foreach($columnas as $columna){
                    $tipos_campos_tabla[$a] = $columna.' - '.$em->getClassMetadata($nombre)->getTypeOfField($columna);
                    //$tipos_campos_tabla[$a] = $em->getMetadataFactory();
                    //$tipos_campos_tabla[$a] = $em->getClassMetadata($nombre)->isCollectionValuedAssociation($nombre);
                    $a++;
                }
                //Relaciones
                foreach($relaciones as $relacion){
                    $tipos_campos_tabla[$a] = $relacion.' - Relación';
                    //$tipos_campos_tabla[$a] = $em->getMetadataFactory();
                    //$tipos_campos_tabla[$a] = $em->getClassMetadata($nombre)->isCollectionValuedAssociation($nombre);
                    $a++;
                }
                
                $tipos_campos[] = $tipos_campos_tabla;
                
            $matriz[$entidad->getName()]['campos']=$tipos_campos_tabla;             
            $i++;
        }
        return $matriz;
    }
    
    /** 
    * Determina año y meses
    * @param - $cant - Double con la contidad de años
    */
    function tiempo($cant) {
	$cadena = "";
        $cadena = floor($cant) . " años(s) ";
        $entero = floor($cant);
        $fraccion = $cant - $entero;
        $cadena = $cadena.floor($fraccion*12)." mes(es)";
	return $cadena;
    }
}
