<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CargoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            //->add('nivel')
            ->add('nivel', 'choice', array(
                'label' => 'Nivel de cargo',
                'required'=> false,
                'attr' => array('help'=>'Seleccione el nivel del cargo'),
                'choices' => array('' => 'Seleccione un nivel','1' => 'I', '2' => 'II', '3' => 'III', '4' => 'IV', '5' => 'V', '6' => 'VI'),
            ))     
            //->add('jerarquia')
            ->add('jerarquia', 'choice', array(
                'label' => 'Jerarquia',
                'attr' => array('help'=>'Seleccione la jerarquia del cargo.'),
                'choices' => array('' => 'Seleccione una jerarquia','1' => 'Contralor','2' => 'Director General','3' => 'Director','4' => 'Jefe de Oficina/Jefe de Division General','5' => 'Jefe de División/Supervisor','6' => 'Funcionario','7' => 'Asistente','8' => 'Auxiliar','9' => 'Obrero'),
            ))     
            ->add('sueldo')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Cargo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_cargo';
    }
}
