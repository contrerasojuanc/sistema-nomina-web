<?php
namespace Cet\NominaBundle\DQL;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;

class Enumerate extends FunctionNode
{
    protected $roundExp;
    protected $roundPrecission;

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        //OJO falta funcion de enumerar para mysql
        return 'row_number() OVER (ORDER BY 1)';
    }

    /**
     * parse - allows DQL to breakdown the DQL string into a processable structure
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}

?>
