<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\DatosGenericos
 *
 * @ORM\Entity(repositoryClass="DatosGenericosRepository")
 * @ORM\Table(name="sis_nomina.datos_genericos", indexes={@ORM\Index(name="fk_datosGenericos_personal1_idx", columns={"personal_cedula"}), @ORM\Index(name="fk_datosGenericos_cargaMasiva1_idx", columns={"carga_masiva"})})
 * @UniqueEntity(
 *     fields={"fk_datosGenericos_personal1","fk_dato_generico_tipo_dato_generico1"},
 *     message="Ya existe ese dato generico para el funcionario"
 * )
 */
class DatosGenericos
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="valor", type="text", nullable=false)
     */
    private $valor;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="datosGenericos")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_datosGenericos_personal1;
    
    /**
     * @ORM\ManyToOne(targetEntity="CargaMasiva", inversedBy="datosGenericos")
     * @ORM\JoinColumn(name="carga_masiva", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $fk_datosGenericos_cargaMasiva1;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoDatoGenerico", inversedBy="datosGenericos")
     * @ORM\JoinColumn(name="tipo_dato_generico_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_dato_generico_tipo_dato_generico1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return DatosGenericos
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fk_datosGenericos_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkDatosGenericosPersonal1
     * @return DatosGenericos
     */
    public function setFkDatosGenericosPersonal1(\Cet\NominaBundle\Entity\Personal $fkDatosGenericosPersonal1)
    {
        $this->fk_datosGenericos_personal1 = $fkDatosGenericosPersonal1;

        return $this;
    }

    /**
     * Get fk_datosGenericos_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkDatosGenericosPersonal1()
    {
        return $this->fk_datosGenericos_personal1;
    }
    
    /**
     * Set fk_datosGenericos_cargaMasiva1
     *
     * @param \Cet\NominaBundle\Entity\CargaMasiva $fkDatosGenericosCargaMasiva1
     * @return DatosGenericos
     */
    public function setFkDatosGenericosCargaMasiva1(\Cet\NominaBundle\Entity\CargaMasiva $fkDatosGenericosCargaMasiva1)
    {
        $this->fk_datosGenericos_cargaMasiva1 = $fkDatosGenericosCargaMasiva1;

        return $this;
    }

    /**
     * Get fk_datosGenericos_cargaMasiva1
     *
     * @return \Cet\NominaBundle\Entity\CargaMasiva 
     */
    public function getFkDatosGenericosCargaMasiva1()
    {
        return $this->fk_datosGenericos_cargaMasiva1;
    }
    
    /**
     * Set TipoDatoGenerico entity (many to one).
     *
     * @param TipoDatoGenerico $tipoDatoGenerico
     * @return \Entity\DatosGenericos
     */
    public function setFkDatoGenericoTipoDatoGenerico1(TipoDatoGenerico $tipoDatoGenerico)
    {
        $this->fk_dato_generico_tipo_dato_generico1 = $tipoDatoGenerico;

        return $this;
    }

    /**
     * Get TipoDatoGenerico entity (many to one).
     *
     * @return TipoDatoGenerico
     */
    public function getFkDatoGenericoTipoDatoGenerico1()
    {
        return $this->fk_dato_generico_tipo_dato_generico1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
//       $metadata->addPropertyConstraint('nombre', new Assert\NotBlank())
//                ->addPropertyConstraint('nombre', new Assert\Type(array('type'=>'string')))
////                ->addPropertyConstraint('ruta', new Assert\Email(array('message' => 'El campo no tiene el formato de correo electrónico.')))
//                ->addPropertyConstraint('ruta', new Assert\NotBlank())
//                ->addPropertyConstraint('tipo', new Assert\NotBlank())
//                ->addPropertyConstraint('tipo', new Assert\Type(array('type'=>'string')))                
//
//
//                ->addPropertyConstraint('tamanio', new Assert\NotBlank())               
//                ->addPropertyConstraint('tamanio', new Assert\Type(array('type'=>'numeric')))
//            ;
        
    }
}