<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PrestacionesSocialesGeneralType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha','date', array(
                'label'=> 'Fecha',
                'widget' => 'single_text',
                'attr' => array('help'=>'Ingrese la fecha para el cálculo de las prestaciones sociales. Considerando que el cálculo será realizado para el trimestre ocurrido justamente antes de esta fecha. ')
                ))  
            ->add('descripcion',null,array('label'=>'Denominación','attr' => array('help'=>'Ingrese el nombre de identificación de las prestaciones sociales a calcular. Advertencia: En caso de generar un nuevo cálculo para el mismo periodo y mismo tipo de nómina, serán procesados únicamente los trabajadores del mismo tipo de nómina que aún no han sido procesados.')))
            ->add('fk_prestaciones_tipo_nomina1',null,array('label'=>'Tipo de Nómina','attr' => array(
                'help'=>'Seleccione el tipo de nómina de las prestaciones sociales a calcular',
                'class' => 'select2me',
                'data-placeholder' => 'Seleccione'
                )))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\PrestacionesSocialesGeneral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_prestacionessocialesgeneral';
    }
}
