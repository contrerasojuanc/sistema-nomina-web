<?php

namespace Cet\NominaBundle\Form\Subscribers;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;

class EntidadSubscriber implements EventSubscriberInterface
{
    private $factory;
    public $entidad_array=array();    
    
    public function __construct(FormFactoryInterface $factory,Array $entidad_array=null)
    {
        $this->factory = $factory;
        $this->entidad_array = $entidad_array;        
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }

    private function addEntidadForm($form, $entidad)
    {
        $form->add($this->factory->createNamed('Entidad', 'choice', $entidad, array(
            'mapped'        => false,
            'empty_value'   => 'Seleccione la Entidad',
            'auto_initialize'=> false,
            'choices'       => $this->entidad_array,
            'required' => false,
        )));
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        ////$country = ($data->city) ? $data->city->getProvince()->getCountry() : null ;
//        $parroquia = ($data->getFkViviendaParroquia1()) ? $data->getFkViviendaParroquia1() : null ;
//        $municipio = ($parroquia) ? $parroquia->getFkParroquiaMunicipio1() : null ;
//        $estado = ($municipio) ? $municipio->getFkMunicipioEstado1() : null ;
        $entidad=null;
        $this->addEntidadForm($form, $entidad);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $entidad = array_key_exists('Entidad', $data) ? $data['Entidad'] : null;
//        $entidad = null;
        $this->addEntidadForm($form, $entidad);
    }
}