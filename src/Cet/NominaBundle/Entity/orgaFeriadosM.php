<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GenOrganizacion.orgaFeriadosM
 *
 * @ORM\Entity(repositoryClass="OrgaFeriadosMRepository")
 * @ORM\Table(name="gen_organizacion.orga_feriados_m")
 */
class orgaFeriadosM
{
    
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=120, nullable=true)
     */
    private $descripcion;

    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;
    
    public function __toString()
    {
        return $this->getDescripcion();
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return orgaFeriadosM
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return orgaFeriadosM
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
