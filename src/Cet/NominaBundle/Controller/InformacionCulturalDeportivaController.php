<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\InformacionCulturalDeportiva;
use Cet\NominaBundle\Form\InformacionCulturalDeportivaType;

/**
 * InformacionCulturalDeportiva controller.
 *
 */
class InformacionCulturalDeportivaController extends Controller
{

    /**
     * Lists all InformacionCulturalDeportiva entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:InformacionCulturalDeportiva')->findAll();

        return $this->render('CetNominaBundle:InformacionCulturalDeportiva:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new InformacionCulturalDeportiva entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new InformacionCulturalDeportiva();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('informacionculturaldeportiva_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:InformacionCulturalDeportiva:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a InformacionCulturalDeportiva entity.
    *
    * @param InformacionCulturalDeportiva $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(InformacionCulturalDeportiva $entity)
    {
        $form = $this->createForm(new InformacionCulturalDeportivaType(), $entity, array(
            'action' => $this->generateUrl('informacionculturaldeportiva_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new InformacionCulturalDeportiva entity.
     *
     */
    public function newAction()
    {
        $entity = new InformacionCulturalDeportiva();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:InformacionCulturalDeportiva:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a InformacionCulturalDeportiva entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionCulturalDeportiva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionCulturalDeportiva.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionCulturalDeportiva:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing InformacionCulturalDeportiva entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionCulturalDeportiva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionCulturalDeportiva.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionCulturalDeportiva:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a InformacionCulturalDeportiva entity.
    *
    * @param InformacionCulturalDeportiva $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(InformacionCulturalDeportiva $entity)
    {
        $form = $this->createForm(new InformacionCulturalDeportivaType(), $entity, array(
            'action' => $this->generateUrl('informacionculturaldeportiva_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing InformacionCulturalDeportiva entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionCulturalDeportiva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionCulturalDeportiva.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('informacionculturaldeportiva_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:InformacionCulturalDeportiva:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a InformacionCulturalDeportiva entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:InformacionCulturalDeportiva')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionCulturalDeportiva.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('informacionculturaldeportiva'));
    }

    /**
     * Creates a form to delete a InformacionCulturalDeportiva entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('informacionculturaldeportiva_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
