<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalHistoricoCargoNewEncargadoType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fechaInicio','date', array(
                'widget' => 'single_text',
                'label'=> 'Fecha de Ingreso',
                'attr' => array('help'=>'Seleccione la fecha de ingreso en la Dirección.'),
            ))
            ->add('fk_informacion_laboral_has_cargo_cargo1',null,array(
                   'query_builder' => function($r) {
                        return $r->createQueryBuilder('cargo')
                                   ->orderBy('cargo.id','ASC');
                        },
                   'label'=>'Cargos','required'=>true,'attr' => array('help'=>'Seleccione el cargo'),
                   'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione el cargo')              
            )) 
            ->add('fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1', null, array(
                'empty_value' => 'Seleccione',
                'label' => 'Dirección',
                'required'=>true,
                'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione el cargo') 
                ))
            ->add('numeroResolucion',null,array(
                'label'=> 'Número de resolución',
                'attr' => array('help'=>'Ingrese el número de resolución'),
                ))    
            ->add('fechaResolucion','date', array(
                'label'=> 'Fecha de resolución',
                'attr' => array('help'=>'Ingrese la fecha de la resolución.'),
                'widget' => 'single_text',
                'required' => true,
            ))     
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\HistoricoCargo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_historicocargo';
    }
}
