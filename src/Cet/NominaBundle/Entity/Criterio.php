<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\Criterio
 *
 * @ORM\Table(name="sis_nomina.criterio")
 * @ORM\Entity
 */
class Criterio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="valor", type="text", nullable=true)
     */
    private $valor;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="text", nullable=true)
     */
    private $comentario;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="text", nullable=true)
     */
    private $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="Reporte", inversedBy="criterios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reporte_id", referencedColumnName="id")
     * })
     */
    private $reporte;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Criterio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return Criterio
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return Criterio
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string 
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set tipo
     *
     * @param integer $tipo
     * @return Criterio
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set reporte
     *
     * @param \Cet\NominaBundle\Entity\Reporte $reporte
     * @return Criterio
     */
    public function setReporte(\Cet\NominaBundle\Entity\Reporte $reporte = null)
    {
        $this->reporte = $reporte;

        return $this;
    }

    /**
     * Get reporte
     *
     * @return \Cet\NominaBundle\Entity\Reporte 
     */
    public function getReporte()
    {
        return $this->reporte;
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
