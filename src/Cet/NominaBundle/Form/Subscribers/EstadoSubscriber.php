<?php

namespace Cet\NominaBundle\Form\Subscribers;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;

class EstadoSubscriber implements EventSubscriberInterface
{
    private $factory;

    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }

    private function addEstadoForm($form, $estado)
    {
        $form->add($this->factory->createNamed('Estado', 'entity', $estado, array(
            'class'         => 'CetNominaBundle:Estado',
            'mapped'        => false,
            'empty_value'   => 'Seleccione el Estado',
            'auto_initialize'=> false,
            'label'=> 'Estado',
            'attr' => array('help'=>'Seleccione el Estado donde se encuentra su lugar de residencia o vivienda. Ejemplo: Táchira, Barinas, Mérida, Zulia'),
            'query_builder' => function (EntityRepository $repository) {
                $qb = $repository->createQueryBuilder('estado');

                return $qb;
            }
        )));
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        //$country = ($data->city) ? $data->city->getProvince()->getCountry() : null ;
        $parroquia = ($data->getFkViviendaParroquia1()) ? $data->getFkViviendaParroquia1() : null ;
        $municipio = ($parroquia) ? $parroquia->getFkParroquiaMunicipio1() : null ;
        $estado = ($municipio) ? $municipio->getFkMunicipioEstado1() : null ;

        $this->addEstadoForm($form, $estado);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $estado = array_key_exists('Estado', $data) ? $data['Estado'] : null;
        $this->addEstadoForm($form, $estado);
    }
}