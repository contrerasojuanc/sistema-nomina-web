<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalCargaFamiliarType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',null,array('label' => 'Nombre del Familiar','attr' => array('help'=>'Introduzca el nombre del familiar en este campo.')))
            ->add('cedula',null,array('label' => 'C.I. del Familiar','attr' => array('help'=>'Introduzca el Nro de cédula del familiar. Ejemplo: 16543210')))
 //           ->add('parentesco',null,array('label' => 'Parentesco','attr' => array('help'=>'Seleccione el tipo de parentesco.')))                
            ->add('parentesco', 'choice', array(
                'label' => 'Parentesco',
                'attr' => array('help'=>'Seleccione el tipo de parentesco.'),
                'choices' => array('' => 'Seleccione','P' => 'Padre', 'M' => 'Madre', 'R' => 'Hermano(a)', 'H' => 'Hijo(a)', 'E' => 'Esposo(a)', 'C' => 'Concubino(a)'),
            ))    
            ->add('fechaNacimiento','date', array(
                'label'=> 'Fecha de Nacimiento',
                'attr' => array('help'=>'Ingrese la Fecha de Nacimiento del familiar.'),
                'widget' => 'single_text',
            ))     
            ->add('genero', 'choice', array(
                'label'=>'Género',
                'attr'=>array('help'=>'Ingrese el género del familiar M = Masculino y F = Femenino'),
                'choices' => array('' => 'Seleccione','M' => 'Masculino', 'F' => 'Femenino'),
            ))    
            ->add('estudia', 'choice', array(
                'label'=>'Indique si Estudia',
                'attr'=>array('help'=>'Seleccione la opción correspondiente.'),
                'choices' => array('' => 'Seleccione','S' => 'Si', 'N' => 'No'),
            ))      
//            ->add('nivelInstruccion')
            ->add('nivelInstruccion', 'choice', array(
                'label' => 'Nivel de Instrucción',
                'attr' => array('help'=>'Seleccione el nivel de instrucción que corresponda.'),
                'choices' => array('' => 'Seleccione','1' => 'Primer Nivel (Inicial)', '2' => 'Segundo Nivel (Básica)', '3' => 'Tercer Nivel (Media)', '4' => 'Cuarto Nivel (Superior)', '5' => 'Quinto Nivel (Avanzada)'),
            ))     
            //->add('baseLegal')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\CargaFamiliar'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_cargafamiliar';
    }
}
