package {
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.display.LoaderInfo;
	import flash.display.StageScaleMode;
	import flash.events.*;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.external.ExternalInterface;
	import flash.system.Security;
	import flash.utils.*;
	import flash.system.System;
	import flash.net.FileReference;
	import flash.net.FileFilter;
	
	import org.alivepdf.encoding.Base64;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	import mx.utils.Base64Decoder;
	
	/* PDF imports */
	import org.alivepdf.pdf.PDF;
	import org.alivepdf.pages.Page;
	import org.alivepdf.events.PageEvent;
	import org.alivepdf.fonts.CoreFont;
	import org.alivepdf.data.Grid;
	import org.alivepdf.data.GridColumn;
	import org.alivepdf.layout.Orientation;
	import org.alivepdf.layout.Size;
	import org.alivepdf.layout.Mode;
	import org.alivepdf.layout.Position;
	import org.alivepdf.layout.Resize;
	import org.alivepdf.layout.Unit;
	import org.alivepdf.display.Display;
	import org.alivepdf.saving.Method;
	import org.alivepdf.fonts.FontFamily;
	import org.alivepdf.fonts.Style;
	import org.alivepdf.fonts.CoreFont;
	import org.alivepdf.colors.RGBColor;
	
	
	import org.alivepdf.text.Cell;	
	import org.alivepdf.layout.Align;
	import org.alivepdf.fonts.FontMetrics;
	
	public class PDF1 extends PDF 
	{
		private var headL:String = "";
		private var headC:String = "";
		private var headR:String = "";
		private var footL:String = "";
		private var footC:String = "";
		private var footR:String = "";
		private var imageC:String = "";
		
		//public function PDF1(orientation:String = "Portrait", unit:String = "Mm", pageSize:Size = null,rotation:int = 0 )
		public function PDF1(orientation:String = "Portrait", unit:String = "Mm", pageSize:Size = null,rotation:int = 0, headL:String = "", headC:String = "", headR:String = "", foorL:String = "", footC:String = "", footR:String = "", imageC:String = "" )
		{
			super(orientation, unit, pageSize, rotation);
			this.headL = headL;
			this.headC = headC;
			this.headR = headR;
			this.footL = footL;
			this.footC = footC;
			this.footR = footR;
			this.imageC = imageC;
		}

		//public function addPage (page:Page=null):Page		
		override public function addPage(page:Page=null):Page
		{
			if ( page == null ) 
				page = new Page ( defaultOrientation, defaultUnit, defaultSize, defaultRotation );
			
			pagesReferences.push ( (3+(arrayPages.length<<1))+' 0 R' );
			
			arrayPages.push ( page );
			
			page.number = pagesReferences.length;
			
			if ( state == PDF.STATE_0 ) 
				open();
			
			if( nbPages > 0 )
			{
				inFooter = true;
				footer();
				inFooter = false;
				finishPage();
			}
      
			currentPage = page;
			
			startPage ( page != null ? page.orientation : defaultOrientation );
			
			/*
			if ( strokeColor != null ) 
				lineStyle ( strokeColor, strokeThickness, strokeFlatness, strokeAlpha, windingRule, strokeBlendMode, strokeDash, strokeCaps, strokeJoints, strokeMiter );
			
			if ( fillColor != null ) 
				beginFill( fillColor );*/
			
			if ( textColor != null ) 
				textStyle ( textColor, textAlpha, textRendering, textSpace, textSpace, textScale, textLeading );
			
			if ( currentFont != null ) 
				setFont ( currentFont, fontSizePt );
			else setFont( new CoreFont(FontFamily.HELVETICA), 9);
			
			inHeader = true;
			header();
			inHeader = false;
			
			dispatcher.dispatchEvent( new PageEvent ( PageEvent.ADDED, currentPage ) );
			
			return page;
		}
		
		override protected function footer():void
		{

			this.setXY(15, -15);
			var newFont:CoreFont = new CoreFont(FontFamily.HELVETICA);
			this.setFont(newFont, 6);
			this.textStyle(new RGBColor(0x000000));

			this.addCell(160, 10,this.footC,0, 0, 'C');
			this.addCell(30, 10,"Pag. " + this.getCurrentPage().number,0, 0, 'C');

			this.newLine(20);
			this.setFont(newFont, 8);

		}

		override protected function header():void
		{

			var newFont:CoreFont = new CoreFont(FontFamily.HELVETICA);
			this.setFont(newFont, 11);
			this.textStyle(new RGBColor(0x000000));
			
			if ( this.imageC != null )
			{
				var decoder:Base64Decoder = new Base64Decoder();
				decoder.decode(this.imageC);
				var ba:ByteArray = decoder.toByteArray();
				
				this.addImageStream(ba ,"DeviceRGB", new Resize(Mode.NONE,Position.LEFT),0,0, 16 ,20,0,1,"Normal",null);
			}
						
			//var image:ByteArray = new imageGIFStream() as ByteArray;
			//this.addImageStream(image, ColorSpace.DEVICE_RGB, new org.alivepdf.layout.Resize(Mode.NONE,Position.LEFT),0, 0, 50, 20, 0, 1, "Normal", null);

			var posicion1:Number = this.getY();
			this.newLine(20);
			var posicion2:Number = this.getY();
			
			//this.addText(this.headC,17,20);
			this.setY(posicion1);
			this.addCell(17,20, "",0);
			this.addMultiCell(this.getCurrentPage().w-38,4, this.headC,0);	
			
			this.setY(posicion2);
			
			this.setFont(newFont, 8);
		}
		
		//public function addMultiCell ( width:Number, height:Number, text:String, border:*=0, align:String='J', filled:int=0):void
		override public function addMultiCell(width:Number, height:Number, text:String, border:*=0, align:String='J', filled:int=0):void
		{
			charactersWidth = currentFont.charactersWidth;
			
			if ( width == 0 ) 
				width = currentPage.w - rightMargin - currentX;
			
			var wmax:Number = (width-2*currentMargin)*I1000/fontSize;
			var s:String = findAndReplace ("\r",'',text);
			var nb:int = s.length;
			
			if( nb > 0 && s.charAt(nb-1) == "\n" ) 
				nb--;
			
			var b:* = 0;
			
			if( border )
			{
				if( border == 1 )
				{
					border = 'LTRB';
					b = 'LRT';
					b2 = 'LR';
				}
				else
				{
					b2 = '';
					if (border.indexOf(Align.LEFT)!= -1) 
						b2+= Align.LEFT;
					if (border.indexOf(Align.RIGHT)!= -1) 
						b2+= Align.RIGHT;
					b = (border.indexOf(Align.TOP)!= -1) ? 
						b2+Align.TOP : b2;
				}
			}
			
			var sep:int = -1;
			var i:int = 0;
			var j:int = 0;
			var l:int = 0;
			var ns:int = 0;
			var nl:int = 1;
			var c:String;
			
			var cwAux:int = 0;
			
			while (i<nb)
			{			
				c = s.charAt(i);
			
				if (c == "\n" )
				{
					if (ws>0)
					{
						ws=0;
						write('0 Tw');
					}
					
					addCell(width, height, s.substr(j,i-j), b, 2, align, filled);
										
					i++;
					sep = -1;
					j = i;
					l = 0;
					ns = 0;
					nl++;
					
					if (border && nl==2) 
						b = b2;
					continue;			
				}
				
				if(c==' ')
				{
					sep = i;
					var ls:int = l;
					ns++;
				}
		
				if (c == "\\" ){
					var caux:String;
					caux = s.charAt(i+1);
				
					if (caux == "n" )
					{
						i++;
						if (ws>0)
						{
							ws=0;
							write('0 Tw');
						}
						
						addCell(width, height, s.substr(j,i-j-1), b, 2, align, filled);
											
						i++;
						sep = -1;
						j = i;
						l = 0;
						ns = 0;
						nl++;
						
						if (border && nl==2) 
							b = b2;
						continue;			
					}
				}
		
				cwAux = charactersWidth[c] as int;
				
				if (cwAux == 0) 
					cwAux = FontMetrics.DEFAULT_WIDTH;
				
				l += cwAux;
				
				if (l>wmax)
				{
					if (sep==-1)
					{
						if (i==j) 
							i++;
						if (ws>0)
						{
							ws=0;
							write('0 Tw');
						}
						addCell(width,height,s.substr(j,i-j),b,2,align,filled);
					}
					else
					{
						if (align == Align.JUSTIFIED)
						{
							ws = (ns>1) ? ((wmax-ls)*.001)*fontSize/(ns-1) : 0;
							//write(sprintf('%.3f Tw',ws*k));
						}
						
						addCell(width,height,s.substr(j,sep-j),b,2,align,filled);
						i=sep+1;
					}
					
					sep = -1;
					j = i;
					l = 0;
					ns = 0;
					nl++;
					
					if ( border && nl == 2 ) 
						b = b2;
					
				}
				else i++;
			}
			
			if ( ws>0 )
			{
				ws = 0;
				write('0 Tw');
			}
			
			if ( border && border.indexOf ('B')!= -1 ) 
				b += 'B';
			
			addCell ( width, height, s.substr(j,i-j), b, 2, align, filled );
			currentX = leftMargin;
		}
	
	}	
	
}