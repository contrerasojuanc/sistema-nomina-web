<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalCorreoPersonalType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
 //           ->add('fk_correo_personal_personal1',null,array('label' => 'Nombre del Funcionario','attr' => array('help'=>'Seleccione el nombre del funcionario de la lista.')))             
            ->add('correo',null,array('label' => 'Correo Electrónico','attr' => array('help'=>'Ingrese el correo electrónico. Ejemplo: usuario@contraloriaestadotachira.gob.ve')))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\CorreoPersonal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_correopersonal';
    }
}
