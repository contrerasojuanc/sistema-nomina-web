<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Cet\NominaBundle\Entity\userPersonalM;

class userPersonalSistemasDType extends AbstractType
{
   
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\userPersonalM");
        $userTransformer->setEntityRepository("CetNominaBundle:userPersonalM");
        $userTransformer->setEntityType("userPersonalM");
        
        $builder
            ->add('acceso','hidden', array(
                'empty_data' => 'A',
            ))
            ->add('nivel', null,array(
                'label' => 'Nivel de Acceso',
                'attr' => array('help'=>'Sistema Nómina Web: 1 = Administrador, 2 = Usuario Avanzado, 3 = Usuario de Remuneraciones, 4 = Usuario de Archivo')))
            ->add('idSistema')
//            ->add('idUsuario','text',array('property_path' => 'idUsuario.id'))
//            ->add($builder->create('idUsuario','text')->addModelTransformer($userTransformer))   
            ->add('idUsuario')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\userPersonalSistemasD'
        ))
        ->setRequired(array(
            'em',
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_userpersonalsistemasd';
    }
}
