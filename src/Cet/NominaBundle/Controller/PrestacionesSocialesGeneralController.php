<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\PrestacionesSocialesGeneral;
use Cet\NominaBundle\Form\PrestacionesSocialesGeneralType;
use Cet\NominaBundle\Entity\PrestacionesSociales;

use Cet\NominaBundle\Entity\VariableFormula;

use DateTime;

/**
 * PrestacionesSocialesGeneral controller.
 *
 */
class PrestacionesSocialesGeneralController extends Controller
{

    /**
     * Lists all PrestacionesSocialesGeneral entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->findAll();

        $prestacionesCalculadas=array();
        foreach($entities as $entity){
            $prestacionesCalculadas[$entity->getId()] = $em->getRepository('CetNominaBundle:PrestacionesSociales')->findOneBy(array('general' => $entity->getId()));
        }
        
        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:index.html.twig', array(
            'entities' => $entities,
            'prestacionesCalculadas' => $prestacionesCalculadas,
        ));
    }
    /**
     * Creates a new PrestacionesSocialesGeneral entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PrestacionesSocialesGeneral();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prestacionessocialesgeneral_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PrestacionesSocialesGeneral entity.
    *
    * @param PrestacionesSocialesGeneral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PrestacionesSocialesGeneral $entity)
    {
        $form = $this->createForm(new PrestacionesSocialesGeneralType(), $entity, array(
            'action' => $this->generateUrl('prestacionessocialesgeneral_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new PrestacionesSocialesGeneral entity.
     *
     */
    public function newAction()
    {
        $entity = new PrestacionesSocialesGeneral();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PrestacionesSocialesGeneral entity.
     *
     */
    public function showAction($id,$imprimir = null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSocialesGeneral.');
        }
        
        $prestaciones = $em->getRepository('CetNominaBundle:PrestacionesSociales')->findBy(array('general' => $entity->getId()));

        $personas = array();
        $arregloResumen = array();
        $totales['funcionarios'] = 0;
        $totales['funcionariosAdicionales'] = 0;
        $totales['prestaciones'] = 0;
        $totales['prestacionesAdicionales'] = 0;
        $totales['total'] = 0;
        foreach ($prestaciones as $prestacion){
            $personal = $prestacion->getPersonal();
            
            $cedula = $personal->getId();
            
            $informacionLaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findBy(array('personal' => $cedula));
            
            foreach($informacionLaboral as $persona){
            
                $personas[$cedula]['cedula'] = $cedula;
                $personas[$cedula]['nombre'] = $personal->getPrimerNombre()." ".$personal->getSegundoNombre()." ".$personal->getPrimerApellido()." ".$personal->getSegundoApellido();
                $personas[$cedula]['fechaIngreso'] = $persona->getFechaIngreso();
                $personas[$cedula]['diasTrimestre'] = $prestacion->getDias();
                $personas[$cedula]['prestaciones'] = $prestacion->getPrestaciones();
                $personas[$cedula]['diasAdicionales'] = $prestacion->getDiasAdicionales();
                $personas[$cedula]['prestacionesAdicionales'] = $prestacion->getPrestacionesAdicionales();
                $personas[$cedula]['cuentaBancaria'] = $persona->getCuentaFideicomiso();
                $personas[$cedula]['ingresoDiario'] = $prestacion->getIngresoDiario();
                $personas[$cedula]['alicotaBonoVacacional'] = $prestacion->getAlicotaBonoVacacional();
                $personas[$cedula]['alicotaAguinaldos'] = $prestacion->getAlicotaAguinaldos();
                $personas[$cedula]['prestacionesDiaria'] = $prestacion->getPrestacionesDiaria();
                
                $vectores=array();
                $vectores = explode(";",$prestacion->getConceptos());
                $vectorConceptos=array();
                foreach($vectores as $vector){
                    $vectorConceptos[] = explode(":",$vector);
                }
                $personas[$cedula]['conceptos'] = $vectorConceptos;
                
                //Bloque para calcular resumen de conceptos
                $codigo = 0;
                foreach($vectorConceptos as $vector){
                    foreach($vector as $key => $valor){                        
                        if($key % 2 == 0){
                            $codigo = trim($valor);
                        }else{
                            if(isset($arregloResumen["_".$codigo])){
                                if(isset($arregloResumen["_".$codigo][2])){
                                    $arregloResumen["_".$codigo][2] = $arregloResumen["_".$codigo][2] + $valor ;
                                }
                            }
                            else{
                                $descripcion = "No Aplica";
                                $consultaConceptos = $em->getRepository('CetNominaBundle:Concepto')->findBy(array('codigo' => $codigo ));
                                foreach($consultaConceptos as $consultaConcepto){
                                    $descripcion = $consultaConcepto->getDenominacion();
                                }
                                $arregloResumen["_".$codigo] = array($codigo,$descripcion,$valor);
                            }
                            $codigo = 0;
                        }                        
                    }
                }
                
                //Totalización
                $totales['funcionarios'] = $totales['funcionarios'] + 1;
                if($prestacion->getDiasAdicionales() > 0){
                    $totales['funcionariosAdicionales'] = $totales['funcionariosAdicionales'] + 1;
                }
                $totales['prestaciones'] = round( $totales['prestaciones'] + $prestacion->getPrestaciones() , 2 );
                $totales['prestacionesAdicionales'] = round( $totales['prestacionesAdicionales'] + $prestacion->getPrestacionesAdicionales() , 2 );               
            }            
        }
        $totales['total'] = $totales['total'] + round( $totales['prestaciones'] + $totales['prestacionesAdicionales'] , 2 );
        
        //Bloque para determinar funcionarios y titulos profesionales del Realizador, Revisor y Aprobador  
        $usuario = $this->get('security.context')->getToken()->getUser();
        $helper = $this->get('cet.nomina.globales');
        $revision = $helper->obtenerRevisores($em,$usuario);
        
        ksort($personas,SORT_NUMERIC);
        ksort($arregloResumen);
        
        //Generar PDF de prestaciones sociales
        if ($imprimir){
            $html = $this->renderView('CetNominaBundle:PrestacionesSocialesGeneral:show.html.twig', array(
                'entity'      => $entity,
                'personas' => $personas,
                'revision' => $revision,
                'totales' => $totales,
                'arreglo' => $arregloResumen,
            ));
            $valores['institucion'] = "CONTRALORÍA DEL ESTADO TÁCHIRA";
//            $valores['nomina'] = "CALCULO TRIMESTRAL DE PRESTACIONES SOCIALES  ( ULTIMO SUELDO )";
            $valores['nomina'] = $entity->getDescripcion();
            $valores['fecha'] = $entity->getFecha()->format('d/m/Y');
            $valores['plantilla'] = $entity->getFkPrestacionesTipoNomina1()->getNombre();
            $header = $this->renderView('CetNominaBundle:PrestacionesSocialesGeneral:header.html.twig', array('valores' => $valores));
            
            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                    'print-media-type'      => true,
                    'no-background'         => true,
    //                'custom-header'         => true,
    //                'custom-header-propagation'    => true,
    //                'header-center'                => true,
    //                'header-font-size'             => 20,
    //                'default-header'         => true,
                    'header-html'           => $header,
                    'footer-html'           => '',
                    'lowquality'                   => false,
                    'grayscale'                    => true,
                    'margin-bottom'                => 12,
                    'margin-left'                  => 8,
                    'margin-right'                 => 8,
                    //Para Windows
                    //'margin-top'                   => 20,
                    //Para Linux
                    'margin-top'                   => 40,
                    'page-size'                    => "Letter",
                )),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="Nomina.pdf"',
                )
            );
        }
        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:show.html.twig', array(
            'entity'      => $entity,
            'personas' => $personas,            
            'revision' => $revision,
            'totales' => $totales,             
            'arreglo' => $arregloResumen,
        ));
    }

    /**
     * Displays a form to edit an existing PrestacionesSocialesGeneral entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSocialesGeneral.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PrestacionesSocialesGeneral entity.
    *
    * @param PrestacionesSocialesGeneral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PrestacionesSocialesGeneral $entity)
    {
        $form = $this->createForm(new PrestacionesSocialesGeneralType(), $entity, array(
            'action' => $this->generateUrl('prestacionessocialesgeneral_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing PrestacionesSocialesGeneral entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSocialesGeneral.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('prestacionessocialesgeneral_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PrestacionesSocialesGeneral entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSocialesGeneral.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('prestacionessocialesgeneral'));
    }

    /**
     * Creates a form to delete a PrestacionesSocialesGeneral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prestacionessocialesgeneral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    /**
     * Calcula las prestaciones sociales de los funcionarios correspondientes.
     *
     */
    public function calcularAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Datos de las prestaciones sociales general seleccionada
        $prestacionesSeleccionada = $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->findOneBy(array('id' => $id));
        $fecha = $prestacionesSeleccionada->getFecha();
        $mes = intval($fecha->format('n'));
        
        $fecha1 = clone $fecha;
        $fecha2 = clone $fecha;
        $fechaDesde = $fecha1->modify('first day of');
        $fechaHasta = $fecha2->modify('last day of');
        
        //Bloque para determinar los funcionarios que han sido procesados en un determinado cálculo de prestaciones sociales
        $consultaPrestacionesPrevias = $em->createQuery("SELECT p FROM CetNominaBundle:PrestacionesSocialesGeneral p WHERE (p.fecha BETWEEN :fechad AND :fechah ) "
                . "AND p.fk_prestaciones_tipo_nomina1=".$prestacionesSeleccionada->getFkPrestacionesTipoNomina1()->getId()." ");
        $consultaPrestacionesPrevias->setParameter('fechad',$fechaDesde,\Doctrine\DBAL\Types\Type::DATETIME);
        $consultaPrestacionesPrevias->setParameter('fechah',$fechaHasta,\Doctrine\DBAL\Types\Type::DATETIME);
        $prestacionesPrevias=$consultaPrestacionesPrevias->getResult();
        
        $personas = array();
        foreach($prestacionesPrevias as $prestacion){
            //con el id obtenido buscar  en prestaciones general y 
            if ($id!=$prestacion->getId()){
                $prestacionesSocial = $em->getRepository('CetNominaBundle:PrestacionesSociales')->findBy(array('general' =>$prestacion->getId()));
                foreach($prestacionesSocial as $prestacionSocial){
                    $personas[] = $prestacionSocial->getPersonal()->getId();
                }
            }
        }
        //Datos personales
//        $funcionarios = $em->getRepository('CetNominaBundle:Personal')->findAll();
        $funcionariosSql = $em->createQuery("SELECT p "
            . "FROM CetNominaBundle:Personal p "
            . "JOIN p.informacionLaboral il "
            . "WHERE il.fk_informacion_laboral_tipo_nomina1 = ".$prestacionesSeleccionada->getFkPrestacionesTipoNomina1()->getId()
            );
        $funcionarios = $funcionariosSql->getResult();
        
        $todosConceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
        foreach ($todosConceptos as $concep){
            $nombreConcepto=$concep->getIdentificador();
            $vector_todos_conceptos[]=$nombreConcepto;
        }
        
        //$variables = $em->getRepository('CetNominaBundle:Variable')->findAll();
        //Deben existir al menos 3 variables denominadas: dias_otros_organismos, dias_servicio_contraloria, sueldo_promedio, bono_vacacional_variable, bono_vacacional_dato_variable
        $cv = $em->createQuery("SELECT v.nombre,v.sql "
                . "FROM CetNominaBundle:Variable v "
                . "WHERE v.nombre LIKE '%dias_otros_organismos%' "
                . "OR v.nombre LIKE '%dias_servicio_contraloria%' "
                . "OR v.nombre LIKE '%bono_vacacional_variable%' "
                . "OR v.nombre LIKE '%bono_vacacional_dato_variable%' "
                . "OR v.nombre LIKE '%vigilante%' "
                . "OR v.nombre LIKE '%sueldo_promedio%'");
        $todasVariables = $cv->getResult();
            
        //Se establece la fecha para incluir los 12 ultimos meses de calculo
        $nuevaFecha = clone $fecha;
        $nuevaFechaFin = clone $fecha;
        $nuevaFecha = $nuevaFecha->modify('-1 year');
        $nuevaFecha = $nuevaFecha->modify('first day of');
        $nuevaFechaFin = $nuevaFechaFin->modify('first day of');
        
        $personacargo = array();
        
        //Bloque de llamadas de funciones globales
        $usuario = $this->get('security.context')->getToken()->getUser();
        $helper = $this->get('cet.nomina.globales');
        $revision = $helper->obtenerRevisores($em,$usuario);
        
        //Bloque para ejecutar calculaAction
        $clientevariable = new VariableController();
        $clienteconcepto = new ConceptoController();
        $clientevariable->setContainer($this->container);
        $clienteconcepto->setContainer($this->container);
        
        foreach ($funcionarios as $funcionario){
            $cedula = $funcionario->getId();
            
            //Revisar arreglo $personas para definir si ya fue calculado en nóminas similares previas.
            if(array_search($cedula,$personas)!==false)
                continue;

            //Datos laborales
            $informacionLaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $cedula));

            //Si el funcionario no tiene información laboral
            if($informacionLaboral == null) continue;
            
            //Determinar si ingresó antes o despues del 07/05/2012 
            $fechaIngreso = $informacionLaboral->getFechaIngreso();
            $mesIngreso = intval($fechaIngreso->format('n'));
            
            //Si ingresó despues del 07/05/2012 (Prestaciones según trimestre de ingreso)
            $calcula = 0;
            $calculaAdicional = 0;                        
            if($fechaIngreso >= new DateTime('2012-05-07')){                                 
                $offset = 3; 
                for($i=0 ; $i<12/$offset ; $i++){
                    $fechaPrueba = clone $fechaIngreso;
                    $fechaPrueba = $fechaPrueba->modify('first day of');
                    $valor = ($i*$offset) + 1; //1, 4, 7, 10
                    $fechaPrueba = $fechaPrueba->modify("+$valor month"); 
                    $mesPrueba = intval($fechaPrueba->format('n')); 
                    if( $mesPrueba == $mes ){
                        $calcula++;
                    }
                }                
            }
            //Si ingresó antes del 07/05/2012 (Según trimestres del año)
            else{
                if($mes == 1 || $mes == 4 || $mes == 7 || $mes == 10){
                    $calcula++;
                }
            }
            
            //Se establece la fecha para excluir a los ingresados durante el trimestre
            $fechaDentroTrimestre = clone $fecha;
            $fechaDentroTrimestre = $fechaDentroTrimestre->modify('first day of');
            $fechaDentroTrimestre = $fechaDentroTrimestre->modify('-3 month');
            if($fechaIngreso > $fechaDentroTrimestre){    
                $calcula = 0;
            }
            //No corresponde el cálculo de prestaciones sociales
            if( !$calcula ){
                continue;
            }
            
            //Bloque para verificar si corresponde calculo de dias adicionales
            $fechaPrueba = clone $fechaIngreso;            
            for($i=1 ; $i<=3 ; $i++){
                $fechaPrueba->modify("+1 month");   
                $mesPrueba = intval($fechaPrueba->format('n'));
                if($mesPrueba == $mes){
                    $calculaAdicional++;
                }
            }            
            if($fechaIngreso <= new DateTime('1997-06-19')){
                if($mes == 7)
                    $calculaAdicional++;
                else
                    $calculaAdicional = 0;
            }
            
            //Se establecen a cero todas las variables necesarias para el cálculo del bono vacacional
            //porcentaje_retencion_islr, dias_servicio_contraloria y dias_otros_organismos
            $vector_variables['porcentaje_retencion_islr'] = 0;
            $vector_variables['dias_servicio_contraloria'] = 0;
            $vector_variables['dias_otros_organismos'] = 0;
            $vector_variables['vigilante'] = 0;

            $fechaTope = clone $fecha;            
            $fechaTope = $fechaTope->modify('first day of');
            $fechaTope = $fechaTope->modify('-1 month');
            $fechaTope = $fechaTope->modify('last day of');
            
            foreach($todasVariables as $objeto){       
                $sqlvariable = $objeto['sql'];
                $nombreVariable = $objeto['nombre'];
    //            $sqlvariable=$objeto->getFkVariableHasPersonalVariable1()->getSql();
    //            $nombreVariable=$objeto->getFkVariableHasPersonalVariable1()->getNombre();

                if(preg_match("/CURRENT_DATE()/i",$sqlvariable)){
                    $sqlvariable = str_replace("CURRENT_DATE()", "(:fecha_tope)", $sqlvariable);
                    $consultasql = $em->createQuery($sqlvariable);
                    $consultasql->setParameter('fecha_tope',$fechaTope,\Doctrine\DBAL\Types\Type::DATETIME);
                }else{
                    $consultasql = $em->createQuery($sqlvariable);
                }  
                
                $consultasql->setParameter('personal_cedula', $cedula);
                $consultasql->setMaxResults(1);
                $valorVariable=0;
                if ($consultasql->getResult()){
                    foreach($consultasql->getSingleResult() as $dato){
                        if(!is_object($dato)){
                            $valorVariable=$dato;
                        }                    
                        elseif($dato instanceof \DateTime){
    //                        $valorVariable=$dato->format('d/m/Y');
    //                        $valorVariable=new \DateTime($valorVariable);
                            $valorVariable=$dato;
                        }
                    }
                }            
    //                    $$nombreVariable=$valorVariable;
                $vector_variables[$nombreVariable]=$valorVariable;
            }
            /////////////////////////////
            //Inicio Variables Predefinidas
            /////////////////////////////
                //Cuenta cantidad de lunes en un lapso de fechas
                $lunes = 1;
                $helper = $this->get('cet.nomina.globales');            
                $lunes = $helper->countDays(1, mktime(0, 0, 0, 1, 1, 2000), mktime(0, 0, 0, 2, 1, 2000));
                $vector_variables["cantidad_lunes"] = $lunes;
            /////////////////////////////
            //Fin Variables Predefinidas
            /////////////////////////////

            $vector_conceptos = $vector_todos_conceptos;
            $vector_evaluado = array();
            $var=new VariableFormula();        
            $vector_variables['Variable']=$var;
            
            //Bloque para determinar nominas para el cálculo
            $ultimaNomina = 0;        
            $consultaUltimaNomina = $em->createQuery("SELECT hc.nominaId AS id_nomina,hc.fechaInicio AS maxima_fecha "
                . "FROM CetNominaBundle:HistoricoConcepto hc "
                . "JOIN hc.fk_historico_concepto_personal1 p "
                . "JOIN p.informacionLaboral il "
                . "JOIN hc.fk_historico_concepto_nomina1 n "
                . "JOIN n.fk_nomina_plantilla_nomina1 pn "
                . "WHERE il.fk_informacion_laboral_tipo_nomina1 = pn.fk_plantilla_nomina_tipo_nomina1 "
                . "AND n.tipo = 'Ordinaria' "
                . "AND hc.fechaInicio >= :fecha "
                . "AND hc.fechaFin < :fechaFin "    
                . "AND p.id = :cedula "
                . "GROUP BY id_nomina,maxima_fecha "
                . "ORDER BY maxima_fecha DESC "               
                );            
            $consultaUltimaNomina->setParameter('fecha',$nuevaFecha,\Doctrine\DBAL\Types\Type::DATETIME);
            $consultaUltimaNomina->setParameter('fechaFin',$nuevaFechaFin,\Doctrine\DBAL\Types\Type::DATETIME);
            $consultaUltimaNomina->setParameter('cedula',$cedula);
//            $consultaUltimaNomina->setMaxResults(2);                    
            $nominas = $consultaUltimaNomina->getResult();

            $ultimaNomina = 0;
            $primeraNomina = 0;
            $contador = 0; 
            $vector_nomina = array();
            $vector_nomina[] = "0";
            foreach($nominas as $nomina){
                if($contador == 0)
                    $primeraNomina = $nomina['id_nomina'];
                if($contador == 1)
                    $ultimaNomina = $nomina['id_nomina'];
                $contador++;
                $vector_nomina[] = $nomina['id_nomina'];
            }

            $fechaUltimoMes = clone $fecha;
            $fechaUltimoMes = $fechaUltimoMes->modify('-1 month');
            
            //Mes con ceros a la izquierda
            $ultimoMes = $fechaUltimoMes->format('m');  
            //Año con cuatro digitos
            $ultimoAnio = $fechaUltimoMes->format('Y');
            
            //Si corresponde el cálculo de prestaciones sociales adicionales
            $total = 0; 
            //Datos de conceptos   calculados en para dias adicionales ultimos doce meses              
            $arregloAdicionales = array();
            if( $calculaAdicional ){   
                
                //Datos para promedio de ultimos 12 meses
                $consultaPromedio = $em->createQuery("SELECT hc "
                    . "FROM CetNominaBundle:HistoricoConcepto hc "
                    . "JOIN hc.fk_historico_concepto_personal1 p "
                    . "JOIN hc.fk_historico_concepto_concepto1 c "
                    . "WHERE p.id=".$cedula." "
                    . "AND "
                    . "( "
                    . "hc.fk_historico_concepto_nomina1 IN (".implode(",", $vector_nomina).") "
                    . ") "
                    . "ORDER BY c.codigo");
                $informacionPromedio = $consultaPromedio->getResult();
                           
                foreach($informacionPromedio as $conceptos)
                {   
                    $idConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getId();
                    $tipoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getTipo();
                    $denominacionConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion();
                    $codigoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getCodigo();
                    if(!preg_match('/^Retroactivo/i',$denominacionConcepto)){
                        if ($tipoConcepto == 'Asignación'){
                            $total = $total + ( $conceptos->getMonto() );
                            $arregloAdicionales["_".$ultimoAnio.$ultimoMes]["_".$idConcepto]=array(str_pad($codigoConcepto, 3, "0", STR_PAD_LEFT),(isset($arregloAdicionales["_".$ultimoAnio.$ultimoMes]["_".$idConcepto][1])?($arregloAdicionales["_".$ultimoAnio.$ultimoMes]["_".$idConcepto][1] +$conceptos->getMonto()): $conceptos->getMonto() ));
                        }
                    }
                }
                //En $total deberian estar incluidas todas las nominas de los ultimos 12 meses, aguinaldos
                //El bono vacacional o se incluye en nominas eventuales o se lee directamente de la base de datos
                $total = round ( $total / 12 , 2 );
                
            }
            
            //Datos de conceptos calculados para garantia de prestaciones (Solo las 2 ultimas quincenas)              
            $arreglo = array();
            $totalAsignacion = 0;
            $totalDeduccion = 0;
           
            $esVigilante = round($clienteconcepto->evaluarConcepto("vigilante",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) , 2);
            if($esVigilante == 0){
                $consultaInformacion = $em->createQuery("SELECT hc "
                        . "FROM CetNominaBundle:HistoricoConcepto hc "
                        . "JOIN hc.fk_historico_concepto_personal1 p "
                        . "JOIN hc.fk_historico_concepto_concepto1 c "
                        . "WHERE p.id=".$cedula." "
                        . "AND "
                        . "( "
                        . "hc.fk_historico_concepto_nomina1=$primeraNomina "
                        . "OR hc.fk_historico_concepto_nomina1=$ultimaNomina"
                        . ") "
                        . "ORDER BY c.codigo");
                $informacion = $consultaInformacion->getResult();
                
                foreach($informacion as $conceptos)
                {   
                    $idConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getId();
                    $codigoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getCodigo();
                    $tipoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getTipo();
                    $denominacionConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion();

                    if(!preg_match('/^Retroactivo/i',$denominacionConcepto)){                   
                        if ($tipoConcepto == 'Asignación'){
                            $totalAsignacion = $totalAsignacion + ( $conceptos->getMonto() );
                            $arreglo["_".$ultimoAnio.$ultimoMes]["_".$idConcepto]=array(str_pad($codigoConcepto, 3, "0", STR_PAD_LEFT),(isset($arreglo["_".$ultimoAnio.$ultimoMes]["_".$idConcepto][1])?($arreglo["_".$ultimoAnio.$ultimoMes]["_".$idConcepto][1] +$conceptos->getMonto()): $conceptos->getMonto() ));
                        }                        
                    }
                }
            }
            else{//O promedio de los últimos 6 meses para personal vigilante por sueldo variable
                
                $totalVigilante = 0;
                $fechaInicio = clone $fecha;
                $fechaComparacion = clone $fecha;
                $fechaCompara6Antes=$fechaComparacion->modify('-6 month');
                $fechaFin = clone $fecha;
                if($fechaCompara6Antes >$fechaIngreso ){
                    $fechaInicio = $fechaInicio->modify('-6 month');
                    $fechaInicio = $fechaInicio->modify('first day of');
                }else //tiene menos de 6 meses de haber ingresado o reingresado
                    $fechaInicio = $fechaIngreso;
                $fechaFin = $fechaFin->modify('first day of');
        
                $consultapromedioSeisMeses = $em->createQuery("SELECT hc.nominaId AS id_nomina,hc.fechaInicio AS maxima_fecha "
                    . "FROM CetNominaBundle:HistoricoConcepto hc "
                    . "JOIN hc.fk_historico_concepto_personal1 p "
                    . "JOIN p.informacionLaboral il "
                    . "JOIN hc.fk_historico_concepto_nomina1 n "
                    . "JOIN n.fk_nomina_plantilla_nomina1 pn "
                    . "WHERE il.fk_informacion_laboral_tipo_nomina1 = pn.fk_plantilla_nomina_tipo_nomina1 "
                    . "AND n.tipo = 'Ordinaria' "
                    . "AND hc.fechaInicio >= :fecha "
                    . "AND hc.fechaFin < :fechaFin "    
                    . "AND p.id = :cedula "
                    . "GROUP BY id_nomina,maxima_fecha "
                    . "ORDER BY maxima_fecha DESC "               
                    );            
                $consultapromedioSeisMeses->setParameter('fecha',$fechaInicio,\Doctrine\DBAL\Types\Type::DATETIME);
                $consultapromedioSeisMeses->setParameter('fechaFin',$fechaFin,\Doctrine\DBAL\Types\Type::DATETIME);
                $consultapromedioSeisMeses->setParameter('cedula',$cedula);
    //            $consultaUltimaNomina->setMaxResults(2);                    
                $nominasPromedio = $consultapromedioSeisMeses->getResult();
             
                //Crear vector con id de ultimas seis nominas
                $vector_nomina_promedio = array();
                $vector_nomina_promedio[] = "0";
                foreach($nominasPromedio as $np){
                    $vector_nomina_promedio[] = $np['id_nomina'];
                }
                //Datos para promedio de ultimos 6 meses
                $cPromedio = $em->createQuery("SELECT hc "
                    . "FROM CetNominaBundle:HistoricoConcepto hc "
                    . "JOIN hc.fk_historico_concepto_personal1 p "
                    . "JOIN hc.fk_historico_concepto_concepto1 c "
                    . "WHERE p.id=".$cedula." "
                    . "AND "
                    . "( "
                    . "hc.fk_historico_concepto_nomina1 IN (".implode(",", $vector_nomina_promedio).") "
                    . ") "
                    . "ORDER BY c.codigo");
                $iPromedio = $cPromedio->getResult();
                    
                foreach($iPromedio as $conceptos)
                {   
                    $idConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getId();
                    $tipoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getTipo();
                    $denominacionConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion();
                    $codigoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getCodigo();
                    
                    if(!preg_match('/^Retroactivo/i',$denominacionConcepto)){
                        if ($tipoConcepto == 'Asignación'){
                            $fechaConcepto = $conceptos->getFechaInicio();
                            //Mes con ceros a la izquierda
                            $ultimoMes = $fechaConcepto->format('m');  
                            //Año con cuatro digitos
                            $ultimoAnio = $fechaConcepto->format('Y');                            
                            $totalVigilante = round ($totalVigilante + $conceptos->getMonto() , 2); 
                            $arreglo["_".$ultimoAnio.$ultimoMes]["_".$idConcepto]=array(str_pad($codigoConcepto, 3, "0", STR_PAD_LEFT),(isset($arreglo["_".$ultimoAnio.$ultimoMes]["_".$idConcepto][1])?round($arreglo["_".$ultimoAnio.$ultimoMes]["_".$idConcepto][1] +$conceptos->getMonto(),2): $conceptos->getMonto() ));
                        }
                    }
                }
//                $totalVigilante = round ( $totalVigilante / 6 , 2 );
            }
            
            //Bloque de sustitución de montos por conceptos por medio de Datos Variables "Garantía de prestaciones sociales y Días adicionales"
            //Buscar los concepto que se sustituirán en datos variables para cada persona       
            $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datos_variables_personal1'=>$cedula));
            if($datosvariables!=null){
                foreach($datosvariables as $datosvariable){
                        $dato = $datosvariable->getFkDatosVariablesConcepto1();
                        $monto = $datosvariable->getValor();
                    if($datosvariable->getSeccionAfectacion()=='prestaciones'){  
                        $arreglo["_".$ultimoAnio.$ultimoMes]["_".$dato->getId()] = array(str_pad($dato->getCodigo(), 3, "0", STR_PAD_LEFT), $monto );
                    }
                    elseif($datosvariable->getSeccionAfectacion()=='adicionales'){
                        $arregloAdicionales["_".$ultimoAnio.$ultimoMes]["_".$dato->getId()] = array(str_pad($dato->getCodigo(), 3, "0", STR_PAD_LEFT), $monto );
                    }                    
                }
            }            
            
            //Promedio de los 12 ultimos meses, incluyendo nominas, aguinaldos y bono vacacional            
            $bonoVacacionalVariable = round($clienteconcepto->evaluarConcepto("bono_vacacional",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) , 2);
            $sueldoPromedio = round($clienteconcepto->evaluarConcepto("sueldo_promedio",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) , 2);
            //$total incluye solo promedio de nominas de sueldo y aguinaldos. Entonces se debe sumar el promedio del bono vacacional
            $totalPromedio = $total + round($bonoVacacionalVariable / 12 , 2);
            //Si no hay dato generico de sueldo promedio se utiliza el calculado
            $sueldoPromedio = ($sueldoPromedio == 0) ? $totalPromedio : $sueldoPromedio;
            
            $aniosOtrosOrganismos = round ($clienteconcepto->evaluarConcepto("dias_otros_organismos",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) / 365 , 2);
            $aniosServicioContraloria = round ($clienteconcepto->evaluarConcepto("dias_servicio_contraloria",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) / 365 , 2);
            $aniosServicio = round($aniosOtrosOrganismos + $aniosServicioContraloria,2);

            if ( $aniosServicio <= 5 ) {
                $diasBonoVacacional = 35;          
            }
            elseif ( $aniosServicio <= 10 ) {
                $diasBonoVacacional = 40; 
            }
            elseif ( $aniosServicio <= 15 ) {
                $diasBonoVacacional = 43;
            }        
            else {
                $diasBonoVacacional = 48;
            }
            $diasDisfrute2 = $aniosServicioContraloria - 1;
            
            //De acuerdo a solicitud de RRHH, cuando sea alto funcionario tiene top de 40 diasBonoVacacional
            if( preg_match('/^Altos Funcionarios/i',$prestacionesSeleccionada->getFkPrestacionesTipoNomina1()->getNombre() ) )
                $diasBonoVacacional = $diasBonoVacacional>40?40:$diasBonoVacacional;
            
            //De acuerdo a solicitud de RRHH, cuando sea alto funcionario tiene top de 40 diasBonoVacacional
            if( preg_match('/^Obreros fijos/i',$prestacionesSeleccionada->getFkPrestacionesTipoNomina1()->getNombre() ) ){
                if ( $aniosServicio <= 16 ) {
                    $diasBonoVacacional = 14 + floor($aniosServicio);          
                }
                else{
                    $diasBonoVacacional = 30; 
                }                
            }
            

            $nombreFuncionario = $funcionario->getPrimerNombre()." ".$funcionario->getSegundoNombre()." ".$funcionario->getPrimerApellido()." ".$funcionario->getSegundoApellido();
            
            $diasTrimestre = 15;
            
            $totalAsignacion = round( $totalAsignacion, 2 );
            
            //Conceptos sumados
            $totalAsignacionConceptosDiarios = 0;
            $arregloNuevo = array();
            foreach ($arreglo as $value) {
                $arregloConceptos = array();
                foreach ($value as $partes) {
                    //Montos de conceptos diarios
                    $partes[1] = round($partes[1] / 30, 2);
                    $totalAsignacionConceptosDiarios += $partes[1];
                    foreach ($partes as $parte) {
                        $arregloConceptos[] = $parte;
                    }
                }
                $arregloNuevo[] = implode(" : ", $arregloConceptos);
            }
            
            //Si es vigilante
            if($esVigilante != 0){
                $totalAsignacionConceptosDiarios = round( $totalAsignacionConceptosDiarios / (count($arreglo)) , 2 );
            }
            
            //Alicota Bono Vacacional
            $factorBonoVacacional = round( $diasBonoVacacional / 365 , 2 );
            $alicotaBonoVacacional = round( $totalAsignacionConceptosDiarios * $factorBonoVacacional , 2 );
            
            //Alicota Aguinaldos
            $factorAguinaldos = round( 90 / 365 , 2 );
            $alicotaAguinaldos = round( round( $totalAsignacionConceptosDiarios + $alicotaBonoVacacional , 2 ) * $factorAguinaldos , 2 );

            //Prestaciones Sociales
            //$salarioIntegralDiario = round( $salarioDiario + $alicotaBonoVacacional + $alicotaAguinaldos , 2 );
            $salarioIntegralDiario = round( $totalAsignacionConceptosDiarios + $alicotaBonoVacacional + $alicotaAguinaldos , 2 );
            $garantiaPrestacionesSociales = round( $salarioIntegralDiario * $diasTrimestre , 2 );
            
            //Dias adicionales
            $diasAdicionales = 0 ;
            if( $calculaAdicional ){
                $interval = date_diff($fecha, $fechaIngreso);
                $diasAdicionales =  ( $interval->format('%y') - 1 ) * 2;
                $diasAdicionales = $diasAdicionales < 1 ? 0 : $diasAdicionales;
                $diasAdicionales = $diasAdicionales > 30 ? 30 : $diasAdicionales;
            }
            
            //Prestaciones por dias adicionales
            //El sueldo promedio se tomará de un dato generico o sumando todas las nóminas mas aguinaldos mas bono vacacional
            $sueldoDiarioPromedio = round( $sueldoPromedio / 30 , 2 );
            $prestacionesAdicionales = round( $sueldoDiarioPromedio * $diasAdicionales , 2 );
            
            //De acuerdo a solicitud de RRHH, cuando un funcionario tenga 0 en el cálculo de garantía de prestaciones sociales, no se debe mostrar en el reporte.
            if($garantiaPrestacionesSociales <= 0)
                continue;
            
            //Almacena el monto del cálculo en la tabla prestacionessociales   
            $prestaciones = $em->getRepository('CetNominaBundle:PrestacionesSociales')->findOneBy(array('personal' => $cedula,'general' => $id));
            if( $prestaciones == null )
                $prestaciones = new PrestacionesSociales();
            
            $prestaciones->setPrestaciones($garantiaPrestacionesSociales);
            $prestaciones->setDias($diasTrimestre);
            $prestaciones->setDiasAdicionales($diasAdicionales);
            $prestaciones->setPrestacionesAdicionales($prestacionesAdicionales);
            
            //$prestaciones->setIngresoDiario($salarioDiario);
            $prestaciones->setIngresoDiario($totalAsignacionConceptosDiarios);
            $prestaciones->setAlicotaBonoVacacional($alicotaBonoVacacional);
            $prestaciones->setAlicotaAguinaldos($alicotaAguinaldos);
            $prestaciones->setPrestacionesDiaria($salarioIntegralDiario);

            $prestaciones->setConceptos(implode(" ; ",$arregloNuevo));
            
            $prestaciones->setPersonal($funcionario);
            $prestaciones->setGeneral($prestacionesSeleccionada);
            
            $em->persist($prestaciones);
            $em->flush();
            
        }   
        
//        $deleteForm = $this->createDeleteForm($id);
//        return $this->render('CetNominaBundle:PrestacionesSocialesGeneral:show.html.twig', array(
//            'entity'      => $prestacionesSeleccionada,
//            'delete_form' => $deleteForm->createView(),        ));
        
        $response = $this->forward('CetNominaBundle:PrestacionesSocialesGeneral:show', array('id' => $id));
        return $response;
    }
    
    /**
     * Reversar una nomina.
     *
     */
    public function reversarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CetNominaBundle:PrestacionesSociales')->findBy(array('general' => $id));
        
        foreach ($entities as $entity) {
            $em->remove($entity);
        }
        $em->flush();

        $response = $this->forward('CetNominaBundle:PrestacionesSocialesGeneral:index');
        return $response;
             
    }
}
