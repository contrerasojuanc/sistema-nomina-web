<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Personal;
use Cet\NominaBundle\Form\PersonalType;
use Cet\NominaBundle\Form\PersonalSinRelacionType;
use Cet\NominaBundle\Form\PersonalSinRelacionNewType;

/**
 * Personal controller.
 *
 */
class PersonalController extends Controller
{

    /**
     * Lists all Personal entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //$consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo'); sirve para mostrar el historico de direcciones no borrar
        //$consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaFin is null');
        //$consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo');
        
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
        
        $queryPersonal = "SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido,il.id AS informacionLaboral "
            . "FROM CetNominaBundle:Personal p "
            . "LEFT JOIN p.informacionLaboral il "
            . "ORDER BY p.id ASC";
        
        $consulta = $em->createQuery($queryPersonal);        
        $informacionpersonal = $consulta->getResult();
        
        foreach($informacionpersonal as &$personal){
            $personal['nombre'] = "SIN UNIDAD ORGANIZATIVA";
            $queryUnidadOrganizativa = "SELECT uo.nombre "
            . "FROM CetNominaBundle:HistoricoCargo hc "
            . "LEFT JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
            . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
            . "WHERE hc.fk_informacion_laboral_has_cargo_informacion_laboral1 = :informacionLaboral "
            . "AND hc.fechaInicio IN "
                . "("
                    . "SELECT MAX(hc2.fechaInicio) " 
                    . "FROM CetNominaBundle:HistoricoCargo hc2 "
                    . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = :informacionLaboral "
                    . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END)) "
                . ")";
            $consulta = $em->createQuery($queryUnidadOrganizativa);
            $consulta->setParameter('informacionLaboral',$personal['informacionLaboral']);  
            
            $unidadOrganizativa = $consulta->getResult(); 
            foreach($unidadOrganizativa as $uo){
                $personal['nombre'] = $uo['nombre'];
            }             
        }
            
        return $this->render('CetNominaBundle:Personal:index.html.twig', array(
            'entities' => $informacionpersonal,
        ));
    }
    /**
     * Creates a new Personal entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Personal();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:Personal:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Personal entity.
    *
    * @param Personal $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Personal $entity)
    {
        $form = $this->createForm(new PersonalSinRelacionNewType(), $entity, array(
            'action' => $this->generateUrl('personal_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Personal entity.
     *
     */
    public function newAction()
    {
        $entity = new Personal();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:Personal:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Personal entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $personal = $em->getRepository('CetNominaBundle:Personal')->find($id);
        $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $id));
        $tiponomina="";
        if(isset($informacionlaboral)){
            $tiponomina=$informacionlaboral->getFkInformacionLaboralTipoNomina1()->getNombre();
        }        
        $consulta = $em->createQuery("select c.nombre, c.sueldo, c.nivel ,hc.fechaInicio as cargoinicia,hc.id as historicocargo,hc.esEncargado,hc.fechaFin from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c where p.id=".$id ." and hc.esEncargado='f' order by hc.fechaInicio DESC");
        $cargosueldo = $consulta->getResult();
        
        $consulta = $em->createQuery('select cf from CetNominaBundle:CargaFamiliar cf where cf.fk_carga_familiar_personal1='.$id.' order by cf.parentesco,cf.fechaNacimiento');
        $cargafamiliar = $consulta->getResult();
        
        $consulta = $em->createQuery('select c from CetNominaBundle:CorreoPersonal c where c.fk_correo_personal_personal1='.$id);
        $correo = $consulta->getResult();
        
        $consulta = $em->createQuery('select t from CetNominaBundle:TelefonoPersonal t where t.fk_telefono_personal_personal='.$id);
        $telefono = $consulta->getResult();
        
        $consulta = $em->createQuery('select vi.condicion,vi.direccion,vi.codigoPostal,vi.referencia,pa.nombre parroquia,mu.nombre municipio,es.nombre estado from CetNominaBundle:Vivienda vi join vi.fk_vivienda_parroquia1 pa join pa.fk_parroquia_municipio1 mu join mu.fk_municipio_estado1 es  where vi.personal='.$id);
        $vivienda = $consulta->getResult();
        
        $consulta = $em->createQuery('select im from CetNominaBundle:InformacionMedica im where im.personal='.$id);
        $informacionmedica = $consulta->getResult();
        
        $consulta = $em->createQuery('select en.id,en.descripcion,en.tratamiento,en.costoTratamiento from CetNominaBundle:InformacionMedica im join im.enfermedads en where im.personal='.$id);
        $enfermedades = $consulta->getResult();
        
        $consulta = $em->createQuery('select es from CetNominaBundle:Estudio es where es.fk_estudios_personal1='.$id.' order by es.fechaInicio');
        $estudio = $consulta->getResult();
        
        $consulta = $em->createQuery('select el from CetNominaBundle:ExperienciaLaboral el where el.fk_experiencia_laboral_personal1='.$id.' order by el.fechaIngreso');
        $experiencialaboral = $consulta->getResult();
        
        $consulta = $em->createQuery('select v from CetNominaBundle:Vehiculo v where v.fk_vehiculo_personal1='.$id);
        $vehiculo = $consulta->getResult();
        
    
        //$consulta = $em->createQuery("select uo.nombre,huo.fechaInicio,huo.fechaFin, c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,huo.id as historicounidadorganizativa,hc.id as historicocargo,hc.esEncargado from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio is null and huo.fechaFin is null and hc.fechaFin is null and hc.esEncargado='f' and p.id=".$id." or huo.fechaInicio is not null and huo.fechaFin is null and hc.fechaFin is null and hc.esEncargado='f' and p.id=".$id);
        $consulta = $em->createQuery("select uo.nombre,huo.fechaInicio,huo.fechaFin,huo.id as historicounidadorganizativa from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where p.id=".$id ." order by huo.fechaInicio DESC");
        $historicounidadorganizativa = $consulta->getResult();
        
        //$consulta = $em->createQuery("select uo.nombre,huo.fechaInicio,huo.fechaFin, c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,huo.id as historicounidadorganizativa,hc.id as historicocargo,hc.esEncargado from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio is null and huo.fechaFin is null and hc.fechaFin is null and hc.esEncargado='f' and p.id=".$id." or huo.fechaInicio is not null and huo.fechaFin is null and hc.fechaFin is null and hc.esEncargado='f' and p.id=".$id);
        //$consulta = $em->createQuery("select c.nombre cargo,c.sueldo,c.nivel,hc.fechaInicio as cargoinicia,hc.id as historicocargo,hc.esEncargado,hc.fechaFin from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c where p.id=".$id ." and (hc.esEncargado is null OR hc.esEncargado='f') order by hc.fechaInicio DESC");
        $consulta = $em->createQuery("select c.nombre cargo,c.sueldo,c.nivel,hc.fechaInicio as cargoinicia,hc.id as historicocargo,hc.esEncargado,hc.fechaFin,uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where p.id=".$id ." and (hc.esEncargado is null OR hc.esEncargado='f') order by hc.fechaInicio DESC");
        $historicocargo = $consulta->getResult();
        
        //$consulta = $em->createQuery("select uo.nombre,huo.fechaInicio,huo.fechaFin, c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,huo.id as historicounidadorganizativa,hc.id as historicocargo,hc.esEncargado from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio is null and huo.fechaFin is null and hc.fechaFin is null and hc.esEncargado='t' and p.id=".$id." or huo.fechaInicio is not null and huo.fechaFin is null and hc.fechaFin is null and hc.esEncargado='t' and p.id=".$id);
        //esta no muestra las encargadurias finalizadas//$consulta = $em->createQuery("select uo.nombre, c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,hc.id as historicocargo,hc.esEncargado from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where hc.fechaInicio is not null and hc.fechaFin is null and hc.esEncargado='t' and p.id=".$id." order by hc.fechaInicio");
        $consulta = $em->createQuery("select uo.nombre, c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,hc.id as historicocargo,hc.esEncargado,hc.fechaFin from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where hc.fechaInicio is not null and hc.esEncargado='t' and p.id=".$id." order by hc.fechaInicio DESC");
        $encargadurias = $consulta->getResult();
        
        //$consulta = $em->createQuery('select uo.nombre,huo.fechaInicio,huo.fechaFin, c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,huo.id as historicounidadorganizativa,hc.id as historicocargo,hc.esEncargado from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join il.historicoCargos hc left join hc.fk_informacion_laboral_has_cargo_cargo1 c left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio is null and huo.fechaFin is null and hc.fechaFin is null and p.id='.$id.' or huo.fechaInicio is not null and huo.fechaFin is null and hc.fechaFin is null and p.id='.$id);
        //$historicounidadorganizativa = $consulta->getResult();
        
        
        if (!$personal) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Personal.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Personal:show.html.twig', array(
            'personal'      => $personal,
            'cargosueldo'=>$cargosueldo,
            'tiponomina'    => $tiponomina,
            'informacionlaboral'     => $informacionlaboral,
            'cargafamiliar'     => $cargafamiliar,
            'correo'     => $correo,
            'telefono'     => $telefono,
            'vivienda'     => $vivienda,
            'informacionmedica'     => $informacionmedica,
            'enfermedades'     => $enfermedades,
            'estudio'     => $estudio,
            'experiencialaboral'     => $experiencialaboral,
            'vehiculo'     => $vehiculo,
            'historicounidadorganizativa'     => $historicounidadorganizativa,
            'historicocargo'     => $historicocargo,
            'encargadurias'     => $encargadurias,
            'delete_form' => $deleteForm->createView(),        ));
        }

    /**
     * Displays a form to edit an existing Personal entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Personal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Personal.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Personal:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Personal entity.
    *
    * @param Personal $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Personal $entity)
    {
        $form = $this->createForm(new PersonalSinRelacionType(), $entity, array(
            'action' => $this->generateUrl('personal_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing Personal entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Personal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Personal.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:Personal:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Personal entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Personal')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Personal.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('personal'));
    }

    /**
     * Creates a form to delete a Personal entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personal_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
