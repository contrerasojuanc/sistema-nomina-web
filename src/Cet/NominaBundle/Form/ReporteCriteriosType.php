<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ReporteCriteriosType extends AbstractType
{
    public $valor;
    
    public function __construct($valor = null)
    {
        $this->valor = $valor;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\Reporte");
        $userTransformer->setEntityRepository("CetNominaBundle:Reporte");
        $userTransformer->setEntityType("Reporte");
        
        $builder
            ->add('nombre', null, array('read_only' => true))
            ->add('comentario', null, array('read_only' => true))
            ->add('tipo', 'hidden')
            ->add($builder->create('reporte','hidden',array(
                'required'  => false,
            ))->addModelTransformer($userTransformer))
        ;
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $reporte = $event->getData();
            $form = $event->getForm();
            if(null === $reporte->getTipo()){
                $form->add('valor');
            }
            else{
                $form->add('valor','entity', array(
                    'class' => $reporte->getTipo(),
    //                'property' => 'primerNombre',
                    'required'  => false,
                    'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione una opción')
                ))
                ;
            }
        });
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Criterio'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_criterio';
    }
}
