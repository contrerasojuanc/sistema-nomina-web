<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\CargaMasiva;
use Cet\NominaBundle\Entity\DatosGenericos;
use Cet\NominaBundle\Entity\DatosVariables;
use Cet\NominaBundle\Form\CargaMasivaType;

/**
 * CargaMasiva controller.
 *
 */
class CargaMasivaController extends Controller
{

    /**
     * Lists all CargaMasiva entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:CargaMasiva')->findAll();

        return $this->render('CetNominaBundle:CargaMasiva:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    /**
     * anular CargaMasiva entities.
     *
     */
    public function anularAction($id,$opcion)
    {   
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CetNominaBundle:CargaMasiva')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaMasiva.');
        }
     
        if ($opcion=='variables'){
//            $datosVariables = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datosVariables_cargaMasiva1'=>$id));
//            foreach ($datosVariables as $datosVariables) {
//                $datosv = $em->getRepository('CetNominaBundle:DatosVariables')->find($datosVariables->getId());
//                $em->remove($datosv);
//                $em->flush();
//            }
        }
        
        $em->remove($entity);
        $em->flush();
        
        if ($opcion=='variables'){
            return $this->redirect($this->generateUrl('cargamasiva_variables'));
        }elseif($opcion=='genericos'){
            return $this->redirect($this->generateUrl('cargamasiva_genericos'));
        }
        
    }
    
    /**
     * Lists all CargaMasiva entities.
     *
     */
    public function genericosAction()
    {
        $em = $this->getDoctrine()->getManager();

        //$entities = $em->getRepository('CetNominaBundle:CargaMasiva')->findAll();
        $entities = $em->getRepository('CetNominaBundle:CargaMasiva')->findBy(array('tipocarga'=>'G'));

        return $this->render('CetNominaBundle:CargaMasiva:index.html.twig', array(
            'entities' => $entities,
            'tipo' => 'genericos',
        ));
    }
    /**
     * Lists all CargaMasiva entities.
     *
     */
    public function variablesAction()
    {
        $em = $this->getDoctrine()->getManager();

        //$entities = $em->getRepository('CetNominaBundle:CargaMasiva')->findAll();
        $entities = $em->getRepository('CetNominaBundle:CargaMasiva')->findBy(array('tipocarga'=>'V'));

        return $this->render('CetNominaBundle:CargaMasiva:index.html.twig', array(
            'entities' => $entities,
            'tipo' => 'variables',
        ));
    }
    /**
     * Creates a new CargaMasiva entity.
     *
     */
    public function createAction(Request $request,$opcion)
    {
        $entity = new CargaMasiva();
        $form = $this->createCreateForm($entity,$opcion);
        $form->handleRequest($request);
        
       $em = $this->getDoctrine()->getManager(); 
       $nombretxt= $form->get('rutatxt')->getData()->getClientOriginalName();
       $buscartxt = $em->getRepository('CetNominaBundle:CargaMasiva')->findOneBy(array('nombretxt'=>$nombretxt));
       
       if ($opcion=='genericos'){
           $entity->setTipoCarga('G');
       }elseif ($opcion=='variables'){
           $entity->setTipoCarga('V');
       }
       
        if (!empty($buscartxt)) {
            throw $this->createDuplicatedException('El archivo ya fue utilizado en otra carga masiva, verifíque');
            
        }
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cargamasiva_show', array('id' => $entity->getId(),'opcion' => $opcion)));
            
        }

        return $this->render('CetNominaBundle:CargaMasiva:new.html.twig', array(
            'entity' => $entity,
            'tipo' => $opcion,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a CargaMasiva entity.
    *
    * @param CargaMasiva $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(CargaMasiva $entity,$opcion)
    {
        $form = $this->createForm(new CargaMasivaType(true,$opcion), $entity, array(
            'action' => $this->generateUrl('cargamasiva_create', array('opcion' => $opcion)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new CargaMasiva entity.
     *
     */
    public function newAction($opcion)
    {
        $entity = new CargaMasiva();
        $form   = $this->createCreateForm($entity,$opcion);

        return $this->render('CetNominaBundle:CargaMasiva:new.html.twig', array(
            'entity' => $entity,
            'tipo' => $opcion,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CargaMasiva entity.
     *
     */
    public function showAction($id,$opcion)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CetNominaBundle:CargaMasiva')->find($id);
        $cargamasivaid=$entity->getId();
        $datosgenericos = $em->getRepository('CetNominaBundle:DatosGenericos')->findBy(array('fk_datosGenericos_cargaMasiva1'=>$cargamasivaid));
        $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datosVariables_cargaMasiva1'=>$cargamasivaid));
        $estado=$entity->getEstado();
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaMasiva.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:CargaMasiva:show.html.twig', array(
            'entity'      => $entity,
            'datosgenericos'      => $datosgenericos,
            'datosvariables'      => $datosvariables,
            'estado'      => $estado,
            'tipo'      => $opcion,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing CargaMasiva entity.
     *
     */
    public function editAction($id,$opcion)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:CargaMasiva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaMasiva.');
        }

        $editForm = $this->createEditForm($entity,$opcion);
        $deleteForm = $this->createDeleteForm($id,$opcion);
        
        return $this->render('CetNominaBundle:CargaMasiva:edit.html.twig', array(
            'entity'      => $entity,
            'tipo' => $opcion,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CargaMasiva entity.
    *
    * @param CargaMasiva $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CargaMasiva $entity,$opcion)
    {
        $form = $this->createForm(new CargaMasivaType(false,$opcion), $entity, array(
            'action' => $this->generateUrl('cargamasiva_update', array('id' => $entity->getId(),'opcion' => $opcion)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing CargaMasiva entity.
     *
     */
    public function updateAction(Request $request, $id,$opcion)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:CargaMasiva')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaMasiva.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity,$opcion);
        $editForm->handleRequest($request);
        
        
        $nombretxt= $editForm->get('rutatxt')->getData();
        
        if (!empty($nombretxt)) {
            $nombretxt= $nombretxt->getClientOriginalName();
            
            $consulta = $em->createQuery("select count(cm.id) cuenta from CetNominaBundle:CargaMasiva cm where cm.id<>".$id ." and cm.nombretxt='$nombretxt'");
            $buscartxt = $consulta->getResult();
            $buscartxt=$buscartxt[0];
            if ($buscartxt['cuenta']>0) {
                throw $this->createDuplicatedException('El archivo ya fue utilizado en otra carga masiva, verifíque');
            }
        }
        
        
        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cargamasiva_show', array('id' => $id,'opcion' => $opcion)));
        }

        return $this->render('CetNominaBundle:CargaMasiva:edit.html.twig', array(
            'entity'      => $entity,
            'tipo' => $opcion,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CargaMasiva entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:CargaMasiva')->find($id);
            $opcion=$entity->getTipoCarga();
            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaMasiva.');
            }

            $em->remove($entity);
            $em->flush();
//        } 
        if ($opcion=='G'){
            $opcion='genericos';
        }else{
            $opcion='variables';
        }    
        return $this->redirect($this->generateUrl('cargamasiva_'.$opcion));
    }

    /**
     * Creates a form to delete a CargaMasiva entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cargamasiva_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Elimina'))
            ->getForm()
        ;
    }
    
    /**
     * Finds and displays a CargaMasiva entity.
     *
     */
    public function procesarAction($id,$opcion)
    {
            $directorio = __DIR__.'/../../../../web/uploads/cargamasiva/';
            
            $em = $this->getDoctrine()->getManager(); 
            //Para evitar el desbordamiento de memoria
            $em->getConnection()->getConfiguration()->setSQLLogger(null);
            
            $cargamasiva = $em->getRepository('CetNominaBundle:CargaMasiva')->find($id);
            $nombrearchivo = $cargamasiva->getNombreTxt();
            $cargamasivaid = $cargamasiva->getId();
            $cargamasivaref = $em->getReference('CetNominaBundle:CargaMasiva',$cargamasivaid);
            
            //lee el archivo completo
            //$datos=file_get_contents($directorio.$nombrearchivo);
            //$datos=trim($datos);
            //$datos = explode("\n", $datos);
            //$numregistros=count($datos);
            //ojo obtener la linea solamente para evitar royos
            //for ($i = 0; $i < $numregistros; $i++) {
                //limpiar espacion blancos, retorno de carro,etc
                //$registro=trim($datos[$i]);
            //}
            
            if ($opcion=='genericos'){
                $tipodato = $cargamasiva->getFkDatoGenericoTipoDatoGenerico1();
                $tipodato = $em->getRepository('CetNominaBundle:TipoDatoGenerico')->find($tipodato);
                $tipodatoid=$tipodato->getId();
                $tipodatoref = $em->getReference('CetNominaBundle:TipoDatoGenerico',$tipodatoid);
            }elseif ($opcion=='variables'){
                $concepto = $cargamasiva->getFkConcepto1();
                $concepto = $em->getRepository('CetNominaBundle:Concepto')->find($concepto);
                $conceptoid=$concepto->getId();
                $conceptoref = $em->getReference('CetNominaBundle:Concepto',$conceptoid);
                
                $modo=$cargamasiva->getModoAfectacion();
                $seccion=$cargamasiva->getSeccionAfectacion();
                $nombre=$cargamasiva->getNombre();
            }
            
            $i=0;
            $e=0;
            //linea a linea
            $gestor = @fopen($directorio.$nombrearchivo, "r");
            if ($gestor) 
            {
                //begin transaction
                //si hay erro hacer rol back
                while (($registro = fgets($gestor, 4096)) !== false) {            
                    $dividir = explode(";", $registro);
                    //limpiar posibles espacios en blanco
                    $cedula= preg_replace('[\s+]','', $dividir[0]);
                    $cedula= trim($cedula);
                    $monto= preg_replace('[\s+]','', $dividir[1]);
                    $monto= trim($monto);
                    if (preg_match ("/^(-){0,1}([0-9]+)(,[0-9][0-9][0-9])*([.][0-9]){0,1}([0-9]*)$/", $monto) == 1){
                        $monto = str_replace(",", "", $monto);
                    }elseif(preg_match ("/^(-){0,1}([0-9]+)(.[0-9][0-9][0-9])*([,][0-9]){0,1}([0-9]*)$/", $monto) == 1){
                        $monto = str_replace(".", "", $monto);
                        $monto = str_replace(",", ".", $monto);
                    }
                    
                    //$resultado = count($a);
                    
                    if (empty($monto) && $monto !== '0') {
                        throw $this->createNotFoundException('Uno o mas registro(s) no contiene(n) nada en el campo valor, verifique el archivo TXT de carga masiva.  ');
                    }
                    
                    //$personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
                    $personal = $em->getReference('CetNominaBundle:Personal',$cedula);
                    if (empty($personal)) {
                        throw $this->createNotFoundException('Uno o mas Números de cedula del archivo de carga masiva no corresponde a los registrados en la base de datos, verifique el archivo TXT de carga masiva. ');
                    }
                    
                    
                if ($opcion=='genericos'){
                    $datogenerico = $em->getRepository('CetNominaBundle:DatosGenericos')->findOneBy(array('fk_datosGenericos_personal1'=>$cedula,'fk_dato_generico_tipo_dato_generico1'=>$tipodatoid));
                    $contardg = count($datogenerico);
                    if ($contardg>0){
                        $datogenerico->setValor($monto);
                        $datogenerico->setFkDatosGenericosCargaMasiva1($cargamasivaref);
                        $em->flush();
                    }else{
                        //si no existe crear el registro
                        $entity = new DatosGenericos();
                        $entity->setFkDatosGenericosPersonal1($personal);
                        $entity->setValor($monto);
                        $entity->setFkDatosGenericosCargaMasiva1($cargamasivaref);
                        $entity->setFkDatoGenericoTipoDatoGenerico1($tipodatoref);
                        $em->persist($entity); 
                    }
                }elseif ($opcion=='variables'){
                    $datovariable = $em->getRepository('CetNominaBundle:DatosVariables')->findOneBy(array('fk_datos_variables_personal1'=>$cedula,'fk_datos_variables_concepto1'=>$conceptoid));
                    
                    $contardv = count($datovariable);
                    if ($contardv>0){
                        //si existe hacer el update
                        $datovariable->setValor($monto);
                        $datovariable->setFkDatosVariablesCargaMasiva1($cargamasivaref);
                        $datovariable->setModoAfectacion($modo);
                        $datovariable->setNombre($nombre);
                        $datovariable->setSeccionAfectacion($seccion);
                        $em->flush();
                    }else{
                        //si no existe crear el registro
                        $entity = new DatosVariables();
                        $entity->setFkDatosVariablesPersonal1($personal);
                        $entity->setValor($monto);
                        $entity->setFkDatosVariablesCargaMasiva1($cargamasivaref);
                        $entity->setFkDatosVariablesConcepto1($conceptoref);
                        
                        $entity->setModoAfectacion($modo);
                        $entity->setNombre($nombre);
                        $entity->setSeccionAfectacion($seccion);
                        
                        $em->persist($entity);  
                        
                    }
                }
                }
                    $fecha=date('Y-m-d');
                    $fechaProceso=new \DateTime($fecha);
                    $cargamasiva->setEstado('P');
                    $cargamasiva->setFechaProceso($fechaProceso);
                    $em->flush();
            }
            fclose($gestor);
            return $this->redirect($this->generateUrl('cargamasiva_show', array('id' => $id,'opcion' => $opcion)));
    }
}
