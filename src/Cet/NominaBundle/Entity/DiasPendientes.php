<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\DiasPendientes
 *
 * @ORM\Entity(repositoryClass="DiasPendientesRepository")
 * @ORM\Table(name="sis_nomina.dias_pendientes")
 */
class DiasPendientes
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(name="dias", type="integer", nullable=true)
     */
    private $dias;
    
    /**
     * @var string
     * @ORM\Column(name="horas", type="integer", nullable=true)
     */
    private $horas;
    
    /**
     * @var string
     * @ORM\Column(name="minutos", type="integer", nullable=true)
     */
    private $minutos;

    /**
     * @var string
     * @ORM\Column(name="documento", type="text", nullable=true)
     */
    private $documento;

    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="diasPendientes")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula")
     */
    protected $personal;

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     * @return DiasPendientes
     */
    public function setDias($dias)
    {
        $this->dias = $dias;

        return $this;
    }

    /**
     * Get dias
     *
     * @return integer 
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set horas
     *
     * @param integer $horas
     * @return DiasPendientes
     */
    public function setHoras($horas)
    {
        $this->horas = $horas;

        return $this;
    }

    /**
     * Get horas
     *
     * @return integer 
     */
    public function getHoras()
    {
        return $this->horas;
    }

    /**
     * Set minutos
     *
     * @param integer $minutos
     * @return DiasPendientes
     */
    public function setMinutos($minutos)
    {
        $this->minutos = $minutos;

        return $this;
    }

    /**
     * Get minutos
     *
     * @return integer 
     */
    public function getMinutos()
    {
        return $this->minutos;
    }

    /**
     * Set documento
     *
     * @param string $documento
     * @return DiasPendientes
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return DiasPendientes
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return DiasPendientes
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
 
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
//       $metadata->addPropertyConstraint('fecha', new Assert\NotBlank())
//                ->addPropertyConstraint('periodo', new Assert\NotBlank())
//            ;
        
    }
}
