<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Enfermedad;
use Cet\NominaBundle\Form\PersonalEnfermedadType;

/**
 * Enfermedad controller.
 *
 */
class PersonalEnfermedadController extends Controller
{

    /**
     * Lists all Enfermedad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Enfermedad')->findAll();

        return $this->render('CetNominaBundle:Enfermedad:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Enfermedad entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new Enfermedad();
        
        $em = $this->getDoctrine()->getManager();
        $informacionmedicabuscar = $em->getRepository('CetNominaBundle:InformacionMedica')->findOneBy(array('personal' => $id));
        $entity->setFkEnfermedadInformacionMedica1($informacionmedicabuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:Enfermedad:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Enfermedad entity.
    *
    * @param Enfermedad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Enfermedad $entity,$id)
    {
        $form = $this->createForm(new PersonalEnfermedadType(), $entity, array(
            'action' => $this->generateUrl('personalenfermedad_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Enfermedad entity.
     *
     */
    public function newAction($id)
    {
        $entity = new Enfermedad();
        
        $em = $this->getDoctrine()->getManager();
        $informacionmedicabuscar = $em->getRepository('CetNominaBundle:InformacionMedica')->findOneBy(array('personal' => $id));
        
        if (!$informacionmedicabuscar) {
            echo "<script>alert('Antes de agregar una enfermedad debe completar los datos de información medica.')</script>";
            echo "<script>javascript:history.back(-1);</script>";
        }
        
        $entity->setFkEnfermedadInformacionMedica1($informacionmedicabuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:Enfermedad:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Enfermedad entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Enfermedad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Enfermedad.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Enfermedad:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Enfermedad entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Enfermedad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Enfermedad.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Enfermedad:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Enfermedad entity.
    *
    * @param Enfermedad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Enfermedad $entity)
    {
        $form = $this->createForm(new PersonalEnfermedadType(), $entity, array(
            'action' => $this->generateUrl('personalenfermedad_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing Enfermedad entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Enfermedad')->find($id);
        $informacionmedicabuscar = $em->getRepository('CetNominaBundle:InformacionMedica')->findOneBy(array('id' => $entity->getFkEnfermedadInformacionMedica1()));

        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Enfermedad.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $informacionmedicabuscar->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:Enfermedad:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Enfermedad entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Enfermedad')->find($id);
            $informacionmedicabuscar = $em->getRepository('CetNominaBundle:InformacionMedica')->findOneBy(array('id' => $entity->getFkEnfermedadInformacionMedica1()));
            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Enfermedad.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        //return $this->redirect($this->generateUrl('personal'));
        return $this->redirect($this->generateUrl('personal_show', array('id' => $informacionmedicabuscar->getPersonal()->getId())));
    }

    /**
     * Creates a form to delete a Enfermedad entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalenfermedad_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
