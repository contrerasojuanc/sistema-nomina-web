<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\CargaFamiliar
 *
 * @ORM\Entity(repositoryClass="CargaFamiliarRepository")
 * @ORM\Table(name="sis_nomina.carga_familiar", indexes={@ORM\Index(name="fk_carga_familiar_base_legal1_idx", columns={"base_legal_id"}), @ORM\Index(name="fk_carga_familiar_personal1_idx", columns={"personal_cedula"})})
 * @UniqueEntity(
 *     fields={"cedula"},
 *     message="Número de cedula ya registrado"
 * )
 */
class CargaFamiliar
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var integer
     * @ORM\Column(name="cedula", type="integer", nullable=true)
     */
    private $cedula;

    /**
     * Solo valores:
     * P = Padre
     * M = Madre
     * H = Hijo(a)
     * R = Hermano(a)
     * E = Esposo(a)
     * C = Concubino(a)
     *
     * @var string
     * @ORM\Column(name="parentesco", type="string", length=1, nullable=false)
     */
    private $parentesco;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_nacimiento", type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * Solo valores:
     * M = Masculino
     * F = Femenino
     *
     * @var string
     * @ORM\Column(name="genero", type="string", length=1, nullable=true)
     */
    private $genero;

    /**
     * Solo valores:
     * N = No
     * S = Si
     *
     * @var string
     * @ORM\Column(name="estudia", type="string", length=1, nullable=true)
     */
    private $estudia;

    /**
     * Solo valores:
     * 1 = Primer Nivel (Inicial)
     * 2 = Segundo Nivel (Básica)
     * 3 = Tercer Nivel (Media)
     * 4 = Cuarto Nivel (Superior)
     * 5 = Quinto Nivel (Avanzada)
     *
     * @var string
     * @ORM\Column(name="nivel_instruccion", type="string", length=1, nullable=true)
     */
    private $nivelInstruccion;

    /**
     * @ORM\OneToOne(targetEntity="BaseLegal", inversedBy="cargaFamiliar")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $baseLegal;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="cargaFamiliars")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_carga_familiar_personal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CargaFamiliar
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cedula
     *
     * @param integer $cedula
     * @return CargaFamiliar
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return integer 
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set parentesco
     *
     * @param string $parentesco
     * @return CargaFamiliar
     */
    public function setParentesco($parentesco)
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return string 
     */
    public function getParentesco()
    {
        return $this->parentesco;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return CargaFamiliar
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return CargaFamiliar
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set estudia
     *
     * @param string $estudia
     * @return CargaFamiliar
     */
    public function setEstudia($estudia)
    {
        $this->estudia = $estudia;

        return $this;
    }

    /**
     * Get estudia
     *
     * @return string 
     */
    public function getEstudia()
    {
        return $this->estudia;
    }

    /**
     * Set nivelInstruccion
     *
     * @param string $nivelInstruccion
     * @return CargaFamiliar
     */
    public function setNivelInstruccion($nivelInstruccion)
    {
        $this->nivelInstruccion = $nivelInstruccion;

        return $this;
    }

    /**
     * Get nivelInstruccion
     *
     * @return string 
     */
    public function getNivelInstruccion()
    {
        return $this->nivelInstruccion;
    }

    /**
     * Set baseLegal
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegal
     * @return CargaFamiliar
     */
    public function setBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegal = null)
    {
        $this->baseLegal = $baseLegal;

        return $this;
    }

    /**
     * Get baseLegal
     *
     * @return \Cet\NominaBundle\Entity\BaseLegal 
     */
    public function getBaseLegal()
    {
        return $this->baseLegal;
    }

    /**
     * Set fk_carga_familiar_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkCargaFamiliarPersonal1
     * @return CargaFamiliar
     */
    public function setFkCargaFamiliarPersonal1(\Cet\NominaBundle\Entity\Personal $fkCargaFamiliarPersonal1)
    {
        $this->fk_carga_familiar_personal1 = $fkCargaFamiliarPersonal1;

        return $this;
    }

    /**
     * Get fk_carga_familiar_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkCargaFamiliarPersonal1()
    {
        return $this->fk_carga_familiar_personal1;
    }
    
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
       $metadata->addPropertyConstraint('nombre', new Assert\NotBlank())
               
                ->addPropertyConstraint('nombre', new Assert\Type(array('type'=>'string','message'=>'El campo debe ser solo texto.')))
//                ->addPropertyConstraint('ruta', new Assert\Email(array('message' => 'El campo no tiene el formato de correo electrónico.')))
               //->addPropertyConstraint('cedula', new Assert\NotBlank())
               ->addPropertyConstraint('cedula', new Assert\Type(array('type'=>'numeric','message'=>'El campo debe ser solo números.')))
               ->addPropertyConstraint ('cedula', new Assert\Length(array(
                    'min'=> 4,
                    'max'=> 8,
                    'minMessage'=> 'El Número de Cédula debe ser mayor a  4 digitos',
                    'maxMessage'=> 'El Número de Cédula no debe tener mas de 8 digitos',
                    )))                 
               ->addPropertyConstraint('parentesco', new Assert\NotBlank())

               ->addPropertyConstraint('fechaNacimiento', new Assert\NotBlank())
//               ->addPropertyConstraint('fechaNacimiento', new Assert\Date(array(
//                     'message' => 'Los campos de la fecha solo deben ser números.',
//                )))
            
               ->addPropertyConstraint('genero', new Assert\NotBlank())  
               
               ->addPropertyConstraint('estudia', new Assert\NotBlank())  
               
               ->addPropertyConstraint('nivelInstruccion', new Assert\NotBlank())  
//               ->addPropertyConstraint('Observaciones', new Assert\Blank)                
//                ->addPropertyConstraint('tamanio', new Assert\Range(array(
//                'min'        => 120,
//                'max'        => 180,
//                'minMessage' => 'El campo no debe ser menor a 120.',
//                'maxMessage' => 'El campo no debe ser mayor a 120.',
//                )))
                ;
        
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
    
}
