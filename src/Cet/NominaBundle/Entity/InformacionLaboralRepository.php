<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * InformacionLaboralRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InformacionLaboralRepository extends EntityRepository
{
    public function cuentarepetida(array $parameter)
    {        
        $query1 = "SELECT il.id FROM Cet\NominaBundle\Entity\InformacionLaboral il "
                . "WHERE il.cuentaFideicomiso =:cuenta ";
        
        $consulta1=$this->getEntityManager()->createQuery($query1);
        $consulta1->setParameter('cuenta', $parameter['cuentaNomina']);
        $consulta1->setMaxResults(1);
            
        if ($consulta1->getResult()){
            return $consulta1->getSingleResult();
        }        
        
        return null;
    }
    
    public function cuentafideicomisorepetida(array $parameter)
    {        
        $query2 = "SELECT il.id FROM Cet\NominaBundle\Entity\InformacionLaboral il "
                . "WHERE il.cuentaNomina =:cuenta ";
        
        $consulta2=$this->getEntityManager()->createQuery($query2);
        $consulta2->setParameter('cuenta', $parameter['cuentaFideicomiso']);
        $consulta2->setMaxResults(1);
     
                    
        if ($consulta2->getResult()){
            return $consulta2->getSingleResult();
        }        
        
        return null;
    }
}
