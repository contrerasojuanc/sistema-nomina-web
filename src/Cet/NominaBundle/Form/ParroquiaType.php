<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParroquiaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('fk_parroquia_municipio1',null, array(
                'label'=> 'Municipio',
                'empty_value' => 'Seleccione',
                'attr' => array('help'=>'Seleccione Parroquia.'),
                'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione Parroquia') 
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Parroquia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_parroquia';
    }
}
