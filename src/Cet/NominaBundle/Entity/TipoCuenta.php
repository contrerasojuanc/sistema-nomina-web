<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cet\NominaBundle\Entity\TipoCuenta
 *
 * @ORM\Entity(repositoryClass="TipoCuentaRepository")
 * @ORM\Table(name="sis_nomina.tipo_cuenta")
 */
class TipoCuenta
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     * @ORM\Column(name="identificador", type="text", nullable=true)
     */
    private $identificador;
    
    /**
     * @ORM\OneToMany(targetEntity="PlantillaNomina", mappedBy="tipoBanco")
     * @ORM\JoinColumn(name="plantilla_id", referencedColumnName="id", nullable=true)
     */
    protected $plantillaNominas;

//    public function __construct()
//    {
//        $this->plantillaNominas = new ArrayCollection();
//        $this->informacionLaborals = new ArrayCollection();
//    }

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plantillaNominas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoCuenta
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set identificador
     *
     * @param string $identificador
     * @return TipoCuenta
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador
     *
     * @return string 
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }

    /**
     * Add plantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas
     * @return TipoCuenta
     */
    public function addPlantillaNomina(\Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas)
    {
        $this->plantillaNominas[] = $plantillaNominas;

        return $this;
    }

    /**
     * Remove plantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas
     */
    public function removePlantillaNomina(\Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas)
    {
        $this->plantillaNominas->removeElement($plantillaNominas);
    }

    /**
     * Get plantillaNominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlantillaNominas()
    {
        return $this->plantillaNominas;
    }
}
