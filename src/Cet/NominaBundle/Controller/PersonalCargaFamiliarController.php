<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\CargaFamiliar;
use Cet\NominaBundle\Form\PersonalCargaFamiliarType;

/**
 * CargaFamiliar controller.
 *
 */
class PersonalCargaFamiliarController extends Controller
{

    /**
     * Lists all CargaFamiliar entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:CargaFamiliar')->findAll();

        return $this->render('CetNominaBundle:CargaFamiliar:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CargaFamiliar entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new CargaFamiliar();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkCargaFamiliarPersonal1($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkCargaFamiliarPersonal1()->getId())));
        }

        return $this->render('CetNominaBundle:CargaFamiliar:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a CargaFamiliar entity.
    *
    * @param CargaFamiliar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(CargaFamiliar $entity,$id)
    {
        $form = $this->createForm(new PersonalCargaFamiliarType(), $entity, array(
            'action' => $this->generateUrl('personalcargafamiliar_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new CargaFamiliar entity.
     *
     */
    public function newAction($id)
    {
        $entity = new CargaFamiliar();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkCargaFamiliarPersonal1($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:CargaFamiliar:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CargaFamiliar entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:CargaFamiliar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaFamiliar.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:CargaFamiliar:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing CargaFamiliar entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:CargaFamiliar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaFamiliar.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:CargaFamiliar:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CargaFamiliar entity.
    *
    * @param CargaFamiliar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CargaFamiliar $entity)
    {
        $form = $this->createForm(new PersonalCargaFamiliarType(), $entity, array(
            'action' => $this->generateUrl('personalcargafamiliar_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing CargaFamiliar entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:CargaFamiliar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaFamiliar.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkCargaFamiliarPersonal1()->getId())));
        }

        return $this->render('CetNominaBundle:CargaFamiliar:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CargaFamiliar entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:CargaFamiliar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad CargaFamiliar.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        //return $this->redirect($this->generateUrl('personal'));
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkCargaFamiliarPersonal1()->getId())));
    }

    /**
     * Creates a form to delete a CargaFamiliar entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalcargafamiliar_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
