<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\ExperienciaLaboral;
use Cet\NominaBundle\Form\ExperienciaLaboralType;

/**
 * ExperienciaLaboral controller.
 *
 */
class ExperienciaLaboralController extends Controller
{

    /**
     * Lists all ExperienciaLaboral entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:ExperienciaLaboral')->findAll();

        return $this->render('CetNominaBundle:ExperienciaLaboral:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new ExperienciaLaboral entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ExperienciaLaboral();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('experiencialaboral_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:ExperienciaLaboral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a ExperienciaLaboral entity.
    *
    * @param ExperienciaLaboral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ExperienciaLaboral $entity)
    {
        $form = $this->createForm(new ExperienciaLaboralType(), $entity, array(
            'action' => $this->generateUrl('experiencialaboral_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new ExperienciaLaboral entity.
     *
     */
    public function newAction()
    {
        $entity = new ExperienciaLaboral();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:ExperienciaLaboral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ExperienciaLaboral entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:ExperienciaLaboral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad ExperienciaLaboral.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:ExperienciaLaboral:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ExperienciaLaboral entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:ExperienciaLaboral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad ExperienciaLaboral.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:ExperienciaLaboral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ExperienciaLaboral entity.
    *
    * @param ExperienciaLaboral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ExperienciaLaboral $entity)
    {
        $form = $this->createForm(new ExperienciaLaboralType(), $entity, array(
            'action' => $this->generateUrl('experiencialaboral_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing ExperienciaLaboral entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:ExperienciaLaboral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad ExperienciaLaboral.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('experiencialaboral_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:ExperienciaLaboral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ExperienciaLaboral entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:ExperienciaLaboral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad ExperienciaLaboral.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('experiencialaboral'));
    }

    /**
     * Creates a form to delete a ExperienciaLaboral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('experiencialaboral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
