<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Estudio
 *
 * @ORM\Entity(repositoryClass="EstudioRepository")
 * @ORM\Table(name="sis_nomina.estudio", indexes={@ORM\Index(name="fk_estudio_base_legal1_idx", columns={"base_legal_id"}), @ORM\Index(name="fk_estudios_personal1_idx", columns={"personal_cedula"})})
 */
class Estudio
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     * @ORM\Column(name="duracion", type="text", nullable=true)
     */
    private $duracion;

    
    /**
     * @var integer
     * @ORM\Column(name="tipo", type="integer", nullable=false)
     */
    
    private $tipo;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_culminacion", type="date", nullable=true)
     */
    private $fechaCulminacion;

    /**
     * @var string
     * @ORM\Column(name="institucion", type="text", nullable=true)
     */
    private $institucion;

    /**
     * Solo valores:
     * T = Terminado
     * C = Cursando
     *
     * @var string
     * @ORM\Column(name="estado", type="string", length=1, nullable=true)
     */
    private $estado;
    
    /**
     * @ORM\OneToOne(targetEntity="BaseLegal", inversedBy="estudio")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $baseLegal;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="estudios")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_estudios_personal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Estudio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set duracion
     *
     * @param string $duracion
     * @return Estudio
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return string 
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Estudio
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Estudio
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaCulminacion
     *
     * @param \DateTime $fechaCulminacion
     * @return Estudio
     */
    public function setFechaCulminacion($fechaCulminacion)
    {
        $this->fechaCulminacion = $fechaCulminacion;

        return $this;
    }

    /**
     * Get fechaCulminacion
     *
     * @return \DateTime 
     */
    public function getFechaCulminacion()
    {
        return $this->fechaCulminacion;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return Estudio
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;

        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Estudio
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fk_estudios_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkEstudiosPersonal1
     * @return Estudio
     */
    public function setFkEstudiosPersonal1(\Cet\NominaBundle\Entity\Personal $fkEstudiosPersonal1)
    {
        $this->fk_estudios_personal1 = $fkEstudiosPersonal1;

        return $this;
    }

    /**
     * Get fk_estudios_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkEstudiosPersonal1()
    {
        return $this->fk_estudios_personal1;
    }
    
    /**
     * Set baseLegal
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegal
     * @return Estudio
     */
    public function setBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegal = null)
    {
        $this->baseLegal = $baseLegal;

        return $this;
    }

    /**
     * Get baseLegal
     *
     * @return \Cet\NominaBundle\Entity\BaseLegal 
     */
    public function getBaseLegal()
    {
        return $this->baseLegal;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('descripcion', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('descripcion', new Assert\Type(array('type'=> 'string')))
                ->addPropertyConstraint('duracion', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('duracion', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint('tipo', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))           
                ->addPropertyConstraint('fechaInicio', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))             
                //->addPropertyConstraint('fechaCulminacion', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))             
                ->addPropertyConstraint('institucion', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('institucion', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint('estado', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('estado', new Assert\Type(array('type'=>'alpha','message'=>'El campo debe ser solo texto.')))             
             ;          
    }
    
    public function __toString()
    {
        return $this->getDescripcion();
    }
}
