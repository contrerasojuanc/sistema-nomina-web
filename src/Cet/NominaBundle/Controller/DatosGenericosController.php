<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\DatosGenericos;
use Cet\NominaBundle\Form\DatosGenericosType;

/**
 * DatosGenericos controller.
 *
 */
class DatosGenericosController extends Controller
{

    /**
     * Lists all DatosGenericos entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:DatosGenericos')->findAll();

        return $this->render('CetNominaBundle:DatosGenericos:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new DatosGenericos entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new DatosGenericos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('datosgenericos_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:DatosGenericos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a DatosGenericos entity.
    *
    * @param DatosGenericos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(DatosGenericos $entity)
    {
        $form = $this->createForm(new DatosGenericosType(), $entity, array(
            'action' => $this->generateUrl('datosgenericos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new DatosGenericos entity.
     *
     */
    public function newAction()
    {
        $entity = new DatosGenericos();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:DatosGenericos:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a DatosGenericos entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosGenericos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGenericos.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DatosGenericos:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing DatosGenericos entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosGenericos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGenericos.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DatosGenericos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a DatosGenericos entity.
    *
    * @param DatosGenericos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DatosGenericos $entity)
    {
        $form = $this->createForm(new DatosGenericosType(), $entity, array(
            'action' => $this->generateUrl('datosgenericos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing DatosGenericos entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosGenericos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGenericos.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('datosgenericos_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:DatosGenericos:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a DatosGenericos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:DatosGenericos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosGenericos.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('datosgenericos'));
    }

    /**
     * Creates a form to delete a DatosGenericos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('datosgenericos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
