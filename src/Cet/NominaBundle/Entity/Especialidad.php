<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Especialidad
 *
 * @ORM\Entity(repositoryClass="EspecialidadRepository")
 * @ORM\Table(name="sis_nomina.especialidad")
 */
class Especialidad
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="especialidad", type="text", nullable=true)
     */
    private $especialidad;

    /**
     * @ORM\OneToMany(targetEntity="Reposo", mappedBy="especialidad", cascade={"remove"})
     * @ORM\JoinColumn(name="reposo_id", referencedColumnName="id")
     */
    protected $reposos;

    public function __construct()
    {
        $this->archivos = new ArrayCollection();
    }

    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('especialidad', new Assert\NotBlank()) 
             ;          
    }

    public function __toString()
    {
        return $this->getEspecialidad();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set especialidad
     *
     * @param string $especialidad
     * @return Especialidad
     */
    public function setEspecialidad($especialidad)
    {
        $this->especialidad = $especialidad;

        return $this;
    }

    /**
     * Get especialidad
     *
     * @return string 
     */
    public function getEspecialidad()
    {
        return $this->especialidad;
    }

    /**
     * Add reposos
     *
     * @param \Cet\NominaBundle\Entity\Reposo $reposos
     * @return Especialidad
     */
    public function addReposo(\Cet\NominaBundle\Entity\Reposo $reposos)
    {
        $this->reposos[] = $reposos;

        return $this;
    }

    /**
     * Remove reposos
     *
     * @param \Cet\NominaBundle\Entity\Reposo $reposos
     */
    public function removeReposo(\Cet\NominaBundle\Entity\Reposo $reposos)
    {
        $this->reposos->removeElement($reposos);
    }

    /**
     * Get reposos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReposos()
    {
        return $this->reposos;
    }
}
