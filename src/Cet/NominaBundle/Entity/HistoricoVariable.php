<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\HistoricoVariable
 *
 * @ORM\Entity(repositoryClass="HistoricoVariableRepository")
 * @ORM\Table(name="sis_nomina.historico_variable", indexes={@ORM\Index(name="fk_variable_has_personal_personal1_idx", columns={"personal_cedula"}), @ORM\Index(name="fk_variable_has_personal_variable1_idx", columns={"variable_id"})})
 */
class HistoricoVariable
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicio", type="datetime", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true)
     */
    private $fechaFin;

    /**
     * @var string
     * @ORM\Column(name="valor", type="text", nullable=true)
     */
    private $valor;
    
    /**
     * @var string
     * @ORM\Column(name="sql", type="text", nullable=true)
     */
    private $sql;

    /**
     * @ORM\ManyToOne(targetEntity="Variable", inversedBy="historicoVariables")
     * @ORM\JoinColumn(name="variable_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_variable_has_personal_variable1;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="historicoVariables")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_variable_has_personal_personal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return HistoricoVariable
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return HistoricoVariable
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return HistoricoVariable
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set fk_variable_has_personal_variable1
     *
     * @param \Cet\NominaBundle\Entity\Variable $fkVariableHasPersonalVariable1
     * @return HistoricoVariable
     */
    public function setFkVariableHasPersonalVariable1(\Cet\NominaBundle\Entity\Variable $fkVariableHasPersonalVariable1)
    {
        $this->fk_variable_has_personal_variable1 = $fkVariableHasPersonalVariable1;

        return $this;
    }

    /**
     * Get fk_variable_has_personal_variable1
     *
     * @return \Cet\NominaBundle\Entity\Variable 
     */
    public function getFkVariableHasPersonalVariable1()
    {
        return $this->fk_variable_has_personal_variable1;
    }

    /**
     * Set fk_variable_has_personal_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkVariableHasPersonalPersonal1
     * @return HistoricoVariable
     */
    public function setFkVariableHasPersonalPersonal1(\Cet\NominaBundle\Entity\Personal $fkVariableHasPersonalPersonal1)
    {
        $this->fk_variable_has_personal_personal1 = $fkVariableHasPersonalPersonal1;

        return $this;
    }

    /**
     * Get fk_variable_has_personal_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkVariableHasPersonalPersonal1()
    {
        return $this->fk_variable_has_personal_personal1;
    }

    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('fecha', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('fecha', new Assert\Date())
                ->addPropertyConstraint('valor', new Assert\Type(array('type'=>'numeric')))                 
            ;         
    }  

    /**
     * Set sql
     *
     * @param string $sql
     * @return HistoricoVariable
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * Get sql
     *
     * @return string 
     */
    public function getSql()
    {
        return $this->sql;
    }
}
