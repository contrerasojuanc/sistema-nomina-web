<?php

/*
 * This file is part of modifications.
 *
 * (c) Eduardo Mendoza <eduardomendoza4@hotmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Symfony\Component\Security\Core\Exception;

/**
 * DuplicatedException is thrown when the record is duplicated
 *
 * @author Eduardo Mendoza <eduardomendoza4@hotmail.com>
 */
class DuplicatedException extends \RuntimeException
{
    public function __construct($message = 'Registro Duplicado', \Exception $previous = null)
    {
        parent::__construct($message, 403, $previous);
    }
}
