<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExperienciaLaboralType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('institucion')
            ->add('tipo')    
            ->add('fechaIngreso')
            ->add('fechaEgreso')
            ->add('cargo')
            ->add('sueldo')
            ->add('baseLegal')
            ->add('fk_experiencia_laboral_personal1')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\ExperienciaLaboral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_experiencialaboral';
    }
}
