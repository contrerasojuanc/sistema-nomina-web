<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Vivienda;
use Cet\NominaBundle\Form\PersonalViviendaType;

/**
 * Vivienda controller.
 *
 */
class PersonalViviendaController extends Controller
{

    /**
     * Lists all Vivienda entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Vivienda')->findAll();

        return $this->render('CetNominaBundle:Vivienda:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Vivienda entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new Vivienda();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setPersonal($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:Vivienda:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Vivienda entity.
    *
    * @param Vivienda $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Vivienda $entity,$id)
    {
        $form = $this->createForm(new PersonalViviendaType(), $entity, array(
            'action' => $this->generateUrl('personalvivienda_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Vivienda entity.
     *
     */
    public function newAction($id)
    {
        $entity = new Vivienda();
        
        $em = $this->getDoctrine()->getManager();
        
        $buscarvivienda = $em->getRepository('CetNominaBundle:Vivienda')->findOneBy(array('personal' => $id));
        if ($buscarvivienda) {
            return $this->redirect($this->generateUrl('personalvivienda_edit', array('id' => $buscarvivienda->getId())));
        }
        
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setPersonal($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:Vivienda:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Vivienda entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vivienda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vivienda.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Vivienda:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Vivienda entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vivienda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vivienda.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Vivienda:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Vivienda entity.
    *
    * @param Vivienda $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Vivienda $entity)
    {
        $form = $this->createForm(new PersonalViviendaType(), $entity, array(
            'action' => $this->generateUrl('personalvivienda_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing Vivienda entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vivienda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vivienda.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:Vivienda:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Vivienda entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Vivienda')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Vivienda.');
            }

            $em->remove($entity);
            $em->flush();
//        }
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));
        //return $this->redirect($this->generateUrl('personal'));
    }

    /**
     * Creates a form to delete a Vivienda entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalvivienda_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    public function citiesAction(Request $request)
    {
        $province_id = $request->request->get('province_id');

        $em = $this->getDoctrine()->getManager();
//        $cities = $em->getRepository('CetNominaBundle:Parroquia')->findByProvinceId($province_id);

        $consulta = $em->createQuery('select pa.id,pa.nombre from CetNominaBundle:Parroquia pa where pa.fk_parroquia_municipio1='.$province_id);
        $cities = $consulta->getResult();
        
        return new JsonResponse($cities);
    }
    
    public function provincesAction(Request $request)
    {
        $province_id = $request->request->get('province_id');

        $em = $this->getDoctrine()->getManager();
//        $cities = $em->getRepository('CetNominaBundle:Municipio')->findBy(array('fk_municipio_estado1' => $province_id));
        
        $consulta = $em->createQuery('select mu.id,mu.nombre from CetNominaBundle:Municipio mu where mu.fk_municipio_estado1='.$province_id);
        $provinces = $consulta->getResult();
        
        return new JsonResponse($provinces);
    }
}
