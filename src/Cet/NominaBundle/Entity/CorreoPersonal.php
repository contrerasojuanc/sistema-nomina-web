<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\CorreoPersonal
 *
 * @ORM\Entity(repositoryClass="CorreoPersonalRepository")
 * @ORM\Table(name="sis_nomina.correo_personal", indexes={@ORM\Index(name="fk_correo_personal_personal1_idx", columns={"personal_cedula"})})
 * @UniqueEntity(
 *     fields={"correo"},
 *     message="Dirección de correo ya registrada, verifique"
 * )
 */
class CorreoPersonal
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="correo", type="string", length=45, nullable=true)
     */
    private $correo;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="correoPersonals")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_correo_personal_personal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return CorreoPersonal
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set fk_correo_personal_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkCorreoPersonalPersonal1
     * @return CorreoPersonal
     */
    public function setFkCorreoPersonalPersonal1(\Cet\NominaBundle\Entity\Personal $fkCorreoPersonalPersonal1)
    {
        $this->fk_correo_personal_personal1 = $fkCorreoPersonalPersonal1;

        return $this;
    }

    /**
     * Get fk_correo_personal_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkCorreoPersonalPersonal1()
    {
        return $this->fk_correo_personal_personal1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('correo', new Assert\NotBlank(array('message' => 'El campo no puede ser vacio.')))
                ->addPropertyConstraint('correo', new Assert\Email(array('message' => 'El campo no tiene el formato de correo electrónico.')))
//                ->addPropertyConstraint('correo', new Assert\Range(array(
//                 'max' => 45,
//                 'maxMessage' => 'El correo no puede tener mas de 45 caracteres.',   
//                )))
    ;      
    }
}
