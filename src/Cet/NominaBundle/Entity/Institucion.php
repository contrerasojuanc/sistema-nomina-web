<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Institucion
 *
 * @ORM\Entity(repositoryClass="InstitucionRepository")
 * @ORM\Table(name="sis_nomina.institucion")
 */
class Institucion
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="UnidadOrganizativa", mappedBy="fk_unidad_organizativa_institucion1")
     * @ORM\JoinColumn(name="institucion_id", referencedColumnName="id", nullable=false)
     */
    protected $unidadOrganizativas;

    public function __construct()
    {
        $this->unidadOrganizativas = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Institucion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add unidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas
     * @return Institucion
     */
    public function addUnidadOrganizativa(\Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas)
    {
        $this->unidadOrganizativas[] = $unidadOrganizativas;

        return $this;
    }

    /**
     * Remove unidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas
     */
    public function removeUnidadOrganizativa(\Cet\NominaBundle\Entity\UnidadOrganizativa $unidadOrganizativas)
    {
        $this->unidadOrganizativas->removeElement($unidadOrganizativas);
    }

    /**
     * Get unidadOrganizativas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUnidadOrganizativas()
    {
        return $this->unidadOrganizativas;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('nombre', new Assert\NotBlank())
            ;
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
