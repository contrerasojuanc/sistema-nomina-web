<?php

namespace Cet\NominaBundle\Form\Subscribers;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;
use Cet\NominaBundle\Entity\Estado;
use Cet\NominaBundle\Controller;

class AtributoSubscriber implements EventSubscriberInterface
{
    private $factory;
    public $columnas_array=array();

    public function __construct(FormFactoryInterface $factory, Array $columnas_array=null)
    {
        $this->factory = $factory;
        $this->columnas_array = $columnas_array;    
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }
     
    private function addAtributoForm($form, $atributo, $entidad)
    {
        $form->add($this->factory->createNamed('Atributo','choice', $atributo, array(
            'choices'       => ($entidad!=null)?$this->columnas_array[$entidad]['campos']:$this->columnas_array,
//            'choices'       => $this->columnas_array,
            'empty_value'   => 'Seleccione el Atributo',
            'mapped'        => false,
            'auto_initialize'=> false,
            'required' => false,
        )));   
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

//        $parroquia = ($data->getFkViviendaParroquia1()) ? $data->getFkViviendaParroquia1() : null ;
//        $municipio = ($parroquia) ? $parroquia->getFkParroquiaMunicipio1() : null ;
//        $estado = ($municipio) ? $municipio->getFkMunicipioEstado1() : null ;
        $atributo=null;
        $entidad=null;
//        $entidad="Cet\NominaBundle\Entity\Archivo";
                
        $this->addAtributoForm($form, $atributo, $entidad);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $atributo = array_key_exists('Atributo', $data) ? $data['Atributo'] : null;
//        $entidad = array_key_exists('Entidad', $data) ? $data['Entidad'] : null;
//        $atributo=null;
//        $entidad="Cet\NominaBundle\Entity\Archivo";
        $entidad=null;
        
        $this->addAtributoForm($form, $atributo, $entidad);
    }
}