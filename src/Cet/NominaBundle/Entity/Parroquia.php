<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Parroquia
 *
 * @ORM\Entity(repositoryClass="ParroquiaRepository")
 * @ORM\Table(name="sis_nomina.parroquia", indexes={@ORM\Index(name="fk_parroquia_municipio1_idx", columns={"municipio_id"})})
 */
class Parroquia
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=125, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Vivienda", mappedBy="fk_vivienda_parroquia1")
     * @ORM\JoinColumn(name="parroquia_id", referencedColumnName="id", nullable=false)
     */
    protected $viviendas;

    /**
     * @ORM\ManyToOne(targetEntity="Municipio", inversedBy="parroquias")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_parroquia_municipio1;

    public function __construct()
    {
        $this->viviendas = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Parroquia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add viviendas
     *
     * @param \Cet\NominaBundle\Entity\Vivienda $viviendas
     * @return Parroquia
     */
    public function addVivienda(\Cet\NominaBundle\Entity\Vivienda $viviendas)
    {
        $this->viviendas[] = $viviendas;

        return $this;
    }

    /**
     * Remove viviendas
     *
     * @param \Cet\NominaBundle\Entity\Vivienda $viviendas
     */
    public function removeVivienda(\Cet\NominaBundle\Entity\Vivienda $viviendas)
    {
        $this->viviendas->removeElement($viviendas);
    }

    /**
     * Get viviendas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getViviendas()
    {
        return $this->viviendas;
    }

    /**
     * Set fk_parroquia_municipio1
     *
     * @param \Cet\NominaBundle\Entity\Municipio $fkParroquiaMunicipio1
     * @return Parroquia
     */
    public function setFkParroquiaMunicipio1(\Cet\NominaBundle\Entity\Municipio $fkParroquiaMunicipio1)
    {
        $this->fk_parroquia_municipio1 = $fkParroquiaMunicipio1;

        return $this;
    }

    /**
     * Get fk_parroquia_municipio1
     *
     * @return \Cet\NominaBundle\Entity\Municipio 
     */
    public function getFkParroquiaMunicipio1()
    {
        return $this->fk_parroquia_municipio1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('nombre', new Assert\NotBlank())

//                ->addPropertyConstraint ('nombre', new Assert\Type(array('type'=>'alpha')))
                          
            ;
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
