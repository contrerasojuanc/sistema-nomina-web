<?php
namespace Cet\NominaBundle\EventListener;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Cet\NominaBundle\Form\ConceptoType;
use ReflectionClass;
//use JMS\Serializer\SerializerBuilder;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\HttpFoundation\Request;

 use Symfony\Component\DependencyInjection\ContainerInterface;
//use Cet\NominaBundle\Entity;
use Cet\NominaBundle\Entity\HistoricoEvento;
use Cet\NominaBundle\Entity\Archivo;
use Cet\NominaBundle\Entity\BaseLegal;
use Cet\NominaBundle\Entity\CargaFamiliar;
use Cet\NominaBundle\Entity\CargaMasiva;
use Cet\NominaBundle\Entity\Cargo;
use Cet\NominaBundle\Entity\Concepto;
use Cet\NominaBundle\Entity\CorreoPersonal;
use Cet\NominaBundle\Entity\Criterio;
use Cet\NominaBundle\Entity\DatosGenericos;
use Cet\NominaBundle\Entity\DatosVariables;
use Cet\NominaBundle\Entity\DatosGlobales;
use Cet\NominaBundle\Entity\DiasPendientes;
use Cet\NominaBundle\Entity\Enfermedad;
use Cet\NominaBundle\Entity\Estado;
use Cet\NominaBundle\Entity\Estudio;
use Cet\NominaBundle\Entity\ExperienciaLaboral;
use Cet\NominaBundle\Entity\Formula;
//26/11/2015: Los  historicos no se registran porque son procesos internos que generan muchos registros
//use Cet\NominaBundle\Entity\HistoricoCargo;
//use Cet\NominaBundle\Entity\HistoricoConcepto;
//use Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa;
//use Cet\NominaBundle\Entity\HistoricoVariable;
use Cet\NominaBundle\Entity\InformacionAcademica;
use Cet\NominaBundle\Entity\InformacionCulturalDeportiva;
use Cet\NominaBundle\Entity\InformacionLaboral;
use Cet\NominaBundle\Entity\InformacionMedica;
use Cet\NominaBundle\Entity\Institucion;
use Cet\NominaBundle\Entity\Municipio;
use Cet\NominaBundle\Entity\Nomina;
use Cet\NominaBundle\Entity\orgaFeriadosM;
use Cet\NominaBundle\Entity\Parroquia;
use Cet\NominaBundle\Entity\Personal;
use Cet\NominaBundle\Entity\PlantillaNomina;
use Cet\NominaBundle\Entity\PrestacionesSocialesGeneral;
use Cet\NominaBundle\Entity\Reporte;
use Cet\NominaBundle\Entity\Respaldo;
use Cet\NominaBundle\Entity\TelefonoPersonal;
use Cet\NominaBundle\Entity\TipoBaseLegal;
use Cet\NominaBundle\Entity\TipoDatoGenerico;
use Cet\NominaBundle\Entity\TipoNomina;
use Cet\NominaBundle\Entity\UnidadOrganizativa;
use Cet\NominaBundle\Entity\Usuario;
use Cet\NominaBundle\Entity\Vacaciones;
use Cet\NominaBundle\Entity\Variable;
use Cet\NominaBundle\Entity\Vehiculo;
use Cet\NominaBundle\Entity\Vivienda;
use Cet\NominaBundle\Entity\Reposo;
use Cet\NominaBundle\Entity\Medico;
use Cet\NominaBundle\Entity\Especialidad;
use DateTime;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SuscriberLyfeCycle
 *
 * @author juan.carreno
 */
class SubscriberLyfeCycle implements EventSubscriber {
    protected $container;

    public function __construct($securityContext)
    {
        $this->container = $securityContext;
    }
     public function getSubscribedEvents()
    {
        return array(
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        );
    }
    
    private function getForm( $entity)
    {
        $serializer = JMS\Serializer\SerializerBuilder::create()->build(); 
        $serializer -> serialize ( $entity ,  'json' );
        $controlador = new \Symfony\Bundle\FrameworkBundle\Controller\Controller(); 
    }
    
    function getArrayEntity($entity) {
        $result = array();
        $class = new ReflectionClass(get_class($entity));
        $propertyAccessor = PropertyAccess::getPropertyAccessor();
        foreach ($class->getProperties() as $prop) {
            $propName = $prop->name;
            $valor= $propertyAccessor->getValue($entity, $propName);
            $result[$propName]=$valor;
    //        $result[$methodName]=$class->getStaticPropertyValue($propName);

    //        if (strpos($methodName, "get") === 0 && strlen($methodName) > 3) {
    //            $propertyName = lcfirst(substr($methodName, 3));
    //            $value = $method->invoke($entity);
    //
    //            if (is_object($value)) {
    //                if ($recursionDepth > 0) {
    //                    $result[$propertyName] = $this->entity2array($value, $recursionDepth - 1);
    //                } else {
    //                    $result[$propertyName] = "***";     //stop recursion
    //                }
    //            } else {
    //                $result[$propertyName] = $value;
    //            }
    //        }
        }
        return $result;
    }
    
     function entity2array_original($entity, $recursionDepth = 2) {
    $result = array();
    $class = new ReflectionClass(get_class($entity));
    //foreach ($class->get(ReflectionMethod::IS_PUBLIC) as $method) {
    foreach ($class->getProperties() as $method) {
        $methodName = $method->name;
        if (strpos($methodName, "get") === 0 && strlen($methodName) > 3) {
            $propertyName = lcfirst(substr($methodName, 3));
            $value = $method->invoke($entity);

            if (is_object($value)) {
                if ($recursionDepth > 0) {
                    $result[$propertyName] = $this->entity2array($value, $recursionDepth - 1);
                } else {
                    $result[$propertyName] = "***";     //stop recursion
                }
            } else {
                $result[$propertyName] = $value;
            }
        }
    }
    return $result;
    }
    
    public function GetTabla($entity) {
        $tabla="";
        if ($entity instanceof Archivo) 
            $tabla="Archivo";
        
        if ($entity instanceof Concepto) 
            $tabla="Concepto";
        
        if ($entity instanceof Variable) {
            $tabla="Variable";
        }
        if ($entity instanceof BaseLegal) {
            $tabla="BaseLegal";
        }
        if ($entity instanceof CargaFamiliar) {
            $tabla="CargaFamiliar";
        }
        if ($entity instanceof CargaMasiva) {
            $tabla="CargaMasiva";
        }
        if ($entity instanceof CorreoPersonal) {
            $tabla="CorreoPersonal";
        }
//        if ($entity instanceof Criterio) {
//            $tabla="Criterio";
//        }
        if ($entity instanceof DatosGenericos) {
            $tabla="DatosGenericos";
        }
        if ($entity instanceof DatosVariables) {
            $tabla="DatosVariables";
        }
        if ($entity instanceof DatosGlobales) {
            $tabla="DatosGlobales";
        }
        if ($entity instanceof DiasPendientes) {
            $tabla="DiasPendientes";
        }
        if ($entity instanceof Enfermedad) {
            $tabla="Enfermedad";
        }
        if ($entity instanceof Estado) {
            $tabla="Estado";
        }
        if ($entity instanceof Estudio) {
            $tabla="Estudio";
        }
        if ($entity instanceof ExperienciaLaboral) {
            $tabla="ExperienciaLaboral";
        }
        if ($entity instanceof InformacionAcademica) {
            $tabla="InformacionAcademica";
        }
        if ($entity instanceof InformacionCulturalDeportiva) {
            $tabla="InformacionCulturalDeportiva";
        }
        if ($entity instanceof InformacionLaboral) {
            $tabla="InformacionLaboral";
        }
        if ($entity instanceof InformacionMedica) {
            $tabla="InformacionMedica";
        }
        if ($entity instanceof Institucion) {
            $tabla="Institucion";
        }
        if ($entity instanceof Municipio) {
            $tabla="Municipio";
        }
        if ($entity instanceof Nomina) {
            $tabla="Nomina";
        }
        if ($entity instanceof orgaFeriadosM) {
            $tabla="orgaFeriadosM";
        }
        if ($entity instanceof Parroquia) {
            $tabla="Parroquia";
        }
        if ($entity instanceof Personal) {
            $tabla="Personal";
        }
        if ($entity instanceof PlantillaNomina) {
            $tabla="PlantillaNomina";
        }
        if ($entity instanceof PrestacionesSocialesGeneral) {
            $tabla="PrestacionesSocialesGeneral";
        }
        if ($entity instanceof Reporte) {
            $tabla="Reporte";
        }
        if ($entity instanceof Respaldo) {
            $tabla="Respaldo";
        }
        if ($entity instanceof TelefonoPersonal) {
            $tabla="TelefonoPersonal";
        }
        if ($entity instanceof TipoBaseLegal) {
            $tabla="TipoBaseLegal";
        }
        if ($entity instanceof TipoDatoGenerico) {
            $tabla="TipoDatoGenerico";
        }
        if ($entity instanceof TipoNomina) {
            $tabla="TipoNomina";
        }
        if ($entity instanceof UnidadOrganizativa) {
            $tabla="UnidadOrganizativa";
        }
        if ($entity instanceof Usuario) {
            $tabla="Usuario";
        }
        if ($entity instanceof Vacaciones) {
            $tabla="Vacaciones";
        }
        if ($entity instanceof Vehiculo) {
            $tabla="Vehiculo";
        }
        if ($entity instanceof Vivienda) {
            $tabla="Vivienda";
        }
        if ($entity instanceof Cargo) {
            $tabla="Cargo";
        }
        if ($entity instanceof userPersonalM) {
            $tabla="userpersonalm";
        }
        if ($entity instanceof Reposo) {
            $tabla="Reposo";
        }
        if ($entity instanceof Medico) {
            $tabla="Medico";
        }
        if ($entity instanceof Especialidad) {
            $tabla="Espacialidad";
        }
        return $tabla;
    }
    
     public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $insertada=$entity;
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $actualizada=$entity;

        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
                $eliminada=$entity;
        }

        foreach ($uow->getScheduledCollectionDeletions() as $col) {

        }

        foreach ($uow->getScheduledCollectionUpdates() as $col) {

        }
    }

    private function getRealIpAddr()
    {
//        $request = new Request(
//        $_GET,
//        $_POST,
//        array(),
//        $_COOKIE,
//        $_FILES,
//        $_SERVER
//    );
//        $ip =$request->getClientIp();
//        $ip =$request->getClientIps();
//        $ip =$request->getTrustedHeaderName('client_ip');
        if(isset( $_SERVER["HTTP_X_FORWARDED_FOR"])){
           $IP = $_SERVER["HTTP_X_FORWARDED_FOR"];
           $host = @gethostbyaddr($_SERVER["HTTP_X_FORWARDED_FOR"]);
           $aitem=  split(",", $IP);
           $resul=$aitem[1]."/".$aitem[0];
        }else{
           $IP = $_SERVER["REMOTE_ADDR"];
           $proxy = "No proxy detected";
           $host = @gethostbyaddr($_SERVER["REMOTE_ADDR"]);
           $resul="$host/$IP";
        }
        return $resul;
    }
    
     public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $tabla=$this->GetTabla($entity);
        if ($tabla!="") {
            $em = $args->getObjectManager();
            $uow= $em->getUnitOfWork();
            $cambios=$uow->getentityChangeSet($entity);
             $usuario = $this->container->getToken('security.context')->getUser();
             $ipcliente =$this->getRealIpAddr();
             $hoy = new DateTime('NOW');
             $historico= new HistoricoEvento; 
             $historico->setUsuarioaplicacion($usuario->getPersLoginU());
             $historico->setEsquema("sis_nomina");
             $historico->setTabla($tabla);
             $historico->setUsuarioconexion("userconexion");
             $historico->setIpconexion($_SERVER['REMOTE_ADDR'] );
             $historico->setIpconexion($ipcliente );
             $historico->setIpaplicacion($ipcliente);
             $historico->setSistema("nomima-web");
             $historico->setFecha($hoy);
             $historico->setAccion("DELETE");
             $fields = $this->getArrayEntity($entity);
             $datosactualizados = new JsonResponse($fields);
             $historico->setDatosnuevos($datosactualizados->getContent());
             $em->persist($historico);
             $em->flush();
        }
    }
    
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $tabla=$this->GetTabla($entity);
        if ($tabla!="") {
            $em = $args->getObjectManager();
            $uow= $em->getUnitOfWork();
            $cambios=$uow->getentityChangeSet($entity);
             $usuario = $this->container->getToken('security.context')->getUser();
             $ipcliente =$this->getRealIpAddr();
             $hoy = new DateTime('NOW');
             $historico= new HistoricoEvento; 
             $historico->setUsuarioaplicacion($usuario->getPersLoginU());
             $historico->setEsquema("sis_nomina");
             $historico->setTabla($tabla);
             $historico->setUsuarioconexion("userconexion");
             $historico->setIpconexion($_SERVER['REMOTE_ADDR'] );
             $historico->setIpconexion($ipcliente );
             $historico->setIpaplicacion($ipcliente);
             $historico->setSistema("nomima-web");
             $historico->setFecha($hoy);
             $historico->setAccion("INSERT");
             $datosactualizados = new JsonResponse($cambios);
             $historico->setDatosnuevos($datosactualizados->getContent());
             $em->persist($historico);
             $em->flush();
        }
    }
    
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $tabla=  $this->GetTabla($entity);
        if ($tabla!="") {
            $em = $args->getObjectManager();
            $uow= $em->getUnitOfWork();
            $cambios=$uow->getentityChangeSet($entity);
             $usuario = $this->container->getToken('security.context')->getUser();
             $ipcliente =$this->getRealIpAddr();
             $cambios['id']=array($entity->getId(),$entity->getId());
             $hoy = new DateTime('NOW');
             $historico= new HistoricoEvento; 
             $historico->setUsuarioaplicacion($usuario->getPersLoginU());
             $historico->setEsquema("sis_nomina");
             $historico->setTabla($tabla);
             $historico->setUsuarioconexion("userconexion");
             $historico->setIpconexion($ipcliente );
             $historico->setIpaplicacion($ipcliente);
             $historico->setSistema("nomima-web");
             $historico->setFecha($hoy);
             $historico->setAccion("UPDATE");
             $datosactualizados = new JsonResponse($cambios);
             $historico->setDatosnuevos($datosactualizados->getContent());
             $em->persist($historico);
             $em->flush();
        }
    }
}
