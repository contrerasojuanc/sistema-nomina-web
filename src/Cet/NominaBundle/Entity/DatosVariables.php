<?php

namespace Cet\NominaBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\DatosVariables
 *
 * @ORM\Entity(repositoryClass="DatosVariablesRepository")
 * @ORM\Table(name="sis_nomina.datos_variables", indexes={@ORM\Index(name="fk_datos_variables_personal1_idx", columns={"personal_cedula"}), @ORM\Index(name="fk_datosVariables_cargaMasiva1_idx", columns={"carga_masiva"})})
 * @UniqueEntity(
 *     fields={"fk_datos_variables_personal1","fk_datos_variables_concepto1","seccionAfectacion"},
 *     message="Ya existe ese dato variable para el funcionario en la sección de afectación seleccionada"
 * )
 */
class DatosVariables
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var numeric
     * @ORM\Column(name="valor", type="float", nullable=false)
     */
    private $valor;

    /**
     * @var string
     * @ORM\Column(name="modo_afectacion", type="text", nullable=true)
     */
    private $modoAfectacion;

    /**
     * @var string
     * @ORM\Column(name="seccion_afectacion", type="text", nullable=true)
     */
    private $seccionAfectacion;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="datosVariables")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_datos_variables_personal1;

    /**
     * @ORM\ManyToOne(targetEntity="Concepto", inversedBy="datosVariables")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_datos_variables_concepto1;
    
    /**
     * @ORM\ManyToOne(targetEntity="CargaMasiva", inversedBy="datosVariables")
     * @ORM\JoinColumn(name="carga_masiva", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $fk_datosVariables_cargaMasiva1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return DatosVariables
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set valor
     *
     * @param string $valor
     * @return DatosVariables
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return string 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set modoAfectacion
     *
     * @param string $modoAfectacion
     * @return DatosVariables
     */
    public function setModoAfectacion($modoAfectacion)
    {
        $this->modoAfectacion = $modoAfectacion;

        return $this;
    }

    /**
     * Get modoAfectacion
     *
     * @return string 
     */
    public function getModoAfectacion()
    {
        return $this->modoAfectacion;
    }

    /**
     * Set seccionAfectacion
     *
     * @param string $seccionAfectacion
     * @return DatosVariables
     */
    public function setSeccionAfectacion($seccionAfectacion)
    {
        $this->seccionAfectacion = $seccionAfectacion;

        return $this;
    }

    /**
     * Get seccionAfectacion
     *
     * @return string 
     */
    public function getSeccionAfectacion()
    {
        return $this->seccionAfectacion;
    }

    /**
     * Set fk_datos_variables_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkDatosVariablesPersonal1
     * @return DatosVariables
     */
    public function setFkDatosVariablesPersonal1(\Cet\NominaBundle\Entity\Personal $fkDatosVariablesPersonal1)
    {
        $this->fk_datos_variables_personal1 = $fkDatosVariablesPersonal1;

        return $this;
    }

    /**
     * Get fk_datos_variables_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkDatosVariablesPersonal1()
    {
        return $this->fk_datos_variables_personal1;
    }

    /**
     * Set fk_datos_variables_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkDatosVariablesConcepto1
     * @return DatosVariables
     */
    public function setFkDatosVariablesConcepto1(\Cet\NominaBundle\Entity\Concepto $fkDatosVariablesConcepto1)
    {
        $this->fk_datos_variables_concepto1 = $fkDatosVariablesConcepto1;

        return $this;
    }

    /**
     * Get fk_datos_variables_concepto1
     *
     * @return \Cet\NominaBundle\Entity\Concepto 
     */
    public function getFkDatosVariablesConcepto1()
    {
        return $this->fk_datos_variables_concepto1;
    }
    
    /**
     * Set fk_datosVariables_cargaMasiva1
     *
     * @param \Cet\NominaBundle\Entity\CargaMasiva $fkDatosVariablesCargaMasiva1
     * @return DatosVariables
     */
    public function setFkDatosVariablesCargaMasiva1(\Cet\NominaBundle\Entity\CargaMasiva $fkDatosVariablesCargaMasiva1)
    {
        $this->fk_datosVariables_cargaMasiva1 = $fkDatosVariablesCargaMasiva1;

        return $this;
    }

    /**
     * Get fk_datosVariables_cargaMasiva1
     *
     * @return \Cet\NominaBundle\Entity\CargaMasiva 
     */
    public function getFkDatosVariablesCargaMasiva1()
    {
        return $this->fk_datosVariables_cargaMasiva1;
    }
}
