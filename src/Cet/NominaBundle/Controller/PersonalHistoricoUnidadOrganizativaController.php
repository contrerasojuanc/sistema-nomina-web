<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa;
use Cet\NominaBundle\Form\PersonalHistoricoUnidadOrganizativaType;

/**
 * HistoricoUnidadOrganizativa controller.
 *
 */
class PersonalHistoricoUnidadOrganizativaController extends Controller
{

    /**
     * Lists all HistoricoUnidadOrganizativa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->findAll();

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new HistoricoUnidadOrganizativa entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new HistoricoUnidadOrganizativa();
        
        $em = $this->getDoctrine()->getManager();
        $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $id));
        $entity->setFkUnidadOrganizativaHasInformacionLaboralInformacionLa1($informacionlaboral);
        
        $idinformacionlaboral=$informacionlaboral->getId();
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
//            $query = $em->createQuery('SELECT huo.fechaInicio FROM CetNominaBundle:HistoricoUnidadOrganizativa huo where huo.id=16');
//            $fechafin = $query->getResult();
//            
//            foreach($fechafin as $fecha)
//            {
//                $fechaf = $fecha->getFechaInicio();
//            }
            
            //$historicounidadorganizativa = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->findOneBy(array('id' => $entity->getId()));
            $fechafin=$entity->getFechaInicio();
            $fechafin=$fechafin->format("Y-m-d");
            
            $query = $em->createQuery("UPDATE CetNominaBundle:HistoricoUnidadOrganizativa huo SET huo.fechaFin='$fechafin' WHERE huo.fechaFin is NULL and huo.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=$idinformacionlaboral");
            //este actuazaliza el id nuevo  debe ser el anterior
            //$query = $em->createQuery("UPDATE CetNominaBundle:HistoricoUnidadOrganizativa huo SET huo.fechaFin='$fechafin' WHERE huo.id=".$entity->getId());
            $query->execute();
            
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            
            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkUnidadOrganizativaHasInformacionLaboralInformacionLa1()->getPersonal()->getId())));
            
        }

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a HistoricoUnidadOrganizativa entity.
    *
    * @param HistoricoUnidadOrganizativa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(HistoricoUnidadOrganizativa $entity,$id)
    {
        $form = $this->createForm(new PersonalHistoricoUnidadOrganizativaType(), $entity, array(
            'action' => $this->generateUrl('personalhistoricounidadorganizativa_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new HistoricoUnidadOrganizativa entity.
     *
     */
    public function newAction($id)
    {
        $entity = new HistoricoUnidadOrganizativa();
        
        $em = $this->getDoctrine()->getManager();
        
        $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $id));
        
        if (!$informacionlaboral) {
            echo "<script>alert('Antes de asignar cargo y dirección debe completar los datos de información laboral de la sección 1 (Información Laboral Sec-1).')</script>";
            echo "<script>javascript:history.back(-1);</script>";
        }
        
        $entity->setFkUnidadOrganizativaHasInformacionLaboralInformacionLa1($informacionlaboral);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a HistoricoUnidadOrganizativa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing HistoricoUnidadOrganizativa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->findOneBy(array('id' => $id));
        

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a HistoricoUnidadOrganizativa entity.
    *
    * @param HistoricoUnidadOrganizativa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(HistoricoUnidadOrganizativa $entity)
    {
        $form = $this->createForm(new PersonalHistoricoUnidadOrganizativaType(), $entity, array(
            'action' => $this->generateUrl('personalhistoricounidadorganizativa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing HistoricoUnidadOrganizativa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkUnidadOrganizativaHasInformacionLaboralInformacionLa1()->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:HistoricoUnidadOrganizativa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a HistoricoUnidadOrganizativa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoUnidadOrganizativa.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        //return $this->redirect($this->generateUrl('personal'));
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkUnidadOrganizativaHasInformacionLaboralInformacionLa1()->getPersonal()->getId())));
    }

    /**
     * Creates a form to delete a HistoricoUnidadOrganizativa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalhistoricounidadorganizativa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
