<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArchivoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null,array('label' => 'Nombre','attr' => array('help'=>'Inserte la ayuda aquí')))
            ->add('ruta', null, array('label' => 'Ruta del archivo','attr' => array('help'=>'Inserte la ayuda aquí')))
            ->add('tipo', null, array('label' => 'Tipo de archivo','attr' => array('help'=>'Inserte la ayuda aquí')))
            ->add('tamanio', null, array('label' => 'Tamaño en bytes','attr' => array('help'=>'Inserte la ayuda aquí')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Archivo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_archivo';
    }
}
