<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Variable
 *
 * @ORM\Entity(repositoryClass="VariableRepository")
 * @ORM\Table(name="sis_nomina.variable")
 * @UniqueEntity(
 *     fields={"nombre"},
 *     message="Ya existe un identificador de variable o concepto con el mismo nombre"
 * )
 */
class Variable
{
    /**
     * Relacionar muchos a muchos con personal
     * 
     * Funciones para popular la lista de atributos por tabla
     * POSTGRES:
     * select * FROM pg_database;
     * select * FROM information_schema.tables where table_schema=nombre_esquema;
     * select column_name from information_schema.columns where table_name =table;
     * MYSQL:
     * show databases;
     * show tables;
     * show columns;
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=false, unique=true)
     */
    private $nombre;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true, unique=true)
     */
    private $descripcion;

    /**
     * Solo valores:
     * 0 = Operando
     * 1 = Operador
     *
     * @var string
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     * @ORM\Column(name="sql", type="text", nullable=true)
     */
    private $sql;

    /**
     * @var string
     * @ORM\Column(name="tipo_gestor_sql", type="string", length=1, nullable=true)
     */
    private $tipoGestorSql;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicia", type="date", nullable=true)
     */
    private $fechaInicia;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_fin", type="date", nullable=true)
     */
    private $fechaFin;
    
    /**
     * @ORM\OneToMany(targetEntity="Formula", mappedBy="fk_variable_has_concepto_variable1")
     * @ORM\JoinColumn(name="variable_id", referencedColumnName="id", nullable=false)
     */
    protected $formulas;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoVariable", mappedBy="fk_variable_has_personal_variable1")
     * @ORM\JoinColumn(name="variable_id", referencedColumnName="id", nullable=false)
     */
    protected $historicoVariables;

    public function __construct()
    {
        $this->formulas = new ArrayCollection();
        $this->historicoVariables = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Variable
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Variable
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set sql
     *
     * @param string $sql
     * @return Variable
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * Get sql
     *
     * @return string 
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * Set tipoGestorSql
     *
     * @param string $tipoGestorSql
     * @return Variable
     */
    public function setTipoGestorSql($tipoGestorSql)
    {
        $this->tipoGestorSql = $tipoGestorSql;

        return $this;
    }

    /**
     * Get tipoGestorSql
     *
     * @return string 
     */
    public function getTipoGestorSql()
    {
        return $this->tipoGestorSql;
    }

    /**
     * Add formulas
     *
     * @param \Cet\NominaBundle\Entity\Formula $formulas
     * @return Variable
     */
    public function addFormula(\Cet\NominaBundle\Entity\Formula $formulas)
    {
        $this->formulas[] = $formulas;

        return $this;
    }

    /**
     * Remove formulas
     *
     * @param \Cet\NominaBundle\Entity\Formula $formulas
     */
    public function removeFormula(\Cet\NominaBundle\Entity\Formula $formulas)
    {
        $this->formulas->removeElement($formulas);
    }

    /**
     * Get formulas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormulas()
    {
        return $this->formulas;
    }

    /**
     * Add historicoVariables
     *
     * @param \Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables
     * @return Variable
     */
    public function addHistoricoVariable(\Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables)
    {
        $this->historicoVariables[] = $historicoVariables;

        return $this;
    }

    /**
     * Remove historicoVariables
     *
     * @param \Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables
     */
    public function removeHistoricoVariable(\Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables)
    {
        $this->historicoVariables->removeElement($historicoVariables);
    }

    /**
     * Get historicoVariables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoVariables()
    {
        return $this->historicoVariables;
    }

    /**
     * Set fechaInicia
     *
     * @param \DateTime $fechaInicia
     * @return Variable
     */
    public function setFechaInicia($fechaInicia)
    {
        $this->fechaInicia = $fechaInicia;

        return $this;
    }

    /**
     * Get fechaInicia
     *
     * @return \DateTime 
     */
    public function getFechaInicia()
    {
        return $this->fechaInicia;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Variable
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Variable
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Validador
     * @param $metadata
     * @return Validado
     */    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('nombre', new Assert\NotBlank())
//                 ->addPropertyConstraint ('nombre', new Assert\Regex(array(
//                     'pattern' => '/[^a-zA-Z0-9_]+/i',
//                     'match'   => false,
//                     'message' => 'El Nombre no puede contener espacios ni caracteres especiales',
//                    )))
                 ->addPropertyConstraint ('nombre', new Assert\Regex(array(
                     'pattern' => '/^[a-zA-Z][a-zA-z0-9\_]+[a-zA-Z0-9]$/i',
                     'match'   => true,
                     'message' => 'El formato debe comenzar con una letra y no debe contener espacios ni caracteres especiales intermedios',
                    )))
                //Seguridad contra DQL inyection
                 ->addPropertyConstraint ('sql', new Assert\Regex(array(
                     'pattern' => '/\binsert\b|\bdelete\b|\bupdate\b|\bcreate\b|\balter\b;/i',
                     'match'   => false,
                     'message' => 'El formato no debe contener palabras de definición de base de datos',
                    )))
            ;        
    }    
    
    public function __toString()
    {
        return $this->getNombre();
    }
}