<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Vacaciones;
use Cet\NominaBundle\Entity\EnLetras;
use Cet\NominaBundle\Entity\Variable;
use Cet\NominaBundle\Entity\Concepto;
use Cet\NominaBundle\Entity\Respaldo;
use Cet\NominaBundle\Entity\Archivo;
use Cet\NominaBundle\Entity\VariableFormula;
use Cet\NominaBundle\Form\VacacionesType;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use DateTime;

/**
 * Vacaciones controller.
 *
 */
class VacacionesController extends Controller
{
    /**
     * Lists all Vacaciones entities.
     *
     */
    public function indexAction()
    {       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Vacaciones')->findAll();

        return $this->render('CetNominaBundle:Vacaciones:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    
    /**
     * Lists all Vacaciones entities.
     *
     */
    public function indexIndividualAction($cedula)
    {       
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Vacaciones')->findBy(array('personal'=>$cedula));
        
        $entity = new Vacaciones();
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
        $entity->setPersonal($personal);
        
        return $this->render('CetNominaBundle:Vacaciones:index.html.twig', array(
            'entities' => $entities,
            'entity'  => $entity,
            'cedula' => $cedula,
        ));
    }
    
    /**
     * Lists all Vacaciones entities.
     *
     */
    public function indexPersonalAction()
    {
        $em = $this->getDoctrine()->getManager();
        //$consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo'); sirve para mostrar el historico de direcciones no borrar
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();

        return $this->render('CetNominaBundle:Vacaciones:indexPersonal.html.twig', array(
            'entities' => $informacionpersonal,
        ));

    }
    /**
     * Creates a new Vacaciones entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Vacaciones();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('vacaciones_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:Vacaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Vacaciones entity.
    *
    * @param Vacaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Vacaciones $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VacacionesType(), $entity, array(
            'action' => $this->generateUrl('vacaciones_create'),
            'method' => 'POST',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Vacaciones entity.
     *
     */
    public function newAction()
    {
        $entity = new Vacaciones();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:Vacaciones:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Vacaciones entity.
     *
     */
    public function newPersonalAction($cedula)
    {
        $entity = new Vacaciones();
        
        $em = $this->getDoctrine()->getManager();
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
        $entity->setPersonal($personal);
        
        $form   = $this->createCreateForm($entity);
        

        return $this->render('CetNominaBundle:Vacaciones:new.html.twig', array(
            'cedula' => $cedula,
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    
    /**
     * Finds and displays a Vacaciones entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vacaciones')->find($id);
        
        $cedula = $entity->getPersonal()->getId();

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vacaciones.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Vacaciones:show.html.twig', array(
            'entity'      => $entity,
            'cedula'      => $cedula,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Vacaciones entity.
     *
     */
    public function editAction($id, $cedula=null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vacaciones')->find($id);
        
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($cedula);
        $entity->setPersonal($personal);
                
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vacaciones.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Vacaciones:edit.html.twig', array(
            'cedula'      => $cedula,
            'personal'    => $personal,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Vacaciones entity.
    *
    * @param Vacaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Vacaciones $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new VacacionesType(), $entity, array(
            'action' => $this->generateUrl('vacaciones_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing Vacaciones entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vacaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vacaciones.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('vacaciones_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:Vacaciones:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Vacaciones entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Vacaciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Vacaciones.');
            }
            
            $cedula = $entity->getPersonal()->getId();

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('vacaciones_individual',array('cedula' => $cedula)));
    }

    /**
     * Creates a form to delete a Vacaciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vacaciones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }

    /**
     * Calcula el bono vacacional.
     *
     */
    public function calcularAction($bono,$cedula,$imprimir=null,$almacenar=null)
    {   
        $em = $this->getDoctrine()->getManager();
        
        //Datos personales
        $funcionario = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $cedula));
        
        //Datos laborales
        $informacionLaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $cedula));

        //Datos de bono vacacional seleccionado
        $bonoSeleccionado = $em->getRepository('CetNominaBundle:Vacaciones')->findOneBy(array('id' => $bono));
        $fecha = $bonoSeleccionado->getFecha();
        $nuevaFecha = $fecha->format('Y-m-01');
        //Consultar si hay datos variables para el bono vacacional
        $consulta = $em->createQuery("SELECT c.id, c.codigo, c.denominacion, dv.valor "
                . "FROM  CetNominaBundle:DatosVariables dv JOIN dv.fk_datos_variables_concepto1 c "
                . "JOIN dv.fk_datos_variables_personal1 p WHERE c.denominacion LIKE '%Vacacional%' AND c.tipo LIKE '%Asigna%' AND dv.seccionAfectacion ='principal' AND p.id=$cedula"            
            );
        $vacacionesDatoVariable = $consulta->getResult();
        $HayVacacionDV = false;
        foreach($vacacionesDatoVariable as $vacacionesDV){
            $HayVacacionDV=true;
            $montoVacacionDV =  $vacacionesDV['valor'];
        }
        
//        $consulta = $em->createQuery("SELECT c.nombre cargo,c.nivel,hc.fechaInicio as cargoinicia,hc.id as historicocargo,hc.esEncargado,hc.fechaFin,il.cuentaNomina,il.tipoCuentaNomina FROM CetNominaBundle:Personal p JOIN p.informacionLaboral il JOIN il.historicoCargos hc JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c WHERE p.id=".$cedula." AND (hc.esEncargado='f' OR hc.esEncargado IS NULL) AND hc.fechaFin IS NULL ORDER BY hc.fechaInicio DESC");
//        $historicocargo = $consulta->getResult();
//
//        $consulta1 = $em->createQuery("SELECT huo.id,uo.id as iduo, uo.nombre FROM CetNominaBundle:HistoricoUnidadOrganizativa huo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_informacion_la1 il JOIN il.personal p WHERE p.id='".$cedula."' AND huo.fechaFin IS NULL");
//        $historicouo = $consulta1->getResult();
        
        $consulta = $em->createQuery("SELECT hc,il.cuentaNomina AS cuentaNomina,il.tipoCuentaNomina AS tipoCuentaNomina,c.nombre AS cargo,c.nivel AS nivel,uo.id AS iduo,uo.nombre AS nombre "
            . "FROM CetNominaBundle:HistoricoCargo hc "
            . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c "
            . "LEFT JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
            . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
            . "LEFT JOIN il.personal p "
            . "WHERE  p.id = ".$cedula." "
            . "AND hc.fechaInicio IN "
                . "("
                    . "SELECT MAX(hc2.fechaInicio) " 
                    . "FROM CetNominaBundle:HistoricoCargo hc2 "
                    . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = il.id "
                    . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END)) "
                . ")"
            );
        $historicocargo = $consulta->getResult();
        
        $ultimaNomina = 0;        
        $consultaUltimaNomina = $em->createQuery("SELECT hc.nominaId AS id_nomina,hc.fechaInicio AS maxima_fecha "
            . "FROM CetNominaBundle:HistoricoConcepto hc "
            . "JOIN hc.fk_historico_concepto_personal1 p "
            . "JOIN p.informacionLaboral il "
            . "JOIN hc.fk_historico_concepto_nomina1 n "
            . "JOIN n.fk_nomina_plantilla_nomina1 pn "
            . "WHERE il.fk_informacion_laboral_tipo_nomina1 = pn.fk_plantilla_nomina_tipo_nomina1 "
            . "AND n.tipo = 'Ordinaria' "
            . "AND hc.fechaInicio < '$nuevaFecha' "
            . "AND p.id = ".$cedula." "
            . "GROUP BY id_nomina,maxima_fecha "
            . "ORDER BY maxima_fecha DESC "               
            );
        $consultaUltimaNomina->setMaxResults(2);        
        $nominas = $consultaUltimaNomina->getResult();
        
        $saldoDiasPendientes[0][1] = 0;
        $saldoHorasPendientes[0][1] = 0;
        $saldoMinutosPendientes[0][1] = 0;
        
        //Saldo Dias Pendientes
        $consultaDiasPendientes = $em->createQuery("SELECT SUM(dp.dias) "
            . "FROM CetNominaBundle:Personal p "
            . "JOIN p.diasPendientes dp "
            . "WHERE p.id = $cedula "               
            );
        
        $saldoDiasPendientes = $consultaDiasPendientes->getResult();
        
        //Saldo Horas Pendientes
        $consultaHorasPendientes = $em->createQuery("SELECT SUM(dp.horas) "
            . "FROM CetNominaBundle:Personal p "
            . "JOIN p.diasPendientes dp "
            . "WHERE p.id = $cedula "               
            );
        
        $saldoHorasPendientes = $consultaHorasPendientes->getResult();
        
        //Saldo Minutos Pendientes
        $consultaMinutosPendientes = $em->createQuery("SELECT SUM(dp.minutos) "
            . "FROM CetNominaBundle:Personal p "
            . "JOIN p.diasPendientes dp "
            . "WHERE p.id = $cedula "               
            );
        
        $saldoMinutosPendientes = $consultaMinutosPendientes->getResult();
        
        //Consulta de Dias no Laborables
        $consultaDiasNoLaborales = $em->createQuery("SELECT f.fecha "
            . "FROM CetNominaBundle:orgaFeriadosM f "            
            );
        $diasNoLaborales = $consultaDiasNoLaborales->getResult();
        
        $vector_no_laborable = array();
        foreach($diasNoLaborales as $d){
            $vector_no_laborable[] = $d['fecha'];
        }
        
        $ultimaNomina = 0;
        $primeraNomina = 0;
        $contador = 0; 
        foreach($nominas as $nomina){
            if($contador == 0)
                $primeraNomina = $nomina['id_nomina'];
            if($contador == 1)
                $ultimaNomina = $nomina['id_nomina'];
            $contador++;
        }
        
        //Datos de conceptos calculados
        $consultaInformacion = $em->createQuery("SELECT hc "
                . "FROM CetNominaBundle:HistoricoConcepto hc "
                . "JOIN hc.fk_historico_concepto_personal1 p "
                . "JOIN hc.fk_historico_concepto_concepto1 c "
                . "WHERE p.id=".$cedula." "
                . "AND "
                . "( "
                . "hc.fk_historico_concepto_nomina1=$primeraNomina "
                . "OR hc.fk_historico_concepto_nomina1=$ultimaNomina"
                . ") "
                . "ORDER BY c.codigo");
        $informacion = $consultaInformacion->getResult();
        
        $arreglo = array();
        $totalAsignacion = 0;
        $totalDeduccion = 0;
        foreach($informacion as $conceptos) 
        {   
            $idConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getId();
            $tipoConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getTipo();
            $denominacionConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion();

            if(!preg_match('/^Retroactivo/i',$denominacionConcepto)){
                if ($tipoConcepto == 'Asignación'){
                    $totalAsignacion = $totalAsignacion + ( $conceptos->getMonto() );
                }
                elseif($denominacionConcepto == 'impuesto_sobre_renta'){
                    $totalDeduccion = $totalDeduccion + ( $conceptos->getMonto() );
                }
                //Se utiliza la concatenacion con _ para poder utilizar un valor cadena como clave de las hash   
                $arreglo["_".$idConcepto]=array($idConcepto,$denominacionConcepto,$tipoConcepto,(isset($arreglo["_".$idConcepto][3])?($arreglo["_".$idConcepto][3] +$conceptos->getMonto()): $conceptos->getMonto() ));
            }
        }
        
        //Bloque de sustitución de montos por conceptos por medio de Datos Variables "Bono Vacacional"
        //Buscar los conceptos que se sustituirán en datos variables para cada persona       
        $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datos_variables_personal1'=>$cedula));
        if($datosvariables!=null){
            foreach($datosvariables as $datosvariable){
                    $dato = $datosvariable->getFkDatosVariablesConcepto1();
                    $monto = $datosvariable->getValor();                    
                    $idConcepto = $dato->getId();
                    $denominacionConcepto = $dato->getDenominacion();
                    $tipoConcepto = $dato->getTipo();
                if($datosvariable->getSeccionAfectacion()=='vacacional'){  
                    $arreglo["_".$idConcepto] = array($idConcepto,$denominacionConcepto,$tipoConcepto,(isset($arreglo["_".$idConcepto][3])?($arreglo["_".$idConcepto][3] + $monto): $monto ));                    
                }                   
            }
        }   
        
        $clientevariable = new VariableController();
        $clienteconcepto = new ConceptoController();
        $clientevariable->setContainer($this->container);
        $clienteconcepto->setContainer($this->container);

//        $variables = $em->getRepository('CetNominaBundle:Variable')->findAll();
        $cv = $em->createQuery('select v.nombre,v.sql from CetNominaBundle:Variable v');
        $todasVariables = $cv->getResult();
        
        
        //Se establecen a cero todas las variables necesarias para el cálculo del bono vacacional
        //porcentaje_retencion_islr, dias_servicio_contraloria y dias_otros_organismos
        $vector_variables['porcentaje_retencion_islr'] = 0;
        $vector_variables['dias_servicio_contraloria'] = 0;
        $vector_variables['dias_otros_organismos'] = 0;
        
        foreach($todasVariables as $objeto){       
            $sqlvariable = $objeto['sql'];
            $nombreVariable = $objeto['nombre'];
//            $sqlvariable=$objeto->getFkVariableHasPersonalVariable1()->getSql();
//            $nombreVariable=$objeto->getFkVariableHasPersonalVariable1()->getNombre();

            $sqlvariable = str_replace("CURRENT_DATE()", "'".$fecha->format("Y-m-d")."'", $sqlvariable);
            
            $consultasql=$em->createQuery($sqlvariable);
            $consultasql->setParameter('personal_cedula', $cedula);
            $consultasql->setMaxResults(1);
            $valorVariable=0;
            if ($consultasql->getResult()){
                foreach($consultasql->getSingleResult() as $dato){
                    if(!is_object($dato)){
                        $valorVariable=$dato;
                    }                    
                    elseif($dato instanceof \DateTime){
//                        $valorVariable=$dato->format('d/m/Y');
//                        $valorVariable=new \DateTime($valorVariable);
                        $valorVariable=$dato;
                    }
                }
            }            
//                    $$nombreVariable=$valorVariable;
            $vector_variables[$nombreVariable]=$valorVariable;
        }
        /////////////////////////////
        //Inicio Variables Predefinidas
        /////////////////////////////
            //Cuenta cantidad de lunes en un lapso de fechas
            $lunes = 1;
            $helper = $this->get('cet.nomina.globales');            
            $lunes = $helper->countDays(1, mktime(0, 0, 0, 1, 1, 2000), mktime(0, 0, 0, 2, 1, 2000));            
            $vector_variables["cantidad_lunes"] = $lunes;
        /////////////////////////////
        //Fin Variables Predefinidas
        /////////////////////////////
                
        $todosConceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
        foreach ($todosConceptos as $concep){
            $nombreConcepto=$concep->getIdentificador();
            $vector_conceptos[]=$nombreConcepto;
        }
        $vector_evaluado=array(); 
        
        $cargo="Sin Asignar";
        $cuenta="Sin Asignar";
        $tipocuenta="Sin Asignar";
        $nivel="";
        $romano="";

        $arreglouo=array();
        $uo="SIN UNIDAD ORGANIZATIVA";
        $uod="SIN UNIDAD ORGANIZATIVA";
        $iduo=99;
        $iduod=99;

        foreach($historicocargo as $hc){
            $cargo=$hc['cargo'];
            $nivel=$hc['nivel'];
            $cuenta=$hc['cuentaNomina'];
            $tipocuenta=$hc['tipoCuentaNomina'];                

            $romano="";
            if($nivel==1)$romano='I';
            elseif($nivel==2)$romano='II';
            elseif($nivel==3)$romano='III';
            elseif($nivel==4)$romano='IV';
            elseif($nivel==5)$romano='V';
            elseif($nivel==6)$romano='VI';
            elseif($nivel==7)$romano='VII';
            elseif($nivel==8)$romano='VIII';
            elseif($nivel==9)$romano='IX';
            elseif($nivel==10)$romano='X';

            $uo=$hc['nombre']; 
            $iduo=$hc['iduo'];
            $claveforanea=$hc['iduo'];
            while($claveforanea!=null){
                $consultas2 = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($claveforanea);
                $claveforanea=$consultas2->getFkUnidadOrganizativaUnidadOrganizativa1();
                $uod=$consultas2->getNombre();
                $iduod=$consultas2->getId();
                //Arreglo de la ruta de las unidades organizativas del funcionario
                $arreglouo[$iduod]=$uod;
            }
        }

        if(count($arreglouo)==0){
           $arreglouo[$iduod]=$uod; 
        }
       
        $var=new VariableFormula();        
        $vector_variables['Variable']=$var;
        
        $aniosOtrosOrganismos = round($clienteconcepto->evaluarConcepto("dias_otros_organismos",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) / 365 , 2);
        $aniosOtrosOrganismosEntero = floor($aniosOtrosOrganismos); 
        $aniosServicioContraloria = round($clienteconcepto->evaluarConcepto("dias_servicio_contraloria",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula) / 365 , 2);
        $aniosServicioContraloriaEntero = floor($aniosServicioContraloria);
        $aniosServicio = round($aniosOtrosOrganismos + $aniosServicioContraloriaEntero ,2 );
        
        $porcentaIslr = $clienteconcepto->evaluarConcepto("porcentaje_retencion_islr",$vector_variables,$vector_conceptos,$vector_evaluado,$cedula);
                
        if ( $aniosServicio <= 5 ) {
            $diasBonoVacacional = 35;
            $diasDisfrute1 = 21;            
        }
        elseif ( $aniosServicio <= 10 ) {
            $diasBonoVacacional = 40; 
            $diasDisfrute1 = 24;
        }
        elseif ( $aniosServicio <= 15 ) {
            $diasBonoVacacional = 43;
            $diasDisfrute1 = 27;
        }        
        else {
            $diasBonoVacacional = 48;
            $diasDisfrute1 = 31;
        }
        $tope = 46 - $diasDisfrute1;
        if(($aniosServicioContraloria - 1) > $tope)
               $diasDisfrute2 = $tope;
        else
            $diasDisfrute2 = $aniosServicioContraloria - 1;
        
        //De acuerdo a solicitud de RRHH, cuando sea alto funcionario tiene top de 40 diasBonoVacacional
        if( preg_match('/^Altos Funcionarios/i',$informacionLaboral->getFkInformacionLaboralTipoNomina1()->getNombre() ) )
            $diasBonoVacacional = $diasBonoVacacional>40?40:$diasBonoVacacional;
        
        //Dias de disfrute para personal obrero fijo y contratado
        $tipo = $informacionLaboral->getFkInformacionLaboralTipoNomina1()->getId();
        if ( $tipo == 3 || $tipo == 4 ) {            
            $diasDisfrute1 = 15;         
            $topec = 30 - $diasDisfrute1;
            if(($aniosServicioContraloria - 1) > $topec)
                   $diasDisfrute2 = $topec;
            else
                $diasDisfrute2 = $aniosServicioContraloria - 1;
            $diasBonoVacacional = $diasDisfrute1 + $diasDisfrute2;
        }
        
        //Incrementar minutos pendientes
        $saldoHorasPendientes[0][1] = intval( $saldoHorasPendientes[0][1] + intval( $saldoMinutosPendientes[0][1] / 60 ) );
        $restoMinutos = $saldoMinutosPendientes[0][1] % 60;
        
        //Incrementar horas pendientes
        $saldoDiasPendientes[0][1] = intval( $saldoDiasPendientes[0][1] + intval( $saldoHorasPendientes[0][1] / 7 ) );
        $restoHoras = $saldoHorasPendientes[0][1] % 7;
        
        //Cálculo de fecha final de disfrute
        $diasHabiles = ($diasDisfrute1 + $diasDisfrute2 + $saldoDiasPendientes[0][1]);
        $fechaFin = $helper->addBusinessDays( $fecha , $diasHabiles, $vector_no_laborable );
        $fechaReincorporacion = clone $fechaFin;
        
        $restoMinutos = $restoMinutos + ( $restoHoras * 60 );
        if($restoMinutos >= 0){
            $fechaReincorporacion = $helper->addBusinessDays( $fechaReincorporacion , 2, $vector_no_laborable );
            $fechaReincorporacion->modify("+8 hour");            
        }
        elseif($restoMinutos < 0){            
            $fechaReincorporacion->modify("+16 hour");            
        }        
        $fechaReincorporacion->modify("+$restoMinutos minute");
        
        $nombreFuncionario = $funcionario->getPrimerNombre()." ".$funcionario->getSegundoNombre()." ".$funcionario->getPrimerApellido()." ".$funcionario->getSegundoApellido();
        
        //Bloque de llamadas de funciones globales
        $usuario = $this->get('security.context')->getToken()->getUser();
        $helper = $this->get('cet.nomina.globales');
        $revision = $helper->obtenerRevisores($em,$usuario);
        
        $personacargo[$cedula]['cuenta'] = $cuenta;
        $personacargo[$cedula]['tipocuenta'] = $tipocuenta;
        $personacargo[$cedula]['cargo'] = $cargo." ".$romano;
        $personacargo[$cedula]['uo'] = $uo;
        $personacargo[$cedula]['iduo'] = $iduo;
        $personacargo[$cedula]['uod'] = $uod;
        $personacargo[$cedula]['iduod'] = $iduod;
        $personacargo[$cedula]['arreglo'] = array_reverse($arreglouo,true);        
        $personacargo[$cedula]['diasbono'] = $diasBonoVacacional;
        $v = new EnLetras();
        $personacargo[$cedula]['diasbonoletra'] = $v->ValorEnLetras($diasBonoVacacional, 'Días');
        $personacargo[$cedula]['periodo'] = $bonoSeleccionado->getPeriodo();
        $personacargo[$cedula]['aniosotros'] = $helper->tiempo($aniosOtrosOrganismos);
        $personacargo[$cedula]['anioscet'] = (int)$aniosServicioContraloria;
        $personacargo[$cedula]['aniosservicio'] = $helper->tiempo($aniosOtrosOrganismos + $aniosServicioContraloriaEntero);
        $personacargo[$cedula]['diasDisfrute1'] = (int)$diasDisfrute1;
        $personacargo[$cedula]['diasDisfrute2'] = (int)$diasDisfrute2;
        $personacargo[$cedula]['saldoDiasPendientes'] = (int)$saldoDiasPendientes[0][1];
        $personacargo[$cedula]['desde'] = $fecha;
        $personacargo[$cedula]['hasta'] = $fechaFin;
        $personacargo[$cedula]['reincorporacion'] = $fechaReincorporacion;
        if($HayVacacionDV)
            $arreglo=array();
        $personacargo[$cedula]['arreglo'] = $arreglo;
        if($HayVacacionDV)
            $personacargo[$cedula]['totalAsignacion'] = 0;   
        else
            $personacargo[$cedula]['totalAsignacion'] = $totalAsignacion;   
        $personacargo[$cedula]['sueldoDiario'] = round($totalAsignacion / 30,2);
        if ($HayVacacionDV)
            $personacargo[$cedula]['bonoVacacional'] = $montoVacacionDV;    
        else
            $personacargo[$cedula]['bonoVacacional'] = round($personacargo[$cedula]['sueldoDiario'] * $diasBonoVacacional,2);
        $personacargo[$cedula]['islrPorcentaje'] = $porcentaIslr;
        if ($HayVacacionDV)
            $personacargo[$cedula]['islrDeducido'] = round(( $montoVacacionDV * ( $porcentaIslr / 100 ) ),2);
        else
            $personacargo[$cedula]['islrDeducido'] = round(( $personacargo[$cedula]['bonoVacacional'] * ( $porcentaIslr / 100 ) ),2);
        if ($HayVacacionDV)
            $personacargo[$cedula]['totalPagar'] = $montoVacacionDV-$personacargo[$cedula]['islrDeducido'];
        else
            $personacargo[$cedula]['totalPagar'] = $personacargo[$cedula]['bonoVacacional'] - $personacargo[$cedula]['islrDeducido'];
//        $personacargo[$cedula]['funcionario'] = htmlspecialchars(ucwords(strtolower($nombreFuncionario)), ENT_COMPAT, 'UTF-8');
        $personacargo[$cedula]['funcionario'] = $nombreFuncionario;
        $personacargo[$cedula]['usuario'] = $revision['usuario'];
        $personacargo[$cedula]['director'] = $revision['director'];
        $personacargo[$cedula]['jefe'] = $revision['jefe'];
        
        //Almacena el monto del cálculo en la tabla vacaciones
        //Esto permite continuar con el proceso de cálculo de aguinaldos y posteriormente garantáa de prestaciones sociales
        $bonoSeleccionado->setBono($personacargo[$cedula]['totalPagar']);
        $bonoSeleccionado->setBonoTotal($personacargo[$cedula]['bonoVacacional']);
        $em->flush();

        $html = $this->renderView('CetNominaBundle:Vacaciones:calcular.html.twig', array(
            'cedula'      => $cedula,
            'bono'   => $bono,  
            'funcionario'   => $funcionario,  
            'informacionLaboral' => $informacionLaboral,
            'personacargo' => $personacargo,
        ));
        
        if($imprimir){
            
//        $valores['institucion'] = "CONTRALORÍA DEL ESTADO TÁCHIRA";
//        $valores['nomina'] = $entity->getDescripcion();
//        $valores['fecha'] = $entity->getFecha()->format('d/m/Y');
//        $valores['plantilla'] = $entity->getFkNominaPlantillaNomina1()->getNombre();
//        $header = $this->renderView('CetNominaBundle:Nomina:header.html.twig', array('valores' => $valores));
            
            return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                    'print-media-type'      => true,
                    'no-background'         => true,
    //                'custom-header'         => true,
    //                'custom-header-propagation'    => true,
    //                'header-center'                => true,
    //                'header-font-size'             => 20,
    //                'default-header'         => true,
                    'header-html'           => '',
                    'footer-html'           => '',
                    'lowquality'                   => false,
                    'grayscale'                    => true,
                    'margin-bottom'                => 12,
                    'margin-left'                  => 8,
                    'margin-right'                 => 8,
                    //Para Windows
                    //'margin-top'                   => 20,
                    //Para Linux
                    'margin-top'                   => 75,
                    'page-size'                    => "Letter",
                )),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="BonoVacacional.pdf"',
                )
            ); 
        }
        
        if($almacenar){
             //Guarda en base de datos los archivos y los respaldos
            $respaldo = new Respaldo();
            $hoy = new DateTime('NOW');
            $respaldo->setDescripcion('Bono Vacacional:'.$bono.' - '.$cedula);
            $respaldo->setFecha($hoy);
            $respaldo->setBonoVacacional($bonoSeleccionado);
            $em->persist($respaldo); 
            
            $this->get('knp_snappy.pdf')->generateFromHtml(
            $html,'uploads/respaldo/BonoVacacional'.$bono.'.pdf',array(
                    'print-media-type'      => true,
                    'no-background'         => true,
    //                'custom-header'         => true,
    //                'custom-header-propagation'    => true,
    //                'header-center'                => true,
    //                'header-font-size'             => 20,
    //                'default-header'         => true,
                    'header-html'           => '',
                    'footer-html'           => '',
                    'lowquality'                   => false,
                    'grayscale'                    => true,
                    'margin-bottom'                => 12,
                    'margin-left'                  => 8,
                    'margin-right'                 => 8,
                    //Para Windows
                    //'margin-top'                   => 20,
                    //Para Linux
                    'margin-top'                   => 75,
                    'page-size'                    => "Letter",
            ));
            
            $archivo2 = new Archivo();
            $archivo2->setNombre('BonoVacacional'.$bono.'.pdf');
            $archivo2->setRuta('uploads/respaldo/');
            $archivo2->setRespaldo($respaldo);
            $em->persist($archivo2); 
            $em->flush();  
        }
        
        return $this->render('CetNominaBundle:Vacaciones:calcular.html.twig', array(
            'cedula'      => $cedula,
            'bono'   => $bono,  
            'funcionario'   => $funcionario,  
            'informacionLaboral' => $informacionLaboral,
            'personacargo' => $personacargo,
        ));
    }    
    
    /**
     * Descarga los archivos de respaldo.
     *
     * @param mixed $procesar The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function respaldoAction($procesar = null,$idBono = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CetNominaBundle:Respaldo')->findBy(array('bonoVacacional'=>$idBono));
        foreach($entities as $respaldo){
            $entities1 = $respaldo->getArchivos();
            $nombreArchivo = "";
            $rutaArchivo = "";
            
            foreach($entities1 as $archivo){
                $nombreArchivo = $archivo->getNombre();
                $rutaArchivo = "uploads/respaldo/";
                
                if(preg_match("/".$procesar."/i",$nombreArchivo)){
                    $response = new BinaryFileResponse($rutaArchivo.$nombreArchivo);
                    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                    return $response;  
                }
            }
        }
    }
}
