<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\HistoricoCargo;
use Cet\NominaBundle\Form\PersonalHistoricoCargoType;
use Cet\NominaBundle\Form\PersonalHistoricoCargoEditEncargadoType;
use Cet\NominaBundle\Form\PersonalHistoricoCargoEditFijoType;
use Cet\NominaBundle\Form\PersonalHistoricoCargoNewEncargadoType;

/**
 * HistoricoCargo controller.
 *
 */
class PersonalHistoricoCargoController extends Controller
{

    /**
     * Lists all HistoricoCargo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:HistoricoCargo')->findAll();

        return $this->render('CetNominaBundle:HistoricoCargo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new HistoricoCargo entity.
     *
     */
    public function createAction(Request $request,$id,$opcion)
    {
        $entity = new HistoricoCargo();
        
        $em = $this->getDoctrine()->getManager();
        $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $id));
        $entity->setFkInformacionLaboralHasCargoInformacionLaboral1($informacionlaboral);
        
        
        $idinformacionlaboral=$informacionlaboral->getId();
        if ($opcion=='fijo'){
            $entity->setEsEncargado('f');
            $titulo="Asignar cargo";
        }else{
            $entity->setEsEncargado('t');
            $titulo="Asignar encargaduria";
        }
        
        $form = $this->createCreateForm($entity,$id,$opcion);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //$historicounidadorganizativa = $em->getRepository('CetNominaBundle:HistoricoUnidadOrganizativa')->findOneBy(array('id' => $entity->getId()));
            $fechafin=$entity->getFechaInicio();
            $fechafin=$fechafin->format("Y-m-d");
            $encargado=$entity->getEsEncargado();
            
            if ($opcion=='fijo'){
                $query = $em->createQuery("UPDATE CetNominaBundle:HistoricoCargo hc SET hc.fechaFin='$fechafin' WHERE hc.fechaFin is NULL and hc.esEncargado='f' and hc.fk_informacion_laboral_has_cargo_informacion_laboral1=$idinformacionlaboral");
                $query->execute();
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkInformacionLaboralHasCargoInformacionLaboral1()->getPersonal()->getId())));
        }
            //return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkInformacionLaboralHasCargoInformacionLaboral1()->getPersonal()->getId())));
        return $this->render('CetNominaBundle:HistoricoCargo:new.html.twig', array(
            'titulo' => $titulo,
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a HistoricoCargo entity.
    *
    * @param HistoricoCargo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(HistoricoCargo $entity,$id,$opcion)
    {
        if ($opcion=='fijo'){
            $form = $this->createForm(new PersonalHistoricoCargoType(), $entity, array(
                'action' => $this->generateUrl('personalhistoricocargo_create', array('id' => $id,'opcion' => $opcion)),
                'method' => 'POST',
            ));
        }elseif($opcion=='encargado'){
            $form = $this->createForm(new PersonalHistoricoCargoNewEncargadoType(), $entity, array(
                'action' => $this->generateUrl('personalhistoricocargo_create', array('id' => $id,'opcion' => $opcion)),
                'method' => 'POST',
            ));
        }

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new HistoricoCargo entity.
     *
     */
    public function newAction($id,$opcion)
    {
        $entity = new HistoricoCargo();
        $em = $this->getDoctrine()->getManager();
        
        $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $id));
        
        if (!$informacionlaboral) {
            echo "<script>alert('Antes de asignar cargo y dirección debe completar los datos de información laboral de la sección 1 (Información Laboral Sec-1).')</script>";
            echo "<script>javascript:history.back(-1);</script>";
        }
        
        $entity->setFkInformacionLaboralHasCargoInformacionLaboral1($informacionlaboral);
        
        $form   = $this->createCreateForm($entity,$id,$opcion);
        
        if ($opcion=='encargado'){
            $titulo="Asignar encargaduria";
        }else{
            $titulo="Asignar cargo";
        }
        
        return $this->render('CetNominaBundle:HistoricoCargo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'titulo' => $titulo,
        ));
    }

    /**
     * Finds and displays a HistoricoCargo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoCargo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoCargo.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:HistoricoCargo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing HistoricoCargo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoCargo')->find($id);
        $encargado=$entity->getEsEncargado();

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoCargo.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);
        
        if ($encargado=='t'){
            $titulo='Editar datos de encargaduria.';
        }else{
            $titulo='Editar información de cargo.';
        }
        
        
        return $this->render('CetNominaBundle:HistoricoCargo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'titulo' => $titulo,
        ));
        
    }

    /**
    * Creates a form to edit a HistoricoCargo entity.
    *
    * @param HistoricoCargo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(HistoricoCargo $entity)
    {   
        
        $encargado=$entity->getEsEncargado();
        if ($encargado=='t'){
            $form = $this->createForm(new PersonalHistoricoCargoEditEncargadoType(), $entity, array(
                'action' => $this->generateUrl('personalhistoricocargo_update', array('id' => $entity->getId())),
                'method' => 'PUT', 
            ));
        }else{
            $form = $this->createForm(new PersonalHistoricoCargoEditFijoType(), $entity, array(
                'action' => $this->generateUrl('personalhistoricocargo_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));
        }
        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing HistoricoCargo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoCargo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoCargo.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            //return $this->redirect($this->generateUrl('historicocargo_show', array('id' => $id)));
            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkInformacionLaboralHasCargoInformacionLaboral1()->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:HistoricoCargo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a HistoricoCargo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:HistoricoCargo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoCargo.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        //return $this->redirect($this->generateUrl('personal'));
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkInformacionLaboralHasCargoInformacionLaboral1()->getPersonal()->getId())));
    }

    /**
     * Creates a form to delete a HistoricoCargo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalhistoricocargo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
