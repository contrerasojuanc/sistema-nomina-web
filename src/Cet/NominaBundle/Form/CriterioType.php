<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CriterioType extends AbstractType
{
    public $entidad_array = array();
    
    public function __construct(Array $entidad_array = null)
    {
        $this->entidad_array = $entidad_array;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\Reporte");
        $userTransformer->setEntityRepository("CetNominaBundle:Reporte");
        $userTransformer->setEntityType("Reporte");
        
        $builder
            ->add('nombre',null,array(
                'required' => true,
            ))
            ->add('valor')
            ->add('comentario')
//            ->add('tipo')
            ->add('tipo', 'choice', array(
                'mapped'        => true,
                'auto_initialize'=> false,
                'choices'       => $this->entidad_array,
                'required' => false,
                'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione la Entidad')
            ))  
//            ->add($builder->create('reporte','text')->addModelTransformer($userTransformer))
            ->add($builder->create('reporte','hidden',array(
                'required'  => false,
            ))->addModelTransformer($userTransformer))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Criterio'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_criterio';
    }
}
