<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cet\NominaBundle\Entity\TipoBanco
 *
 * @ORM\Entity(repositoryClass="TipoBancoRepository")
 * @ORM\Table(name="sis_nomina.tipo_banco")
 */
class TipoBanco
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="PlantillaNomina", mappedBy="tipoBanco")
     * @ORM\JoinColumn(name="plantilla_id", referencedColumnName="id", nullable=true)
     */
    protected $plantillaNominas;

    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plantillaNominas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoBanco
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add plantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas
     * @return TipoBanco
     */
    public function addPlantillaNomina(\Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas)
    {
        $this->plantillaNominas[] = $plantillaNominas;

        return $this;
    }

    /**
     * Remove plantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas
     */
    public function removePlantillaNomina(\Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas)
    {
        $this->plantillaNominas->removeElement($plantillaNominas);
    }

    /**
     * Get plantillaNominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlantillaNominas()
    {
        return $this->plantillaNominas;
    }
}
