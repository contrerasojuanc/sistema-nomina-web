<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Vehiculo;
use Cet\NominaBundle\Form\PersonalVehiculoType;

/**
 * Vehiculo controller.
 *
 */
class PersonalVehiculoController extends Controller
{

    /**
     * Lists all Vehiculo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Vehiculo')->findAll();

        return $this->render('CetNominaBundle:Vehiculo:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Vehiculo entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new Vehiculo();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkVehiculoPersonal1($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkVehiculoPersonal1()->getId())));
        }

        return $this->render('CetNominaBundle:Vehiculo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Vehiculo entity.
    *
    * @param Vehiculo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Vehiculo $entity,$id)
    {
        $form = $this->createForm(new PersonalVehiculoType(), $entity, array(
            'action' => $this->generateUrl('personalvehiculo_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Vehiculo entity.
     *
     */
    public function newAction($id)
    {
        $entity = new Vehiculo();
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkVehiculoPersonal1($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:Vehiculo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Vehiculo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vehiculo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vehiculo.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Vehiculo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Vehiculo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vehiculo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vehiculo.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Vehiculo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Vehiculo entity.
    *
    * @param Vehiculo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Vehiculo $entity)
    {
        $form = $this->createForm(new PersonalVehiculoType(), $entity, array(
            'action' => $this->generateUrl('personalvehiculo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing Vehiculo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Vehiculo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Vehiculo.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkVehiculoPersonal1()->getId())));
        }

        return $this->render('CetNominaBundle:Vehiculo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Vehiculo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Vehiculo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Vehiculo.');
            }

            $em->remove($entity);
            $em->flush();
//        }
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkVehiculoPersonal1()->getId())));
        //return $this->redirect($this->generateUrl('personal'));
    }

    /**
     * Creates a form to delete a Vehiculo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalvehiculo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
