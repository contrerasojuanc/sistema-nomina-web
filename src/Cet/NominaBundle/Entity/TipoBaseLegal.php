<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\TipoBaseLegal
 *
 * @ORM\Entity(repositoryClass="TipoBaseLegalRepository")
 * @ORM\Table(name="sis_nomina.tipo_base_legal")
 */
class TipoBaseLegal
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="tipo", type="text", nullable=true)
     */
    private $tipo;

    /**
     * @ORM\OneToMany(targetEntity="BaseLegal", mappedBy="fk_base_legal_tipo_base_legal1")
     * @ORM\JoinColumn(name="tipo_base_legal_id", referencedColumnName="id", nullable=false)
     */
    protected $baseLegals;

    public function __construct()
    {
        $this->baseLegals = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return TipoBaseLegal
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add baseLegals
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegals
     * @return TipoBaseLegal
     */
    public function addBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegals)
    {
        $this->baseLegals[] = $baseLegals;

        return $this;
    }

    /**
     * Remove baseLegals
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegals
     */
    public function removeBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegals)
    {
        $this->baseLegals->removeElement($baseLegals);
    }

    /**
     * Get baseLegals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBaseLegals()
    {
        return $this->baseLegals;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('tipo', new Assert\NotBlank())
                ->addPropertyConstraint ('tipo', new Assert\Type(array('type'=>'alpha')))
             
            ;
    }
    
    public function __toString()
    {
        return $this->getTipo();
    }
}
