<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Vacaciones
 *
 * @ORM\Entity(repositoryClass="VacacionesRepository")
 * @ORM\Table(name="sis_nomina.vacaciones")
 */
class Vacaciones
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="periodo", type="text", nullable=true)
     */
    private $periodo;

    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;
    
    /**
     * @var float
     * @ORM\Column(name="bono", type="float", nullable=true)
     */
    private $bono;
    
    /**
     * @var float
     * @ORM\Column(name="bono_total", type="float", nullable=true)
     */
    private $bonoTotal;
    
    /**
     * @var integer
     * @ORM\Column(name="dias_disfrute", type="integer", nullable=true)
     */
    private $diasDisfrute;
    
    /**
     * @var integer
     * @ORM\Column(name="horas_disfrute", type="integer", nullable=true)
     */
    private $horasDisfrute;
    
    /**
     * @var integer
     * @ORM\Column(name="minutos_disfrute", type="integer", nullable=true)
     */
    private $minutosDisfrute;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="vacaciones")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula")
     */
    protected $personal;
    
    /**
     * @ORM\OneToOne(targetEntity="Respaldo", mappedBy="bonoVacacional")
     */
    protected $respaldos;
    
    /**
     * @ORM\ManyToOne(targetEntity="Nomina", inversedBy="vacaciones")
     * @ORM\JoinColumn(name="nomina_id", referencedColumnName="id", nullable=true)
     */
    protected $fk_vacaciones;

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set periodo
     *
     * @param string $periodo
     * @return Vacacion
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return string 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Vacacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return Vacacion
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    
    /**
     * Set bono
     *
     * @param float $bono
     * @return Vacaciones
     */
    public function setBono($bono)
    {
        $this->bono = $bono;

        return $this;
    }

    /**
     * Get bono
     *
     * @return float 
     */
    public function getBono()
    {
        return $this->bono;
    }

    /**
     * Set diasDisfrute
     *
     * @param integer $diasDisfrute
     * @return Vacaciones
     */
    public function setDiasDisfrute($diasDisfrute)
    {
        $this->diasDisfrute = $diasDisfrute;

        return $this;
    }

    /**
     * Get diasDisfrute
     *
     * @return integer 
     */
    public function getDiasDisfrute()
    {
        return $this->diasDisfrute;
    }

    /**
     * Set horasDisfrute
     *
     * @param integer $horasDisfrute
     * @return Vacaciones
     */
    public function setHorasDisfrute($horasDisfrute)
    {
        $this->horasDisfrute = $horasDisfrute;

        return $this;
    }

    /**
     * Get horasDisfrute
     *
     * @return integer 
     */
    public function getHorasDisfrute()
    {
        return $this->horasDisfrute;
    }

    /**
     * Set minutosDisfrute
     *
     * @param integer $minutosDisfrute
     * @return Vacaciones
     */
    public function setMinutosDisfrute($minutosDisfrute)
    {
        $this->minutosDisfrute = $minutosDisfrute;

        return $this;
    }

    /**
     * Get minutosDisfrute
     *
     * @return integer 
     */
    public function getMinutosDisfrute()
    {
        return $this->minutosDisfrute;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
       $metadata->addPropertyConstraint('fecha', new Assert\NotBlank())
                ->addPropertyConstraint('periodo', new Assert\NotBlank())
            ;
        
    }
    

    /**
     * Set bonoTotal
     *
     * @param float $bonoTotal
     * @return Vacaciones
     */
    public function setBonoTotal($bonoTotal)
    {
        $this->bonoTotal = $bonoTotal;

        return $this;
    }

    /**
     * Get bonoTotal
     *
     * @return float 
     */
    public function getBonoTotal()
    {
        return $this->bonoTotal;
    }

    /**
     * Set respaldos
     *
     * @param \Cet\NominaBundle\Entity\Respaldo $respaldos
     * @return Vacaciones
     */
    public function setRespaldos(\Cet\NominaBundle\Entity\Respaldo $respaldos = null)
    {
        $this->respaldos = $respaldos;

        return $this;
    }

    /**
     * Get respaldos
     *
     * @return \Cet\NominaBundle\Entity\Respaldo 
     */
    public function getRespaldos()
    {
        return $this->respaldos;
    }

    /**
     * Set fk_vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Nomina $fkVacaciones
     * @return Vacaciones
     */
    public function setFkVacaciones(\Cet\NominaBundle\Entity\Nomina $fkVacaciones = null)
    {
        $this->fk_vacaciones = $fkVacaciones;

        return $this;
    }

    /**
     * Get fk_vacaciones
     *
     * @return \Cet\NominaBundle\Entity\Nomina 
     */
    public function getFkVacaciones()
    {
        return $this->fk_vacaciones;
    }
    
    public function __toString()
    {
        return $this->getPeriodo().": ". $this->getPersonal()->getPrimerApellido()." ".$this->getPersonal()->getPrimerNombre();
    }
}
