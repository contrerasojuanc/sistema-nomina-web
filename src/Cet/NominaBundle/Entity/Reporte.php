<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection; 

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Reporte
 *
 * @ORM\Table(name="sis_nomina.reporte")
 * @ORM\Entity
 */
class Reporte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="dql", type="text", nullable=true)
     */
    private $dql;

    /**
     * @ORM\OneToMany(targetEntity="Criterio", mappedBy="reporte", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reporte_id", referencedColumnName="id")
     * })
     */
    private $criterios;
    
    public function __construct()
    {
        $this->criterios = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Reporte
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Reporte
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set dql
     *
     * @param string $dql
     * @return Reporte
     */
    public function setDql($dql)
    {
        $this->dql = $dql;

        return $this;
    }

    /**
     * Get dql
     *
     * @return string 
     */
    public function getDql()
    {
        return $this->dql;
    }

    public function setCriterio(Collection $registros)
    {
        $this->criterios = $registros;
        foreach ($registros as $registro) {
            $registro->setReporte($this);
        }
    }
    
    /**
     * Add criterios
     *
     * @param \Cet\NominaBundle\Entity\Criterio $criterios
     * @return Reporte
     */
    public function addCriterio(\Cet\NominaBundle\Entity\Criterio $criterios)
    {
//        $this->criterios[] = $criterios;
//
//        return $this;
        
            $criterios->setReporte($this);

            $this->criterios->add($criterios);
    }

    /**
     * Remove criterios
     *
     * @param \Cet\NominaBundle\Entity\Criterio $criterios
     */
    public function removeCriterio(\Cet\NominaBundle\Entity\Criterio $criterios)
    {
        $this->criterios->removeElement($criterios);
    }

    /**
     * Get criterios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCriterios()
    {
        return $this->criterios;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */       
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('descripcion', new Assert\NotBlank())
                ->addPropertyConstraint ('dql', new Assert\Regex(array(
                        'pattern' => '/\binsert\b|\bdelete\b|\bupdate\b|\bcreate\b|\balter\b;/i', 
                        'match'   => false,
                        'message' => 'El campo dql no puede contener sintaxis de modificación de base de datos',
                    )))        
             ;          
    } 
    
    public function __toString()
    {
        return $this->getDescripcion();
    }
}
