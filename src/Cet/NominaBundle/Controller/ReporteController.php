<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Reporte;
use Cet\NominaBundle\Form\ReporteType;
use Cet\NominaBundle\Form\ReporteCriterioType;
use Cet\NominaBundle\Form\ReporteCriteriosType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Reporte controller.
 *
 */
class ReporteController extends Controller
{

    /**
     * Lists all Reporte entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Reporte')->findAll();

        return $this->render('CetNominaBundle:Reporte:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Reporte entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Reporte();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('reporte_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:Reporte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Reporte entity.
    *
    * @param Reporte $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Reporte $entity)
    {
        $helper = $this->get('cet.nomina.globales');
        $matriz = $helper->llenar_matriz();
        $matriz = array_combine(array_keys($matriz),array_keys($matriz));
        
        $form = $this->createForm(new ReporteType($matriz), $entity, array(
            'action' => $this->generateUrl('reporte_create'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager()
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Reporte entity.
     *
     */
    public function newAction()
    {
        $entity = new Reporte();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:Reporte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Reporte entity.
     *
     */
    public function showAction($id, $bandera = null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Reporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Reporte.');
        }

        $deleteForm = $this->createDeleteForm($id);

        /////En caso de reporte con criterios
        $criterios = $entity->getCriterios();
        
//        $linea = "REPORTE: ".$entity->getDescripcion()."\\n";
        $linea = $entity->getDescripcion();
        
        /////En caso de reporte sin criterios
        if(count($criterios) == 0 || $criterios == null){            
            //Se hace la consulta del DQL almacenado en el reporte
            $reporte_dql = $entity->getDql();
            $consulta = $em->createQuery($reporte_dql);        
    //        $consulta->setParameter('personal_cedula', $idpersonal);
            $datos = $consulta->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

            $columnas = "";
            if(isset($datos[0]))
                $columnas = array_keys($datos[0]);

//            Agregar fila final de totales
            $tamanio = count($datos);
            $i = 0;        
            foreach($columnas as $columna){  
               $datos[$tamanio][$columna] = ""; 
            }
            foreach($columnas as $columna){                
                $suma = 0;
                $cuenta = -1;
                foreach($datos as $dato){                    
                    if(is_numeric($dato[$columna]))
                        $suma += $dato[$columna];                    
                    else
                        $cuenta++;
                }
                if($i == 0)
                    $datos[$tamanio][$columna] = "TOTALES";
                else
                    $datos[$tamanio][$columna] = $suma==0?$cuenta:$suma; 
                $i++;
            }
            
            return $this->render('CetNominaBundle:Reporte:reporte.html.twig', array(
                'entities' => $datos,
                'entity' => $entity,
                'columnas' => $columnas,
                'cadena' => $linea,
            ));
        }
        
        if($criterios !== null && $bandera === null){
                $form = $this->createForm(new ReporteCriterioType(array()), $entity, array(
                    'action' => $this->generateUrl('reportecriterio_update', array('id' => $entity->getId(),'bandera' => true )),
                    'method' => 'PUT',
                    'em' => $em
                ));

                $form->add('submit', 'submit', array('label' => 'Actualizar'))
                     ->add('reset', 'reset', array('label' => 'Limpiar'));

                return $this->render('CetNominaBundle:Reporte:criterio.html.twig', array(
                    'entity'      => $entity,
                    'form'   => $form->createView(),
                ));
        }
        
        /////En caso de reporte con  criterios
        if($criterios !== null && $bandera !== null){            
            //Se hace la consulta del DQL almacenado en el reporte
            $reporte_dql = $entity->getDql();
            $reporte_dql = preg_replace('/ñ/i','n',$reporte_dql);
            $consulta = $em->createQuery($reporte_dql);        
      
            $vector_criterios = array();
            foreach($criterios as $criterio){
                $sinEspecial = preg_replace('/ñ/i','n',$criterio->getNombre());
                $vector_criterios[$sinEspecial] = $criterio->getValor();
                // consultar la entidad  asociada al tipo de parametro: ejem si es tipo de nomina  buscar la entidad tipo de nomina
                if($criterio->gettipo()!==null){
                    $entityTipo = $em->getRepository($criterio->gettipo())->find($criterio->getValor());
                    $valor="";
                    if ($entityTipo){
                        $valor=$entityTipo->__toString();
                    }
                    $vector_titulos[$criterio->getNombre()] = $valor;
                }
                else{
                    $vector_titulos[$criterio->getNombre()] = $criterio->getValor();
                }
                
//                $linea .=  $criterio->getNombre()." = ".$vector_titulos[$criterio->getNombre()]."\\n";
                $linea .=  " ".$criterio->getNombre()." = ".$vector_titulos[$criterio->getNombre()];
            }
            
            $consulta->setParameters($vector_criterios);
            
            $datos = "";
            try {
                $datos = $consulta->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
            } catch (\Exception $exc) {
//                echo $exc->getTraceAsString();
                throw $exc;
            }
            
            $columnas = array();
            if(isset($datos[0]))
                $columnas = array_keys($datos[0]);

//            Agregar fila final de totales
            $tamanio = count($datos);
            $i = 0;        
            foreach($columnas as $columna){  
               $datos[$tamanio][$columna] = ""; 
            }
            foreach($columnas as $columna){                
                $suma = 0;
                $cuenta = -1;
                foreach($datos as $dato){                    
                    if(is_numeric($dato[$columna]))
                        $suma += $dato[$columna];                    
                    else
                        $cuenta++;
                }
                if($i == 0)
                    $datos[$tamanio][$columna] = "TOTALES";
                else
                    $datos[$tamanio][$columna] = $suma==0?$cuenta:$suma; 
                $i++;
            }
            
            return $this->render('CetNominaBundle:Reporte:reporte.html.twig', array(
                'entities' => $datos,
                'entity' => $entity,
                'columnas' => $columnas,
                'criterios' => $criterios,
                'titulos' => $vector_titulos,
                'cadena' => $linea,
            ));
        }
//        return $this->render('CetNominaBundle:Reporte:show.html.twig', array(
//            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Reporte entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Reporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Reporte.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Reporte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Reporte entity.
    *
    * @param Reporte $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Reporte $entity)
    {
        $helper = $this->get('cet.nomina.globales');
        $matriz = $helper->llenar_matriz();
        $matriz = array_combine(array_keys($matriz),array_keys($matriz));
        
        $form = $this->createForm(new ReporteType($matriz), $entity, array(
            'action' => $this->generateUrl('reporte_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $this->getDoctrine()->getManager()
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing Reporte entity.
     *
     */
    public function updateAction(Request $request, $id, $bandera = null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Reporte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Reporte.');
        }

        $originalTags = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($entity->getCriterios() as $tag) {
            $originalTags->add($tag);
        }
        
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // remove the relationship between the tag and the Task
            foreach ($originalTags as $tag) {
                if (false === $entity->getCriterios()->contains($tag)) {
                    // remove the Task from the Tag
//                    $tag->getIdUsuario()->removeElement($entity);

                    // if it was a many-to-one relationship, remove the relationship like this
                     $tag->setReporte(null);

//                    $em->persist($tag);

                    // if you wanted to delete the Tag entirely, you can also do that
                     $em->remove($tag);
                }
            }
            
            $em->persist($entity);
            $em->flush();

            if(null === $bandera)
                return $this->redirect($this->generateUrl('reporte_show', array('id' => $id)));
            else
                return $this->redirect($this->generateUrl('reportecriterio_show', array('id' => $id, 'bandera' => $bandera )));
        }

        return $this->render('CetNominaBundle:Reporte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Reporte entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Reporte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Reporte.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('reporte'));
    }

    /**
     * Creates a form to delete a Reporte entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reporte_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
