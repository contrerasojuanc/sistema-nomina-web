<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\InformacionMedica;
use Cet\NominaBundle\Form\PersonalInformacionMedicaType;

/**
 * InformacionMedica controller.
 *
 */
class PersonalInformacionMedicaController extends Controller
{

    /**
     * Lists all InformacionMedica entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:InformacionMedica')->findAll();

        return $this->render('CetNominaBundle:InformacionMedica:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new InformacionMedica entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new InformacionMedica();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setPersonal($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:InformacionMedica:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a InformacionMedica entity.
    *
    * @param InformacionMedica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(InformacionMedica $entity,$id)
    {
        $form = $this->createForm(new PersonalInformacionMedicaType(), $entity, array(
            'action' => $this->generateUrl('personalinformacionmedica_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new InformacionMedica entity.
     *
     */
    public function newAction($id)
    {
        $entity = new InformacionMedica();
        $em = $this->getDoctrine()->getManager();
        
        $buscarinformacionmedica = $em->getRepository('CetNominaBundle:InformacionMedica')->findOneBy(array('personal' => $id));
        if ($buscarinformacionmedica) {
            return $this->redirect($this->generateUrl('personalinformacionmedica_edit', array('id' => $buscarinformacionmedica->getId())));
        }
        
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setPersonal($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:InformacionMedica:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a InformacionMedica entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionMedica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionMedica.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionMedica:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing InformacionMedica entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionMedica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionMedica.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionMedica:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a InformacionMedica entity.
    *
    * @param InformacionMedica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(InformacionMedica $entity)
    {
        $form = $this->createForm(new PersonalInformacionMedicaType(), $entity, array(
            'action' => $this->generateUrl('personalinformacionmedica_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing InformacionMedica entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionMedica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionMedica.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:InformacionMedica:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a InformacionMedica entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:InformacionMedica')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionMedica.');
            }
            // cascade{"remove"} en la entidad
//            foreach ($entity->getEnfermedads() AS $enfermedades) {
//                $em->remove($enfermedades);
//            }
            
            $em->remove($entity);
            $em->flush();
//        }
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));    
        //return $this->redirect($this->generateUrl('personal'));
    }

    /**
     * Creates a form to delete a InformacionMedica entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalinformacionmedica_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
