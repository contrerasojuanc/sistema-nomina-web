<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\CargaMasiva
 *
 * @ORM\Entity(repositoryClass="CargaMasivaRepository")
 * @ORM\Table(name="sis_nomina.carga_masiva", indexes={@ORM\Index(name="fk_concepto1_idx", columns={"concepto_id"})})
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields={"nombre","nombretxt"},
 *     message="Un archivo TXT con el mismo nombre ya fue cargado o ya existe un registro de carga masiva con esa descripción, verifiqué"
 * )
 */
class CargaMasiva
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * Solo valores:
     * M = Cargado
     * P = Procesado
     * E = Procesado con errores
     *
     * @var string
     * @ORM\Column(name="estado", type="string", length=1, nullable=true)
     */
    private $estado;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_carga", type="date", nullable=true)
     */
    private $fechaCarga;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_proceso", type="date", nullable=true)
     */
    private $fechaProceso;
    
    /**
     * @var string
     * @ORM\Column(name="ruta_txt", type="text", nullable=true)
     */
    private $rutatxt;
    
    /**
     * @var string
     * @ORM\Column(name="nombretxt", type="text", nullable=true)
     */
    private $nombretxt;
    
    /**
     * @ORM\OneToMany(targetEntity="DatosGenericos", mappedBy="fk_datosGenericos_cargaMasiva1")
     * @ORM\JoinColumn(name="carga_masiva", referencedColumnName="id", nullable=false)
     */
    protected $datosGenericos;
    
    /**
     * @ORM\OneToMany(targetEntity="DatosVariables", mappedBy="fk_datosVariables_cargaMasiva1")
     * @ORM\JoinColumn(name="carga_masiva", referencedColumnName="id", nullable=false)
     */
    protected $datosVariables;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoDatoGenerico", inversedBy="cargaMasiva")
     * @ORM\JoinColumn(name="tipo_dato_generico_id", referencedColumnName="id", nullable=true)
     */
    protected $fk_dato_generico_tipo_dato_generico1;
    
    /**
     * Solo valores:
     * G = Datos Genericos
     * V = Datos Variables
     *
     * @var string
     * @ORM\Column(name="tipo_carga", type="string", length=1, nullable=true)
     */
    private $tipocarga;
    
    /**
     * @ORM\ManyToOne(targetEntity="Concepto", inversedBy="cargaMasiva")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=true)
     */
    protected $fk_concepto1;
    
    /**
     * @var string
     * @ORM\Column(name="modo_afectacion", type="text", nullable=true)
     */
    private $modoAfectacion;

    /**
     * @var string
     * @ORM\Column(name="seccion_afectacion", type="text", nullable=true)
     */
    private $seccionAfectacion;
    
    public $archivo;
    public $archivoanterior;
    
    public function __construct()
    {
        $this->datosGenericos = new ArrayCollection();
        $this->datosVariables = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return CargaMasiva
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return CargaMasiva
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaCarga
     *
     * @param \DateTime $fechaCarga
     * @return CargaMasiva
     */
    public function setFechaCarga($fechaCarga)
    {   
        $this->fechaCarga = $fechaCarga;

        return $this;
    }

    /**
     * Get fechaCarga
     *
     * @return \DateTime 
     */
    public function getFechaCarga()
    {
        return $this->fechaCarga;
    }

    /**
     * Set fechaProceso
     *
     * @param \DateTime $fechaProceso
     * @return CargaMasiva
     */
    public function setFechaProceso($fechaProceso)
    {
        $this->fechaProceso = $fechaProceso;

        return $this;
    }

    /**
     * Get fechaProceso
     *
     * @return \DateTime 
     */
    public function getFechaProceso()
    {
        return $this->fechaProceso;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setRutaTxt(UploadedFile $file = null)
    {
        if (!empty($file)){
            $this->rutatxt = $this->getUploadDir().'/'.$file->getClientOriginalName();
            $this->archivo = $file;
        }
        
    }
    
    /**
     * Get rutatxt
     *
     * @return string 
     */
    public function getRutaTxt()
    {
            return $this->rutatxt;
    }
    
    /**
     * Get archivo
     *
     * @return string 
     */
    public function getArchivo()
    {
            return $this->archivo;
    }
    
    /**
     * Get archivo
     *
     * @return string 
     */
    public function getArchivoAnterior()
    {
            return $this->archivoanterior;
    }

    /**
     * Set nombretxt
     *
     * @param string $nombretxt
     * @return CargaMasiva
     */
    public function setNombreTxt($nombretxt)
    {
        $this->nombretxt = $nombretxt;

        return $this;
    }

    /**
     * Get nombretxt
     *
     * @return string 
     */
    public function getNombreTxt()
    {
        return $this->nombretxt;
    }
    
    /**
     * Add datosGenericos
     *
     * @param \Cet\NominaBundle\Entity\DatosGenericos $datosGenericos
     * @return CargaMasiva
     */
    public function addDatosGenericos(\Cet\NominaBundle\Entity\DatosGenericos $datosGenericos)
    {
        $this->datosGenericos[] = $datosGenericos;

        return $this;
    }

    /**
     * Remove datosGenericos
     *
     * @param \Cet\NominaBundle\Entity\DatosGenericos $datosGenericos
     */
    public function removeDatosGenericos(\Cet\NominaBundle\Entity\DatosGenericos $datosGenericos)
    {
        $this->datosGenericos->removeElement($datosGenericos);
    }

    /**
     * Get datosGenericos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatosGenericos()
    {
        return $this->datosGenericos;
    }
    
    /**
     * Add datosVariables
     *
     * @param \Cet\NominaBundle\Entity\DatosVariables $datosVariables
     * @return CargaMasiva
     */
    public function addDatosVariables(\Cet\NominaBundle\Entity\DatosVariables $datosVariables)
    {
        $this->datosVariables[] = $datosVariables;

        return $this;
    }

    /**
     * Remove datosVariables
     *
     * @param \Cet\NominaBundle\Entity\DatosVariables $datosVariables
     */
    public function removeDatosVariables(\Cet\NominaBundle\Entity\DatosVariables $datosVariables)
    {
        $this->datosVariables->removeElement($datosVariables);
    }

    /**
     * Get datosVariables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatosVariables()
    {
        return $this->datosVariables;
    }
    
    /**
     * Set TipoDatoGenerico entity (many to one).
     *
     * @param TipoDatoGenerico $tipoDatoGenerico
     * @return \Entity\CargaMasiva
     */
    public function setFkDatoGenericoTipoDatoGenerico1(TipoDatoGenerico $tipoDatoGenerico)
    {
        $this->fk_dato_generico_tipo_dato_generico1 = $tipoDatoGenerico;

        return $this;
    }

    /**
     * Get TipoDatoGenerico entity (many to one).
     *
     * @return TipoDatoGenerico
     */
    public function getFkDatoGenericoTipoDatoGenerico1()
    {
        return $this->fk_dato_generico_tipo_dato_generico1;
    }
    
    /**
     * Set tipoCarga
     *
     * @param string $tipoCarga
     * @return CargaMasiva
     */
    public function setTipoCarga($tipoCarga)
    {
        $this->tipocarga = $tipoCarga;

        return $this;
    }

    /**
     * Get tipoCarga
     *
     * @return string 
     */
    public function getTipoCarga()
    {
        return $this->tipocarga;
    }
    
    /**
     * Set fk_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkConcepto1
     * @return CargaMasiva
     */
    public function setFkConcepto1(\Cet\NominaBundle\Entity\Concepto $fkConcepto1)
    {
        $this->fk_concepto1 = $fkConcepto1;

        return $this;
    }

    /**
     * Get fk_concepto1
     *
     * @return \Cet\NominaBundle\Entity\Concepto 
     */
    public function getFkConcepto1()
    {
        return $this->fk_concepto1;
    }
    
    /**
     * Set modoAfectacion
     *
     * @param string $modoAfectacion
     * @return CargaMasiva
     */
    public function setModoAfectacion($modoAfectacion)
    {
        $this->modoAfectacion = $modoAfectacion;

        return $this;
    }

    /**
     * Get modoAfectacion
     *
     * @return string 
     */
    public function getModoAfectacion()
    {
        return $this->modoAfectacion;
    }

    /**
     * Set seccionAfectacion
     *
     * @param string $seccionAfectacion
     * @return CargaMasiva
     */
    public function setSeccionAfectacion($seccionAfectacion)
    {
        $this->seccionAfectacion = $seccionAfectacion;

        return $this;
    }

    /**
     * Get seccionAfectacion
     *
     * @return string 
     */
    public function getSeccionAfectacion()
    {
        return $this->seccionAfectacion;
    }
    
    public function getAbsolutePath()//en desuso
    {
        return null === $this->nombretxt
            ? null
            : $this->getUploadRootDir().'/'.$this->id.'.'.$this->nombretxt;
    }

    public function getWebPath()//en desuso
    {
        return null === $this->nombretxt
            ? null
            : $this->getUploadDir().'/'.$this->nombretxt;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/cargamasiva';
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()//
    {
        if (null !== $this->getArchivo()) {
            //obtener el nombre del archivo a eliminar antes de setear el nuevo
            $this->archivoanterior=$this->getNombreTxt();
            //nombre de nuevo archivo
            $this->nombretxt = $this->getArchivo()->getClientOriginalName();
            $fecha=date('Y-m-d');
            $this->fechaCarga=new \DateTime($fecha);
            $this->estado='C';
        }
    }
  

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getArchivo()) {
            return;
        }
        
        //remover archivo anterior si lo hay
        $archivoanterior=$this->getArchivoAnterior();
        if (null !== $archivoanterior) {
            $this->removerArchivo($archivoanterior);
        }
        
        
        
        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
//        try{
            $archivo = $this->getArchivo();
            $archivo->move($this->getUploadRootDir(), $archivo->getClientOriginalName());
            
//        }catch(\Exception $e){
//            echo $e;
//        }
        // check if we have an old image
//        if (isset($this->temp)) {
//            // delete the old image
//            unlink($this->getUploadRootDir().'/'.$this->temp);
//            // clear the temp image path
//            $this->temp = null;
//        }
//        $this->rutatxt = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()//se ejecuta con el boton eliminar del formulario
    {
        $archivoremover=$this->getNombreTxt();
        $this->removerArchivo($archivoremover);
    }
    
    public function removerArchivo($archivoremover)
    {
        $file_path = $this->getUploadRootDir().'/'.$archivoremover;
        if(file_exists($file_path)) unlink($file_path);
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
//        $metadata->addPropertyConstraint('gender', new Choice(array(
//            'choices' => array('male', 'female'),
//            'message' => 'Choose a valid gender.',
//        )));
       $metadata->addPropertyConstraint('nombre', new Assert\NotBlank())
       ->addPropertyConstraint('nombre', new Assert\Type(array('type'=>'string','message'=>'El campo debe ser solo texto.')))
        ;
        
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
    
}
