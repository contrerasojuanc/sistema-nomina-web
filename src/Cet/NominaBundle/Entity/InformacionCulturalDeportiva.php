<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\InformacionCulturalDeportiva
 *
 * @ORM\Entity(repositoryClass="InformacionCulturalDeportivaRepository")
 * @ORM\Table(name="sis_nomina.informacion_cultural_deportiva", indexes={@ORM\Index(name="fk_informacion_cultural_deportiva_personal1_idx", columns={"personal_cedula"})})
 */
class InformacionCulturalDeportiva
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * Solo valores:
     * D = Deporte
     * C = Actividad Cultural
     *
     * @var string
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * Solo valores:
     * S = Si
     * N = No
     *
     * @var string
     * @ORM\Column(name="representa_institucion", type="string", length=1, nullable=true)
     */
    private $representaInstitucion;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="informacionCulturalDeportivas")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_informacion_cultural_deportiva_personal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return InformacionCulturalDeportiva
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return InformacionCulturalDeportiva
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set representaInstitucion
     *
     * @param string $representaInstitucion
     * @return InformacionCulturalDeportiva
     */
    public function setRepresentaInstitucion($representaInstitucion)
    {
        $this->representaInstitucion = $representaInstitucion;

        return $this;
    }

    /**
     * Get representaInstitucion
     *
     * @return string 
     */
    public function getRepresentaInstitucion()
    {
        return $this->representaInstitucion;
    }

    /**
     * Set fk_informacion_cultural_deportiva_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkInformacionCulturalDeportivaPersonal1
     * @return InformacionCulturalDeportiva
     */
    public function setFkInformacionCulturalDeportivaPersonal1(\Cet\NominaBundle\Entity\Personal $fkInformacionCulturalDeportivaPersonal1)
    {
        $this->fk_informacion_cultural_deportiva_personal1 = $fkInformacionCulturalDeportivaPersonal1;

        return $this;
    }

    /**
     * Get fk_informacion_cultural_deportiva_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkInformacionCulturalDeportivaPersonal1()
    {
        return $this->fk_informacion_cultural_deportiva_personal1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('nombre', new Assert\NotBlank())
                ->addPropertyConstraint ('nombre', new Assert\Type(array('type'=>'string','message'=>'El campo debe ser solo texto.')))
                            
                ->addPropertyConstraint ('tipo', new Assert\NotBlank())
             
                ->addPropertyConstraint ('representaInstitucion', new Assert\NotBlank())
              
            ;
    }
}
