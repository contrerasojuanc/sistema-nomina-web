<?php
 
namespace Cet\NominaBundle\Form\Subscribers;
 
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;
use Cet\NominaBundle\Entity\Municipio;
 
class ParroquiaSubscriber implements EventSubscriberInterface
{
    private $factory;
 
    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }
 
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }
 
    private function addParroquiaForm($form, $parroquia, $municipio)
    {
        $form->add($this->factory->createNamed('fk_vivienda_parroquia1','entity', $parroquia, array(
            'label'         => 'Parroquia',
            'class'         => 'CetNominaBundle:Parroquia',
            'empty_value'   => 'Seleccione la Parroquia',            
            'mapped'        => true,
            'auto_initialize'=> false,
            'label'=> 'Parroquia',
            'attr' => array('help'=>'Seleccione la parroquia donde se encuentra su lugar de residencia o vivienda. Ejemplo: Andrés Bello, Antonio Rómulo Costa, Ayacucho'),
            'query_builder' => function (EntityRepository $repository) use ($municipio) {                
                $qb = $repository->createQueryBuilder('pa')
                    ->innerJoin('pa.fk_parroquia_municipio1', 'mu');
                if ($municipio instanceof Municipio) {
                    $qb->where('pa.fk_parroquia_municipio1 = :municipio')
                    ->setParameter('municipio', $municipio);
                } elseif (is_numeric($municipio)) {
                    $qb->where('mu.id = :municipio')
                    ->setParameter('municipio', $municipio);
                } else {
                    $qb->where('mu.nombre = :municipio')
                    ->setParameter('municipio', null);
                }            
                return $qb;
            }
        )));
       
//        $form->add('fk_vivienda_parroquia1', null, array('label' => 'Parroquia','attr' => array('help'=>'Inserte la ayuda aquí')));
    }
 
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
 
        if (null === $data) {
            return;
        }
 
//        $province = ($data->city) ? $data->city->getProvince() : null ;
        $parroquia = ($data->getFkViviendaParroquia1()) ? $data->getFkViviendaParroquia1() : null ;
        $municipio = ($parroquia) ? $parroquia->getFkParroquiaMunicipio1() : null ;
        $country = ($municipio) ? $municipio->getFkMunicipioEstado1() : null ;
        
        $this->addParroquiaForm($form, $parroquia, $municipio);
    }
 
    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
 
        if (null === $data) {
            return;
        }
        
        $Parroquia = array_key_exists('Parroquia', $data) ? $data['Parroquia'] : null;
        $Municipio = array_key_exists('Municipio', $data) ? $data['Municipio'] : null;
        $this->addParroquiaForm($form, $Parroquia, $Municipio);
    }
}