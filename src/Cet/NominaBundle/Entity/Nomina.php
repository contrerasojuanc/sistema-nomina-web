<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Nomina
 *
 * @ORM\Entity(repositoryClass="NominaRepository")
 * @ORM\Table(name="sis_nomina.nomina")
 */
class Nomina
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_abono", type="date", nullable=true)
     */
    private $fechaAbono;

    /**
     * @var datetime
     * @ORM\Column(name="desde", type="date", nullable=true)
     */
    private $desde;

    /**
     * @var datetime
     * @ORM\Column(name="hasta", type="date", nullable=true)
     */
    private $hasta;

    /**
     * @var integer
     * @ORM\Column(name="periodo", type="integer", nullable=true)
     */
    private $periodo;

    /**
     * @var integer
     * @ORM\Column(name="anio", type="integer", nullable=true)
     */
    private $anio;
    
    /**
     * @var integer
     * @ORM\Column(name="mes", type="integer", nullable=true)
     */
    private $mes;

    /**
     * @var string
     * @ORM\Column(name="tipo", type="text", nullable=true)
     */
    private $tipo;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoConcepto", mappedBy="fk_historico_concepto_nomina1")
     * @ORM\JoinColumn(name="nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $historicoConceptos;

    /**
     * @ORM\OneToOne(targetEntity="Respaldo", mappedBy="nomina")
     */
    protected $respaldos;
    
    /**
     * @ORM\ManyToOne(targetEntity="PlantillaNomina", inversedBy="nominas")
     * @ORM\JoinColumn(name="plantilla_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_nomina_plantilla_nomina1;
    
    /**
     * @ORM\OneToOne(targetEntity="PrestacionesSocialesGeneral", mappedBy="fk_nomina_id")
     * @ORM\JoinColumn(name="prestacion_id", referencedColumnName="id", nullable=true)
     */
    protected $prestacionesSocialesGeneral;
    
    /**
     * @ORM\OneToMany(targetEntity="Vacaciones", mappedBy="fk_vacaciones", cascade={"persist"})
     * @ORM\JoinColumn(name="nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $vacaciones;


    public function __construct()
    {
        $this->historicoConceptos = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Nomina
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Nomina
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     * @return Nomina
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return integer 
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return Nomina
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Nomina
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add historicoConceptos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos
     * @return Nomina
     */
    public function addHistoricoConcepto(\Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos)
    {
        $this->historicoConceptos[] = $historicoConceptos;

        return $this;
    }

    /**
     * Remove historicoConceptos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos
     */
    public function removeHistoricoConcepto(\Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos)
    {
        $this->historicoConceptos->removeElement($historicoConceptos);
    }

    /**
     * Get historicoConceptos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoConceptos()
    {
        return $this->historicoConceptos;
    }

    /**
     * Set desde
     *
     * @param \DateTime $desde
     * @return Nomina
     */
    public function setDesde($desde)
    {
        $this->desde = $desde;

        return $this;
    }

    /**
     * Get desde
     *
     * @return \DateTime 
     */
    public function getDesde()
    {
        return $this->desde;
    }

    /**
     * Set hasta
     *
     * @param \DateTime $hasta
     * @return Nomina
     */
    public function setHasta($hasta)
    {
        $this->hasta = $hasta;

        return $this;
    }

    /**
     * Get hasta
     *
     * @return \DateTime 
     */
    public function getHasta()
    {
        return $this->hasta;
    }

    /**
     * Set periodo
     *
     * @param integer $periodo
     * @return Nomina
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return integer 
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }
    
    /**
     * Set PlantillaNomina entity (many to one).
     *
     * @param PlantillaNomina $plantillaNomina
     * @return \Entity\Nomina
     */
    public function setFkNominaPlantillaNomina1(PlantillaNomina $plantillaNomina)
    {
        $this->fk_nomina_plantilla_nomina1 = $plantillaNomina;

        return $this;
    }

    /**
     * Get PlantillaNomina entity (many to one).
     *
     * @return PlantillaNomina
     */
    public function getFkNominaPlantillaNomina1()
    {
        return $this->fk_nomina_plantilla_nomina1;
    }
    
    
    /**
     * Set respaldos
     *
     * @param \Cet\NominaBundle\Entity\Respaldo $respaldos
     * @return Nomina
     */
    public function setRespaldos(\Cet\NominaBundle\Entity\Respaldo $respaldos = null)
    {
        $this->respaldos = $respaldos;

        return $this;
    }

    /**
     * Get respaldos
     *
     * @return \Cet\NominaBundle\Entity\Respaldo 
     */
    public function getRespaldos()
    {
        return $this->respaldos;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public function __toString()
    {
        return $this->getId().' - '.$this->getDescripcion();
    }
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('descripcion', new Assert\NotBlank())
//                ->addPropertyConstraint ('descripcion', new Assert\Type(array('type'=>'alpha','message'=>'El campo debe ser solo texto.')))
                ->addPropertyConstraint ('fecha', new Assert\NotBlank())            
                ->addPropertyConstraint ('fecha', new Assert\Date())
               
                ->addPropertyConstraint ('periodo', new Assert\NotBlank())            
                ->addPropertyConstraint ('periodo', new Assert\Type(array('type'=>'integer')))
        

                ->addPropertyConstraint ('anio', new Assert\NotBlank())            
//                ->addPropertyConstraint ('anio', new Assert\Type(array('Type'=>'digit','message'=>'El campo debe ser solo números.')))
//                ->addPropertyConstraint ('anio', new Assert\Length(array(
//                    'min'=> 4,
//                    'max'=> 4,
//                    'minMessage'=> 'El año debe de contener cuatro (4) digitos.',
//                    'maxMessage'=> 'El año debe de contener cuatro (4) digitos.',
//                )))  
//                ->addPropertyConstraint ('tipo', new Assert\NotBlank())
//                ->addPropertyConstraint ('tipo', new Assert\Type(array('type'=>'alpha','message' => 'El campo debe ser solo texto.')))    
             ;
    }

    /**
     * Set fechaAbono
     *
     * @param \DateTime $fechaAbono
     * @return Nomina
     */
    public function setFechaAbono($fechaAbono)
    {
        $this->fechaAbono = $fechaAbono;

        return $this;
    }

    /**
     * Get fechaAbono
     *
     * @return \DateTime 
     */
    public function getFechaAbono()
    {
        return $this->fechaAbono;
    }
    
    /**
     * Set prestacionesSocialesGeneral
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $prestacionesSocialesGeneral
     * @return Nomina
     */
    public function setPrestacionesSocialesGeneral(\Cet\NominaBundle\Entity\PrestacionesSocialesGeneral $prestacionesSocialesGeneral = null)
    {
        $this->prestacionesSocialesGeneral = $prestacionesSocialesGeneral;

        return $this;
    }

    /**
     * Get prestacionesSocialesGeneral
     *
     * @return \Cet\NominaBundle\Entity\PrestacionesSocialesGeneral 
     */
    public function getPrestacionesSocialesGeneral()
    {
        return $this->prestacionesSocialesGeneral;
    }

    /**
     * Add vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $vacaciones
     * @return Nomina
     */
    public function addVacacione(\Cet\NominaBundle\Entity\Vacaciones $vacaciones)
    {
        $this->vacaciones[] = $vacaciones;

        return $this;
    }

    /**
     * Remove vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $vacaciones
     */
    public function removeVacacione(\Cet\NominaBundle\Entity\Vacaciones $vacaciones)
    {
        //$vacaciones->setFkVacaciones(null);
        $this->vacaciones->removeElement($vacaciones);
        
    }

    /**
     * Get vacaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVacaciones()
    {
        return $this->vacaciones;
    }
}
