<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SecuredType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('_username', null, array('label' => 'Usuario','attr' => array('help'=>'Ingrese el nombre de usuario con privilegios de acceso')))    
            ->add('_password', 'password', array('label' => 'Contraseña','attr' => array('help'=>'Ingrese la contraseña relacionada con el usuario')))    
            ->add('submit', 'submit', array('label' => 'Ingresar'))
            ->add('reset', 'reset', array('label' => 'Limpiar'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
//        $resolver->setDefaults(array(
//            'data_class' => 'Cet\NominaBundle\Entity\CorreoPersonal'
//        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_secured';
    }
}
