<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\InformacionLaboral;
use Cet\NominaBundle\Entity\TipoNomina;
use Cet\NominaBundle\Form\PersonalInformacionLaboralType;

class PersonalInformacionLaboralController extends Controller
{
    /**
     * Displays a form to edit an existing InformacionLaboral entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionLaboral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionLaboral.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:InformacionLaboral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    /**
    * Creates a form to edit a InformacionLaboral entity.
    *
    * @param InformacionLaboral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(InformacionLaboral $entity)
    {
        $form = $this->createForm(new PersonalInformacionLaboralType(), $entity, array(
            'action' => $this->generateUrl('personalinformacionlaboral_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing InformacionLaboral entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:InformacionLaboral')->find($id);
        
        $tipo_nomina = new TipoNomina();
        $tipo_nomina = $em->getRepository('CetNominaBundle:TipoNomina')->findOneBy(array('nombre'=>'Egresados'));
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionLaboral.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //En caso de haber sido establecida una condición de egreso (Ojo: Sin importar la fecha)
            //Y Tipo de nómina es diferente a Jubilados y Pensionados
            //Se cambia el tipo de nomina a Ex-Empleados
            if($editForm->getData()->getCondicionEgreso() && !($editForm->getData()->getFkInformacionLaboralTipoNomina1()->getNombre() == 'Jubilados' || $editForm->getData()->getFkInformacionLaboralTipoNomina1()->getNombre() == 'Pensionados') ){
                $entity->setFkInformacionLaboralTipoNomina1($tipo_nomina);
            }            
                
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:InformacionLaboral:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a InformacionLaboral entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:InformacionLaboral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad InformacionLaboral.');
            }
            // cascade{"remove"} en la entidad
//            foreach ($entity->getHistoricoCargos() AS $cargos) {
//                $em->remove($cargos);
//            }
//            
//            foreach ($entity->getHistoricoUnidadOrganizativas() AS $direcciones) {
//                $em->remove($direcciones);
//            }

            $em->remove($entity);
            $em->flush();
//        }

        //return $this->redirect($this->generateUrl('personal'));
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getPersonal()->getId())));
    }

    /**
     * Creates a form to delete a InformacionLaboral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personalinformacionlaboral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    /**
     * Displays a form to create a new InformacionLaboral entity.
     *
     */
    public function newAction($id)
    {   
        $entity = new InformacionLaboral();
        $em = $this->getDoctrine()->getManager();
        
        $buscainformacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $id));
        if ($buscainformacionlaboral) {
            return $this->redirect($this->generateUrl('personalinformacionlaboral_edit', array('id' => $buscainformacionlaboral->getId())));
        }
        
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setPersonal($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);
        
        return $this->render('CetNominaBundle:InformacionLaboral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
    
    /**
    * Creates a form to create a InformacionLaboral entity.
    *
    * @param InformacionLaboral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(InformacionLaboral $entity,$id)
    {
        $form = $this->createForm(new PersonalInformacionLaboralType(), $entity, array(
            'action' => $this->generateUrl('personalinformacionlaboral_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }
    
    /**
     * Creates a new InformacionLaboral entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new InformacionLaboral();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setPersonal($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:InformacionLaboral:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
}
