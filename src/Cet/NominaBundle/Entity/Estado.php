<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Estado
 *
 * @ORM\Entity(repositoryClass="EstadoRepository")
 * @ORM\Table(name="sis_nomina.estado")
 */
class Estado
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Municipio", mappedBy="fk_municipio_estado1")
     * @ORM\JoinColumn(name="estado_id", referencedColumnName="id", nullable=false)
     */
    protected $municipios;

    public function __construct()
    {
        $this->municipios = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Estado
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add municipios
     *
     * @param \Cet\NominaBundle\Entity\Municipio $municipios
     * @return Estado
     */
    public function addMunicipio(\Cet\NominaBundle\Entity\Municipio $municipios)
    {
        $this->municipios[] = $municipios;

        return $this;
    }

    /**
     * Remove municipios
     *
     * @param \Cet\NominaBundle\Entity\Municipio $municipios
     */
    public function removeMunicipio(\Cet\NominaBundle\Entity\Municipio $municipios)
    {
        $this->municipios->removeElement($municipios);
    }

    /**
     * Get municipios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMunicipios()
    {
        return $this->municipios;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('nombre', new Assert\NotBlank())
                ->addPropertyConstraint('nombre', new Assert\Type(array('type' => 'string')))   
    ;      
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
