<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\HistoricoEvento
 *
 * @ORM\Entity(repositoryClass="HistoricoEventoRepository")
 * @ORM\Table(name="sis_nomina.historico_evento")
 */
class HistoricoEvento
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="esquema", type="text", nullable=true)
     */
    private $esquema;

    /**
     * @var string
     * @ORM\Column(name="tabla", type="text", nullable=true)
     */
    private $tabla;

    /**
     * @var string
     * @ORM\Column(name="usuarioconexion", type="text", nullable=true)
     */
    private $usuarioconexion;

    /**
     * @var string
     * @ORM\Column(name="usuarioaplicacion", type="text", nullable=true)
     */
    private $usuarioaplicacion;

    /**
     * @var string
     * @ORM\Column(name="ipconexion", type="text", nullable=true)
     */
    private $ipconexion;

    /**
     * @var string
     * @ORM\Column(name="ipaplicacion", type="text", nullable=true)
     */
    private $ipaplicacion;

    /**
     * @var string
     * @ORM\Column(name="sistema", type="text", nullable=true)
     */
    private $sistema;

    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     * @ORM\Column(name="accion", type="text", nullable=true)
     */
    private $accion;

    /**
     * @var string
     * @ORM\Column(name="datosviejos", type="text", nullable=true)
     */
    private $datosviejos;

    /**
     * @var string
     * @ORM\Column(name="datosnuevos", type="text", nullable=true)
     */
    private $datosnuevos;

    /**
     * @var string
     * @ORM\Column(name="sentencia", type="text", nullable=true)
     */
    private $sentencia;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set esquema
     *
     * @param string $esquema
     * @return HistoricoEvento
     */
    public function setEsquema($esquema)
    {
        $this->esquema = $esquema;

        return $this;
    }

    /**
     * Get esquema
     *
     * @return string 
     */
    public function getEsquema()
    {
        return $this->esquema;
    }

    /**
     * Set tabla
     *
     * @param string $tabla
     * @return HistoricoEvento
     */
    public function setTabla($tabla)
    {
        $this->tabla = $tabla;

        return $this;
    }

    /**
     * Get tabla
     *
     * @return string 
     */
    public function getTabla()
    {
        return $this->tabla;
    }

    /**
     * Set usuarioconexion
     *
     * @param string $usuarioconexion
     * @return HistoricoEvento
     */
    public function setUsuarioconexion($usuarioconexion)
    {
        $this->usuarioconexion = $usuarioconexion;

        return $this;
    }

    /**
     * Get usuarioconexion
     *
     * @return string 
     */
    public function getUsuarioconexion()
    {
        return $this->usuarioconexion;
    }

    /**
     * Set usuarioaplicacion
     *
     * @param string $usuarioaplicacion
     * @return HistoricoEvento
     */
    public function setUsuarioaplicacion($usuarioaplicacion)
    {
        $this->usuarioaplicacion = $usuarioaplicacion;

        return $this;
    }

    /**
     * Get usuarioaplicacion
     *
     * @return string 
     */
    public function getUsuarioaplicacion()
    {
        return $this->usuarioaplicacion;
    }

    /**
     * Set ipconexion
     *
     * @param string $ipconexion
     * @return HistoricoEvento
     */
    public function setIpconexion($ipconexion)
    {
        $this->ipconexion = $ipconexion;

        return $this;
    }

    /**
     * Get ipconexion
     *
     * @return string 
     */
    public function getIpconexion()
    {
        return $this->ipconexion;
    }

    /**
     * Set ipaplicacion
     *
     * @param string $ipaplicacion
     * @return HistoricoEvento
     */
    public function setIpaplicacion($ipaplicacion)
    {
        $this->ipaplicacion = $ipaplicacion;

        return $this;
    }

    /**
     * Get ipaplicacion
     *
     * @return string 
     */
    public function getIpaplicacion()
    {
        return $this->ipaplicacion;
    }

    /**
     * Set sistema
     *
     * @param string $sistema
     * @return HistoricoEvento
     */
    public function setSistema($sistema)
    {
        $this->sistema = $sistema;

        return $this;
    }

    /**
     * Get sistema
     *
     * @return string 
     */
    public function getSistema()
    {
        return $this->sistema;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return HistoricoEvento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set accion
     *
     * @param string $accion
     * @return HistoricoEvento
     */
    public function setAccion($accion)
    {
        $this->accion = $accion;

        return $this;
    }

    /**
     * Get accion
     *
     * @return string 
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Set datosviejos
     *
     * @param string $datosviejos
     * @return HistoricoEvento
     */
    public function setDatosviejos($datosviejos)
    {
        $this->datosviejos = $datosviejos;

        return $this;
    }

    /**
     * Get datosviejos
     *
     * @return string 
     */
    public function getDatosviejos()
    {
        return $this->datosviejos;
    }

    /**
     * Set datosnuevos
     *
     * @param string $datosnuevos
     * @return HistoricoEvento
     */
    public function setDatosnuevos($datosnuevos)
    {
        $this->datosnuevos = $datosnuevos;

        return $this;
    }

    /**
     * Get datosnuevos
     *
     * @return string 
     */
    public function getDatosnuevos()
    {
        return $this->datosnuevos;
    }

    /**
     * Set sentencia
     *
     * @param string $sentencia
     * @return HistoricoEvento
     */
    public function setSentencia($sentencia)
    {
        $this->sentencia = $sentencia;

        return $this;
    }

    /**
     * Get sentencia
     *
     * @return string 
     */
    public function getSentencia()
    {
        return $this->sentencia;
    }
}
