<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Cargo
 *
 * @ORM\Entity(repositoryClass="CargoRepository")
 * @ORM\Table(name="sis_nomina.cargo")
 */
class Cargo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * Solo valores:
     * 0 = Nivel mas alto
     * .
     * .
     * .
     * 9 = Nivel mas bajo
     *
     * @var integer
     * @ORM\Column(name="jerarquia", type="integer", nullable=true)
     */
    private $jerarquia;
    
    /**
     * Solo valores:
     * I = Nivel mas alto
     * .
     * .
     * .
     * VI = Nivel mas bajo
     *
     * @var string
     * @ORM\Column(name="nivel", type="string", length=5, nullable=true)
     */
    private $nivel;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoCargo", mappedBy="fk_informacion_laboral_has_cargo_cargo1")
     * @ORM\JoinColumn(name="cargo_id", referencedColumnName="id", nullable=false)
     */
    
    protected $historicoCargos;
    
    /**
     * @var float
     * @ORM\Column(name="sueldo", type="float", nullable=true)
     */
    private $sueldo;
    
    public function __construct()
    {
        $this->historicoCargos = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cargo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     * @return Cargo
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return string 
     */
    public function getNivel()
    {
        return $this->nivel;
    }
    
    /**
     * Set jerarquia
     *
     * @param integer $jerarquia
     * @return Cargo
     */
    public function setJerarquia($jerarquia)
    {
        $this->jerarquia = $jerarquia;

        return $this;
    }

    /**
     * Get jerarquia
     *
     * @return integer 
     */
    public function getJerarquia()
    {
        return $this->jerarquia;
    }

    /**
     * Add historicoCargos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos
     * @return Cargo
     */
    public function addHistoricoCargo(\Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos)
    {
        $this->historicoCargos[] = $historicoCargos;

        return $this;
    }

    /**
     * Remove historicoCargos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos
     */
    public function removeHistoricoCargo(\Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos)
    {
        $this->historicoCargos->removeElement($historicoCargos);
    }

    /**
     * Get historicoCargos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoCargos()
    {
        return $this->historicoCargos;
    }
    
    public function __toString()
    {
        $nivel='';
        if ($this->getNivel()==1){
            $nivel='I';
        }elseif($this->getNivel()==2){
            $nivel='II';
        }elseif($this->getNivel()==3){
            $nivel='III';
        }elseif($this->getNivel()==4){
            $nivel='IV';
        }elseif($this->getNivel()==5){
            $nivel='V';
        }elseif($this->getNivel()==6){
            $nivel='VI';
        }
        $cod=str_pad($this->getId(),3, '0', STR_PAD_LEFT);
        return $cod.' - '.$this->getNombre().' '.$nivel;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {

       $metadata   ->addPropertyConstraint('nombre', new Assert\NotBlank())
                   ->addPropertyConstraint('nombre', new Assert\Type(array('type'=>'string', 'message'=>'El campo debe ser solo texto.')))
                    ->addPropertyConstraint ('nombre', new Assert\Regex(array(
                        'pattern' => '/\d+/',   
//                        'pattern' => '/[\D\w\s áÁéÉíÍóÓúñÑ]+/i',                          
                        'match'   => False,
                        'message' => 'El Nombre no puede contener números ni caracteres especiales',
                    )))                
                   

//               El nombre del cargo puede llevar numero, así que se omite la validación de puro texto
                    //->addPropertyConstraint('nivel', new Assert\NotBlank())
                    //->addPropertyConstraint('nivel', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('sueldo', new Assert\NotBlank())
                ->addPropertyConstraint ('sueldo', new Assert\Type(array('type'=>'real','message'=>'El campo debe ser solo números.')))
               ;
        
    }

    /**
     * Set sueldo
     *
     * @param float $sueldo
     * @return Cargo
     */
    public function setSueldo($sueldo)
    {
        $this->sueldo = $sueldo;
    
        return $this;
    }

    /**
     * Get sueldo
     *
     * @return float 
     */
    public function getSueldo()
    {
        return $this->sueldo;
    }
}
