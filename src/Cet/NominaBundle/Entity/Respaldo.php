<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Respaldo
 *
 * @ORM\Entity(repositoryClass="RespaldoRepository")
 * @ORM\Table(name="sis_nomina.respaldo")
 */
class Respaldo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var datetime
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\OneToOne(targetEntity="Nomina", inversedBy="respaldos")
     * @ORM\JoinColumn(name="nomina_id", referencedColumnName="id", nullable=true)
     */
    protected $nomina;

    /**
     * @ORM\OneToOne(targetEntity="Vacaciones", inversedBy="respaldos")
     * @ORM\JoinColumn(name="vacaciones_id", referencedColumnName="id", nullable=true)
     */
    protected $bonoVacacional;
    
    /**
     * @ORM\OneToMany(targetEntity="Archivo", mappedBy="respaldo", cascade={"remove"})
     * @ORM\JoinColumn(name="archivo_id", referencedColumnName="id")
     */
    protected $archivos;

    public function __construct()
    {
        $this->archivos = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Respaldo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     * @return Respaldo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set nomina
     *
     * @param \Cet\NominaBundle\Entity\Nomina $nomina
     * @return Respaldo
     */
    public function setNomina(\Cet\NominaBundle\Entity\Nomina $nomina)
    {
        $this->nomina = $nomina;

        return $this;
    }

    /**
     * Get nomina
     *
     * @return \Cet\NominaBundle\Entity\Nomina 
     */
    public function getNomina()
    {
        return $this->nomina;
    }

    /**
     * Add archivos
     *
     * @param \Cet\NominaBundle\Entity\Archivo $archivos
     * @return Respaldo
     */
    public function addArchivo(\Cet\NominaBundle\Entity\Archivo $archivos)
    {
        $this->archivos[] = $archivos;

        return $this;
    }

    /**
     * Remove archivos
     *
     * @param \Cet\NominaBundle\Entity\Archivo $archivos
     */
    public function removeArchivo(\Cet\NominaBundle\Entity\Archivo $archivos)
    {
        $this->archivos->removeElement($archivos);
    }

    /**
     * Get archivos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArchivos()
    {
        return $this->archivos;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('descripcion', new Assert\NotBlank())
                ->addPropertyConstraint('fecha', new Assert\NotBlank())
                ->addPropertyConstraint('fecha', new Assert\Date()) 
             ;          
    }  

    /**
     * Set bonoVacacional
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $bonoVacacional
     * @return Respaldo
     */
    public function setBonoVacacional(\Cet\NominaBundle\Entity\Vacaciones $bonoVacacional = null)
    {
        $this->bonoVacacional = $bonoVacacional;

        return $this;
    }

    /**
     * Get bonoVacacional
     *
     * @return \Cet\NominaBundle\Entity\Vacaciones 
     */
    public function getBonoVacacional()
    {
        return $this->bonoVacacional;
    }
}
