<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiasPendientesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\Personal");
        $userTransformer->setEntityRepository("CetNominaBundle:Personal");
        $userTransformer->setEntityType("Personal");
        
        $builder
            ->add('dias')
            ->add('horas')
            ->add('minutos')
            ->add('documento')
            ->add('fecha','date', array(
                'label'=> 'Fecha',
                'widget' => 'single_text',
                ))  
            ->add($builder->create('personal','hidden')->addModelTransformer($userTransformer))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\DiasPendientes'
        ))        
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_diaspendientes';
    }
}
