<?php
namespace Cet\NominaBundle\DQL;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;

class Trunc extends FunctionNode
{
    protected $truncExp;
    protected $truncPrecission;

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        //solo para postgresql
        return 'TRUNC(' .
            $sqlWalker->walkArithmeticExpression($this->truncExp) . ','.
            $sqlWalker->walkArithmeticExpression($this->truncPrecission)
        .')';
        // la implementacion para mysql se debe hacer utilizando la funcion Round(Expresion, LugaresDecimales,Valor_0_Redondea_Valor_1_Trunca)
    }

    /**
     * parse - allows DQL to breakdown the DQL string into a processable structure
     * @param \Doctrine\ORM\Query\Parser $parser
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->truncExp = $parser->ArithmeticExpression(); 
        $parser->match(Lexer::T_COMMA); 
        $this->truncPrecission = $parser->ArithmeticExpression(); 

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}

?>
