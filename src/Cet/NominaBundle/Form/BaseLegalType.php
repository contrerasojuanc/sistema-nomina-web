<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BaseLegalType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rutaDigital')
            ->add('fechaExpedicion')
            ->add('fechaExpiracion')
            ->add('observaciones')
            ->add('cargaFamiliar')
            ->add('experienciaLaboral')
            ->add('informacionMedica')
            ->add('estudio')
            ->add('informacionLaboral')
            ->add('fk_base_legal_tipo_base_legal1')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\BaseLegal'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_baselegal';
    }
}
