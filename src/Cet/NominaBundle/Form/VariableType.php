<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Cet\NominaBundle\Form\Subscribers\EntidadSubscriber;
use Cet\NominaBundle\Form\Subscribers\AtributoSubscriber;

class VariableType extends AbstractType
{
    public $entidad_array=array();
    public $atributo_array=array();
    
    public function __construct(Array $entidad_array=null, Array $atributo_array=null)
    {
        $this->entidad_array = $entidad_array;
        $this->atributo_array = $atributo_array;    
    }
    
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $achoices_tipos=array('f'=>'Fecha','n'=>'Numerica','t'=>'Texto');
        //,'choice',array('choices'=>$achoices_tipos)
        $builder
            ->add('nombre')
            ->add('descripcion')
        ;                
        $factory = $builder->getFormFactory();
        $entidadSubscriber = new EntidadSubscriber($factory,$this->entidad_array);
        $builder->addEventSubscriber($entidadSubscriber);
        $atributoSubscriber = new AtributoSubscriber($factory,$this->atributo_array);
        $builder->addEventSubscriber($atributoSubscriber);  
        $builder
            ->add('sql',null,array(
                'label'=>'Expresión DQL',
                'required' => true,
                'attr' => array('help'=>'Ingrese la Expresión DQL para generar la fórmula')
            ))
            ->add('reservado','choice', array(
                'mapped' => false,
                'required' => false,
                'choices' => array('' => 'Seleccione una Palabra Reservada', 'SELECT', 'FROM', 'WHERE',  'AND',  'OR', '(', ')', ':personal_cedula'),
            ))
//            ->add('tipo','choice', array(
//                'mapped' => true,
//                'required' => true,
//                'choices' =>$achoices_tipos,
//            ))
//            ->add('tipoGestorSql')
//            ->add('fechaInicia')
//            ->add('fechaFin')                                       
        ; 
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Variable'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_variable';
    }
}
