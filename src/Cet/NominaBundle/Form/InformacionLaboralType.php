<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InformacionLaboralType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $array_tipo=array(0=>'Cuenta Corriente', 1 =>'Cuenta de Ahorro');
        
        $builder
            ->add('cuentaNomina')
            ->add('tipoCuentaNomina','choice', array(
                'mapped' => true,
                'required' => false,
                'multiple' => false,
                'choices' => $array_tipo
            )) 
            ->add('cuentaFideicomiso')
            ->add('tipoCuentaFideicomiso','choice', array(
                'mapped' => true,
                'required' => false,
                'multiple' => false,
                'choices' => $array_tipo
            )) 
            ->add('fechaIngreso')
            ->add('fechaIngreso')
            ->add('condicionEgreso')    
            
            ->add('fk_informacion_laboral_tipo_nomina1')
            ->add('baseLegal')
            ->add('personal')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\InformacionLaboral'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_informacionlaboral';
    }
}
