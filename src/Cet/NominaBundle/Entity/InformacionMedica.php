<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\InformacionMedica
 *
 * @ORM\Entity(repositoryClass="InformacionMedicaRepository")
 * @ORM\Table(name="sis_nomina.informacion_medica", indexes={@ORM\Index(name="fk_informacio_medica_base_legal1_idx", columns={"base_legal_id"}), @ORM\Index(name="fk_informacion_medica_personal1_idx", columns={"personal_cedula"})})
 */
class InformacionMedica
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="tipo_sangre", type="text", nullable=true)
     */
    private $tipoSangre;

    /**
     * @var boolean
     * @ORM\Column(name="usa_lentes", type="boolean", nullable=true)
     */
    private $usaLentes;

    /**
     * @var boolean
     * @ORM\Column(name="es_zurdo", type="boolean", nullable=true)
     */
    private $esZurdo;

    /**
     * @ORM\OneToMany(targetEntity="Enfermedad", mappedBy="fk_enfermedad_informacion_medica1",cascade={"all"})
     * @ORM\JoinColumn(name="informacion_medica_id", referencedColumnName="id", nullable=false)
     */
    protected $enfermedads;

    /**
     * @ORM\OneToOne(targetEntity="BaseLegal", inversedBy="informacionMedica")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $baseLegal;

    /**
     * @ORM\OneToOne(targetEntity="Personal", inversedBy="informacionMedica")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $personal;

    public function __construct()
    {
        $this->enfermedads = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoSangre
     *
     * @param string $tipoSangre
     * @return InformacionMedica
     */
    public function setTipoSangre($tipoSangre)
    {
        $this->tipoSangre = $tipoSangre;

        return $this;
    }

    /**
     * Get tipoSangre
     *
     * @return string 
     */
    public function getTipoSangre()
    {
        return $this->tipoSangre;
    }

    /**
     * Set usaLentes
     *
     * @param boolean $usaLentes
     * @return InformacionMedica
     */
    public function setUsaLentes($usaLentes)
    {
        $this->usaLentes = $usaLentes;

        return $this;
    }

    /**
     * Get usaLentes
     *
     * @return boolean 
     */
    public function getUsaLentes()
    {
        return $this->usaLentes;
    }

    /**
     * Set esZurdo
     *
     * @param boolean $esZurdo
     * @return InformacionMedica
     */
    public function setEsZurdo($esZurdo)
    {
        $this->esZurdo = $esZurdo;

        return $this;
    }

    /**
     * Get esZurdo
     *
     * @return boolean 
     */
    public function getEsZurdo()
    {
        return $this->esZurdo;
    }

    /**
     * Add enfermedads
     *
     * @param \Cet\NominaBundle\Entity\Enfermedad $enfermedads
     * @return InformacionMedica
     */
    public function addEnfermedad(\Cet\NominaBundle\Entity\Enfermedad $enfermedads)
    {
        $this->enfermedads[] = $enfermedads;

        return $this;
    }

    /**
     * Remove enfermedads
     *
     * @param \Cet\NominaBundle\Entity\Enfermedad $enfermedads
     */
    public function removeEnfermedad(\Cet\NominaBundle\Entity\Enfermedad $enfermedads)
    {
        $this->enfermedads->removeElement($enfermedads);
    }

    /**
     * Get enfermedads
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEnfermedads()
    {
        return $this->enfermedads;
    }

    /**
     * Set baseLegal
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegal
     * @return InformacionMedica
     */
    public function setBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegal = null)
    {
        $this->baseLegal = $baseLegal;

        return $this;
    }

    /**
     * Get baseLegal
     *
     * @return \Cet\NominaBundle\Entity\BaseLegal 
     */
    public function getBaseLegal()
    {
        return $this->baseLegal;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return InformacionMedica
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('tipoSangre', new Assert\NotBlank())
//                ->addPropertyConstraint ('usaLentes', new Assert\NotBlank())
//                ->addPropertyConstraint ('esZurdo', new Assert\NotBlank())
             

             ;
    }
    
    public function __toString()
    {
        return $this->getPersonal().'';
    }
}
