<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Vivienda
 *
 * @ORM\Entity(repositoryClass="ViviendaRepository")
 * @ORM\Table(name="sis_nomina.vivienda", indexes={@ORM\Index(name="fk_vivienda_parroquia1_idx", columns={"parroquia_id"}), @ORM\Index(name="fk_vivienda_personal1_idx", columns={"personal_cedula"})})
 */
class Vivienda
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Solo valores:
     * P = Propia
     * A = Alquilada
     * C = Compartida
     * O = Otra
     *
     * @var string
     * @ORM\Column(name="condicion", type="string", length=1, nullable=true)
     */
    private $condicion;

    /**
     * @var string
     * @ORM\Column(name="direccion", type="text", nullable=false)
     */
    private $direccion;

    /**
     * @var string
     * @ORM\Column(name="codigo_postal", type="string", length=10, nullable=true)
     */
    private $codigoPostal;

    /**
     * @var string
     * @ORM\Column(name="referencia", type="text", nullable=true)
     */
    private $referencia;

    /**
     * @ORM\ManyToOne(targetEntity="Parroquia", inversedBy="viviendas")
     * @ORM\JoinColumn(name="parroquia_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_vivienda_parroquia1;

    /**
     * @ORM\OneToOne(targetEntity="Personal", inversedBy="vivienda")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $personal;

    public function __construct()
    {
    }


    /**
     * Set id
     *
     * @param integer $id
     * @return Vivienda
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set condicion
     *
     * @param string $condicion
     * @return Vivienda
     */
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;

        return $this;
    }

    /**
     * Get condicion
     *
     * @return string 
     */
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Vivienda
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set codigoPostal
     *
     * @param string $codigoPostal
     * @return Vivienda
     */
    public function setCodigoPostal($codigoPostal)
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    /**
     * Get codigoPostal
     *
     * @return string 
     */
    public function getCodigoPostal()
    {
        return $this->codigoPostal;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     * @return Vivienda
     */
    public function setReferencia($referencia)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string 
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set fk_vivienda_parroquia1
     *
     * @param \Cet\NominaBundle\Entity\Parroquia $fkViviendaParroquia1
     * @return Vivienda
     */
    public function setFkViviendaParroquia1(\Cet\NominaBundle\Entity\Parroquia $fkViviendaParroquia1)
    {
        $this->fk_vivienda_parroquia1 = $fkViviendaParroquia1;

        return $this;
    }

    /**
     * Get fk_vivienda_parroquia1
     *
     * @return \Cet\NominaBundle\Entity\Parroquia 
     */
    public function getFkViviendaParroquia1()
    {
        return $this->fk_vivienda_parroquia1;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return Vivienda
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  //->addPropertyConstraint ('id', new Assert\NotBlank())
                ->addPropertyConstraint ('condicion', new Assert\NotBlank())
                ->addPropertyConstraint ('direccion', new Assert\NotBlank())             
                ->addPropertyConstraint ('direccion', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('codigoPostal', new Assert\NotBlank())   
                ->addPropertyConstraint ('codigoPostal', new Assert\Type(array('type' => 'numeric'))) 
                ->addPropertyConstraint ('codigoPostal', new Assert\Length(array(
                    'min'=> 4,
                    'minMessage'=> 'El Código Postal no debe ser menor a 4 digitos.', 
                    'max'=> 4,
                    'maxMessage'=> 'El Código Postal no debe ser mayor a 4 digitos.',
                )))               
                ->addPropertyConstraint ('referencia',  new Assert\NotBlank())   
                ->addPropertyConstraint ('referencia', new Assert\Type(array('type'=>'string'))) 
                ->addPropertyConstraint ('fk_vivienda_parroquia1', new Assert\NotBlank())
            ;     
    }
    
    public function __toString()
    {
        return $this->getDireccion();
    }
}
