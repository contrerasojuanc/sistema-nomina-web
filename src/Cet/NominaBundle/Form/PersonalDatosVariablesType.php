<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalDatosVariablesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $choices_modo=array('adicion'=>'Adición','sustraccion'=>'Sustracción','sustitucion'=>'Sustitución');
//        $choices_seccion=array('principal'=>'Sección Principal','patronal'=>'Sección Patronal','acumulado'=>'Sección de Acumulado');
        $choices_modo=array('sustitucion'=>'Sustitución');
        $choices_seccion=array('principal'=>'Sección Principal','patronal'=>'Sección Patronal','acumulado'=>'Sección de Acumulado','vacacional'=>'Bono Vacacional','prestaciones'=>'Garantia Prestaciones sociales','adicionales'=>'Días Adicionales de Garantía de Prestaciones Sociales');

        $builder
            ->add('nombre')
            ->add('valor',null, array(
                'attr' => array('help'=>'Establezca el monto deseado. Si se desea excluir el cálculo de Garantia de Prestaciones Sociales para un trabajador, ingrese el siguiente valor negativo: -9999999 además seleccione la sección de afectación "Garantía de Prestaciones Sociales" ')  
            ))
            ->add('modoAfectacion','choice', array(
                'mapped' => true,
                'required' => true,
                'choices' =>$choices_modo                
            ))
            ->add('seccionAfectacion','choice', array(
                'mapped' => true,
                'required' => true,
                'choices' =>$choices_seccion,
                'attr' => array('help'=>'Seleccione la sección donde será modificado el monto.','class' => 'select2me','data-placeholder' => 'Seleccione una sección')   
            ))
            //->add('fk_datos_variables_personal1')
            ->add('fk_datos_variables_concepto1',null,array(
                       'query_builder' => function($r) {
                        return $r->createQueryBuilder('concepto')
                                   ->orderBy('concepto.codigo','ASC');
                        },
                   'label'=>'Concepto de nomina','property'=>'getNombreCompleto','attr' => array('help'=>'Seleccione el concepto que será afectado','class' => 'select2me','data-placeholder' => 'Seleccione el concepto')))   
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\DatosVariables'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_datosvariables';
    }
}
