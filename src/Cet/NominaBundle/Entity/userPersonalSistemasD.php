<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GenUsuarios.userPersonalSistemasD
 *
 * @ORM\Table(name="gen_usuarios.user_personal_sistemas_d", indexes={@ORM\Index(name="IDX_F50290AE4F5F0211", columns={"id_sistema"}), @ORM\Index(name="IDX_F50290AEFCF8192D", columns={"id_usuario"})})
 * @ORM\Entity
 */
class userPersonalSistemasD
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gen_usuarios.user_personal_sistemas_d_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="acceso", type="string", nullable=true)
     */
    private $acceso;

    /**
     * @var integer
     *
     * @ORM\Column(name="nivel", type="integer", nullable=true)
     */
    private $nivel;

    /**
     * @var \GenSistemas.sistSistemasM
     *
     * @ORM\ManyToOne(targetEntity="sistSistemasM")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sistema", referencedColumnName="id")
     * })
     */
    private $idSistema;

    /**
     * @var \GenUsuarios.userPersonalM
     *
     * @ORM\ManyToOne(targetEntity="userPersonalM",inversedBy="personalSistemas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * Set id
     *
     * @return integer 
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set acceso
     *
     * @param string $acceso
     * @return userPersonalSistemasD
     */
    public function setAcceso($acceso)
    {
        $this->acceso = $acceso;

        return $this;
    }

    /**
     * Get acceso
     *
     * @return string 
     */
    public function getAcceso()
    {
        return $this->acceso;
    }

    /**
     * Set nivel
     *
     * @param integer $nivel
     * @return userPersonalSistemasD
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return integer 
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set idSistema
     *
     * @param \Cet\NominaBundle\Entity\sistSistemasM $idSistema
     * @return userPersonalSistemasD
     */
    public function setIdSistema(\Cet\NominaBundle\Entity\sistSistemasM $idSistema = null)
    {
        $this->idSistema = $idSistema;

        return $this;
    }

    /**
     * Get idSistema
     *
     * @return \Cet\NominaBundle\Entity\sistSistemasM 
     */
    public function getIdSistema()
    {
        return $this->idSistema;
    }

    /**
     * Set idUsuario
     *
     * @param \Cet\NominaBundle\Entity\userPersonalM $idUsuario
     * @return userPersonalSistemasD
     */
    public function setIdUsuario(\Cet\NominaBundle\Entity\userPersonalM $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Cet\NominaBundle\Entity\userPersonalM 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
    
    public function __toString()
    {
        return "".$this->getIdSistema()->getNombre();
    }
}
