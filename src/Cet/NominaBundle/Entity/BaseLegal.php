<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\BaseLegal
 *
 * @ORM\Entity(repositoryClass="BaseLegalRepository")
 * @ORM\Table(name="sis_nomina.base_legal", indexes={@ORM\Index(name="fk_base_legal_tipo_base_legal1_idx", columns={"tipo_base_legal_id"})})
 */
class BaseLegal
{
    /**
     * Siendo base_legal la tabla maestra. Las entidades que necesiten relacionarse
     * con base_legal tendrán su fk
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Ruta del archivo digital
     *
     * @var string
     * @ORM\Column(name="ruta_digital", type="text", nullable=true)
     */
    private $rutaDigital;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_expedicion", type="date", nullable=true)
     */
    private $fechaExpedicion;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_expiracion", type="date", nullable=true)
     */
    private $fechaExpiracion;

    /**
     * @var string
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\OneToOne(targetEntity="CargaFamiliar", mappedBy="baseLegal")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $cargaFamiliar;

    /**
     * @ORM\OneToOne(targetEntity="ExperienciaLaboral", mappedBy="baseLegal")
     */
    protected $experienciaLaboral;

    /**
     * @ORM\OneToOne(targetEntity="InformacionMedica", mappedBy="baseLegal")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $informacionMedica;
    
    /**
     * @ORM\OneToOne(targetEntity="Estudio", mappedBy="baseLegal")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $estudio;
    
    /**
     * @ORM\OneToOne(targetEntity="InformacionLaboral", mappedBy="baseLegal")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $informacionLaboral;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoBaseLegal", inversedBy="baseLegals")
     * @ORM\JoinColumn(name="tipo_base_legal_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_base_legal_tipo_base_legal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rutaDigital
     *
     * @param string $rutaDigital
     * @return BaseLegal
     */
    public function setRutaDigital($rutaDigital)
    {
        $this->rutaDigital = $rutaDigital;

        return $this;
    }

    /**
     * Get rutaDigital
     *
     * @return string 
     */
    public function getRutaDigital()
    {
        return $this->rutaDigital;
    }

    /**
     * Set fechaExpedicion
     *
     * @param \DateTime $fechaExpedicion
     * @return BaseLegal
     */
    public function setFechaExpedicion($fechaExpedicion)
    {
        $this->fechaExpedicion = $fechaExpedicion;

        return $this;
    }

    /**
     * Get fechaExpedicion
     *
     * @return \DateTime 
     */
    public function getFechaExpedicion()
    {
        return $this->fechaExpedicion;
    }

    /**
     * Set fechaExpiracion
     *
     * @param \DateTime $fechaExpiracion
     * @return BaseLegal
     */
    public function setFechaExpiracion($fechaExpiracion)
    {
        $this->fechaExpiracion = $fechaExpiracion;

        return $this;
    }

    /**
     * Get fechaExpiracion
     *
     * @return \DateTime 
     */
    public function getFechaExpiracion()
    {
        return $this->fechaExpiracion;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return BaseLegal
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set cargaFamiliar
     *
     * @param \Cet\NominaBundle\Entity\CargaFamiliar $cargaFamiliar
     * @return BaseLegal
     */
    public function setCargaFamiliar(\Cet\NominaBundle\Entity\CargaFamiliar $cargaFamiliar = null)
    {
        $this->cargaFamiliar = $cargaFamiliar;

        return $this;
    }

    /**
     * Get cargaFamiliar
     *
     * @return \Cet\NominaBundle\Entity\CargaFamiliar 
     */
    public function getCargaFamiliar()
    {
        return $this->cargaFamiliar;
    }

    /**
     * Set experienciaLaboral
     *
     * @param \Cet\NominaBundle\Entity\ExperienciaLaboral $experienciaLaboral
     * @return BaseLegal
     */
    public function setExperienciaLaboral(\Cet\NominaBundle\Entity\ExperienciaLaboral $experienciaLaboral = null)
    {
        $this->experienciaLaboral = $experienciaLaboral;

        return $this;
    }

    /**
     * Get experienciaLaboral
     *
     * @return \Cet\NominaBundle\Entity\ExperienciaLaboral 
     */
    public function getExperienciaLaboral()
    {
        return $this->experienciaLaboral;
    }

    /**
     * Set informacionMedica
     *
     * @param \Cet\NominaBundle\Entity\InformacionMedica $informacionMedica
     * @return BaseLegal
     */
    public function setInformacionMedica(\Cet\NominaBundle\Entity\InformacionMedica $informacionMedica = null)
    {
        $this->informacionMedica = $informacionMedica;

        return $this;
    }

    /**
     * Get informacionMedica
     *
     * @return \Cet\NominaBundle\Entity\InformacionMedica 
     */
    public function getInformacionMedica()
    {
        return $this->informacionMedica;
    }
    
    /**
     * Set estudio
     *
     * @param \Cet\NominaBundle\Entity\Estudio $estudio
     * @return BaseLegal
     */
    public function setEstudio(\Cet\NominaBundle\Entity\Estudio $estudio = null)
    {
        $this->estudio = $estudio;

        return $this;
    }

    /**
     * Get estudio
     *
     * @return \Cet\NominaBundle\Entity\Estudio 
     */
    public function getEstudio()
    {
        return $this->estudio;
    }
    
    /**
     * Set informacionLaboral
     *
     * @param \Cet\NominaBundle\Entity\InformacionLaboral $informacionLaboral
     * @return BaseLegal
     */
    public function setInformacionLaboral(\Cet\NominaBundle\Entity\InformacionLaboral $informacionLaboral = null)
    {
        $this->informacionLaboral = $informacionLaboral;

        return $this;
    }

    /**
     * Get informacionLaboral
     *
     * @return \Cet\NominaBundle\Entity\InformacionLaboral 
     */
    public function getInformacionLaboral()
    {
        return $this->informacionLaboral;
    }
    
    /**
     * Set fk_base_legal_tipo_base_legal1
     *
     * @param \Cet\NominaBundle\Entity\TipoBaseLegal $fkBaseLegalTipoBaseLegal1
     * @return BaseLegal
     */
    public function setFkBaseLegalTipoBaseLegal1(\Cet\NominaBundle\Entity\TipoBaseLegal $fkBaseLegalTipoBaseLegal1)
    {
        $this->fk_base_legal_tipo_base_legal1 = $fkBaseLegalTipoBaseLegal1;

        return $this;
    }

    /**
     * Get fk_base_legal_tipo_base_legal1
     *
     * @return \Cet\NominaBundle\Entity\TipoBaseLegal 
     */
    public function getFkBaseLegalTipoBaseLegal1()
    {
        return $this->fk_base_legal_tipo_base_legal1;
    }

    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
      $metadata->addPropertyConstraint('rutaDigital', new Assert\NotBlank())
               ->addPropertyConstraint('fechaExpedicion', new Assert\NotBlank())  
//               ->addPropertyConstraint('fechaExpedicion', new Assert\Date())
               ->addPropertyConstraint('fechaExpiracion', new Assert\NotBlank()) 
//               ->addPropertyConstraint('fechaExpiracion', new Assert\Date())
               ->addPropertyConstraint('observaciones', new Assert\NotBlank())
                
        ;         
    }
    
    public function __toString()
    {
        return $this->getObservaciones();
    }
}
