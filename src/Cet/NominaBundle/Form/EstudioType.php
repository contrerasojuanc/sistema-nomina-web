<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EstudioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion')
            ->add('duracion')
            ->add('tipo')
            ->add('fechaInicio')
            ->add('fechaCulminacion')
            ->add('institucion')
            ->add('estado')
            ->add('baseLegal')
            ->add('fk_estudios_informacion_academica1')
            ->add('fk_estudios_personal1')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Estudio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_estudio';
    }
}
