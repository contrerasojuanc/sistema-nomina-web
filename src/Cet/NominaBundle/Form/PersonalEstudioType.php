<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalEstudioType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion',null,array('label' => 'Descripción','attr' => array('help'=>'Ingrese la descripción del estudio en esta área.'))) 
            ->add('duracion',null,array('label' => 'Duración','attr' => array('help'=>'Ingrese la duración del estudio en esta área.')))            
            ->add('tipo', 'choice', array(
                'label' => 'Tipo de Estudio',
                'attr' => array('help'=>'Seleccione el tipo de estudio que corresponda.'),
                'choices' => array('' => 'Seleccione','0' => 'Bachillerato','1' => 'Taller', '2' => 'Curso', '3' => 'T.S.U', '4' => 'Universitario', '5' => 'Diplomado', '6' => 'Especialización', '7' => 'Maestria', '8' => 'Doctorado'),
            ))     
            ->add('fechaInicio','date', array(
                'label'=> 'Fecha de Incio',
                'attr' => array('help'=>'Ingrese la fecha de inicio del estudio.'),
                'widget' => 'single_text',
            ))    
            ->add('fechaCulminacion','date', array(
                'label'=> 'Fecha de Culminación',
                'attr' => array('help'=>'Ingrese la fecha de culminación del estudio sino ha culminado dejar en blanco.'),
                'widget' => 'single_text',
                'required' => false,
            ))     
            ->add('institucion',null,array(
                'label'=>'Instituto-Academia-Universidad',
                'attr'=> array('help'=>'Ingrese el nombre de la institución, academia o universidad.'),
                ))
                
            ->add('estado', 'choice', array(
                'label'=> 'Estado del Estudio',
                'attr' => array('help'=>'Seleccione el estado del estudio que corresponda.'),
                'choices' => array('' => 'Seleccione','C' => 'Cursando','T' => 'Terminado','S' => 'Sin Incidencia'),
            ))    
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Estudio'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_estudio';
    }
}
