<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cet\NominaBundle\Entity;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
/**
 * Description of LenguajeFormula
 *
 * @author Juan.Contreras
 */
class LenguajeFormula extends ExpressionLanguage{
    protected function registerFunctions()
    {
        parent::registerFunctions();
        // funciones de fecha
        
        //Calcula dia Actual        
        $this->register('hoy', function () {
            return sprintf('(new \DateTime())');
        }, function () {
            return new \DateTime();
        });

        //Calcula el valor redondeado a 2 decimales
//        $this->register('redondear', function ($variables,$valor) {            
        $this->register('redondear', function ($valor) {            
            return sprintf('0');
        }, function ($variables,$valor) {
            return round($valor,2);
        });
        
        $this->register('diferencia_dias', function ($fechaInicio,$fechaFinal) {
            return sprintf('(new \DateTime())');
        }, function ($fechaInicio,$fechaFinal) {
//            $fechaInicio=strtotime($fechaInicio);
            $intervalo=$fechaInicio->diff($fechaFinal);
            return $intervalo->days;
        });
        
        $this->register('diferencia_anios', function ($fechaInicio,$fechaFinal) {
            return sprintf('(new \DateTime())');
        }, function ($fechaInicio,$fechaFinal) {
//            $fechaInicio=strtotime($fechaInicio);
            $intervalo=$fechaInicio->diff($fechaFinal);
            return $intervalo->years;
        });
        // Registering our 'date_modify' function
//        $this->register('date_modify', function ($date, $modify) {
//            return sprintf('%s->modify(%s)', $date, $modify);
//        }, function (array $values, $date, $modify) {
//            if (!$date instanceof \DateTime) {
//                throw new \RuntimeException('date_modify() expects parameter 1 to be a Date');
//            }
//            return $date->modify($modify);
//        });
    }
    
}
