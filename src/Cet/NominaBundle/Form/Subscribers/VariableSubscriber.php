<?php

namespace Cet\NominaBundle\Form\Subscribers;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;

class VariableSubscriber implements EventSubscriberInterface
{
    private $factory;

    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }

    private function addVariableForm($form, $variable)
    {
        $form->add($this->factory->createNamed('Variable', 'entity', $variable, array(
            'class'         => 'CetNominaBundle:Variable',
            'mapped'        => false,
            'empty_value'   => 'Seleccione el Variable',
            'auto_initialize'=> false,
            'required'=> false,
            'multiple'=> false,
            'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione'), 
            'query_builder' => function (EntityRepository $repository) {
                $qb = $repository->createQueryBuilder('variable');

                return $qb;
            }
        )))
            ->add($this->factory->createNamed('Concepto', 'entity', $variable, array(
            'class'         => 'CetNominaBundle:Concepto',
            'property'      => 'identificador',
            'mapped'        => false,
            'empty_value'   => 'Seleccione un Concepto',
            'auto_initialize'=> false,
            'required'=> false,
            'multiple'=> false,
            //'property'=>'getNombreFormula',    
            'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione'),    
            
            'query_builder' => function (EntityRepository $repository) {
                $qb = $repository->createQueryBuilder('concepto');

                return $qb;
            }
        )))
        ;
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        if (null === $data) {
            return;
        }
        $variable =  null ;

        $this->addVariableForm($form, $variable);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $variable = array_key_exists('Variable', $data) ? $data['Variable'] : null;
        $this->addVariableForm($form, $variable);
    }
}