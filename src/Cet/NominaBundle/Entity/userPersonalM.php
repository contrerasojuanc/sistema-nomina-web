<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection; 

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * GenUsuarios.userPersonalM
 *
 * @ORM\Table(name="gen_usuarios.user_personal_m", indexes={@ORM\Index(name="IDX_7F44B071642FEB76", columns={"funcionario_id"})})
 * @ORM\Entity(repositoryClass="userPersonalMRepository")
 * @UniqueEntity(
 *     fields={"personal"},
 *     message="Ya existe un usuario asignado a ese funcionario"
 * )
 * @UniqueEntity(
 *     fields={"persLoginu"},
 *     message="Ya existe este usuario"
 * )
 */
class userPersonalM implements \Symfony\Component\Security\Core\User\UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gen_usuarios.user_personal_m_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_nombr1", type="string", length=100, nullable=false)
     */
    private $persNombr1;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_nombr2", type="string", length=100, nullable=true)
     */
    private $persNombr2;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_apell1", type="string", length=100, nullable=false)
     */
    private $persApell1;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_apell2", type="string", length=100, nullable=true)
     */
    private $persApell2;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_cedula", type="string", length=10, nullable=false)
     */
    private $persCedula;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_nacion", type="string", length=1, nullable=false)
     */
    private $persNacion;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_dirdom", type="text", nullable=true)
     */
    private $persDirdom;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_loginu", type="string", length=20, nullable=true)
     */
    private $persLoginu;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_passwd", type="string", length=64, nullable=true)
     */
    private $persPasswd;

    /**
     * @var integer
     *
     * @ORM\Column(name="pers_cargou", type="integer", nullable=true)
     */
    private $persCargou;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_e_mail", type="string", length=30, nullable=true)
     */
    private $persEMail;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_avatar", type="string", length=64, nullable=true)
     */
    private $persAvatar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pers_fec_in", type="date", nullable=true)
     */
    private $persFecIn;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_status", type="string", nullable=true)
     */
    private $persStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pers_fecreg", type="date", nullable=true)
     */
    private $persFecreg;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_direcc", type="string", length=10, nullable=true)
     */
    private $persDirecc;

    /**
     * @var integer
     *
     * @ORM\Column(name="division_id", type="integer", nullable=true)
     */
    private $divisionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="departamento_id", type="integer", nullable=true)
     */
    private $departamentoId;

    /**
     * @var string
     *
     * @ORM\Column(name="pers_passwdmd5", type="text", nullable=true)
     */
    private $persPasswdmd5;

    /**
     * @var \GenOrganizacion.orgaFuncionario
     *
     * @ORM\Column(name="funcionario_id", type="integer", nullable=true)
     */
    private $funcionario;

    /**
     * @ORM\OneToOne(targetEntity="Personal", inversedBy="userPersonalM")
     * @ORM\JoinColumn(name="personal_id", referencedColumnName="cedula")
     */
    private $personal;

    /**     
     * @var \GenOrganizacion.userPersonalSistemasD
     *
     * @ORM\OneToMany(targetEntity="userPersonalSistemasD", mappedBy="idUsuario", cascade={"all"})
     */
    private $personalSistemas;

    
    public function __construct()
    {
        $this->personalSistemas = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set persNombr1
     *
     * @param string $persNombr1
     * @return userPersonalM
     */
    public function setPersNombr1($persNombr1)
    {
        $this->persNombr1 = $persNombr1;

        return $this;
    }

    /**
     * Get persNombr1
     *
     * @return string 
     */
    public function getPersNombr1()
    {
        return $this->persNombr1;
    }

    /**
     * Set persNombr2
     *
     * @param string $persNombr2
     * @return userPersonalM
     */
    public function setPersNombr2($persNombr2)
    {
        $this->persNombr2 = $persNombr2;

        return $this;
    }

    /**
     * Get persNombr2
     *
     * @return string 
     */
    public function getPersNombr2()
    {
        return $this->persNombr2;
    }

    /**
     * Set persApell1
     *
     * @param string $persApell1
     * @return userPersonalM
     */
    public function setPersApell1($persApell1)
    {
        $this->persApell1 = $persApell1;

        return $this;
    }

    /**
     * Get persApell1
     *
     * @return string 
     */
    public function getPersApell1()
    {
        return $this->persApell1;
    }

    /**
     * Set persApell2
     *
     * @param string $persApell2
     * @return userPersonalM
     */
    public function setPersApell2($persApell2)
    {
        $this->persApell2 = $persApell2;

        return $this;
    }

    /**
     * Get persApell2
     *
     * @return string 
     */
    public function getPersApell2()
    {
        return $this->persApell2;
    }

    /**
     * Set persCedula
     *
     * @param string $persCedula
     * @return userPersonalM
     */
    public function setPersCedula($persCedula)
    {
        $this->persCedula = $persCedula;

        return $this;
    }

    /**
     * Get persCedula
     *
     * @return string 
     */
    public function getPersCedula()
    {
        return $this->persCedula;
    }

    /**
     * Set persNacion
     *
     * @param string $persNacion
     * @return userPersonalM
     */
    public function setPersNacion($persNacion)
    {
        $this->persNacion = $persNacion;

        return $this;
    }

    /**
     * Get persNacion
     *
     * @return string 
     */
    public function getPersNacion()
    {
        return $this->persNacion;
    }

    /**
     * Set persDirdom
     *
     * @param string $persDirdom
     * @return userPersonalM
     */
    public function setPersDirdom($persDirdom)
    {
        $this->persDirdom = $persDirdom;

        return $this;
    }

    /**
     * Get persDirdom
     *
     * @return string 
     */
    public function getPersDirdom()
    {
        return $this->persDirdom;
    }

    /**
     * Set persLoginu
     *
     * @param string $persLoginu
     * @return userPersonalM
     */
    public function setPersLoginu($persLoginu)
    {
        $this->persLoginu = $persLoginu;

        return $this;
    }

    /**
     * Get persLoginu
     *
     * @return string 
     */
    public function getPersLoginu()
    {
        return $this->persLoginu;
    }

    /**
     * Set persPasswd
     *
     * @param string $persPasswd
     * @return userPersonalM
     */
    public function setPersPasswd($persPasswd)
    {
        $this->persPasswd = $persPasswd;

        return $this;
    }

    /**
     * Get persPasswd
     *
     * @return string 
     */
    public function getPersPasswd()
    {
        return $this->persPasswd;
    }

    /**
     * Set persCargou
     *
     * @param integer $persCargou
     * @return userPersonalM
     */
    public function setPersCargou($persCargou)
    {
        $this->persCargou = $persCargou;

        return $this;
    }

    /**
     * Get persCargou
     *
     * @return integer 
     */
    public function getPersCargou()
    {
        return $this->persCargou;
    }

    /**
     * Set persEMail
     *
     * @param string $persEMail
     * @return userPersonalM
     */
    public function setPersEMail($persEMail)
    {
        $this->persEMail = $persEMail;

        return $this;
    }

    /**
     * Get persEMail
     *
     * @return string 
     */
    public function getPersEMail()
    {
        return $this->persEMail;
    }

    /**
     * Set persAvatar
     *
     * @param string $persAvatar
     * @return userPersonalM
     */
    public function setPersAvatar($persAvatar)
    {
        $this->persAvatar = $persAvatar;

        return $this;
    }

    /**
     * Get persAvatar
     *
     * @return string 
     */
    public function getPersAvatar()
    {
        return $this->persAvatar;
    }

    /**
     * Set persFecIn
     *
     * @param \DateTime $persFecIn
     * @return userPersonalM
     */
    public function setPersFecIn($persFecIn)
    {
        $this->persFecIn = $persFecIn;

        return $this;
    }

    /**
     * Get persFecIn
     *
     * @return \DateTime 
     */
    public function getPersFecIn()
    {
        return $this->persFecIn;
    }

    /**
     * Set persStatus
     *
     * @param string $persStatus
     * @return userPersonalM
     */
    public function setPersStatus($persStatus)
    {
        $this->persStatus = $persStatus;

        return $this;
    }

    /**
     * Get persStatus
     *
     * @return string 
     */
    public function getPersStatus()
    {
        return $this->persStatus;
    }

    /**
     * Set persFecreg
     *
     * @param \DateTime $persFecreg
     * @return userPersonalM
     */
    public function setPersFecreg($persFecreg)
    {
        $this->persFecreg = $persFecreg;

        return $this;
    }

    /**
     * Get persFecreg
     *
     * @return \DateTime 
     */
    public function getPersFecreg()
    {
        return $this->persFecreg;
    }

    /**
     * Set persDirecc
     *
     * @param string $persDirecc
     * @return userPersonalM
     */
    public function setPersDirecc($persDirecc)
    {
        $this->persDirecc = $persDirecc;

        return $this;
    }

    /**
     * Get persDirecc
     *
     * @return string 
     */
    public function getPersDirecc()
    {
        return $this->persDirecc;
    }

    /**
     * Set divisionId
     *
     * @param integer $divisionId
     * @return userPersonalM
     */
    public function setDivisionId($divisionId)
    {
        $this->divisionId = $divisionId;

        return $this;
    }

    /**
     * Get divisionId
     *
     * @return integer 
     */
    public function getDivisionId()
    {
        return $this->divisionId;
    }

    /**
     * Set departamentoId
     *
     * @param integer $departamentoId
     * @return userPersonalM
     */
    public function setDepartamentoId($departamentoId)
    {
        $this->departamentoId = $departamentoId;

        return $this;
    }

    /**
     * Get departamentoId
     *
     * @return integer 
     */
    public function getDepartamentoId()
    {
        return $this->departamentoId;
    }

    /**
     * Set persPasswdmd5
     *
     * @param string $persPasswdmd5
     * @return userPersonalM
     */
    public function setPersPasswdmd5($persPasswdmd5)
    {
        if(!empty($persPasswdmd5)){
            $this->persPasswdmd5 = md5($persPasswdmd5);
        }

        return $this;
    }

    /**
     * Get persPasswdmd5
     *
     * @return string 
     */
    public function getPersPasswdmd5()
    {
        return $this->persPasswdmd5;
    }

    /**
     * Set funcionario
     *
     * @param string $funcionario
     * @return userPersonalM
     */
    public function setFuncionario($funcionario)
    {
        $this->funcionario = $funcionario;

        return $this;
    }

    /**
     * Get funcionario
     *
     * @return string 
     */
    public function getFuncionario()
    {
        return $this->funcionario;
    }

    /**
     * Get personalSistemas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPersonalSistemas()
    {
        return $this->personalSistemas;
    }
    
    public function setPersonalSistemas(Collection $registros)
    {
        $this->personalSistemas = $registros;
        foreach ($registros as $registro) {
            $registro->setIdUsuario($this);
        }
    }
    
    /**
     * Add personalSistemas
     *
     * @param \Cet\NominaBundle\Entity\userPersonalSistemasD $personalSistemas
     * @return userPersonalM
     */
    public function addPersonalSistema(\Cet\NominaBundle\Entity\userPersonalSistemasD $personalSistemas)
    {
        $this->personalSistemas[] = $personalSistemas;

        return $this;
    }

    /**
     * Remove personalSistemas
     *
     * @param \Cet\NominaBundle\Entity\userPersonalSistemasD $personalSistemas
     */
    public function removePersonalSistema(\Cet\NominaBundle\Entity\userPersonalSistemasD $personalSistemas)
    {
        $this->personalSistemas->removeElement($personalSistemas);
    }
    
    public function eraseCredentials() {
        
    }

    public function getPassword() {
        return $this->persPasswdmd5;
    }

    public function getRoles() {
        //Roles tomados de atributo:nivel tabla:userpersonalsistemasd donde:1=ROLE_ADMIN 2=ROLE_USER ...
        //Si cambia el id del sistema se debe cambiar en loadUserByUsername userPersonalMRepository.php 
//        $rol=$this->getPersonalSistemas()->first()->getNivel();        
        $roles=array();
        foreach ($this->getPersonalSistemas() as $rol => $valor){
            if($valor->getIdSistema()->getId()==9)//Sistema Nomina Web
            {
                switch ($valor->getNivel()){
                    case 1:
                        $roles[]='ROLE_ADMIN';
                        break;
                    case 2:
                        $roles[]='ROLE_USER';
                        break;
                    case 3:
                        $roles[]='ROLE_REMUNERACIONES';
                        break;
                    case 4:
                        $roles[]='ROLE_ARCHIVO';
                        break;
                    default:
                        $roles[]='';
                        break;
                }
            }
        }
        return $roles;
    }

    public function getSalt() {
        return "";
    }

    public function getUsername() {
        return $this->persLoginu;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->persLoginu,
            $this->persPasswdmd5,
        ));
    }

    public function unserialize($serialized) {
        list (
            $this->id,
            $this->persLoginu,
            $this->persPasswdmd5,
        ) = unserialize($serialized);
    }


    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return userPersonalM
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal = null)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }
    
    public function __toString()
    {
        return $this->getPersLoginu();
    }
}
