<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cet\NominaBundle\Entity\Formula
 *
 * @ORM\Entity(repositoryClass="FormulaRepository")
 * @ORM\Table(name="sis_nomina.formula", indexes={@ORM\Index(name="fk_variable_has_concepto_concepto1_idx", columns={"concepto_id"}), @ORM\Index(name="fk_variable_has_concepto_variable1_idx", columns={"variable_id"})})
 */
class Formula
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="variable_id", type="integer", nullable=false)
     */
    private $variableId;

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="concepto_id", type="integer", nullable=false)
     */
    private $conceptoId;

    /**
     * Posicion para ordenar operandos y operadores en la formula
     *
     * @var integer
     * @ORM\Column(name="posicion", type="integer", nullable=true)
     */
    private $posicion;

    /**
     * @ORM\ManyToOne(targetEntity="Variable", inversedBy="formulas")
     * @ORM\JoinColumn(name="variable_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_variable_has_concepto_variable1;

    /**
     * @ORM\ManyToOne(targetEntity="Concepto", inversedBy="formulas")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_variable_has_concepto_concepto1;

    public function __construct()
    {
    }


    /**
     * Set variableId
     *
     * @param integer $variableId
     * @return Formula
     */
    public function setVariableId($variableId)
    {
        $this->variableId = $variableId;

        return $this;
    }

    /**
     * Get variableId
     *
     * @return integer 
     */
    public function getVariableId()
    {
        return $this->variableId;
    }

    /**
     * Set conceptoId
     *
     * @param integer $conceptoId
     * @return Formula
     */
    public function setConceptoId($conceptoId)
    {
        $this->conceptoId = $conceptoId;

        return $this;
    }

    /**
     * Get conceptoId
     *
     * @return integer 
     */
    public function getConceptoId()
    {
        return $this->conceptoId;
    }

    /**
     * Set posicion
     *
     * @param integer $posicion
     * @return Formula
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return integer 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Set fk_variable_has_concepto_variable1
     *
     * @param \Cet\NominaBundle\Entity\Variable $fkVariableHasConceptoVariable1
     * @return Formula
     */
    public function setFkVariableHasConceptoVariable1(\Cet\NominaBundle\Entity\Variable $fkVariableHasConceptoVariable1)
    {
        $this->fk_variable_has_concepto_variable1 = $fkVariableHasConceptoVariable1;

        return $this;
    }

    /**
     * Get fk_variable_has_concepto_variable1
     *
     * @return \Cet\NominaBundle\Entity\Variable 
     */
    public function getFkVariableHasConceptoVariable1()
    {
        return $this->fk_variable_has_concepto_variable1;
    }

    /**
     * Set fk_variable_has_concepto_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkVariableHasConceptoConcepto1
     * @return Formula
     */
    public function setFkVariableHasConceptoConcepto1(\Cet\NominaBundle\Entity\Concepto $fkVariableHasConceptoConcepto1)
    {
        $this->fk_variable_has_concepto_concepto1 = $fkVariableHasConceptoConcepto1;

        return $this;
    }

    /**
     * Get fk_variable_has_concepto_concepto1
     *
     * @return \Cet\NominaBundle\Entity\Concepto 
     */
    public function getFkVariableHasConceptoConcepto1()
    {
        return $this->fk_variable_has_concepto_concepto1;
    }
}
