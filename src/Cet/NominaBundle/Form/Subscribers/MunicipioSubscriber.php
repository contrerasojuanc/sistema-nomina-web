<?php

namespace Cet\NominaBundle\Form\Subscribers;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityRepository;
use Cet\NominaBundle\Entity\Estado;

class MunicipioSubscriber implements EventSubscriberInterface
{
    private $factory;

    public function __construct(FormFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_BIND     => 'preBind'
        );
    }

    private function addMunicipioForm($form, $municipio, $estado)
    {
        $form->add($this->factory->createNamed('Municipio','entity', $municipio, array(
            'class'         => 'CetNominaBundle:Municipio',
            'empty_value'   => 'Seleccione el Municipio',
            'mapped'        => false,
            'auto_initialize'=> false,
            'label'=> 'Municipio',
            'attr' => array('help'=>'Seleccione el municipio donde se encuentra su lugar de residencia o vivienda. Ejemplo: San Cristóbal, Cárdenas, Guásimos'),
            'query_builder' => function (EntityRepository $repository) use ($estado) {
                $qb = $repository->createQueryBuilder('mu')
                    ->innerJoin('mu.fk_municipio_estado1', 'es');
                if ($estado instanceof Estado) {
                    $qb->where('mu.fk_municipio_estado1 = :estado')
                    ->setParameter('estado', $estado);
                } elseif (is_numeric($estado)) {
                    $qb->where('es.id = :estado')
                    ->setParameter('estado', $estado);
                } else {
                    $qb->where('es.nombre = :estado')
                    ->setParameter('estado', null);
                }                            
                return $qb;
            }
        )));   
    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $parroquia = ($data->getFkViviendaParroquia1()) ? $data->getFkViviendaParroquia1() : null ;
        $municipio = ($parroquia) ? $parroquia->getFkParroquiaMunicipio1() : null ;
        $estado = ($municipio) ? $municipio->getFkMunicipioEstado1() : null ;
        
        $this->addMunicipioForm($form, $municipio, $estado);
    }

    public function preBind(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $municipio = array_key_exists('Municipio', $data) ? $data['Municipio'] : null;
        $estado = array_key_exists('Estado', $data) ? $data['Estado'] : null;
        $this->addMunicipioForm($form, $municipio, $estado);
    }
}