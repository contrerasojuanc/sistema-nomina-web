<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Cet\NominaBundle\Form\SecuredType;


class SecuredController extends Controller
{
    public function loginAction(Request $request)
    {
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        $form = $this->createForm(new SecuredType(), null, array(
            'action' => $this->generateUrl('_verificar'),            
            'method' => 'POST',
            'attr' => array('id'=>'login') 
            ));
        $form->handleRequest($request);
        
        return $this->render(
            'CetNominaBundle:Secured:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
                'form' => $form->createView(),
            )
        );
    }

    public function securityCheckAction()
    {
        // The security layer will intercept this request
//        return $this->render(
//            'CetNominaBundle:Archivo:index.html.twig'
//        );
    }

    public function logoutAction()
    {
        // The security layer will intercept this request
    }

}
