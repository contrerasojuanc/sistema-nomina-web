<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cet\NominaBundle\Entity;

/**
 * Description of VariableFormula
 *
 * @author juan.contreras
 */
class VariableFormula {
    private $valor;
    private $variables=array();
    
    public function __construct()
    {
        $this->valor=0;
        $this->variables=array();
    }
    
    public function getValor()
    {
        return $this->valor;
    }
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $valor;
    }
    
    public function setVariable($nombre,$valor)
    {
        $this->variables[$nombre] = $valor;
        return 0;
    }
    
    public function getVariable($nombre)
    {
        return $this->variables[$nombre];        
    }
    
    public function __toString() {
        return "";
    }
}
