<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;

use Cet\NominaBundle\Entity\Nomina;
use Cet\NominaBundle\Entity\EnLetras;
use Cet\NominaBundle\Entity\Respaldo;
use Cet\NominaBundle\Entity\Archivo;
use Cet\NominaBundle\Form\NominaType;

use Cet\NominaBundle\Entity\Variable;
use Cet\NominaBundle\Form\VariableType;
use Cet\NominaBundle\Entity\HistoricoVariable;

use Cet\NominaBundle\Entity\Concepto;
use Cet\NominaBundle\Entity\HistoricoConcepto;
use Cet\NominaBundle\Form\ConceptoType;

use Cet\NominaBundle\Entity\Vacaciones;
use Cet\NominaBundle\Entity\PrestacionesSocialesGeneral;
use Cet\NominaBundle\Entity\PrestacionesSociales;
use Cet\NominaBundle\Entity\Personal;
use Cet\NominaBundle\Entity\VariableFormula;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\ParsedExpression;

use Symfony\Component\Debug\ErrorHandler;
use Cet\NominaBundle\Entity\LenguajeFormula;

use DateTime;

/**
 * Nomina controller.
 *
 */
class NominaController extends Controller
{
    /**
     * Lists all Nomina entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

//        $entities = $em->getRepository('CetNominaBundle:Nomina')->findAll();
        $entities = $em->getRepository('CetNominaBundle:Nomina')->findAllOrderedByFecha();
        $historicoConceptos=array();
        foreach($entities as $entity){
            $historicoConceptos[$entity->getId()] = $em->getRepository('CetNominaBundle:HistoricoConcepto')->findOneBy(array('nominaId' => $entity->getId()));
        }
        
        return $this->render('CetNominaBundle:Nomina:index.html.twig', array(
            'entities' => $entities,
            'historicoConceptos' => $historicoConceptos,
        ));
    }
    /**
     * Creates a new Nomina entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Nomina();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //Se verifica que exista un tipo de nomina para seleccionar a los funcionarios para el calculo
            $idtiponomina=$entity->getFkNominaPlantillaNomina1()->getFkPlantillaNominaTipoNomina1()->getId();
            if(!isset($idtiponomina) OR $idtiponomina==null)
            {   
                return $this->render('CetNominaBundle:Nomina:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                ));
            }
            
            $em = $this->getDoctrine()->getManager();
            
            //Mantener el atributo mes actualizado
            $mes = intval($entity->getDesde()->format('n'));
            $entity->setMes($mes);
            
            $em->persist($entity);
            $em->flush();
            $idnomina=$entity->getId();
            $vacacionesSel=$entity->getVacaciones();
            if(!$vacacionesSel)
                $vacacionesSel=array();
            foreach($vacacionesSel as $v){
                $arregloVacacion = $em->getRepository('CetNominaBundle:Vacaciones')->findById($v->getId());
                foreach($arregloVacacion as $vacacion){
                    $vacacion->setFkVacaciones($entity);
                    $em->persist($vacacion);
                }
            }  
            $em->flush();
            
//            $idnomina=$entity->getId();
            
            
//            //Se ejecuta el procedimiento en todos los funcionarios segun el tipo de nomina para actualizar historico_varaible e Historico_concepto
//            $variables = $em->getRepository('CetNominaBundle:Variable')->findAll();
//                        
//            //Obtener solo los conceptos  de la plantilla
//            $idplantillanomina=$entity->getFkNominaPlantillaNomina1()->getId();
//            $plantilla = $em->getRepository('CetNominaBundle:PlantillaNomina')->findById($idplantillanomina);
//            foreach ($plantilla as $dato){
//                    $conceptosplantilla=$dato->getConceptos(); // conceptos fijos
//                    foreach($conceptosplantilla as $conce){
//                        $vectorFijos[]=$conce->getId();
//                    }
//            }
//            $consulta = $em->createQuery('select p.id from CetNominaBundle:Personal p join p.informacionLaboral il where il.fk_informacion_laboral_tipo_nomina1=:tiponomina');
//            $consulta->setParameter('tiponomina',$idtiponomina);
//            
//            $personales = $consulta->getResult();
//
//            //$personales = $em->getRepository('CetNominaBundle:InformacionLaboral')->findBy(array('fk_informacion_laboral_tipo_nomina1',$idtiponomina));
////            $personales = $em->getRepository('CetNominaBundle:Personal')->findBy(array(''));
//            
////            $clientevariable = new VariableController();
////            $clienteconcepto = new ConceptoController();
////            $clientevariable->setContainer($this->container);
////            $clienteconcepto->setContainer($this->container);
//            //Actualiza historico_variable
//            foreach ($personales as $personal){
//                foreach ($variables as $variable){                                        
//                    //$clientevariable->calculaAction($variable->getId(), $personal['id']);
//                    
//                    $this->calculaVariable($variable->getId(), $personal['id']);
////                    $this->forward('variable_calcula',array($variable->getId(), $personal->getId()));
//                }
//            }
//            //Actualiza historico_concepto
//            foreach ($personales as $personal){
//                //Se determina que conceptos se van a calcular por funcionario de acuerdo a la personalizacion de los mismos
//                $plantillapersonalizada = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')->findBy(array('fk_concepto_has_plantilla_nomina_plantilla_nomina1'=>$idplantillanomina,'fk_concepto_has_plantilla_nomina_personal1'=>$personal['id']));
////                $plantillapersonalizada = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')->findAll();
////                foreach ($plantillapersonalizada as $datopersonalizado){
////                        $conceptosplantillapersonalizada=$datopersonalizado->getConceptos(); // conceptos fijos
////                        foreach($conceptosplantillapersonalizada as $conceper){
////                            $vectorPersonalizado[]=$conceper->getCodigo();
////                        }
////                }
//                $vectorPersonalizado=array();
//                foreach ($plantillapersonalizada as $datopersonalizado){                   
//                    $vectorPersonalizado[]=$datopersonalizado->getConceptoId(); // conceptos fijos
//                }
//                $vectorProcesar=array_merge( array_diff($vectorPersonalizado,$vectorFijos), array_diff($vectorFijos,$vectorPersonalizado));
//                foreach ($vectorProcesar as $concepto){
////                    $clienteconcepto->calculaAction($concepto, $personal['id'],$entity->getId()); 
//                    
//                    $this->calculaConcepto($concepto, $personal['id'],$entity->getId()); 
//                }
//            }

        return $this->redirect($this->generateUrl('nomina_show_nomina',array('id'=>$entity->getId())));
            
        }

        return $this->render('CetNominaBundle:Nomina:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Verifica el estado del calculo de nomina.
     *
     */
    public function verificarAction()
    {
        $session=$this->get('session');
        return new JsonResponse(array('mensaje' => $session->get('mensaje'), 'valor' => $session->get('valor'), 'total' => $session->get('total')));
//        return new JsonResponse($this->calculando);
    }
    
    /**
     * Calcula una nomina.
     *
     */
    public function calcularAction($id)
    {
        //Se establecen a 0 las variables set_time_limit y max_execution_time en php.ini
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);

        $session = $this->get('session');
        $session->set('mensaje', 'Inicio del cálculo de Nómina');
        $session->set('valor', 0);
        $session->set('total', '0 de 0');
        $session->save();
        session_write_close();

//        $todasVariables = $em->getRepository('CetNominaBundle:Variable')->findAll();
        $cv = $em->createQuery('select v.nombre,v.sql from CetNominaBundle:Variable v');
        $todasVariables = $cv->getResult();
        
        $todosConceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();

        ////////////////////////////
//      $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
        foreach ($todosConceptos as $concep){
            $nombreConcepto=$concep->getIdentificador();
            $vector_conceptos[]=$nombreConcepto;
            $vector_conceptos_id[]=$concep->getId();
        }

        
        //Se verifica que exista un tipo de nomina para seleccionar a los funcionarios para el calculo
        $idtiponomina = $entity->getFkNominaPlantillaNomina1()->getFkPlantillaNominaTipoNomina1()->getId();
        if(!isset($idtiponomina) OR $idtiponomina==null)
        {   
            return new JsonResponse("No existe el tipo de nómina");
        }

        $idnomina=$entity->getId();

        //Se ejecuta el procedimiento en todos los funcionarios segun el tipo de nomina para actualizar historico_varaible e Historico_concepto
        $variables = $em->getRepository('CetNominaBundle:Variable')->findAll();

        //Obtener solo los conceptos  de la plantilla
        $idplantillanomina=$entity->getFkNominaPlantillaNomina1()->getId();
        $plantilla = $em->getRepository('CetNominaBundle:PlantillaNomina')->findById($idplantillanomina);
        foreach ($plantilla as $dato){
                $conceptosplantilla=$dato->getConceptos(); // conceptos fijos
                if($dato->getFkTipocalculo())
                    $tipoPlantillaNombre=$dato->getFkTipocalculo()->getNombre(); // Get tipo_calculo object
                else
                    $tipoPlantillaNombre="";
                foreach($conceptosplantilla as $conce){
                    $vectorFijos[]=$conce->getId();
                }
                if(!isset($vectorFijos) OR $vectorFijos==null)
                {   
                    return new JsonResponse("Plantilla de Nomina sin conceptos fijos");
                }
        }
        if ( preg_match('/Vacacional/',$tipoPlantillaNombre)){
            //Buscar  las vacaciones que corresponden
            return($this->procesarVacacioneAction($id,$variables,$todasVariables,$vectorFijos, $vector_conceptos,$vector_conceptos_id,$todosConceptos,$session));
        }
        elseif(preg_match('/Prestaciones/',$tipoPlantillaNombre)){
            return($this->procesarPrestacionAction($id,$variables,$todasVariables,$vectorFijos, $vector_conceptos,$vector_conceptos_id,$todosConceptos,$session));
        }
        $consulta = $em->createQuery("SELECT p.id "
                . "FROM CetNominaBundle:Personal p "
                . "JOIN p.informacionLaboral il "
                . "WHERE il.fk_informacion_laboral_tipo_nomina1 = :tiponomina");
        $consulta->setParameter('tiponomina',$idtiponomina);

        $personales = $consulta->getResult();

        //$personales = $em->getRepository('CetNominaBundle:InformacionLaboral')->findBy(array('fk_informacion_laboral_tipo_nomina1',$idtiponomina));
//            $personales = $em->getRepository('CetNominaBundle:Personal')->findBy(array(''));

//        $clientevariable = new VariableController();
//        $clienteconcepto = new ConceptoController();
//        $clientevariable->setContainer($this->container);
//        $clienteconcepto->setContainer($this->container);

        $indice=1;
        //Actualiza historico_variable
        foreach ($personales as $personal){
            unset($_SESSION);
            $session = $this->get('session');
            $session->start();
            $session->set('mensaje', 'Calculando Variables de Funcionario: '.$personal['id']);
            $session->set('valor', ($indice*100)/count($personales));
            $session->set('total', $indice.' de '.count($personales));
            $session->save();
            session_write_close();
            foreach ($variables as $variable){                                        
//                $clientevariable->calculaAction($variable->getId(), $personal['id']);    

                $this->calculaVariable($variable->getId(), $personal['id'],$entity);
//                    $this->forward('variable_calcula',array($variable->getId(), $personal->getId()));
            }
            $indice++;
        }
        
        $indice=1;
        
        $totalAsignacion = 0;
        $totalDeduccion = 0;
        $totalNeto = 0;
        //Actualiza historico_concepto
        foreach ($personales as $personal){
            $vector_variables = array();
            $vector_variables_patronal = array();
            $vector_variables_acumulado = array();
            $vector_evaluado = array();
            $vector_evaluado_patronal = array();
            $vector_evaluado_acumulado = array();
            
            unset($_SESSION);
            $session = $this->get('session');
            $session->start();
            $session->set('mensaje', 'Calculando Conceptos de Funcionario: '.$personal['id']);
            $session->set('valor', ($indice*100)/count($personales));
            $session->set('total', $indice.' de '.count($personales));
            $session->save();
            session_write_close();
                
            //Se determina que conceptos se van a calcular por funcionario de acuerdo a la personalizacion de los mismos
            $plantillapersonalizada = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')->findBy(array('fk_concepto_has_plantilla_nomina_plantilla_nomina1'=>$idplantillanomina,'fk_concepto_has_plantilla_nomina_personal1'=>$personal['id']));
//                $plantillapersonalizada = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')->findAll();
//                foreach ($plantillapersonalizada as $datopersonalizado){
//                        $conceptosplantillapersonalizada=$datopersonalizado->getConceptos(); // conceptos fijos
//                        foreach($conceptosplantillapersonalizada as $conceper){
//                            $vectorPersonalizado[]=$conceper->getCodigo();
//                        }
//                }

                //Asignamos las variables de cada funcionario en memoria utilizando $$ como variables dinamicas
                
                foreach($todasVariables as $objeto){       
                    $sqlvariable = $objeto['sql'];
                    
                    //Para calcular la variable con la fecha de la nómina     
                    $fechaNomina = $entity->getHasta();
                    
                    $nombreVariable = $objeto['nombre'];
        //            $sqlvariable=$objeto->getFkVariableHasPersonalVariable1()->getSql();
        //            $nombreVariable=$objeto->getFkVariableHasPersonalVariable1()->getNombre();

                try {
                    if(preg_match("/CURRENT_DATE()/i",$sqlvariable)){
//                        $sqlvariable = str_replace("CURRENT_DATE()", ":fecha_nomina", $sqlvariable);
                        $sqlvariable = preg_replace("/CURRENT_DATE\(\)/i", "(:fecha_nomina)", $sqlvariable);
                        $consultasql = $em->createQuery($sqlvariable);
//                        $consultasql->setParameter('fecha_nomina',$fechaNomina,\Doctrine\DBAL\Types\Type::DATETIME);
                        $consultasql->setParameter('fecha_nomina',$fechaNomina,\Doctrine\DBAL\Types\Type::DATETIME);
                    }else{
                        $consultasql = $em->createQuery($sqlvariable);
                    }                       
                    
                    $consultasql->setParameter('personal_cedula', $personal['id']);
                    
                    $consultasql->setMaxResults(1);
                    
                    $valorVariable=0;
                    if ($consultasql->getResult()){
                        foreach($consultasql->getSingleResult() as $dato){
                            if(!is_object($dato)){
                                $valorVariable=$dato;
                            }                    
                            elseif($dato instanceof \DateTime){
        //                        $valorVariable=$dato->format('d/m/Y');
        //                        $valorVariable=new \DateTime($valorVariable);
                                $valorVariable=$dato;
                            }
                        }
                    }            
//                    $$nombreVariable=$valorVariable;
                    $vector_variables[$nombreVariable]=$valorVariable;
                    
                }catch (\Exception $e) {
                    //$info = toString($e);
                    $info = $e->getMessage();
                    
                    unset($_SESSION);
                    $session = $this->get('session');
                    $session->start();
                    $session->set('mensaje', "Error calculando variable:$nombreVariable ".$info);
                    $session->set('valor', 0);
                    $session->set('total', "0 de 0");
                    $session->save();
                    session_write_close();
                    
                }

                }
            /////////////////////////////
            //Inicio Variables Predefinidas
            /////////////////////////////
                //Cuenta cantidad de lunes en un lapso de fechas
                $helper = $this->get('cet.nomina.globales');
                $lunes = 1;
                $lunes = $helper->countDays(1,strtotime($entity->getDesde()->format('d-m-Y')),strtotime($entity->getHasta()->format('d-m-Y')));

//                $lunes=$this->countDays(1,strtotime($entity->getDesde()->format('d-m-Y')),strtotime($entity->getHasta()->format('d-m-Y')));
                $vector_variables["cantidad_lunes"]=$lunes;
                
                $vector_variables["primera_quincena"] = true;
                //Verifica si Fecha Desde es mayor que 9 probablemente sea segunda quincena
                if( $entity->getDesde()->format('d') > 6 )
                    $vector_variables["primera_quincena"] = false;
            /////////////////////////////
            //Fin Variables Predefinidas
            /////////////////////////////
                
            $vectorPersonalizado=array();
            foreach ($plantillapersonalizada as $datopersonalizado){                   
                $vectorPersonalizado[]=$datopersonalizado->getConceptoId(); // conceptos fijos
            }
            
            $vectorProcesar=array_merge( array_diff($vectorPersonalizado,$vectorFijos), array_diff($vectorFijos,$vectorPersonalizado));
            
            //Establece todos los conceptos eliminados de manera perzonalizada a cero
            $vectorEliminados = array_diff($vectorFijos,$vectorProcesar);
            foreach ($vectorEliminados as $eliminado){
                $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($eliminado);
                $vector_variables[$objeto->getIdentificador()] = 0;
                
                //Establecer el concepto como evaluado
                $vector_evaluado[] = $objeto->getIdentificador();
                
                //Busca el concepto eliminado dentro de la lista de conceptos y lo elimina
//                $clave = array_search($objeto->getIdentificador(),$vector_conceptos);
//                if( $clave !== false ){
//                    unset($vector_conceptos[$clave]);
//                    $vector_conceptos = array_values($vector_conceptos);
//                    
//                }                        
            }

            //Establece los valores de los conceptos a fuerza, según datos variables
            foreach ($vector_conceptos_id as $idconcepto){
                //Buscar si el concepto esta en datos variables para esa persona
                $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datos_variables_concepto1'=>$idconcepto,'fk_datos_variables_personal1'=>$personal['id']));
                if($datosvariables!=null){
                    foreach($datosvariables as $datosvariable){                        
                        if($datosvariable->getSeccionAfectacion()=='principal'){
                            $valor = $datosvariable->getValor();
                            
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables[$objeto->getIdentificador()] = $valor;
                            $vector_evaluado[] = $objeto->getIdentificador();
                        }
                        elseif($datosvariable->getSeccionAfectacion()=='patronal'){
                            $valorPatronal = $datosvariable->getValor();
                             
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables_patronal[$objeto->getIdentificador()] = $valorPatronal;
                            $vector_evaluado_patronal[] = $objeto->getIdentificador();
                        }
                        elseif($datosvariable->getSeccionAfectacion()=='acumulado'){
                            $acumulado = $datosvariable->getValor();
                             
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables_acumulado[$objeto->getIdentificador()] = $acumulado;
                            $vector_evaluado_acumulado[] = $objeto->getIdentificador();
                        }
                    }
                }
            }
            
            $var=new VariableFormula();        
            $vector_variables['Variable']=$var;
            foreach ($vectorProcesar as $concepto){
//                $clienteconcepto->calculaAction($concepto, $personal['id'],$entity->getId()); 

//                $this->calculaConcepto($concepto, $personal['id'],$entity,$conceptoEntity,$personalEntity); 
                $this->calculaConcepto($concepto, $personal['id'],$idnomina,$todasVariables,$todosConceptos,$vector_variables,$vector_variables_patronal,$vector_variables_acumulado,$vector_conceptos,$vector_evaluado,$vector_evaluado_patronal,$vector_evaluado_acumulado); 
            }
            $indice++;
        }
       
        $indice--;
        unset($_SESSION);
        $session = $this->get('session');
        $session->start();
        $session->set('mensaje', 'La nómina ha sido completamente calculada');
        $session->set('valor', 100);
        $session->set('total', $indice.' de '.$indice);
        $session->save();
        session_write_close();
        
        unset($_SESSION);        
        return new JsonResponse(array('mensaje' => 'La nómina ha sido completamente calculada','valor' => 100,'total' => $indice.' de '.$indice));
          
    }
    
    /**
     * Reversar una nomina.
     *
     */
    public function reversarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CetNominaBundle:HistoricoConcepto')->findBy(array('nominaId' => $id));
        
        foreach ($entities as $entity) {
            $em->remove($entity);
        }
        $em->flush();

        $response = $this->forward('CetNominaBundle:Nomina:index');
        return $response;
             
    }
    
    /**
    * Creates a form to create a Nomina entity.
    *
    * @param Nomina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Nomina $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new NominaType(), $entity, array(
            'action' => $this->generateUrl('nomina_create'),
            'method' => 'POST',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new Nomina entity.
     *
     */
    public function newAction()
    {
        $entity = new Nomina();
        $form   = $this->createCreateForm($entity);
        
        $em = $this->getDoctrine()->getManager();

        return $this->render('CetNominaBundle:Nomina:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Genera los Recibos de pago
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Nomina.');
        }

        $deleteForm = $this->createDeleteForm($id);
        
        
        //Redirige a la vista de los recibos de pago
        return $this->render('CetNominaBundle:Nomina:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }
    
    /**
     * Finds and displays a Nomina entity.
     *
     */
    public function showNominaAction($id,$imprimir=null,$abono=null,$carta=null,$almacenar=null,$recibos=null,$idRecibo=null)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Nomina.');
        }

        //Unidades Organizativas padres (Direcciones)
        $root_uo = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->findBy(array('fk_unidad_organizativa_unidad_organizativa1' => null), array('nivel' => 'asc'));
        
        //Ruta de cada unidad organizativa
        //$inner_uo = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->findAll();        
        $inner_uo = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->findBy(array(),array('nivel' => 'asc'));
        $rutas=array();
        foreach ($root_uo as $value) {            
            foreach ($inner_uo as $rutauo) {                
                $arreglouo=array();
                $nombreuo = $rutauo->getNombre();
                $claveuo = $rutauo->getId();
                while ($claveuo != null) {
                    $consultas3 = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($claveuo);
                    $claveuo = $consultas3->getFkUnidadOrganizativaUnidadOrganizativa1();
                    $nuevonombreuo = $consultas3->getNombre();
                    $nuevoiduod = $consultas3->getId();
                    //Arreglo de todas las unidades organizativas del funcionario
                    $arreglouo[$nuevoiduod] = $nuevonombreuo;
                }
                if($value->getId()==$nuevoiduod){
                    $rutas[]=array_reverse($arreglouo,true);
                }
            }
        }
        
                
        //Recibos de pago
//        $consultaInformacion = $em->createQuery('SELECT p,hc,c,il,huo,uo FROM CetNominaBundle:Personal p JOIN p.historicoConceptos hc JOIN p.informacionLaboral il JOIN il.historicoUnidadOrganizativas huo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo JOIN hc.fk_historico_concepto_concepto1 c WHERE hc.fk_historico_concepto_nomina1='.$id.' ORDER BY hc.fk_historico_concepto_concepto1');

//        $consultaInformacion = $em->createQuery('SELECT p,hc,c FROM CetNominaBundle:Personal p JOIN p.historicoConceptos hc JOIN hc.fk_historico_concepto_concepto1 c WHERE hc.fk_historico_concepto_nomina1='.$id.' ORDER BY c.codigo');
        
//        $consultaInformacion = $em->createQuery('SELECT p,hc,c,hca,il,ca FROM CetNominaBundle:Personal p JOIN p.historicoConceptos hc JOIN hc.fk_historico_concepto_concepto1 c JOIN p.informacionLaboral il JOIN il.historicoCargos hca JOIN hca.fk_informacion_laboral_has_cargo_cargo1 ca WHERE hc.fk_historico_concepto_nomina1='.$id.' ORDER BY ca.jerarquia,ca.nivel DESC,p.id,c.codigo');

        $consultaInformacion = $em->createQuery("SELECT p,hc,c,hca,il,ca "
//        $consultaInformacion = $em->createQuery("SELECT p,hc,c,hca,il,ca.nombre AS cargooriginal,ca.nivel AS niveloriginal "
                . "FROM CetNominaBundle:Personal p "
                . "LEFT JOIN p.historicoConceptos hc "
                . "LEFT JOIN hc.fk_historico_concepto_concepto1 c "
                . "LEFT JOIN p.informacionLaboral il "
                . "LEFT JOIN il.historicoCargos hca "
                . "JOIN hca.fk_informacion_laboral_has_cargo_cargo1 ca "
                . "WHERE hc.fk_historico_concepto_nomina1='$id' "
                . "AND hca.fechaInicio IN "
                . "( "
                    . "SELECT MAX(hc2.fechaInicio) " 
                    . "FROM CetNominaBundle:HistoricoCargo hc2 "
                    . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = il.id "
                    . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END))"
                    . "AND ( hc2.esEncargado = 'f' OR hc2.esEncargado IS NULL ) "
                . ") "
                . "ORDER BY ca.jerarquia,ca.nivel DESC,ca.sueldo DESC,p.id,c.codigo");        
        $informacion = $consultaInformacion->getResult();
        
        $personacargo=array();
        foreach($informacion as $persona)
        {   
            //Datos laborales
            $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $persona->getId()));
            
            $consulta = $em->createQuery("SELECT hc,il.cuentaNomina AS cuentaNomina,il.tipoCuentaNomina AS tipoCuentaNomina,il.cuentaFideicomiso AS cuentaFideicomiso,il.tipoCuentaFideicomiso AS tipoCuentaFideicomiso,c.nombre AS cargo,c.nivel AS nivel,uo.id AS iduo,uo.nombre AS nombre "
                . "FROM CetNominaBundle:HistoricoCargo hc "
                . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c "
                . "LEFT JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
                . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
                . "LEFT JOIN il.personal p "
                . "WHERE  p.id = ".$persona->getId() ." "
                . "AND hc.fechaInicio IN "
                    . "("
                        . "SELECT MAX(hc2.fechaInicio) " 
                        . "FROM CetNominaBundle:HistoricoCargo hc2 "
                        . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = il.id "
                        . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END)) "
                    . ")"
                );
            $historicocargo = $consulta->getResult();

            //$consulta1 = $em->createQuery("SELECT huo.id,uo.id as iduo, uo.nombre FROM CetNominaBundle:HistoricoUnidadOrganizativa huo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_informacion_la1 il JOIN il.personal p WHERE p.id='".$persona->getId()."' AND huo.fechaFin IS NULL");
//            $consulta1 = $em->createQuery("SELECT huo.id,uo.id as iduo, uo.nombre "
//                    . "FROM CetNominaBundle:HistoricoUnidadOrganizativa huo "
//                    . "JOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
//                    . "JOIN huo.fk_unidad_organizativa_has_informacion_laboral_informacion_la1 il "
//                    . "JOIN il.personal p "
//                    . "WHERE p.id='".$persona->getId()."' "
//                    . "AND huo.fechaInicio in ("
//                        . "select max(huo2.fechaInicio) "
//                        . "from CetNominaBundle:HistoricoUnidadOrganizativa huo2 "
//                        . "where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)");
//            
//            $historicouo = $consulta1->getResult();
           
            $cargo="Sin Asignar";
            $cuenta="Sin Asignar";
            $tipocuenta="Sin Asignar";
            $cuentaFideicomiso="Sin Asignar";
            $tipocuentaFideicomiso="Sin Asignar";
            $nivel="";
            $romano="";
            
            $arreglouo=array();
            $uo="SIN UNIDAD ORGANIZATIVA";
            $uod="SIN UNIDAD ORGANIZATIVA";
            $iduo=99;
            $iduod=99;
                        
            foreach($historicocargo as $hc){
                $cargo=$hc['cargo'];
                $nivel=$hc['nivel'];
                $cuenta=$hc['cuentaNomina'];
                $tipocuenta=$hc['tipoCuentaNomina'];
                $cuentaFideicomiso=$hc['cuentaFideicomiso'];
                $tipocuentaFideicomiso=$hc['tipoCuentaFideicomiso'];
                
                $romano="";
                if($nivel==1)$romano='I';
                elseif($nivel==2)$romano='II';
                elseif($nivel==3)$romano='III';
                elseif($nivel==4)$romano='IV';
                elseif($nivel==5)$romano='V';
                elseif($nivel==6)$romano='VI';
                elseif($nivel==7)$romano='VII';
                elseif($nivel==8)$romano='VIII';
                elseif($nivel==9)$romano='IX';
                elseif($nivel==10)$romano='X';
                
                $uo=$hc['nombre']; 
                $iduo=$hc['iduo'];
                $claveforanea=$hc['iduo'];
                while($claveforanea!=null){
                    $consultas2 = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($claveforanea);
                    $claveforanea=$consultas2->getFkUnidadOrganizativaUnidadOrganizativa1();
                    $uod=$consultas2->getNombre();
                    $iduod=$consultas2->getId();
                    //Arreglo de la ruta de las unidades organizativas del funcionario
                    $arreglouo[$iduod]=$uod;
                }
            }
            
            if(count($arreglouo)==0){
               $arreglouo[$iduod]=$uod; 
            }
            
            $cargooriginal="Sin Asignar";
            $niveloriginal="";
            foreach($persona->getinformacionLaboral()->gethistoricoCargos() as $historicoCargo){
                $cargooriginal = $historicoCargo->getFkInformacionLaboralHasCargoCargo1()->getNombre();
                $niveloriginal = $historicoCargo->getFkInformacionLaboralHasCargoCargo1()->getNivel();
            }

            $romanooriginal="";
            if($niveloriginal==1)$romanooriginal='I';
            elseif($niveloriginal==2)$romanooriginal='II';
            elseif($niveloriginal==3)$romanooriginal='III';
            elseif($niveloriginal==4)$romanooriginal='IV';
            elseif($niveloriginal==5)$romanooriginal='V';
            elseif($niveloriginal==6)$romanooriginal='VI';
            elseif($niveloriginal==7)$romanooriginal='VII';
            elseif($niveloriginal==8)$romanooriginal='VIII';
            elseif($niveloriginal==9)$romanooriginal='IX';
            elseif($niveloriginal==10)$romanooriginal='X';
                
//            $cargo = $cargo." ".$romano;
            $cargo = $cargooriginal." ".$romanooriginal;
            $personacargo[$persona->getId()]['cuenta']=$cuenta;
            $personacargo[$persona->getId()]['tipocuenta']=$tipocuenta;
            $personacargo[$persona->getId()]['cuentaFideicomiso']=$cuentaFideicomiso;
            $personacargo[$persona->getId()]['tipocuentaFideicomiso']=$tipocuentaFideicomiso;
            $personacargo[$persona->getId()]['cargo']=$cargo;
            $personacargo[$persona->getId()]['uo']=$uo;
            $personacargo[$persona->getId()]['iduo']=$iduo;
            $personacargo[$persona->getId()]['uod']=$uod;
            $personacargo[$persona->getId()]['iduod']=$iduod;
            $personacargo[$persona->getId()]['arreglo']=array_reverse($arreglouo,true);
        }

        //Calculo de totales por funcionario, por direccion y por nomina
        $totalFuncionarioAsignacion = array();
        $totalFuncionarioDeduccion = array();
        $totalFuncionarioPatronal = array();
        $totalFuncionarioNeto = array();
        
        $totalDireccionAsignacion = array();
        $totalDireccionDeduccion = array();
        $totalDireccionPatronal = array();
        $totalDireccionNeto = array();
        
        $totalNominaAsignacion = 0;
        $totalNominaDeduccion = 0;
        $totalNominaPatronal = 0;                                
        $totalNominaNeto = 0;
        
        $totalNominaPersonal = 0;
        $totalNominaConcepto = array(); 
        $totalDireccionConcepto = array();
        
        $arregloPersona = array();
        $arregloOrdenado = array();
        
        foreach($root_uo as $unidadOrganizativa){
            $reciboNumero = 0;
            $hayfuncionario = false;
            
            $DireccionAsignacion = 0;
            $DireccionDeduccion = 0;
            $DireccionPatronal = 0;
            $DireccionNeto = 0;            
            
            
            foreach($rutas as $ruta){
                $grupo = true;
                foreach($informacion as $persona){
                    $reciboNumero=$reciboNumero + 1;
                    $cargo=$personacargo[$persona->getId()];
                    if ($cargo['iduod'] == $unidadOrganizativa->getId()){
                        $hayfuncionario = true;
                        
                        //Grupo por Unidad Organizativa 
                        if ($ruta == $cargo['arreglo']){
                            $totalAsignacion = 0;
                            $totalDeduccion = 0;
                            $totalPatronal = 0;
                            $totalAcumulado = 0;                                                        
                            foreach($persona->getHistoricoConceptos() as $conceptos){
                                if ($conceptos->getFkHistoricoConceptoConcepto1()->getTipo() == 'Asignación'){
                                    $totalAsignacion = $totalAsignacion+$conceptos->getMonto();
                                }
                                else{
                                    $totalDeduccion = $totalDeduccion+$conceptos->getMonto();
                                }
                                $totalPatronal = $totalPatronal+$conceptos->getMontoPatronal();
                                $totalAcumulado = $totalAcumulado+$conceptos->getAcumulado();
                                
                                //Bloque calculo detalle de conceptos por direccion
                                    $idConcepto = $conceptos->getFkHistoricoConceptoConcepto1()->getCodigo();
                                    $sumarConcepto = $conceptos->getMonto();
                                    $sumarPatronal = $conceptos->getMontoPatronal();
                                    $sumarAcumulado = $conceptos->getAcumulado();
                                    $sumarPersonas = 1;
                                    //Se utiliza la concatenacion con _ para poder utilizar un valor cadena como clave de las hash
                                    $arreglo2=array();
                                    $arreglo2[0]["_".$unidadOrganizativa->getId()]=array();
                                    if (isset($totalDireccionConcepto["_".$unidadOrganizativa->getId()])){
                                        $arreglo2[0]["_".$unidadOrganizativa->getId()]=$totalDireccionConcepto["_".$unidadOrganizativa->getId()];
                                    }
                                    if (isset($totalDireccionConcepto["_".$unidadOrganizativa->getId()]["_".$idConcepto])){
                                        $sumarConcepto = $totalDireccionConcepto["_".$unidadOrganizativa->getId()]["_".$idConcepto][3] + $conceptos->getMonto();
                                        $sumarPatronal = $totalDireccionConcepto["_".$unidadOrganizativa->getId()]["_".$idConcepto][4] + $conceptos->getMontoPatronal();
                                        $sumarAcumulado = $totalDireccionConcepto["_".$unidadOrganizativa->getId()]["_".$idConcepto][5] + $conceptos->getAcumulado();
                                        $sumarPersonas = $totalDireccionConcepto["_".$unidadOrganizativa->getId()]["_".$idConcepto][7] + 1;
                                    }
                                    $arreglo1=array();
                                    $arreglo1[0]["_".$idConcepto]=array($idConcepto,$conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion(),$conceptos->getFkHistoricoConceptoConcepto1()->getTipo(),$sumarConcepto,$sumarPatronal,$sumarAcumulado,$conceptos->getFkHistoricoConceptoConcepto1()->getEsAcumulado(),$sumarPersonas);
                                    
                                    //Agrega un nuevo concepto a la lista de conceptos totales por direccion
                                    $totalDireccionConcepto = array_merge($totalDireccionConcepto,$arreglo2[0]);
                                    asort($totalDireccionConcepto["_".$unidadOrganizativa->getId()]);
                                    $totalDireccionConcepto["_".$unidadOrganizativa->getId()] = array_merge($totalDireccionConcepto["_".$unidadOrganizativa->getId()],$arreglo1[0]);
                                    
                                //Bloque calculo detalle de conceptos por Nomina
                                    $sumarConcepto = $conceptos->getMonto();
                                    $sumarPatronal = $conceptos->getMontoPatronal();
                                    $sumarAcumulado = $conceptos->getAcumulado();
                                    $sumarPersonas = 1;
                                    //Se utiliza la concatenacion con _ para poder utilizar un valor cadena como clave de las hash
                                    if (isset($totalNominaConcepto["_".$idConcepto])){
                                        $sumarConcepto = $totalNominaConcepto["_".$idConcepto][3] + $conceptos->getMonto();
                                        $sumarPatronal = $totalNominaConcepto["_".$idConcepto][4] + $conceptos->getMontoPatronal();
                                        $sumarAcumulado = $totalNominaConcepto["_".$idConcepto][5] + $conceptos->getAcumulado();
                                        $sumarPersonas = $totalNominaConcepto["_".$idConcepto][7] + 1;
                                    }
                                    $arreglo=array();
                                    $arreglo[0]["_".$idConcepto]=array($idConcepto,$conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion(),$conceptos->getFkHistoricoConceptoConcepto1()->getTipo(),$sumarConcepto,$sumarPatronal,$sumarAcumulado,$conceptos->getFkHistoricoConceptoConcepto1()->getEsAcumulado(),$sumarPersonas);
                                    //Agrega un nuevo concepto a la lista de conceptos totales por direccion
                                    $totalNominaConcepto = array_merge($totalNominaConcepto,$arreglo[0]);
                            }
                            //Totales por funcionario
                                $totalFuncionarioAsignacion[$persona->getId()] = $totalAsignacion;
                                $totalFuncionarioDeduccion[$persona->getId()] = $totalDeduccion;
                                $totalFuncionarioPatronal[$persona->getId()] = $totalPatronal;
                                $totalFuncionarioNeto[$persona->getId()] = $totalAsignacion - $totalDeduccion;
                            
                            //Arreglo contentivo de cedula, nombre, cuenta nomina, tipo cuenta, y monto
                                $nombreFuncionario=$persona->getPrimerNombre()." ".$persona->getSegundoNombre()." ".$persona->getPrimerApellido()." ".$persona->getSegundoApellido();
                                $soloNombre = $persona->getPrimerNombre()." ".$persona->getSegundoNombre();
                                $soloApellido = $persona->getPrimerApellido()." ".$persona->getSegundoApellido();
                                $arregloPersona[]=array($persona->getId(),$nombreFuncionario,$cargo['cuenta'],$cargo['tipocuenta'],$totalFuncionarioNeto[$persona->getId()],$soloNombre,$soloApellido,$cargo['cuentaFideicomiso'],$cargo['tipocuentaFideicomiso']);
                                $arregloOrdenado[] = $persona; 
                            //Totales por Direccion
                                $DireccionAsignacion = $DireccionAsignacion + $totalAsignacion;
                                $DireccionDeduccion = $DireccionDeduccion + $totalDeduccion;
                                $DireccionPatronal = $DireccionPatronal + $totalPatronal;
                                $DireccionNeto = $DireccionNeto + ($totalAsignacion - $totalDeduccion);
                        }
                    }
                }
            }
            if ($hayfuncionario){
                $totalDireccionAsignacion[$unidadOrganizativa->getId()] = $DireccionAsignacion;
                $totalDireccionDeduccion[$unidadOrganizativa->getId()] = $DireccionDeduccion;
                $totalDireccionPatronal[$unidadOrganizativa->getId()] = $DireccionPatronal;
                $totalDireccionNeto[$unidadOrganizativa->getId()] = $DireccionNeto;
                
                $totalNominaAsignacion = $totalNominaAsignacion + $DireccionAsignacion;
                $totalNominaDeduccion = $totalNominaDeduccion + $DireccionDeduccion;
                $totalNominaPatronal = $totalNominaPatronal + $DireccionPatronal;
                $totalNominaNeto = $totalNominaNeto + $DireccionNeto;
            }
        }
        
        $total['totalFuncionarioAsignacion']=$totalFuncionarioAsignacion;
        $total['totalFuncionarioDeduccion']=$totalFuncionarioDeduccion;
        $total['totalFuncionarioPatronal']=$totalFuncionarioPatronal;
        $total['totalFuncionarioNeto']=$totalFuncionarioNeto;
        
        $total['totalDireccionAsignacion']=$totalDireccionAsignacion;
        $total['totalDireccionDeduccion']=$totalDireccionDeduccion;
        $total['totalDireccionPatronal']=$totalDireccionPatronal;
        $total['totalDireccionNeto']=$totalDireccionNeto;
        $total['totalDireccionConcepto']=$totalDireccionConcepto;
        
        asort($totalNominaConcepto);
        $total['totalNominaAsignacion']=$totalNominaAsignacion;
        $total['totalNominaDeduccion']=$totalNominaDeduccion;
        $total['totalNominaPatronal']=$totalNominaPatronal;
        $total['totalNominaNeto']=$totalNominaNeto;
        $total['totalNominaConcepto']=$totalNominaConcepto;
        
        //Bloque para determinar funcioanrios y titulos profesionales del Realizador, Revisor y Aprobador  
        $usuario = $this->get('security.context')->getToken()->getUser();
        $helper = $this->get('cet.nomina.globales');
        $revision = $helper->obtenerRevisores($em,$usuario);
  
        //Imprime recibo de pago
        if($imprimir && $idRecibo){            
            $this->recibosAction($this->getRequest(),false,null,$id,$idRecibo,null);
        }
        
        //Imprime recibos de pago
        if($imprimir && $recibos){            
            $this->recibosAction(new Request(),false,null,$id,null,$arregloOrdenado);
        }
        
        if ($recibos) {
            //Redirige a la vista de los recibos de pago
            return $this->render('CetNominaBundle:Nomina:show.html.twig', array(
                        'entity' => $entity,
//                        'informacion' => $informacion,
                        'informacion' => $arregloOrdenado,
                        'personacargo' => $personacargo,
                        'total' => $total
                    ));
        }
        //Almacena recibos de pago
        if($almacenar){
            //Guarda en base de datos los archivos y los respaldos
            $respaldo = new Respaldo();
            $hoy = new DateTime('NOW');
            $respaldo->setDescripcion('Nomina:'.$entity->getId().' - '.$entity->getDescripcion());
            $respaldo->setFecha($hoy);
            $respaldo->setNomina($entity);
            $em->persist($respaldo);   
//            $em->flush();
            $this->recibosAction(new Request(),true,$respaldo,$id,null,$arregloOrdenado);
        }
                
        //Generar PDF de Nómina
        if ($imprimir || $almacenar){
            $html = $this->renderView('CetNominaBundle:Nomina:showNomina.html.twig', array(
                'entity'      => $entity,
                'informacion'   => $informacion,
                'personacargo'   => $personacargo,
                'root_uo' => $root_uo,
                'rutas' => $rutas, 
                'total' => $total,
                'revision' => $revision
            ));

            $valores['institucion'] = "CONTRALORÍA DEL ESTADO TÁCHIRA";
            $valores['nomina'] = $entity->getDescripcion();
            $valores['fecha'] = $entity->getFecha()->format('d/m/Y');
            $valores['plantilla'] = $entity->getFkNominaPlantillaNomina1()->getNombre();
            $valores['iniciales'] = $revision['iniciales'];
            $header = $this->renderView('CetNominaBundle:Nomina:header.html.twig', array('valores' => $valores));
            $footer = $this->renderView('CetNominaBundle:Nomina:footer.html.twig', array('valores' => $valores));
            
            if($imprimir){
                return new Response(
                    $this->get('knp_snappy.pdf')->getOutputFromHtml($html,array(
                        'print-media-type'      => true,
                        'no-background'         => true,
        //                'custom-header'         => true,
        //                'custom-header-propagation'    => true,
        //                'header-center'                => true,
        //                'header-font-size'             => 20,
        //                'default-header'         => true,
                        'header-html'           => $header,
                        'footer-html'           => $footer,
                        'lowquality'                   => false,
                        'grayscale'                    => true,
                        'margin-bottom'                => 12,
                        'margin-left'                  => 8,
                        'margin-right'                 => 8,
                        //Para Windows
                        //'margin-top'                   => 20,
                        //Para Linux
                        'margin-top'                   => 40,
                        'page-size'                    => "Letter",
                    )),
                    200,
                    array(
                        'Content-Type'          => 'application/pdf',
                        'Content-Disposition'   => 'attachment; filename="Nomina.pdf"',
                    )
                ); 
            }
            elseif($almacenar){ //Si es guardar
                $this->get('knp_snappy.pdf')->generateFromHtml(
                        $html,'uploads/respaldo/Nomina'.$id.'.pdf',array(
                            'print-media-type'      => true,
                            'no-background'         => true,
                            'header-html'           => $header,
                            'footer-html'           => '',
                            'lowquality'                   => false,
                            'grayscale'                    => true,
                            'margin-bottom'                => 12,
                            'margin-left'                  => 8,
                            'margin-right'                 => 8,
                            //Para Windows
                            //'margin-top'                   => 20,
                            //Para Linux
                            'margin-top'                   => 40,
                            'page-size'                    => "Letter",
                        ));
                
                
                $archivo2 = new Archivo();
                $archivo2->setNombre('Nomina'.$id.'.pdf');
                $archivo2->setRuta('uploads/respaldo/');
                $archivo2->setRespaldo($respaldo);
                $em->persist($archivo2);   
            }
        }
        
        //Generar TXT de abono de Nómina
        if ($abono || $carta || $almacenar){         
            $directorioConfiguracion = __DIR__.'/../../../../web/uploads/configuracion/';
            $directorio = __DIR__.'/../../../../web/uploads/respaldo/';
            $archivo = 'Abono'.$id.'.dat';
            $linea="";
            
            $plantilla = $entity->getFkNominaPlantillaNomina1();
            $tipoNomina = $plantilla->getFkPlantillaNominaTipoNomina1()->getNombre();
            $banco = $plantilla->getTipoBanco();
            $cuenta =  $plantilla->getTipoCuenta();
            $cuentaEmpresa="00000000000000000000";
            $cuentaEmpresa=file_get_contents($directorioConfiguracion."CuentaContraloria.txt", $linea);
            $cuentaFideicomisoEmpresa="00000000000000000000";            
            $cuentaFideicomisoEmpresa=file_get_contents($directorioConfiguracion."CuentaFideicomisoContraloria.txt", $linea);
            $l = array();
            $totales = 0;
            $texto = "";
            $final = "";
            
            ////////////////////////////////////////////////////////////////////
            //if(isset($entity->getFechaAbono()))
            //Ojo: El condicional anterior arroja error 500 i pantalla en blanco en el servidor
            ////////////////////////////////////////////////////////////////////

            if($entity->getFechaAbono() !== null){
                $fechaPago=$entity->getFechaAbono()->format('d/m/y');
                $mesPago = intval($entity->getFechaAbono()->format('n'));
                $anioPago = intval($entity->getFechaAbono()->format('y'));
            }
            else{
                $fechaPago=$entity->getFecha()->format('d/m/y');
                $mesPago = intval($entity->getFecha()->format('n'));
                $anioPago = intval($entity->getFecha()->format('y'));
            }
            
            //Nulo o Banco de Venezuela
            if($banco == null || preg_match('/Banco de Venezuela/i',$banco->getNombre())){
            
                //REGISTRO CABECERA    
                $identificacion="H";
                $nombreEmpresa="CONTRALORIA DEL ESTADO TACHIRA";
                $nombreEmpresa=str_pad($nombreEmpresa,40," ",STR_PAD_RIGHT);                
                $numeroControl="01"; // Siempre 01

                $montoTotal=$total['totalNominaNeto'];
                $montoTotal=number_format($montoTotal, 2, '', ''); // Quitar los separadores de miles y separador decimal
                $montoTotal=$montoTotal.""; //Hacerlo cadena de texto
                $montoTotal=str_pad($montoTotal,13,"0",STR_PAD_LEFT); // Ceros a las izq
                $serialNomina="03291 ";

                $linea=($identificacion.$nombreEmpresa.$cuentaEmpresa.$numeroControl.$fechaPago.$montoTotal.$serialNomina)."\n";

                //REGISTRO DETALLE
                //Segun Diseño de Registro de abono nomina 20 digitos 160909. Provisto por el Banco Venezuela
                /*
                 $identificacionRegistro 01 caracter
                 $numeroCuenta           20 caracteres
                 $montoTransaccion       11 caracteres ceros a la izq
                 $tipoCuenta             04 caracteres
                 $nombreBeneficiario     40 caracteres
                 $cedula                 10 caracteres ceros a la izq
                 $serialNomina           06 caracteres
                 EspaciosBlancos         02 caracteres 
                 */

                /*
                0 => Cedula
                1 => Nombre Completo
                2 => Cuenta Nomina
                3 => Tipo cuenta nomina
                4 => Monto Neto                         
                */
                
                foreach($arregloPersona as $persona){
                    if($persona[3]==0){//Cuenta corriente
                        $identificacionRegistro='0';
                        $tipoCuenta="0770";
                    }
                    else{
                        $identificacionRegistro='1';
                        $tipoCuenta="1770";
                    }
                    
                    $numeroCuenta="00000000000000000000";
                    if($cuenta == null || preg_match('/Nómina/i',$cuenta->getNombre())){
                        $numeroCuenta=$persona[2];
                    }
                    elseif(preg_match('/Fideicomiso/i',$cuenta->getNombre())){
                        $numeroCuenta=$persona[7];
                    }
                    
                    $montoTransaccion=$persona[4];
                    $montoTransaccion=number_format($montoTransaccion, 2, '', ''); // Quitar los separadores de miles y separador decimal
    //                $montoTransaccion=$montoTransaccion*100; //Quitar los 2 decimales
                    $montoTransaccion=$montoTransaccion.""; //Hacerlo cadena de texto
                    $montoTransaccion=str_pad($montoTransaccion,11,"0",STR_PAD_LEFT); // Ceros a las izq

                    $nombreBeneficiario=$persona[1];
                    $nombreBeneficiario=utf8_decode($nombreBeneficiario);
                    $nombreBeneficiario=substr($nombreBeneficiario, 0, 40);
                    $nombreBeneficiario=str_pad($nombreBeneficiario,40," ",STR_PAD_RIGHT);

                    $cedula=$persona[0];
                    $cedula=str_pad($cedula,10,"0",STR_PAD_LEFT); // Ceros a las izq

                    $serialNomina="003291  ";

                    $linea=$linea.($identificacionRegistro.$numeroCuenta.$montoTransaccion.$tipoCuenta.$nombreBeneficiario.$cedula.$serialNomina)."\n";            

                    $laux = array();
                    $laux['ced'] = str_pad($persona[0],8,"0",STR_PAD_LEFT);
                    $laux['nom'] = $nombreBeneficiario;
                    $laux['cta'] = $numeroCuenta;
                    $laux['mon'] = number_format($persona[4], 2, ',', '.');
                    $totales = round(( $totales + round($persona[4], 2) ) , 2 );

                    $l[] = $laux;
                }
                $nombreBanco = utf8_decode("BANCO DE VENEZUELA\n219 - SUCURSAL SAN CRISTOBAL\nSAN CRISTOBAL");
                $tipo = "";
                $texto = utf8_decode("para que sea abonada en cuentas de nuestros empleados el día ").$fechaPago;
                $final = utf8_decode("Agradecemos se sirvan entregar a nuestro mensajero las notas de crédito o depósitos individuales por dichos abonos.");
            }
            elseif(preg_match('/Banco Occidental de Descuento/i',$banco->getNombre())){
                foreach($arregloPersona as $persona){
                    $cedula = $persona[0];
                    $cedula = str_pad($cedula,12,"0",STR_PAD_LEFT); // Ceros a las izq
                    
                    $nombreBeneficiario = $persona[5];
                    $nombreBeneficiario = utf8_decode($nombreBeneficiario);
                    $nombreBeneficiario = substr($nombreBeneficiario, 0, 15);
                    $nombreBeneficiario = str_pad($nombreBeneficiario,15," ",STR_PAD_RIGHT);
                    
                    $apellidoBeneficiario = $persona[6];
                    $apellidoBeneficiario = utf8_decode($apellidoBeneficiario);
                    $apellidoBeneficiario = substr($apellidoBeneficiario, 0, 20);
                    $apellidoBeneficiario = str_pad($apellidoBeneficiario,20," ",STR_PAD_RIGHT);
                    
                    $montoTransaccion = $persona[4];
                    $montoTransaccion = number_format($montoTransaccion, 2, '', ''); // Quitar los separadores de miles y separador decimal
    //                $montoTransaccion=$montoTransaccion*100; //Quitar los 2 decimales
                    $montoTransaccion = $montoTransaccion.""; //Hacerlo cadena de texto
                    $montoTransaccion = str_pad($montoTransaccion,15,"0",STR_PAD_LEFT); // Ceros a las izq
                    
                    $numeroCuenta="00000000000000000000";
                    if($cuenta == null || preg_match('/Nómina/i',$cuenta->getNombre())){
                        $numeroCuenta = $persona[2];
                    }
                    elseif(preg_match('/Fideicomiso/i',$cuenta->getNombre())){
                        $numeroCuenta = $persona[7];
                    }
                    
                    $linea=$linea.($cedula.$nombreBeneficiario.$apellidoBeneficiario.$montoTransaccion.$numeroCuenta)."\n";
                    
                    $laux = array();
                    $laux['ced'] = str_pad($persona[0],8,"0",STR_PAD_LEFT);
                    $laux['nom'] = utf8_decode($persona[6]).", ".utf8_decode($persona[5]);
                    $laux['cta'] = $numeroCuenta;
                    $laux['mon'] = number_format($persona[4], 2, ',', '.');
                    $totales = round(( $totales + round($persona[4], 2) ) , 2 );

                    $l[] = $laux;
                }
                $cuentaEmpresa = $cuentaFideicomisoEmpresa;
                $nombreBanco = utf8_encode("FIDEICOMISO B. O. D.");
                
                if(preg_match('/^Funcionarios/i',$tipoNomina))
                    $cadena = "APORTE FUNCIONARIOS";
                elseif(preg_match('/^Altos Funcionarios/i',$tipoNomina))
                    $cadena = "APORTE ALTOS FUNCIONARIOS";
                elseif(preg_match('/^Contratados/i',$tipoNomina))
                    $cadena = "APORTE CONTRATADOS";
                elseif(preg_match('/^Obreros fijos/i',$tipoNomina))
                    $cadena = "APORTE OBREROS FIJOS";
                else
                    $cadena = "";
                
                $tipo = utf8_decode("FONDO 13755  ".$cadena);
                $texto = utf8_decode("para abonar al FIDEICOMISO DE PRESTACIONES SOCIALES DE ANTIGÜEDAD identificado con el Nro. 13755 de la CONTRALORÍA DEL ESTADO TÁCHIRA, los aportes correspondientes al (los) mes(es): $mesPago de $anioPago, según archivos anexos desglosados de la siguiente manera:");
                $final = utf8_decode("Agradeciendo la atención prestada.");
            }
            if (!is_dir($directorio) or !is_writable($directorio)) {
                // Error if directory doesn't exist or isn't writable.
            } elseif (is_file($archivo) and !is_writable($archivo)) {
                // Error if the file exists and isn't writable.
            }
            file_put_contents($directorio.$archivo, $linea);
            
//            Bloque para generar carta banco
            $v = array();
//            $hoy = new DateTime('NOW');
//            $hoy = $hoy->format('d/m/y');
            
            $hoy = $entity->getFecha()->format('d/m/y');
            $v['hoy'] = $hoy;
            $v['cta'] = $cuentaEmpresa;            
            $val = new EnLetras();
            $v['let'] = utf8_decode($val->ValorEnLetras($totales, ''));
            $v['tex'] = $texto;
            $v['tot'] = count($l);
            $v['mon'] = number_format($totales, 2, ',', '.');
            $v['tip'] = $tipo;
            $v['fin'] = $final;
            //Bloque para determinar funcioanrios y titulos profesionales del Realizador, Revisor y Aprobador  
            $usuario = $this->get('security.context')->getToken()->getUser();
            $helper = $this->get('cet.nomina.globales');
            $revision = $helper->obtenerRevisores($em,$usuario);            
            $iniciales = $revision['iniciales'];

            $r = array();
            $r[] = $v;

            $directorio='uploads/respaldo/';

            $TBS = $this->container->get('opentbs');
            // load your template
            //$TBS->LoadTemplate('uploads/plantillas/recibo.docx', OPENTBS_ALREADY_UTF8);
            $TBS->LoadTemplate('uploads/plantillas/carta.docx');
            // replace variables

            $TBS->MergeBlock('r', $r);
            $TBS->MergeBlock('l', $l);

            $TBS->LoadTemplate('#word/header1.xml');            
            $TBS->MergeField('hoy',$hoy);
            $TBS->MergeField('banco',$nombreBanco);
            $TBS->LoadTemplate('#word/footer1.xml');            
            $TBS->MergeField('iniciales',$iniciales);
            
            // send the file
            if($almacenar){
                $TBS->Show(OPENTBS_FILE,$directorio.'CartaBanco'.$id.'.docx');

                $archivo1 = new Archivo();
                $archivo1->setNombre('CartaBanco'.$id.'.docx');
                $archivo1->setRuta('uploads/respaldo/');
                $archivo1->setRespaldo($respaldo);
                $em->persist($archivo1); 
                $em->flush(); 
            }
            if($carta){
                $TBS->Show(OPENTBS_DOWNLOAD, 'CartaBanco'.$id.'.docx');                
            }
           
//            Fin Bloque para generar carta banco
            
            if($almacenar){
                //Guarda en base de datos los archivos y los respaldos                
                $archivo3 = new Archivo();
                $archivo3->setNombre('Abono'.$id.'.dat');
                $archivo3->setRuta('uploads/respaldo/');
                $archivo3->setRespaldo($respaldo);
                
                $em->persist($archivo3);
                
                $em->flush();  
            }
            
            if($abono){
                $response = new BinaryFileResponse($directorio.$archivo);
                $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                return $response;                
            }            
        }
        
        //Redirige a la vista de la Nomina
        return $this->render('CetNominaBundle:Nomina:showNomina.html.twig', array(
            'entity'      => $entity,
            'informacion'   => $informacion,
            'personacargo'   => $personacargo,
            'root_uo' => $root_uo,
            'rutas' => $rutas,
            'total' => $total,
            'revision' => $revision,
            'almacenar' => $almacenar?true:false
        ));
    }
    
    /**
     * Displays a form to edit an existing Nomina entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Nomina.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Nomina:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Nomina entity.
    *
    * @param Nomina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Nomina $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new NominaType(), $entity, array(
            'action' => $this->generateUrl('nomina_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $em
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing Nomina entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Nomina.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            //Mantener el atributo mes actualizado
            $mes = intval($entity->getDesde()->format('n'));
            $entity->setMes($mes);
    
            //Colocar Null las vacaciones  que tenia asociada la nomina editada
            
            $procesados=array();
            $vacaciones=$entity->getVacaciones();
            if($vacaciones)
                $anteriores=$vacaciones->getSnapshot();
            else
                $anteriores=array();
            foreach($anteriores as $v){
                $vacacionEdit= new Vacaciones();
                $vacacionEdit=$v;
                $procesados[]=$vacacionEdit;
                $vacacionEdit->setFkVacaciones(null);
                $em->persist($vacacionEdit);
            }
            foreach($vacaciones as $v){
                $vacacionAdd= new Vacaciones();
                $vacacionAdd=$v;
                $vacacionAdd->setFkVacaciones($entity);
                $em->persist($vacacionAdd);
            }  
            $em->flush();
            return $this->redirect($this->generateUrl('nomina_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:Nomina:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Nomina entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Nomina.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('nomina'));
    }

    /**
     * Creates a form to delete a Nomina entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nomina_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    /**
     * Crea los recibos de pago.
     *
     */
    public function recibosAction(Request $request,$almacenar=null,$respaldo=null,$idNomina=null,$idRecibo=null,$arregloOrdenado=null)
    {
        $em = $this->getDoctrine()->getManager();
        $idPersona=$request->get('idPersona');
        $idRecibo=$request->get('idRecibo');
        //Recibos de pago
        if($idPersona==null)
//            $consultaInformacion = $em->createQuery('SELECT p,hc,c FROM CetNominaBundle:Personal p JOIN p.historicoConceptos hc JOIN hc.fk_historico_concepto_concepto1 c WHERE hc.fk_historico_concepto_nomina1='.$idNomina.' ORDER BY c.codigo');
            $consultaInformacion = $em->createQuery("SELECT p,hc,c,hca,il,ca "
                . "FROM CetNominaBundle:Personal p "
                . "LEFT JOIN p.historicoConceptos hc "
                . "LEFT JOIN hc.fk_historico_concepto_concepto1 c "
                . "LEFT JOIN p.informacionLaboral il "
                . "LEFT JOIN il.historicoCargos hca "
                . "JOIN hca.fk_informacion_laboral_has_cargo_cargo1 ca "
                . "WHERE hc.fk_historico_concepto_nomina1='$idNomina' "
                . "AND hca.fechaInicio IN "
                . "( "
                    . "SELECT MAX(hc2.fechaInicio) " 
                    . "FROM CetNominaBundle:HistoricoCargo hc2 "
                    . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = il.id "
                    . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END))"
                    . "AND ( hc2.esEncargado = 'f' OR hc2.esEncargado IS NULL ) "
                . ") "
                . "ORDER BY ca.jerarquia,ca.nivel DESC,ca.sueldo DESC,p.id,c.codigo");        
        else{
//            $consultaInformacion = $em->createQuery('SELECT p,hc,c FROM CetNominaBundle:Personal p JOIN p.historicoConceptos hc JOIN hc.fk_historico_concepto_concepto1 c WHERE hc.fk_historico_concepto_nomina1='.$idNomina.' AND p.id='.$idPersona.' ORDER BY c.codigo');
            $consultaInformacion = $em->createQuery("SELECT p,hc,c,hca,il,ca "
                . "FROM CetNominaBundle:Personal p "
                . "LEFT JOIN p.historicoConceptos hc "
                . "LEFT JOIN hc.fk_historico_concepto_concepto1 c "
                . "LEFT JOIN p.informacionLaboral il "
                . "LEFT JOIN il.historicoCargos hca "
                . "JOIN hca.fk_informacion_laboral_has_cargo_cargo1 ca "
                . "WHERE hc.fk_historico_concepto_nomina1='$idNomina' "
                . "AND p.id='$idPersona' "
                . "AND hca.fechaInicio IN "
                . "( "
                    . "SELECT MAX(hc2.fechaInicio) " 
                    . "FROM CetNominaBundle:HistoricoCargo hc2 "
                    . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = il.id "
                    . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END))"
                    . "AND ( hc2.esEncargado = 'f' OR hc2.esEncargado IS NULL ) "
                . ") "
                . "ORDER BY ca.jerarquia,ca.nivel DESC,ca.sueldo DESC,p.id,c.codigo");  
        }
        if($idRecibo==null)
            $contador=0;
        else{
            $contador=$idRecibo-1;
        }
        
        //Obtener datos de la nomina
        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($idNomina);
       
        $informacion = $consultaInformacion->getResult();
        
        //Sustituir la consulta con el arreglo ordenado de nómina
        if($arregloOrdenado !== null && $idPersona==null)
            $informacion = $arregloOrdenado;
        
        $r=array();
        //num: 4 caracteres, ceros a la izq
        //nom: 30 caracteres, espacios a la der
        //ced: 8 caracteres, ceros a la izq
        //car,div: 52 caracteres, espacios a la der        
        //cod: 4 caracteres, ceros a la izq
        //des: 29 caracteres, espacios a la der
        //mona,mond,ta,td: 13 caracteres, espacios a la izq
        //acu: 17 caracteres, espacios a la izq
        //nomi: 61 caracteres, espacios a la der
        foreach($informacion as $persona){
            //Obtener datos laborales
            $informacionlaboral = $em->getRepository('CetNominaBundle:InformacionLaboral')->findOneBy(array('personal' => $persona->getId()));
            
            $consulta = $em->createQuery("SELECT hc,il.cuentaNomina AS cuentaNomina,il.tipoCuentaNomina AS tipoCuentaNomina,c.nombre AS cargo,c.nivel AS nivel,uo.id AS iduo,uo.nombre AS nombre "
                . "FROM CetNominaBundle:HistoricoCargo hc "
                . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_cargo1 c "
                . "LEFT JOIN hc.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
                . "LEFT JOIN hc.fk_informacion_laboral_has_cargo_informacion_laboral1 il "
                . "LEFT JOIN il.personal p "
                . "WHERE  p.id = ".$persona->getId() ." "
                . "AND hc.fechaInicio IN "
                    . "("
                        . "SELECT MAX(hc2.fechaInicio) " 
                        . "FROM CetNominaBundle:HistoricoCargo hc2 "
                        . "WHERE hc2.fk_informacion_laboral_has_cargo_informacion_laboral1 = il.id "
                        . "AND ( CURRENT_DATE() BETWEEN hc2.fechaInicio AND (CASE WHEN hc2.fechaFin IS NULL THEN CURRENT_DATE() ELSE hc2.fechaFin END)) "
                    . ")"
                );
            $historicocargo = $consulta->getResult();

            //$consulta1 = $em->createQuery("SELECT huo.id,uo.id as iduo, uo.nombre FROM CetNominaBundle:HistoricoUnidadOrganizativa huo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo JOIN huo.fk_unidad_organizativa_has_informacion_laboral_informacion_la1 il JOIN il.personal p WHERE p.id='".$persona->getId()."' AND huo.fechaFin IS NULL");
//            $consulta1 = $em->createQuery("SELECT huo.id,uo.id as iduo, uo.nombre "
//                    . "FROM CetNominaBundle:HistoricoUnidadOrganizativa huo "
//                    . "JOIN huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo "
//                    . "JOIN huo.fk_unidad_organizativa_has_informacion_laboral_informacion_la1 il "
//                    . "JOIN il.personal p "
//                    . "WHERE p.id='".$persona->getId()."' "
//                    . "AND huo.fechaInicio in ("
//                        . "select max(huo2.fechaInicio) "
//                        . "from CetNominaBundle:HistoricoUnidadOrganizativa huo2 "
//                        . "where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)");
//            
//            $historicouo = $consulta1->getResult();
           
            $cargo="Sin Asignar";
            $cuenta="Sin Asignar";
            $tipocuenta="Sin Asignar";
            $nivel="";
            $romano="";
            
            $arreglouo=array();
            $uo="Sin Asignar";
            $uod="Sin Asignar";
            $iduo=0;
            $iduod=0;
            
            foreach($historicocargo as $hc){
                $cargo=$hc['cargo'];
                $nivel=$hc['nivel'];
                $cuenta=$hc['cuentaNomina'];
                $tipocuenta=$hc['tipoCuentaNomina'];
                
                $romano="";
                if($nivel==1)$romano='I';
                elseif($nivel==2)$romano='II';
                elseif($nivel==3)$romano='III';
                elseif($nivel==4)$romano='IV';
                elseif($nivel==5)$romano='V';
                elseif($nivel==6)$romano='VI';
                elseif($nivel==7)$romano='VII';
                elseif($nivel==8)$romano='VIII';
                elseif($nivel==9)$romano='IX';
                elseif($nivel==10)$romano='X';
                
                $uo=$hc['nombre']; 
                $iduo=$hc['iduo'];
                $claveforanea=$hc['iduo'];
                while($claveforanea!=null){
                    $consultas2 = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($claveforanea);
                    $claveforanea=$consultas2->getFkUnidadOrganizativaUnidadOrganizativa1();
                    $uod=$consultas2->getNombre();
                    $iduod=$consultas2->getId();
                    //Arreglo de la ruta de las unidades organizativas del funcionario
                    $arreglouo[$iduod]=$uod;
                }
            }
            
            $cargo = $cargo." ".$romano;
            
            $v=array();
            $contador=$contador+1;
            $v['num']=str_pad($contador,4,"0",STR_PAD_LEFT);
            $v['fec']=$entity->getFecha()->format('d/m/Y');
            $nombre=$persona->getPrimerNombre()." ".$persona->getSegundoNombre()." ".$persona->getPrimerApellido()." ".$persona->getSegundoApellido();
            $nombre=utf8_decode($nombre);
            $nombre=substr($nombre, 0, 30);
            $v['nom']=str_pad($nombre,30," ",STR_PAD_RIGHT);
            $v['ced']=str_pad($persona->getId(),8,"0",STR_PAD_LEFT);
            $cargo=utf8_decode($cargo);
            $cargo=substr($cargo, 0, 52);            
            $v['car']=str_pad($cargo,52," ",STR_PAD_RIGHT);
            $uo=utf8_decode($uo);
            $uo=substr($uo, 0, 52);
            $v['div']=str_pad($uo,52," ",STR_PAD_RIGHT);
            $uod=utf8_decode($uod);
            $uod=substr($uod, 0, 43);
            $v['dir']=str_pad($uod,43," ",STR_PAD_RIGHT);
            $v['feci']=$informacionlaboral->getFechaIngreso()->format('d/m/Y');
            $nominaDescripcion=$entity->getDescripcion();
            $nominaDescripcion=utf8_decode($nominaDescripcion);
            $nominaDescripcion=substr($nominaDescripcion, 0, 61);            
            $v['nomi']=str_pad($nominaDescripcion,61," ",STR_PAD_RIGHT);
            
            $totalAsignacion=0;
            $totalDeduccion=0;
            foreach($persona->getHistoricoConceptos() as $conceptos){
                $c=array();
                $c['cod']=str_pad($conceptos->getFkHistoricoConceptoConcepto1()->getCodigo(),4,"0",STR_PAD_LEFT);                
                $conceptoDescripcion=$conceptos->getFkHistoricoConceptoConcepto1()->getDenominacion();
                $conceptoDescripcion=utf8_decode($conceptoDescripcion);
                $conceptoDescripcion=substr($conceptoDescripcion, 0, 31);
                $c['des']=str_pad($conceptoDescripcion,31," ",STR_PAD_RIGHT);
                $tipo=$conceptos->getFkHistoricoConceptoConcepto1()->getTipo();
                $principal=number_format($conceptos->getMonto(), 2, ',', '.');
                $patronal=number_format($conceptos->getMontoPatronal(), 2, ',', '.');
                $acumulado=number_format($conceptos->getAcumulado(), 2, ',', '.');
                if($tipo=="Asignación"){
                    $c['mona']=str_pad($principal,13," ",STR_PAD_LEFT);
                    $c['mond']=str_pad("",13," ",STR_PAD_LEFT);
                    $totalAsignacion=$totalAsignacion+$conceptos->getMonto();
                }
                else{
                    $c['mond']=str_pad($principal,13," ",STR_PAD_LEFT);
                    $c['mona']=str_pad("",13," ",STR_PAD_LEFT);
                    $totalDeduccion=$totalDeduccion+$conceptos->getMonto();
                } 
                if($patronal>0){
                    $c['monp']=str_pad($patronal,13," ",STR_PAD_LEFT);                    
                }
                else{
                    $c['monp']=str_pad("",13," ",STR_PAD_LEFT);
                }                
                $esAcumulado=$conceptos->getFkHistoricoConceptoConcepto1()->getEsAcumulado();
                if($esAcumulado){
                    $c['acu']=str_pad($acumulado,17," ",STR_PAD_LEFT);                    
                }
                else{
                    $c['acu']=str_pad("",17," ",STR_PAD_LEFT);
                }
                    
                $v['con'][]=$c;
            }
            $totalNeto=$totalAsignacion-$totalDeduccion;
            $totalAsignacion=number_format($totalAsignacion, 2, ',', '.');
            $totalDeduccion=number_format($totalDeduccion, 2, ',', '.');
            $v['ta']=str_pad($totalAsignacion,13," ",STR_PAD_LEFT);
            $v['td']=str_pad($totalDeduccion,13," ",STR_PAD_LEFT);
            $totalNeto=number_format($totalNeto, 2, ',', '.');
            $v['tn']=str_pad($totalNeto,17," ",STR_PAD_LEFT);
            $r[]=$v;
            
        }
        
        $directorio='uploads/respaldo/';
        
        $TBS = $this->container->get('opentbs');
        // load your template
        //$TBS->LoadTemplate('uploads/plantillas/recibo.docx', OPENTBS_ALREADY_UTF8);
        $TBS->LoadTemplate('uploads/plantillas/recibo.docx');
        // replace variables
        $TBS->MergeBlock('r', $r);
//        $TBS->MergeBlock('c', $r[0]);
        // send the file
        if($idPersona==null){
            if($almacenar){
                $TBS->Show(OPENTBS_FILE,$directorio.'Recibos'.$idNomina.'.docx');
            
                $archivo1 = new Archivo();
                $archivo1->setNombre('Recibos'.$idNomina.'.docx');
                $archivo1->setRuta('uploads/respaldo/');
                $archivo1->setRespaldo($respaldo);
                $em->persist($archivo1); 
            }
            else
                $TBS->Show(OPENTBS_DOWNLOAD, 'Recibos Nomina '.$idNomina.'.docx');
        }
        else
            $TBS->Show(OPENTBS_DOWNLOAD, 'Recibo Nomina '.$idNomina.'-'.$idPersona.'.docx');
        
        return $this->redirect($this->generateUrl('nomina_show', array('id' => $idNomina)));
    } 
    
    /**
     * Descarga los archivos de respaldo.
     *
     * @param mixed $procesar The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    public function procesarNominaAction($procesar = null,$idNomina = null)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CetNominaBundle:Respaldo')->findBy(array('nomina'=>$idNomina));
        foreach($entities as $respaldo){
            $entities1 = $respaldo->getArchivos();
            $nombreArchivo = "";
            $rutaArchivo = "";
            
            foreach($entities1 as $archivo){
                $nombreArchivo = $archivo->getNombre();
                $rutaArchivo = "uploads/respaldo/";
                
                if(preg_match("/".$procesar."/i",$nombreArchivo)){
                    $response = new BinaryFileResponse($rutaArchivo.$nombreArchivo);
                    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                    return $response;  
                }
            }
        }
    }
    
    /**
     * Calcula el valor de las variables y lo almacena en historicovariable
     * Parametros: $idvariable = Variable
     * Parametros: $idpersonal = Cedula
     */
    public function calculaVariable($idvariable,$idpersonal,$nomina)
    {
       try{
        $em = $this->getDoctrine()->getManager();
        
        //Para evitar el desbordamiento de memoria
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $variable = $em->getRepository('CetNominaBundle:Variable')->find($idvariable);
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($idpersonal);
       
        //Se hace la consulta del DQL almacenado en la variable
        $consulta = $em->createQuery($variable->getSql());
        $formula = $variable->getSql();
        
        $fecha = $nomina->getHasta();
        $formula = str_replace("CURRENT_DATE()", "'".$fecha->format("Y-m-d")."'", $formula);
        
        $consulta->setParameter('personal_cedula', $idpersonal);
//        try {
            $datos=$consulta->getOneOrNullResult();
//        } 
        //catch (\Doctrine\ORM\NonUniqueResultException $exc) {
//        catch (\Exception $exc) {
//            $info = toString($e);
//            $datos=array();
//        }
        if ($datos==null){
            $datos=array();
        }
        foreach($datos as $dato){
            if(!is_object($dato)){
                //return new JsonResponse(array($dato));
                $valor = $dato;
            }                
            elseif($dato instanceof \DateTime){
                    $resul=$dato->format('d/m/Y');
                    //return new JsonResponse(array($resul));
                    $valor=$resul;

            }
            else {
                //return new JsonResponse(array("!No hay resultado¡"));
                $valor=0;
            }
        }

//            $valor = $consulta->getSingleScalarResult();
        $hoy = new DateTime('NOW');
                        
        //Se verifica si la variable existe
        $historicoVariable = $em->getRepository('CetNominaBundle:HistoricoVariable')->encuentra($idvariable,$idpersonal);
        
        $historicoVariablenuevo = new HistoricoVariable(); 
        if (isset($valor)){
            if($historicoVariable){
                //Se verifica si el valor cambia
                //if($historicoVariable->getValor()!=$valor ){                       
                if($historicoVariable->getValor()!=$valor or $historicoVariable->getSql()!=$formula ){                       
                    $historicoVariablenuevo->setFkVariableHasPersonalVariable1($variable);
                    $historicoVariablenuevo->setFkVariableHasPersonalPersonal1($personal);
                    $historicoVariablenuevo->setValor($valor);        
                    $historicoVariablenuevo->setSql($formula);    
                    $historicoVariablenuevo->setFechaInicio($hoy);

                    $historicoVariable->setFechaFin($hoy);   

                    $em->persist($historicoVariable); 
                    $em->persist($historicoVariablenuevo); 
                    $em->flush();
                }
            }else{
                //Se almacena el valor de la variable en historicovariable       
                $historicoVariablenuevo->setFkVariableHasPersonalVariable1($variable);
                $historicoVariablenuevo->setFkVariableHasPersonalPersonal1($personal);
                $historicoVariablenuevo->setValor($valor);
                $historicoVariablenuevo->setSql($formula); 
                $historicoVariablenuevo->setFechaInicio($hoy);        

                $em->persist($historicoVariablenuevo);        
                $em->flush(); 
            }    
        }  
         }
        catch (\Exception $e) {
            //$info = toString($e);
            $info = $e->getMessage();
            $e=null;
        }
        //if (trim($info)== )
        $em->clear();
//        return $this->redirect($this->generateUrl('variable'));
    }    
    
    /**
     * Calcula el valor de los conceptos y lo almacena en historicoconcepto
     * Parametros: $idconcepto = Concepto
     * Parametros: $idpersonal = Cedula
     */
    public function calculaConcepto($idconcepto,$idpersonal,$idnomina,$datos,$conceptos,&$vector_variables,&$vector_variables_patronal,&$vector_variables_acumulado,$vector_conceptos,&$vector_evaluado,&$vector_evaluado_patronal,&$vector_evaluado_acumulado)
    {
        $em = $this->getDoctrine()->getManager();     
        
        //Para evitar el desbordamiento de memoria
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $concepto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
//        $queryConcepto = "SELECT c FROM Cet\NominaBundle\Entity\Concepto c "
//                . "WHERE c.id = :concepto";
//        $consultaConcepto=$em->createQuery($queryConcepto)->setParameter('concepto', $idconcepto);
//        $concepto = $consultaConcepto->getResult();
        
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($idpersonal);
//        $querypersonal = "SELECT p FROM Cet\NominaBundle\Entity\Personal p "
//                . "WHERE p.id = :personal";
//        $consultapersonal = $em->createQuery($querypersonal)->setParameter('personal', $idpersonal);
//        $personal = $consultapersonal->getResult();
        
        $nomina = $em->getRepository('CetNominaBundle:Nomina')->find($idnomina);
//        $idnomina = $nomina->getId();
        
        $identificador=$concepto->getIdentificador();
        //Se hace la consulta de la formula almacenada en Concepto
        $formula_concepto = $concepto->getSql();
        $formula_concepto_patronal = $concepto->getFormulaPatronal();
        
//        //Obtiene las variables de cada funcionario
//        $query = "SELECT hv,v FROM Cet\NominaBundle\Entity\HistoricoVariable hv "
//                . "JOIN hv.fk_variable_has_personal_variable1 v "
//                . "WHERE hv.fk_variable_has_personal_personal1 = :personal "
//                . "AND hv.fechaFin IS NULL";
//
//        $consulta=$em->createQuery($query)->setParameter('personal', $idpersonal);
////        $consulta->setParameter('personal', $idpersonal);
//        
//        $datos=$consulta->getResult();
   
        $vector_conceptosInicial= $vector_conceptos;
        $vector_variablesInicial=$vector_variables;
        $language = new LenguajeFormula();

        $valor = 0;        
        //Verificar si el concepto no ha sido evaluado
        $clave1 = array_search($identificador,$vector_evaluado);
        if( $clave1 === false ){        
            //Calcula el valor para la sección principal                
            try {  
    //            $valor = $language->evaluate("3+4");
                //Funcion Recursiva para calcular conceptos anidados            
                $valor = $this->evaluarConcepto($formula_concepto, $vector_variables, $vector_conceptos, $vector_evaluado, $idpersonal);
            } catch (\Exception $exc) {
                //Retornar error al calcular en caso de no poder evaluar recursivamente la formula
    //            return false;
                $valor = 0;
            }
            $vector_variables[$identificador] = $valor;
            $vector_evaluado[] = $identificador;
        }else{
            $valor = $vector_variables[$identificador];            
        }
        
        $valorPatronal = 0;
        //Verificar si el aporte patronal del concepto no ha sido evaluado
        $clave2 = array_search($identificador,$vector_evaluado_patronal);
        if( $clave2 === false ){  
            //Calcula el valor de la sección de aporte patronal
            try {  
                //Funcion Recursiva para calcular conceptos anidados en aporte patronal             
                $valorPatronal = $this->evaluarConcepto($formula_concepto_patronal, $vector_variables, $vector_conceptos, $vector_evaluado, $idpersonal);
            } catch (\Exception $exc) {
                //Retornar error al calcular en caso de no poder evaluar recursivamente la formula
    //            return false;
                $valorPatronal = 0;
            }
        }else{
            $valorPatronal = $vector_variables_patronal[$identificador];            
        }
        
        $acumulado=0;
        //Verificar si el acumulado del concepto no ha sido evaluado
        $clave3 = array_search($identificador,$vector_evaluado_acumulado);
        if( $clave3 === false ){ 
            //Verificar  si este concepto tiene otros que lo acumulen
            $acumuladoOtros=0;
            foreach($concepto->getFkConceptoHasConcepto2() as $conceptoAcumulador){
                $acm[]=$conceptoAcumulador->getId();
                $conceptoId=$conceptoAcumulador->getId();
                $formula_concepto=$conceptoAcumulador->getSql();
    //            $vector_evaluadoA=array();
                $vector_evaluadoA=$vector_evaluado;
                $vector_conceptosA=$vector_conceptosInicial;
                $vector_variablesA=$vector_variablesInicial;
                try {  
                    $valorA = $this->evaluarConcepto($formula_concepto, $vector_variablesA, $vector_conceptosA, $vector_evaluadoA, $idpersonal);
                } catch (\Exception $exc) {
                    $valorA = 0;
                }
                $acumuladoOtros+=$valorA;
            }    
            //Calcula el valor para la sección de acumulado                
            $valoracumulado=0;

            $historicoConceptoAcumulado = $em->getRepository('CetNominaBundle:HistoricoConcepto')->encuentra($idconcepto,$idpersonal);
            if($historicoConceptoAcumulado){
                $valoracumulado=$historicoConceptoAcumulado->getAcumulado();
            }        
            $acumulado=(($valoracumulado==null)?0:$valoracumulado)+ $valor + $valorPatronal + $acumuladoOtros;            
        }else{
            $acumulado = $vector_variables_acumulado[$identificador];            
        }    

        $hoy = new DateTime('NOW');
        $historicoConceptonuevo = new HistoricoConcepto(); 
        
        //No guardar en historico si el valor o aporte patronal es cero 0
        if( ($valor<>0 || $valorPatronal<>0) || ( preg_match('/^permiso_no_remunerado/i',$identificador) ) )
        {
                $historicoConceptonuevo->setConceptoId($idconcepto);
                $historicoConceptonuevo->setFkHistoricoConceptoConcepto1($concepto);
                $historicoConceptonuevo->setPersonalCedula($idpersonal);
                $historicoConceptonuevo->setFkHistoricoConceptoPersonal1($personal);
                $historicoConceptonuevo->setNominaId($idnomina);
                $historicoConceptonuevo->setFkHistoricoConceptoNomina1($nomina);
                $historicoConceptonuevo->setMonto($valor);
                $historicoConceptonuevo->setMontoPatronal($valorPatronal);
                $historicoConceptonuevo->setAcumulado($acumulado);
                $historicoConceptonuevo->setFechaInicio($nomina->getDesde()); 
                $historicoConceptonuevo->setFechaFin($nomina->getHasta());                 
                
                $em->persist($historicoConceptonuevo); 
                $em->flush();
        } 
        $em->clear();
        
        //Bloque para guardar total asignaciones, total deducciones y total neto
//        if(){
//            $totalAsignacion = $totalAsignacion + $valor;
//        }
//        else
//            $totalDeduccion = $totalDeduccion + $valor;
//        return new JsonResponse("Concepto: ".$idconcepto." Funcionario: ".$idpersonal);
//        return $this->redirect($this->generateUrl('concepto'));
    }
    
    public function evaluarConcepto($formula,&$vector_variables,$vector_conceptos,&$vector_evaluado,$id_personal){
        //Buscar un concepto en la formula
        foreach (array_diff($vector_conceptos,$vector_evaluado) as $concepto){
            if(preg_match("/\b$concepto\b/", $formula)){
                $seleccionado=$concepto;
                break;
            }                    
        }
        $language = new LenguajeFormula();
        //Confirmar que el concepto hallado no haya sido evaluado($vector_evaluado)
        //En caso afirmativo devolver error de redundancia ciclica
        if(isset($seleccionado)){
            if(array_search($seleccionado,$vector_evaluado)){
                return false;
            }
            //Hallar formula del concepto seleccionado
            $em = $this->getDoctrine()->getManager();
            $concepto = $em->getRepository('CetNominaBundle:Concepto')->findBy(array('identificador'=>$seleccionado));
            if(isset($concepto[0])){
                $formulaNueva=$concepto[0]->getSql();
            }
            else{
                throw new \Exception('Error no se consigue el concepto: '.$seleccionado);
            }
            

            //Agregamos a $vector_evaluado el concepto hallado
            $vector_evaluado[]=$seleccionado;
//            foreach($vector_variables as $key=>$value){
//                $vector_nombres_variables[$key]=$key;
//            }            
//            //Agregamos a $vector_variables el identificador del concepto hallado
//            $vector_nombres_variables[$seleccionado]=$seleccionado;
            
            //Compilar formula dentro de un try catch
            //En caso de excepcion arrojar la excepcion con throw
//            try {  
//                $compilado = $language->compile($formula, $vector_nombres_variables);
//            } catch (\Exception $exc) {
//                throw new \Exception('Error compilando fórmula de concepto: '.$seleccionado);
//                return false;
//            }

            try {
                //Agregamos a $vector_variables el valor del concepto hallado
                $vector_variables[$seleccionado] = $this->evaluarConcepto($formulaNueva, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
            } catch (Exception $exc) {
                throw $exc;
            }

            try {  
                return round($language->evaluate($formula, $vector_variables),2);
            } catch (\Exception $exc) {
//                $vector_variables[$seleccionado] = $this->evaluarConcepto($formula, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
                return $this->evaluarConcepto($formula, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
                //throw new \Exception('Error ejecutando fórmula de concepto: '.$seleccionado);
                //return false;
            }  
        }
        else{ //En caso de no conseguir algun concepto en la formula
            try {  
                return round($language->evaluate($formula, $vector_variables),2);
            } catch (\Exception $exc) {
                throw $exc;
            }
        }
    }  
    public function tipoCalculoPlantillaAction($id){
        $em = $this->getDoctrine()->getManager();      
        $plantilla = $em->getRepository('CetNominaBundle:PlantillaNomina')->find($id);
        $tipoCalculo = $plantilla->getFkTipocalculo();
        
        $nombre = "";
        if($tipoCalculo !== null)
            $nombre = $tipoCalculo->getNombre();
        
        return new JsonResponse(array('nombre' => $nombre));
    }
    
    public function procesarVacacioneAction($id,$variables,$todasVariables,$vectorFijos, $vector_conceptos,$vector_conceptos_id,$todosConceptos,$session){
        $em = $this->getDoctrine()->getManager();   
        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);
        $idplantillanomina=$entity->getFkNominaPlantillaNomina1()->getId();
        $vacaciones=$entity->getVacaciones();
        $idnomina=$id;
        if(!$vacaciones)
            $vacaciones=array();
        $personales=array();
        foreach($vacaciones as $v){ 
            $vacacionEdit= new Vacaciones();
            $vacacionEdit=$v;
            //$persona= new Personal();
            $personaId=$vacacionEdit->getPersonal()->getId();// cada vacacion está asociada a una persona
            $conceptos=array('vacacional'=>$vacacionEdit->getBonoTotal());
            $itemPerson=array('id'=>$personaId,'conceptos'=>$conceptos);
            $personales[]=$itemPerson;
        }   
        //Obtener las personas que corresponden a las vacaciones
        $indice=1;
        foreach ($personales as $personal){
            unset($_SESSION);
            $session = $this->get('session');
            $session->start();
            $session->set('mensaje', 'Calculando Variables de Funcionario: '.$personal['id']);
            $session->set('valor', ($indice*100)/count($personales));
            $session->set('total', $indice.' de '.count($personales));
            $session->save();
            session_write_close();
            foreach ($variables as $variable){                                        
                $this->calculaVariable($variable->getId(), $personal['id'],$entity);
            }
            $indice++;
        }
        $indice=1;
        $totalAsignacion = 0;
        $totalDeduccion = 0;
        $totalNeto = 0;
        ////////////////a152.ini
        foreach ($personales as $personal){
            $vector_variables = array();
            $vector_variables_patronal = array();
            $vector_variables_acumulado = array();
            $vector_evaluado = array();
            $vector_evaluado_patronal = array();
            $vector_evaluado_acumulado = array();
            unset($_SESSION);
            $session = $this->get('session');
            $session->start();
            $session->set('mensaje', 'Calculando Conceptos de Funcionario: '.$personal['id']);
            $session->set('valor', ($indice*100)/count($personales));
            $session->set('total', $indice.' de '.count($personales));
            $session->save();
            session_write_close();
            //Se determina que conceptos se van a calcular por funcionario de acuerdo a la personalizacion de los mismos
            $plantillapersonalizada = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')
                                    ->findBy(array('fk_concepto_has_plantilla_nomina_plantilla_nomina1'=>$idplantillanomina,'fk_concepto_has_plantilla_nomina_personal1'=>$personal['id']));
            //Asignamos las variables de cada funcionario en memoria utilizando $$ como variables dinamicas
            foreach($todasVariables as $objeto){       
                $sqlvariable = $objeto['sql'];
                //Para calcular la variable con la fecha de la nómina     
                $fechaNomina = $entity->getHasta();
                $nombreVariable = $objeto['nombre'];
                try {
                    if(preg_match("/CURRENT_DATE()/i",$sqlvariable)){
                        $sqlvariable = preg_replace("/CURRENT_DATE\(\)/i", "(:fecha_nomina)", $sqlvariable);
                        $consultasql = $em->createQuery($sqlvariable);
                        $consultasql->setParameter('fecha_nomina',$fechaNomina,\Doctrine\DBAL\Types\Type::DATETIME);
                    }else{
                        $consultasql = $em->createQuery($sqlvariable);
                    }                       
                    $consultasql->setParameter('personal_cedula', $personal['id']);
                    $consultasql->setMaxResults(1);
                    $valorVariable=0;
                    if ($consultasql->getResult()){
                        foreach($consultasql->getSingleResult() as $dato){
                            if(!is_object($dato)){
                                $valorVariable=$dato;
                            }                    
                            elseif($dato instanceof \DateTime){
                                $valorVariable=$dato;
                            }
                        }
                    }            
                    $vector_variables[$nombreVariable]=$valorVariable;
                }catch (\Exception $e) {
                    $info = $e->getMessage();
                    unset($_SESSION);
                    $session = $this->get('session');
                    $session->start();
                    $session->set('mensaje', "Error calculando variable:$nombreVariable ".$info);
                    $session->set('valor', 0);
                    $session->set('total', "0 de 0");
                    $session->save();
                    session_write_close();
                }
            }
            ///////Inicio Variables Predefinidas
            //Cuenta cantidad de lunes en un lapso de fechas
            $helper = $this->get('cet.nomina.globales');
            $lunes = 1;
            $lunes = $helper->countDays(1,strtotime($entity->getDesde()->format('d-m-Y')),strtotime($entity->getHasta()->format('d-m-Y')));
            $vector_variables["cantidad_lunes"]=$lunes;
            $vector_variables["primera_quincena"] = true;
            //Verifica si Fecha Desde es mayor que 9 probablemente sea segunda quincena
            if( $entity->getDesde()->format('d') > 6 )
                $vector_variables["primera_quincena"] = false;
            //////////Fin Variables Predefinidas
            $vectorPersonalizado=array();
            foreach ($plantillapersonalizada as $datopersonalizado){                   
                $vectorPersonalizado[]=$datopersonalizado->getConceptoId(); // conceptos fijos
            }
            $vectorProcesar=array_merge( array_diff($vectorPersonalizado,$vectorFijos), array_diff($vectorFijos,$vectorPersonalizado));
            //Establece todos los conceptos eliminados de manera perzonalizada a cero
            $vectorEliminados = array_diff($vectorFijos,$vectorProcesar);
            foreach ($vectorEliminados as $eliminado){
                $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($eliminado);
                $vector_variables[$objeto->getIdentificador()] = 0;
                //Establecer el concepto como evaluado
                $vector_evaluado[] = $objeto->getIdentificador();
            }
            //Establece los valores de los conceptos a fuerza, según datos variables
            foreach ($vector_conceptos_id as $idconcepto){
                //Buscar si el concepto esta en datos variables para esa persona
                $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')
                                ->findBy(array('fk_datos_variables_concepto1'=>$idconcepto,'fk_datos_variables_personal1'=>$personal['id']));
                if($datosvariables!=null){
                    foreach($datosvariables as $datosvariable){                        
                        if($datosvariable->getSeccionAfectacion()=='principal'){
                            $valor = $datosvariable->getValor();
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables[$objeto->getIdentificador()] = $valor;
                            $vector_evaluado[] = $objeto->getIdentificador();
                        }
                        elseif($datosvariable->getSeccionAfectacion()=='patronal'){
                            $valorPatronal = $datosvariable->getValor();
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables_patronal[$objeto->getIdentificador()] = $valorPatronal;
                            $vector_evaluado_patronal[] = $objeto->getIdentificador();
                        }
                        elseif($datosvariable->getSeccionAfectacion()=='acumulado'){
                            $acumulado = $datosvariable->getValor();
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables_acumulado[$objeto->getIdentificador()] = $acumulado;
                            $vector_evaluado_acumulado[] = $objeto->getIdentificador();
                        }
                    }
                }
            }
            $var=new VariableFormula();        
            $vector_variables['Variable']=$var;
            foreach ($vectorProcesar as $concepto){
                $this->calculaConceptoDirecto($concepto, $personal['id'],$idnomina,$todasVariables,$todosConceptos,$vector_variables,$vector_variables_patronal,$vector_variables_acumulado,$vector_conceptos,$vector_evaluado,$vector_evaluado_patronal,$vector_evaluado_acumulado,$personal['conceptos']); 
            }
            $indice++;
        }
        ////////////////////a152.fin
        unset($_SESSION);
        $session = $this->get('session');
        $session->start();
        $session->set('mensaje', 'Calculando Variables de Funcionario: '.$personal['id']);
        $session->set('valor', ($indice*100)/count($personales));
        $session->set('total', $indice.' de '.count($personales));
        $session->save();
        session_write_close();
        unset($_SESSION);        
        return new JsonResponse(array('mensaje' => 'La nómina ha sido completamente calculada','valor' => 100,'total' => $indice.' de '.$indice));
    }
    
    public function procesarPrestacionAction($id,$variables,$todasVariables,$vectorFijos, $vector_conceptos,$vector_conceptos_id,$todosConceptos,$session){
        $prestacionGeneral= new PrestacionesSocialesGeneral;
        $prestaciones= new PrestacionesSociales;
        $em = $this->getDoctrine()->getManager();   
        $entity = $em->getRepository('CetNominaBundle:Nomina')->find($id);
        $idplantillanomina=$entity->getFkNominaPlantillaNomina1()->getId();
        $prestacionGeneral=$entity->getPrestacionesSocialesGeneral();
        $prestaciones=$prestacionGeneral->getPrestaciones();
        $idnomina=$id;
        if(!$prestaciones)
            $prestaciones=array();
        $personales=array();
        foreach($prestaciones as $p){ 
            $prestacionEdit= new PrestacionesSociales();
            $prestacionEdit=$p;
            $personaId=$prestacionEdit->getPersonal()->getId();
            $conceptos=array('prestaciones'=>$prestacionEdit->getPrestaciones(),'diasadicional'=>$prestacionEdit->getPrestacionesAdicionales());
            $itemPerson=array('id'=>$personaId,'conceptos'=>$conceptos);
            $personales[]=$itemPerson;
        }   
        $indice=1;
        foreach ($personales as $personal){
            unset($_SESSION);
            $session = $this->get('session');
            $session->start();
            $session->set('mensaje', 'Calculando Variables de Funcionario: '.$personal['id']);
            $session->set('valor', ($indice*100)/count($personales));
            $session->set('total', $indice.' de '.count($personales));
            $session->save();
            session_write_close();
            foreach ($variables as $variable){                                        
                $this->calculaVariable($variable->getId(), $personal['id'],$entity);
            }
            $indice++;
        }
        $indice=1;
        $totalAsignacion = 0;
        $totalDeduccion = 0;
        $totalNeto = 0;
        foreach ($personales as $personal){
            $vector_variables = array();
            $vector_variables_patronal = array();
            $vector_variables_acumulado = array();
            $vector_evaluado = array();
            $vector_evaluado_patronal = array();
            $vector_evaluado_acumulado = array();
            unset($_SESSION);
            $session = $this->get('session');
            $session->start();
            $session->set('mensaje', 'Calculando Conceptos de Funcionario: '.$personal['id']);
            $session->set('valor', ($indice*100)/count($personales));
            $session->set('total', $indice.' de '.count($personales));
            $session->save();
            session_write_close();
            //Se determina que conceptos se van a calcular por funcionario de acuerdo a la personalizacion de los mismos
            $plantillapersonalizada = $em->getRepository('CetNominaBundle:ConceptoHasPlantillaNomina')
                                    ->findBy(array('fk_concepto_has_plantilla_nomina_plantilla_nomina1'=>$idplantillanomina,'fk_concepto_has_plantilla_nomina_personal1'=>$personal['id']));
            //Asignamos las variables de cada funcionario en memoria utilizando $$ como variables dinamicas
            foreach($todasVariables as $objeto){       
                $sqlvariable = $objeto['sql'];
                //Para calcular la variable con la fecha de la nómina     
                $fechaNomina = $entity->getHasta();
                $nombreVariable = $objeto['nombre'];
                try {
                    if(preg_match("/CURRENT_DATE()/i",$sqlvariable)){
                        $sqlvariable = preg_replace("/CURRENT_DATE\(\)/i", "(:fecha_nomina)", $sqlvariable);
                        $consultasql = $em->createQuery($sqlvariable);
                        $consultasql->setParameter('fecha_nomina',$fechaNomina,\Doctrine\DBAL\Types\Type::DATETIME);
                    }else{
                        $consultasql = $em->createQuery($sqlvariable);
                    }                       
                    $consultasql->setParameter('personal_cedula', $personal['id']);
                    $consultasql->setMaxResults(1);
                    $valorVariable=0;
                    if ($consultasql->getResult()){
                        foreach($consultasql->getSingleResult() as $dato){
                            if(!is_object($dato)){
                                $valorVariable=$dato;
                            }                    
                            elseif($dato instanceof \DateTime){
                                $valorVariable=$dato;
                            }
                        }
                    }            
                    $vector_variables[$nombreVariable]=$valorVariable;
                }catch (\Exception $e) {
                    $info = $e->getMessage();
                    unset($_SESSION);
                    $session = $this->get('session');
                    $session->start();
                    $session->set('mensaje', "Error calculando variable:$nombreVariable ".$info);
                    $session->set('valor', 0);
                    $session->set('total', "0 de 0");
                    $session->save();
                    session_write_close();
                }
            }
            ///////Inicio Variables Predefinidas
            //Cuenta cantidad de lunes en un lapso de fechas
            $helper = $this->get('cet.nomina.globales');
            $lunes = 1;
            $lunes = $helper->countDays(1,strtotime($entity->getDesde()->format('d-m-Y')),strtotime($entity->getHasta()->format('d-m-Y')));
            $vector_variables["cantidad_lunes"]=$lunes;
            $vector_variables["primera_quincena"] = true;
            //Verifica si Fecha Desde es mayor que 9 probablemente sea segunda quincena
            if( $entity->getDesde()->format('d') > 6 )
                $vector_variables["primera_quincena"] = false;
            //////////Fin Variables Predefinidas
            $vectorPersonalizado=array();
            foreach ($plantillapersonalizada as $datopersonalizado){                   
                $vectorPersonalizado[]=$datopersonalizado->getConceptoId(); // conceptos fijos
            }
            $vectorProcesar=array_merge( array_diff($vectorPersonalizado,$vectorFijos), array_diff($vectorFijos,$vectorPersonalizado));
            //Establece todos los conceptos eliminados de manera perzonalizada a cero
            $vectorEliminados = array_diff($vectorFijos,$vectorProcesar);
            foreach ($vectorEliminados as $eliminado){
                $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($eliminado);
                $vector_variables[$objeto->getIdentificador()] = 0;
                //Establecer el concepto como evaluado
                $vector_evaluado[] = $objeto->getIdentificador();
            }
            //Establece los valores de los conceptos a fuerza, según datos variables
            foreach ($vector_conceptos_id as $idconcepto){
                //Buscar si el concepto esta en datos variables para esa persona
                $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')
                                ->findBy(array('fk_datos_variables_concepto1'=>$idconcepto,'fk_datos_variables_personal1'=>$personal['id']));
                if($datosvariables!=null){
                    foreach($datosvariables as $datosvariable){                        
                        if($datosvariable->getSeccionAfectacion()=='principal'){
                            $valor = $datosvariable->getValor();
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables[$objeto->getIdentificador()] = $valor;
                            $vector_evaluado[] = $objeto->getIdentificador();
                        }
                        elseif($datosvariable->getSeccionAfectacion()=='patronal'){
                            $valorPatronal = $datosvariable->getValor();
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables_patronal[$objeto->getIdentificador()] = $valorPatronal;
                            $vector_evaluado_patronal[] = $objeto->getIdentificador();
                        }
                        elseif($datosvariable->getSeccionAfectacion()=='acumulado'){
                            $acumulado = $datosvariable->getValor();
                            $objeto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
                            $vector_variables_acumulado[$objeto->getIdentificador()] = $acumulado;
                            $vector_evaluado_acumulado[] = $objeto->getIdentificador();
                        }
                    }
                }
            }
            $var=new VariableFormula();        
            $vector_variables['Variable']=$var;
            foreach ($vectorProcesar as $concepto){
                $this->calculaConceptoDirecto($concepto, $personal['id'],$idnomina,$todasVariables,$todosConceptos,$vector_variables,$vector_variables_patronal,$vector_variables_acumulado,$vector_conceptos,$vector_evaluado,$vector_evaluado_patronal,$vector_evaluado_acumulado,$personal['conceptos']); 
            }
            $indice++;
        }
        unset($_SESSION);
        $session = $this->get('session');
        $session->start();
        $session->set('mensaje', 'Calculando Variables de Funcionario: '.$personal['id']);
        $session->set('valor', ($indice*100)/count($personales));
        $session->set('total', $indice.' de '.count($personales));
        $session->save();
        session_write_close();
        unset($_SESSION);        
        return new JsonResponse(array('mensaje' => 'La nómina ha sido completamente calculada','valor' => 100,'total' => $indice.' de '.$indice));
    }
    
    public function calculaConceptoDirecto($idconcepto,$idpersonal,$idnomina,$datos,$conceptos,&$vector_variables,&$vector_variables_patronal,&$vector_variables_acumulado,$vector_conceptos,&$vector_evaluado,&$vector_evaluado_patronal,&$vector_evaluado_acumulado, $conceptodirecto)
    {
        $em = $this->getDoctrine()->getManager();     
        //Para evitar el desbordamiento de memoria
        $ubicarConcepto=array('vacacional'=>array("Bono Vacacional","vacaci","bonovaci","bono_vacaci"),
                       'prestaciones'=>array("prestaciones sociales","prestaciones_socia","prestacionessocia"),
                       'diasadicional'=>array("Dias Adicionales","dias_adicionales","Dias_adcionales"));
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $concepto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($idpersonal);
        $nomina = $em->getRepository('CetNominaBundle:Nomina')->find($idnomina);
        
        $identificador=$concepto->getIdentificador();
        $descripcion=$concepto->getDescripcion();
        //Se hace la consulta de la formula almacenada en Concepto
        $formula_concepto = $concepto->getSql();
        $formula_concepto_patronal = $concepto->getFormulaPatronal();
        
        $vector_conceptosInicial= $vector_conceptos;
        $vector_variablesInicial=$vector_variables;
        $language = new LenguajeFormula();
        $valor = 0;        
        //Verificar si el concepto no ha sido evaluado
        $clave1 = array_search($identificador,$vector_evaluado);
        if( $clave1 === false ){        
            //Calcula el valor para la sección principal                
            try {  
                //Funcion Recursiva para calcular conceptos anidados            
                $patrones=$ubicarConcepto['vacacional'];
                foreach($patrones as $patron){
                    if(preg_match("/$patron/", $identificador) or  preg_match("/$patron/", $descripcion )){
                        $valor=$conceptodirecto['vacacional'];
                    }
                }
                $patrones=$ubicarConcepto['prestaciones'];
                foreach($patrones as $patron){
                    if(preg_match("/$patron/", $identificador) or  preg_match("/$patron/", $descripcion )){
                        $valor=$conceptodirecto['prestaciones'];
                    }
                }
                $patrones=$ubicarConcepto['diasadicional'];
                foreach($patrones as $patron){
                    if(preg_match("/$patron/", $identificador) or  preg_match("/$patron/", $descripcion )){
                        $valor=$conceptodirecto['diasadicional'];
                    }
                }
            } catch (\Exception $exc) {
                //Retornar error al calcular en caso de no poder evaluar recursivamente la formula
                $valor = 0;
            }
            $vector_variables[$identificador] = $valor;
            $vector_evaluado[] = $identificador;
        }else{
            $valor = $vector_variables[$identificador];            
        }
        $valorPatronal = 0;
        //Verificar si el aporte patronal del concepto no ha sido evaluado
        $clave2 = array_search($identificador,$vector_evaluado_patronal);
        if( $clave2 === false ){  
            //Calcula el valor de la sección de aporte patronal
            try {  
                //Funcion Recursiva para calcular conceptos anidados en aporte patronal             
                //$valorPatronal = $this->evaluarConcepto($formula_concepto_patronal, $vector_variables, $vector_conceptos, $vector_evaluado, $idpersonal);
                $valorPatronal = 0;
            } catch (\Exception $exc) {
                //Retornar error al calcular en caso de no poder evaluar recursivamente la formula
                $valorPatronal = 0;
            }
        }else{
            $valorPatronal = $vector_variables_patronal[$identificador];            
        }
        $acumulado=0;
        //Verificar si el acumulado del concepto no ha sido evaluado
        $clave3 = array_search($identificador,$vector_evaluado_acumulado);
        if( $clave3 === false ){ 
            //Verificar  si este concepto tiene otros que lo acumulen
            $acumuladoOtros=0;
            foreach($concepto->getFkConceptoHasConcepto2() as $conceptoAcumulador){
                $acm[]=$conceptoAcumulador->getId();
                $conceptoId=$conceptoAcumulador->getId();
                $formula_concepto=$conceptoAcumulador->getSql();
    //            $vector_evaluadoA=array();
                $vector_evaluadoA=$vector_evaluado;
                $vector_conceptosA=$vector_conceptosInicial;
                $vector_variablesA=$vector_variablesInicial;
                try {  
                    $valorA = 0;
                } catch (\Exception $exc) {
                    $valorA = 0;
                }
                $acumuladoOtros+=$valorA;
            }    
            //Calcula el valor para la sección de acumulado                
            $valoracumulado=0;
            $historicoConceptoAcumulado = $em->getRepository('CetNominaBundle:HistoricoConcepto')->encuentra($idconcepto,$idpersonal);
            if($historicoConceptoAcumulado){
                $valoracumulado=$historicoConceptoAcumulado->getAcumulado();
            }        
            $acumulado=(($valoracumulado==null)?0:$valoracumulado)+ $valor + $valorPatronal + $acumuladoOtros;            
        }else{
            $acumulado = $vector_variables_acumulado[$identificador];            
        }    
        $hoy = new DateTime('NOW');
        $historicoConceptonuevo = new HistoricoConcepto(); 
        //No guardar en historico si el valor o aporte patronal es cero 0
        if( ($valor<>0 || $valorPatronal<>0) || ( preg_match('/^permiso_no_remunerado/i',$identificador) ) )
        {
                $historicoConceptonuevo->setConceptoId($idconcepto);
                $historicoConceptonuevo->setFkHistoricoConceptoConcepto1($concepto);
                $historicoConceptonuevo->setPersonalCedula($idpersonal);
                $historicoConceptonuevo->setFkHistoricoConceptoPersonal1($personal);
                $historicoConceptonuevo->setNominaId($idnomina);
                $historicoConceptonuevo->setFkHistoricoConceptoNomina1($nomina);
                $historicoConceptonuevo->setMonto($valor);
                $historicoConceptonuevo->setMontoPatronal($valorPatronal);
                $historicoConceptonuevo->setAcumulado($acumulado);
                $historicoConceptonuevo->setFechaInicio($nomina->getDesde()); 
                $historicoConceptonuevo->setFechaFin($nomina->getHasta());                 
                $em->persist($historicoConceptonuevo); 
                $em->flush();
        } 
        $em->clear();
        //Bloque para guardar total asignaciones, total deducciones y total neto
    }
//    public function evalPatron($ubicarConcepto,$seccion,$conceptodirecto, $identificador,$descripcion){
//        $valor=0;
//        $patrones=$ubicarConcepto["$seccion"];
//        foreach($patrones as $patron){
//            if(preg_match("/$patron/", $identificador) or  preg_match("/$patron/", $descripcion )){
//                return $conceptodirecto["$seccion"];
//            }
//        }
//    }
}
