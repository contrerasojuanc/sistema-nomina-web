<?php

namespace Cet\NominaBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\HistoricoEvento;
use Cet\NominaBundle\Form\HistoricoEventoType;

/**
 * HistoricoEvento controller.
 *
 */
class HistoricoEventoController extends Controller
{

    /**
     * Lists all HistoricoEvento entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

//        $entities = $em->getRepository('CetNominaBundle:HistoricoEvento')->findAll();
        
        //Solo se muestran los ultimos 300 registros. Para ver la totalidad de los mismos se requeire usar el gestor de base de datos.
        $entities = $em->getRepository('CetNominaBundle:HistoricoEvento')->findBy(
            array(), //where
            array('fecha' => 'DESC'), //order by
            300, //limit
            0 //offset
        );

        return $this->render('CetNominaBundle:HistoricoEvento:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a HistoricoEvento entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:HistoricoEvento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad HistoricoEvento.');
        }

        return $this->render('CetNominaBundle:HistoricoEvento:show.html.twig', array(
            'entity'      => $entity,
        ));
    }
}
