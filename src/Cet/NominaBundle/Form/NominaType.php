<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NominaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $array_tipo=array('Ordinaria'=>'Ordinaria', 'Eventual'=>'Eventual', 'Ocasional'=>'Ocasional');
        $em = $options['em'];
        
        $builder
            ->add('descripcion', null,array('label' => 'Descripción de Nómina','attr' => array('help'=>'Ingrese la denominación de la nómina')))
//            ->add('fecha', 'date',array('data' => new \DateTime(),'label' => 'Fecha de Proceso','widget' => 'single_text','attr' => array('help'=>'Fecha de generación de nómina')))
            ->add('fecha', 'date',array('label' => 'Fecha de Proceso','widget' => 'single_text','attr' => array('help'=>'Fecha de generación de nómina')))
            ->add('desde', 'date',array('label' => 'Lapso Desde','widget' => 'single_text','attr' => array('help'=>'Fecha de inicio del lapso de la nómina')))
            ->add('hasta', 'date',array('label' => 'Lapso Hasta','widget' => 'single_text','attr' => array('help'=>'Fecha de fin del lapso de la nómina')))
//            ->add('fechaAbono', 'date',array('data' => new \DateTime(),'label' => 'Fecha de Abono','widget' => 'single_text','attr' => array('help'=>'Fecha de abono de nómina en cuentas de funcionarios')))
            ->add('fechaAbono', 'date',array('label' => 'Fecha de Abono','widget' => 'single_text','attr' => array('help'=>'Fecha de abono de nómina en cuentas de funcionarios')))
            ->add('periodo', null,array('label' => 'Periodo','attr' => array('help'=>'Ingrese el número del periodo a procesar (2 = nómina mensual de febrero, 2 = segunda quincena de enero)')))
            ->add('anio', null,array('label' => 'Año','attr' => array('help'=>'Ingrese el año correspondiente a la nómina')))
                        ->add('tipo', 'choice',array(
                'label' => 'Carácter de nómina',
                'mapped' => true,
                'required' => true,
                'multiple' => false,
                'choices' => $array_tipo,
                'attr' => array('class' => 'select2me','help'=>'Seleccione el carácter de la nómina (Ordinaria=Nómina Mensual; Eventual=Pagos con incidencia Salarial; Ocasional=Sin Incidencia Salarial)')))    
            ->add('fk_nomina_plantilla_nomina1', null,array('label' => 'Plantilla de Nómina',
                'attr' => array('class' => 'select2me', 'data-placeholder' => 'Seleccione Plantilla de Nómina','help'=>'Seleccione la plantilla de nómina base')))
               
            ->add('prestacionesSocialesGeneral',null,array(
                   'multiple' => false,
                   'query_builder' => $em->getRepository('CetNominaBundle:PrestacionesSocialesGeneral')->showAvailables($builder),
                   'label'=>'Seleccionar Prestación Social',
                            'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione Prestación Social','help'=>'Seleccione el calculo de prestaciones')))                

            ->add('vacaciones',null,array(
                   'multiple' => true,                   
                   'mapped' => true,
                   'query_builder' => $em->getRepository('CetNominaBundle:Vacaciones')->showAvailables($builder),

                'label'=>'Seleccionar Bonos Vacacionales','attr' => array('help'=>'Seleccione el cálculo de bono vacacional')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Nomina'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_nomina';
    }
}
