<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\TipoCalculo
 *
 * @ORM\Entity(repositoryClass="TipoCalculoRepository")
 * @ORM\Table(name="sis_nomina.tipo_calculo")
 */
class TipoCalculo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="PlantillaNomina", mappedBy="fk_tipocalculo")
     * @ORM\JoinColumn(name="tipocalculo_id", referencedColumnName="id", nullable=true)
     */
    protected $nominas;
    
    public function __construct()
    {
        $this->nominas = new ArrayCollection();
    }

   
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('nombre', new Assert\NotBlank()) 
             ;          
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoCalculo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add nominas
     *
     * @param \Cet\NominaBundle\Entity\Nomina $nominas
     * @return TipoCalculo
     */
    public function addNomina(\Cet\NominaBundle\Entity\Nomina $nominas)
    {
        $this->nominas[] = $nominas;

        return $this;
    }

    /**
     * Remove nominas
     *
     * @param \Cet\NominaBundle\Entity\Nomina $nominas
     */
    public function removeNomina(\Cet\NominaBundle\Entity\Nomina $nominas)
    {
        $this->nominas->removeElement($nominas);
    }

    /**
     * Get nominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNominas()
    {
        return $this->nominas;
    }
     public function __toString()
    {
        return $this->getNombre();
    }
}
