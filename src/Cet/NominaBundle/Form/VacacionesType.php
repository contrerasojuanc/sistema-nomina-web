<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use DateTime;

class VacacionesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new EntityToIntTransformer($options['em']);
        $userTransformer->setEntityClass("Cet\NominaBundle\Entity\Personal");
        $userTransformer->setEntityRepository("CetNominaBundle:Personal");
        $userTransformer->setEntityType("Personal");
        
        $inicio=2010;
        $fecha = new DateTime('NOW');
        $anio = $fecha->format('Y');
        $iteracion = $anio - $inicio;
        $periodos=array();
        for($i=0;$i<=$iteracion;$i++){
            $periodos[($inicio+$i)."-".($inicio+($i+1))]= ($inicio+$i)."-".($inicio+($i+1));
        }        
        $builder
            ->add('periodo','choice', array(
                'mapped' => true,
                'required' => true,
                'choices' =>$periodos,
            ))
            ->add('fecha','date', array(
                'label'=> 'Fecha',
                'widget' => 'single_text',
                ))  
            ->add($builder->create('personal','hidden')->addModelTransformer($userTransformer))
            //->add('fecha','text',array('attr' => array('class' => 'date-picker','readonly'=>'readonly')))
//          
//          ->add('personal', null,array('label' => 'Cedula','attr' => array('help'=>'','disabled'=>'disabled')))
//          ->add('personal')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Vacaciones'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_vacaciones';
    }
}
