<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Vehiculo
 *
 * @ORM\Entity(repositoryClass="VehiculoRepository")
 * @ORM\Table(name="sis_nomina.vehiculo", indexes={@ORM\Index(name="fk_vehiculo_personal1_idx", columns={"personal_cedula"})}, uniqueConstraints={@ORM\UniqueConstraint(name="placa_UNIQUE", columns={"placa"})})
  * @UniqueEntity(
 *     fields={"placa"},
 *     message="Número de placa ya registrado, verifique"
 * )
 */
class Vehiculo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="placa", type="string", length=10, unique=true, nullable=true)
     */
    private $placa;

    /**
     * @var string
     * @ORM\Column(name="modelo", type="string", length=45, nullable=true)
     */
    private $modelo;

    /**
     * @var integer
     * @ORM\Column(name="anio", type="integer", nullable=true)
     */
    private $anio;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="vehiculos")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_vehiculo_personal1;

    public function __construct()
    {
    }


    /**
     * Set id
     *
     * @param integer $id
     * @return Vehiculo
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set placa
     *
     * @param string $placa
     * @return Vehiculo
     */
    public function setPlaca($placa)
    {
        $this->placa = $placa;

        return $this;
    }

    /**
     * Get placa
     *
     * @return string 
     */
    public function getPlaca()
    {
        return $this->placa;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Vehiculo
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return Vehiculo
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set fk_vehiculo_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkVehiculoPersonal1
     * @return Vehiculo
     */
    public function setFkVehiculoPersonal1(\Cet\NominaBundle\Entity\Personal $fkVehiculoPersonal1)
    {
        $this->fk_vehiculo_personal1 = $fkVehiculoPersonal1;

        return $this;
    }

    /**
     * Get fk_vehiculo_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkVehiculoPersonal1()
    {
        return $this->fk_vehiculo_personal1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  
                ->addPropertyConstraint ('placa', new Assert\NotBlank())             
                ->addPropertyConstraint('placa', new Assert\Length(array(
                    'min'=> 6,
                    'minMessage'=> 'El campo no debe ser menor a 6 digitos o caracteres.', 
                    'max'=> 10,
                    'maxMessage'=> 'El campo no debe ser mayor a 10 digitos o caracteres.',
                ))) 
                ->addPropertyConstraint ('modelo',  new Assert\NotBlank())   
                ->addPropertyConstraint ('anio', new Assert\NotBlank())
                ->addPropertyConstraint ('anio', new Assert\Type(array('type'=>'numeric')))             
                ->addPropertyConstraint('anio', new Assert\Length(array(
                    'min'=> 4,
                    'minMessage'=> 'El campo no debe ser menor a 4 digitos.', 
                    'max'=> 4,
                    'maxMessage'=> 'El campo no debe ser mayor a 4 digitos.',
                )))             
            ;
    } 
}
