<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\PrestacionesSociales;
use Cet\NominaBundle\Form\PrestacionesSocialesType;

/**
 * PrestacionesSociales controller.
 *
 */
class PrestacionesSocialesController extends Controller
{

    /**
     * Lists all PrestacionesSociales entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:PrestacionesSociales')->findAll();

        return $this->render('CetNominaBundle:PrestacionesSociales:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new PrestacionesSociales entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new PrestacionesSociales();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('prestacionessociales_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:PrestacionesSociales:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a PrestacionesSociales entity.
    *
    * @param PrestacionesSociales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(PrestacionesSociales $entity)
    {
        $form = $this->createForm(new PrestacionesSocialesType(), $entity, array(
            'action' => $this->generateUrl('prestacionessociales_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new PrestacionesSociales entity.
     *
     */
    public function newAction()
    {
        $entity = new PrestacionesSociales();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:PrestacionesSociales:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a PrestacionesSociales entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PrestacionesSociales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSociales.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:PrestacionesSociales:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing PrestacionesSociales entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PrestacionesSociales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSociales.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:PrestacionesSociales:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a PrestacionesSociales entity.
    *
    * @param PrestacionesSociales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(PrestacionesSociales $entity)
    {
        $form = $this->createForm(new PrestacionesSocialesType(), $entity, array(
            'action' => $this->generateUrl('prestacionessociales_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing PrestacionesSociales entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:PrestacionesSociales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSociales.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('prestacionessociales_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:PrestacionesSociales:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a PrestacionesSociales entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:PrestacionesSociales')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad PrestacionesSociales.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('prestacionessociales'));
    }

    /**
     * Creates a form to delete a PrestacionesSociales entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prestacionessociales_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
