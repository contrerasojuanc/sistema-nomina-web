<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\ExperienciaLaboral
 *
 * @ORM\Entity(repositoryClass="ExperienciaLaboralRepository")
 * @ORM\Table(name="sis_nomina.experiencia_laboral", indexes={@ORM\Index(name="fk_experiencia_laboral_base_legal1_idx", columns={"base_legal_id"}), @ORM\Index(name="fk_experiencia_laboral_personal1_idx", columns={"personal_cedula"})})
 */
class ExperienciaLaboral
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="institucion", type="text", nullable=true)
     */
    private $institucion;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_ingreso", type="date", nullable=true)
     */
    private $fechaIngreso;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_egreso", type="date", nullable=true)
     */
    private $fechaEgreso;

    /**
     * @var string
     * @ORM\Column(name="cargo", type="text", nullable=true)
     */
    private $cargo;

    /**
     * @var float
     * @ORM\Column(name="sueldo", type="float", nullable=true)
     */
    private $sueldo;
    
     /**
     * Solo valores:
     * A = Admon pública
     * E = Empresa privada
     *
     * @var string
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @ORM\OneToOne(targetEntity="BaseLegal", inversedBy="experienciaLaboral")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $baseLegal;

    /**
     * @ORM\ManyToOne(targetEntity="Personal", inversedBy="experienciaLaborals")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $fk_experiencia_laboral_personal1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set institucion
     *
     * @param string $institucion
     * @return ExperienciaLaboral
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;

        return $this;
    }

    /**
     * Get institucion
     *
     * @return string 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return ExperienciaLaboral
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set fechaEgreso
     *
     * @param \DateTime $fechaEgreso
     * @return ExperienciaLaboral
     */
    public function setFechaEgreso($fechaEgreso)
    {
        $this->fechaEgreso = $fechaEgreso;

        return $this;
    }

    /**
     * Get fechaEgreso
     *
     * @return \DateTime 
     */
    public function getFechaEgreso()
    {
        return $this->fechaEgreso;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     * @return ExperienciaLaboral
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string 
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set sueldo
     *
     * @param float $sueldo
     * @return ExperienciaLaboral
     */
    public function setSueldo($sueldo)
    {
        $this->sueldo = $sueldo;

        return $this;
    }

    /**
     * Get sueldo
     *
     * @return float 
     */
    public function getSueldo()
    {
        return $this->sueldo;
    }
    
    /**
     * Set tipo
     *
     * @param string $tipo
     * @return ExperienciaLaboral
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set baseLegal
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegal
     * @return ExperienciaLaboral
     */
    public function setBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegal = null)
    {
        $this->baseLegal = $baseLegal;

        return $this;
    }

    /**
     * Get baseLegal
     *
     * @return \Cet\NominaBundle\Entity\BaseLegal 
     */
    public function getBaseLegal()
    {
        return $this->baseLegal;
    }

    /**
     * Set fk_experiencia_laboral_personal1
     *
     * @param \Cet\NominaBundle\Entity\Personal $fkExperienciaLaboralPersonal1
     * @return ExperienciaLaboral
     */
    public function setFkExperienciaLaboralPersonal1(\Cet\NominaBundle\Entity\Personal $fkExperienciaLaboralPersonal1)
    {
        $this->fk_experiencia_laboral_personal1 = $fkExperienciaLaboralPersonal1;

        return $this;
    }

    /**
     * Get fk_experiencia_laboral_personal1
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getFkExperienciaLaboralPersonal1()
    {
        return $this->fk_experiencia_laboral_personal1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('institucion', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('institucion', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint('fechaIngreso', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                //->addPropertyConstraint('fechaEgreso', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('cargo', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('cargo', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint('sueldo', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))         
                ->addPropertyConstraint('sueldo', new Assert\Type(array('type'=>'numeric','message'=>'El campo debe ser solo números.')))             
    ;     
    }
    
    public function __toString()
    {
        return $this->getInstitucion();
    }
}
