<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CargaMasivaType extends AbstractType
{
    private $nuevo = true;
    private $opcion;
    
    public function __construct($valor, $opcion, $id = null){
        $this->nuevo = $valor;
        $this->opcion = $opcion;
    }
    
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->nuevo){
            if($this->opcion=='genericos'){
                $builder
                ->add('nombre',null,array(
                    'label'=> 'Descripción',
                    'attr' => array('help'=>'Descripción detallada de la carga masiva',
                        )))
                ->add('fk_dato_generico_tipo_dato_generico1',null,array(
                    'empty_value' => 'Seleccione el tipo dde dato generico',
                    'required' => true,
                    'label'=> 'Tipo dato Generico',
                    'attr' => array('help'=>'Tipo de dato generico de la carga masiva',
                    'class' => 'select2me','data-placeholder' => 'Seleccione'  
                        )))        
                ->add('rutatxt', 'file', array(
                    'label'=> 'Adjuntar archivo TXT',
                    'data_class' => null,
                    'attr' => array('help'=>'Utilice para adjuntar nuevo archivo, solo se permite un (1) por registro.'),
                    'required' => true,
                ))
                ;
            }elseif($this->opcion=='variables'){
//                $choices_modo=array('sustitucion'=>'Sustitución');
//                $choices_seccion=array('principal'=>'Sección Principal','patronal'=>'Sección Patronal','acumulado'=>'Sección de Acumulado');
                $choices_modo=array('sustitucion'=>'Sustitución');
                $choices_seccion=array('principal'=>'Sección Principal','patronal'=>'Sección Patronal','acumulado'=>'Sección de Acumulado','vacacional'=>'Bono Vacacional','prestaciones'=>'Garantia Prestaciones sociales','adicionales'=>'Días Adicionales de Garantía de Prestaciones Sociales'); 

                $builder
                ->add('nombre',null,array(
                    'label'=> 'Descripción',
                    'attr' => array('help'=>'Descripción detallada de la carga masiva',
                        )))
//                ->add('fk_concepto1',null,array(
//                    'empty_value' => 'Seleccione el concepto que afectara la carga masiva',
//                    'required' => true,
//                    'label'=> 'Concepto',
//                    'attr' => array('help'=>'Concepto que será afectado por la carga masiva',
//                    'class' => 'select2me','data-placeholder' => 'Seleccione'      
//                        )))
                ->add('fk_concepto1',null,array(
                       'query_builder' => function($r) {
                        return $r->createQueryBuilder('concepto')
                                   ->orderBy('concepto.codigo','ASC');
                        },
                   'label'=>'Concepto de nomina','property'=>'getNombreCompleto','attr' => array('help'=>'Seleccione el concepto que será afectado por la carga masiva','class' => 'select2me','data-placeholder' => 'Seleccione el concepto')))   
                ->add('modoAfectacion', 'choice', array(
                    'label' => 'Modo de Afectación',
                    'attr' => array('help'=>'Seleccione el modo de afectación del concepto en la carga masiva.'),
                    'choices' =>$choices_modo,
                    'mapped' => true,   
                ))         
                ->add('seccionAfectacion','choice', array(
                    'mapped' => true,
                    'required' => true,
                    'choices' =>$choices_seccion,
                    'attr' => array('help'=>'Seleccione la sección donde será modificado el monto.','class' => 'select2me','data-placeholder' => 'Seleccione una sección')   
                ))          
                ->add('rutatxt', 'file', array(
                    'label'=> 'Adjuntar archivo TXT',
                    'data_class' => null,
                    'attr' => array('help'=>'Utilice para adjuntar nuevo archivo, solo se permite un (1) por registro.'),
                    'required' => true,
                ))
                ;
            }
        }else{
            if($this->opcion=='genericos'){
                $builder
                ->add('nombre',null,array(
                    'label'=> 'Descripción',
                    'attr' => array('help'=>'Descripción detallada de la carga masiva',
                        )))
                ->add('rutatxt', 'file', array(
                    'label'=> 'Adjuntar archivo TXT',
                    'data_class' => null,
                    'attr' => array('help'=>'Utilice para adjuntar nuevo archivo, solo se permite un (1) por registro.'),
                    'required' => false,
                ))
                ->add('fk_dato_generico_tipo_dato_generico1',null,array(
                    'empty_value' => 'Seleccione el tipo dde dato generico',
                    'required' => true,
                    'label'=> 'Tipo dato Generico',
                    'attr' => array('help'=>'Descripción detallada de la carga masiva',
                    'class' => 'select2me','data-placeholder' => 'Seleccione'      
                     )))        
                ->add('nombretxt',null, array(
                    'label'=> 'Archivo TXT cargado',
                    'data_class' => null,
                    'required' => false,
                    'attr' => array('help'=>'Nombre del archivo de cargado, Solo para verificación.'),
                    'disabled'=>'disabled'
                )) 
                ->add('estado','choice', array(
                    'label'=> 'Estado',
                    'choices' => array('' => 'Ninguno','C' => 'Cargada', 'P' => 'Procesado', 'E' => 'Procesado con errores'),
                    'data_class' => null,
                    'required' => false,
                    'attr' => array('help'=>'Estado de la carga masiva, Solo para verificación.'),
                    'disabled'=>'disabled'
                ))     
                ->add('fechaCarga','date', array(
                    'label'=> 'Fecha de carga',
                    'attr' => array('help'=>'Fecha de carga del archivo TXT, Solo para verificación.'),
                    'widget' => 'single_text',
                    'required'=>false,
                    'disabled'=>'disabled'
                ))
                ->add('fechaProceso','date', array(
                    'label'=> 'Fecha de procesamiento',
                    'attr' => array('help'=>'Fecha de procesamiento del archivo TXT, Solo para verificación.'),
                    'widget' => 'single_text',
                    'required'=>false,
                    'disabled'=>'disabled'
                ))  
                ;
            }elseif($this->opcion=='variables'){
                $choices_modo=array('sustitucion'=>'Sustitución');                
                $choices_seccion=array('principal'=>'Sección Principal','patronal'=>'Sección Patronal','acumulado'=>'Sección de Acumulado','vacacional'=>'Bono Vacacional','prestaciones'=>'Garantia Prestaciones sociales','adicionales'=>'Días Adicionales de Garantía de Prestaciones Sociales'); 
//                $choices_seccion=array('principal'=>'Sección Principal','patronal'=>'Sección Patronal','acumulado'=>'Sección de Acumulado','prestaciones'=>'Garantia Prestaciones sociales','adicionales'=>'Días Adicionales de Garantía de Prestaciones Sociales');
                $builder
                ->add('nombre',null,array(
                    'label'=> 'Descripción',
                    'attr' => array('help'=>'Descripción detallada de la carga masiva',
                        )))
                ->add('rutatxt', 'file', array(
                    'label'=> 'Adjuntar archivo TXT',
                    'data_class' => null,
                    'attr' => array('help'=>'Utilice para adjuntar nuevo archivo, solo se permite un (1) por registro.'),
                    'required' => false,
                ))   
                ->add('nombretxt',null, array(
                    'label'=> 'Archivo TXT cargado',
                    'data_class' => null,
                    'required' => false,
                    'attr' => array('help'=>'Nombre del archivo de cargado, Solo para verificación.'),
                    'disabled'=>'disabled'
                )) 
                ->add('fk_concepto1',null,array(
                    'empty_value' => 'Seleccione el concepto que afectará la carga masiva',
                    'required' => true,
                    'label'=> 'Concepto',
                    'attr' => array('help'=>'Concepto que será afectado por la carga masiva',
                        'class' => 'select2me','data-placeholder' => 'Seleccione'      
                    ))) 
                ->add('modoAfectacion', 'choice', array(
                    'label' => 'Modo de Afectación',
                    'required' => true,
                    'attr' => array('help'=>'Seleccione el modo de afectación del concepto en la carga masiva.'),
                    'choices' =>$choices_modo,
                    'mapped' => true,   
                ))         
                ->add('seccionAfectacion','choice', array(
                    'mapped' => true,
                    'required' => true,
                    'choices' =>$choices_seccion,
                    'attr' => array('help'=>'Seleccione la sección donde será modificado el monto.','class' => 'select2me','data-placeholder' => 'Seleccione una sección')   
                ))     
                ->add('estado','choice', array(
                    'label'=> 'Estado',
                    'choices' => array('' => 'Ninguno','C' => 'Cargada', 'P' => 'Procesado', 'E' => 'Procesado con errores'),
                    'data_class' => null,
                    'required' => false,
                    'attr' => array('help'=>'Estado de la carga masiva, Solo para verificación.'),
                    'disabled'=>'disabled'
                ))     
                ->add('fechaCarga','date', array(
                    'label'=> 'Fecha de carga',
                    'attr' => array('help'=>'Fecha de carga del archivo TXT, Solo para verificación.'),
                    'widget' => 'single_text',
                    'required'=>false,
                    'disabled'=>'disabled'
                ))
                ->add('fechaProceso','date', array(
                    'label'=> 'Fecha de procesamiento',
                    'attr' => array('help'=>'Fecha de procesamiento del archivo TXT, Solo para verificación.'),
                    'widget' => 'single_text',
                    'required'=>false,
                    'disabled'=>'disabled'
                ))  
                ;
            }
        }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\CargaMasiva'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_cargamasiva';
    }
}
