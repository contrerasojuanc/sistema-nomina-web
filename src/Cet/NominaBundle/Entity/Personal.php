<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Personal
 *
 * @ORM\Entity(repositoryClass="PersonalRepository")
 * @ORM\Table(name="sis_nomina.personal")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields={"id"},
 *     message="Número de cedula ya registrado, verifique"
 * )
 */
class Personal
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="cedula", type="integer", nullable=false)
     */
    private $id;

    /**
     * Solo valores:
     * V = Venezolano(a)
     * E = Extranjero(a)
     *
     * @var string
     * @ORM\Column(name="nacionalidad", type="string", length=1, nullable=true)
     */
    private $nacionalidad;

    /**
     * @var string
     * @ORM\Column(name="primer_nombre", type="text", nullable=true)
     */
    private $primerNombre;

    /**
     * @var string
     * @ORM\Column(name="segundo_nombre", type="text", nullable=true)
     */
    private $segundoNombre;

    /**
     * @var string
     * @ORM\Column(name="primer_apellido", type="text", nullable=true)
     */
    private $primerApellido;

    /**
     * @var string
     * @ORM\Column(name="segundo_apellido", type="text", nullable=true)
     */
    private $segundoApellido;

    /**
     * Solo valores:
     * M = Masculino
     * F = Femenino
     *
     * @var string
     * @ORM\Column(name="genero", type="string", length=1, nullable=true)
     */
    private $genero;

    /**
     * Solo valores:
     * S = Soltero(a)
     * C = Casado(a)
     * D = Divorciado(a)
     * V = Viudo(a)
     *
     * @var string
     * @ORM\Column(name="estado_civil", type="string", length=1, nullable=true)
     */
    private $estadoCivil;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_nacimiento", type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @var string
     * @ORM\Column(name="libreta_militar", type="string", length=45, nullable=true)
     */
    private $libretaMilitar;

    /**
     * @var string
     * @ORM\Column(name="rif", type="string", length=45, nullable=true)
     */
    private $rif;

    /**
     * @var string
     * @ORM\Column(name="licencia", type="string", length=45, nullable=true)
     */
    private $licencia;

    /**
     * @var string
     * @ORM\Column(name="foto", type="text", nullable=true)
     */
    private $foto;
    
    /**
     * @var string
     * @ORM\Column(name="ruta_foto", type="text", nullable=true)
     */
    private $rutaFoto;
    
    /**
     * @var string
     * @ORM\Column(name="altura", type="string", length=10, nullable=true)
     */
    private $altura;

    /**
     * @var string
     * @ORM\Column(name="peso", type="string", length=10, nullable=true)
     */
    private $peso;

    /**
     * @var string
     * @ORM\Column(name="talla_camisa", type="string", length=10, nullable=true)
     */
    private $tallaCamisa;

    /**
     * @var string
     * @ORM\Column(name="talla_pantalon", type="string", length=10, nullable=true)
     */
    private $tallaPantalon;

    /**
     * @var string
     * @ORM\Column(name="talla_calzado", type="string", length=10, nullable=true)
     */
    private $tallaCalzado;

    /**
     * @ORM\OneToMany(targetEntity="CargaFamiliar", mappedBy="fk_carga_familiar_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $cargaFamiliars;

    /**
     * @ORM\OneToMany(targetEntity="CorreoPersonal", mappedBy="fk_correo_personal_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $correoPersonals;
    
    /**
     * @ORM\OneToMany(targetEntity="ConceptoHasPlantillaNomina", mappedBy="fk_concepto_has_plantilla_nomina_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $conceptoHasPlantillaNominas;
    
    /**
     * @ORM\OneToMany(targetEntity="DatosVariables", mappedBy="fk_datos_variables_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $datosVariables;
    
    /**
     * @ORM\OneToMany(targetEntity="Estudio", mappedBy="fk_estudios_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $estudios;

    /**
     * @ORM\OneToMany(targetEntity="ExperienciaLaboral", mappedBy="fk_experiencia_laboral_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $experienciaLaborals;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoConcepto", mappedBy="fk_historico_concepto_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $historicoConceptos;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoVariable", mappedBy="fk_variable_has_personal_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $historicoVariables;

    /**
     * @ORM\OneToMany(targetEntity="InformacionCulturalDeportiva", mappedBy="fk_informacion_cultural_deportiva_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $informacionCulturalDeportivas;

    /**
     * @ORM\OneToOne(targetEntity="InformacionLaboral", mappedBy="personal")
     */
    protected $informacionLaboral;

    /**
     * @ORM\OneToOne(targetEntity="InformacionMedica", mappedBy="personal")
     */
    protected $informacionMedica;

    /**
     * @ORM\OneToMany(targetEntity="TelefonoPersonal", mappedBy="fk_telefono_personal_personal")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $telefonoPersonals;

    /**
     * @ORM\OneToMany(targetEntity="Vehiculo", mappedBy="fk_vehiculo_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $vehiculos;

    /**
     * @ORM\OneToMany(targetEntity="Vacaciones", mappedBy="personal")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $vacaciones;
    
    /**
     * @ORM\OneToMany(targetEntity="PrestacionesSociales", mappedBy="personal")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $prestaciones;
    
    /**
     * @ORM\OneToMany(targetEntity="DiasPendientes", mappedBy="personal")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $diasPendientes;
    
    /**
     * @ORM\OneToOne(targetEntity="Vivienda", mappedBy="personal")
     */
    protected $vivienda;
    
    /**
     * @ORM\OneToMany(targetEntity="DatosGenericos", mappedBy="fk_datosGenericos_personal1")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $datosGenericos;

    /**
     * @ORM\OneToOne(targetEntity="userPersonalM", mappedBy="personal")     
     */
    protected $userPersonalM;
    
    /**
     * @ORM\OneToMany(targetEntity="Reposo", mappedBy="especialidad")
     * @ORM\JoinColumn(name="reposo_id", referencedColumnName="id")
     */
    protected $reposos;
    
    public function __construct()
    {
        $this->cargaFamiliars = new ArrayCollection();
        $this->correoPersonals = new ArrayCollection();
        $this->conceptoHasPlantillaNominas = new ArrayCollection();
        $this->datosVariables = new ArrayCollection();
        $this->estudios = new ArrayCollection();
        $this->experienciaLaborals = new ArrayCollection();
        $this->historicoConceptos = new ArrayCollection();
        $this->historicoVariables = new ArrayCollection();
        $this->informacionCulturalDeportivas = new ArrayCollection();
        $this->telefonoPersonals = new ArrayCollection();
        $this->vehiculos = new ArrayCollection();
        $this->datosGenericos = new ArrayCollection();
        $this->diasPendientes = new ArrayCollection();
        $this->vacaciones = new ArrayCollection();
        $this->prestaciones = new ArrayCollection();
        $this->reposos = new ArrayCollection();
    }


    /**
     * Set cedula
     *
     * @param integer $cedula
     * @return Personal
     */
    public function setId($cedula)
    {
        $this->id = $cedula;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nacionalidad
     *
     * @param string $nacionalidad
     * @return Personal
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    /**
     * Get nacionalidad
     *
     * @return string 
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Set primerNombre
     *
     * @param string $primerNombre
     * @return Personal
     */
    public function setPrimerNombre($primerNombre)
    {
        $this->primerNombre = $primerNombre;

        return $this;
    }

    /**
     * Get primerNombre
     *
     * @return string 
     */
    public function getPrimerNombre()
    {
        return $this->primerNombre;
    }

    /**
     * Set segundoNombre
     *
     * @param string $segundoNombre
     * @return Personal
     */
    public function setSegundoNombre($segundoNombre)
    {
        $this->segundoNombre = $segundoNombre;

        return $this;
    }

    /**
     * Get segundoNombre
     *
     * @return string 
     */
    public function getSegundoNombre()
    {
        return $this->segundoNombre;
    }

    /**
     * Set primerApellido
     *
     * @param string $primerApellido
     * @return Personal
     */
    public function setPrimerApellido($primerApellido)
    {
        $this->primerApellido = $primerApellido;

        return $this;
    }

    /**
     * Get primerApellido
     *
     * @return string 
     */
    public function getPrimerApellido()
    {
        return $this->primerApellido;
    }

    /**
     * Set segundoApellido
     *
     * @param string $segundoApellido
     * @return Personal
     */
    public function setSegundoApellido($segundoApellido)
    {
        $this->segundoApellido = $segundoApellido;

        return $this;
    }

    /**
     * Get segundoApellido
     *
     * @return string 
     */
    public function getSegundoApellido()
    {
        return $this->segundoApellido;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return Personal
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string 
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     * @return Personal
     */
    public function setEstadoCivil($estadoCivil)
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string 
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return Personal
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime 
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set libretaMilitar
     *
     * @param string $libretaMilitar
     * @return Personal
     */
    public function setLibretaMilitar($libretaMilitar)
    {
        $this->libretaMilitar = $libretaMilitar;

        return $this;
    }

    /**
     * Get libretaMilitar
     *
     * @return string 
     */
    public function getLibretaMilitar()
    {
        return $this->libretaMilitar;
    }

    /**
     * Set rif
     *
     * @param string $rif
     * @return Personal
     */
    public function setRif($rif)
    {
        $this->rif = $rif;

        return $this;
    }

    /**
     * Get rif
     *
     * @return string 
     */
    public function getRif()
    {
        return $this->rif;
    }

    /**
     * Set licencia
     *
     * @param string $licencia
     * @return Personal
     */
    public function setLicencia($licencia)
    {
        $this->licencia = $licencia;

        return $this;
    }

    /**
     * Get licencia
     *
     * @return string 
     */
    public function getLicencia()
    {
        return $this->licencia;
    }

    /**
     * Set altura
     *
     * @param string $altura
     * @return Personal
     */
    public function setAltura($altura)
    {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return string 
     */
    public function getAltura()
    {
        return $this->altura;
    }

    /**
     * Set peso
     *
     * @param string $peso
     * @return Personal
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get peso
     *
     * @return string 
     */
    public function getPeso()
    {
        return $this->peso;
    }
    
    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set rutaFoto
     *
     * @param string $rutaFoto
     * @return Personal
     */
    public function setRutaFoto($rutaFoto)
    {
        $this->rutaFoto = $rutaFoto;

        return $this;
    }

    /**
     * Get rutaFoto
     *
     * @return string 
     */
    public function getRutaFoto()
    {
        return $this->rutaFoto;
    }
    
    /**
     * Set tallaCamisa
     *
     * @param string $tallaCamisa
     * @return Personal
     */
    public function setTallaCamisa($tallaCamisa)
    {
        $this->tallaCamisa = $tallaCamisa;

        return $this;
    }

    /**
     * Get tallaCamisa
     *
     * @return string 
     */
    public function getTallaCamisa()
    {
        return $this->tallaCamisa;
    }

    /**
     * Set tallaPantalon
     *
     * @param string $tallaPantalon
     * @return Personal
     */
    public function setTallaPantalon($tallaPantalon)
    {
        $this->tallaPantalon = $tallaPantalon;

        return $this;
    }

    /**
     * Get tallaPantalon
     *
     * @return string 
     */
    public function getTallaPantalon()
    {
        return $this->tallaPantalon;
    }

    /**
     * Set tallaCalzado
     *
     * @param string $tallaCalzado
     * @return Personal
     */
    public function setTallaCalzado($tallaCalzado)
    {
        $this->tallaCalzado = $tallaCalzado;

        return $this;
    }

    /**
     * Get tallaCalzado
     *
     * @return string 
     */
    public function getTallaCalzado()
    {
        return $this->tallaCalzado;
    }

    /**
     * Add cargaFamiliars
     *
     * @param \Cet\NominaBundle\Entity\CargaFamiliar $cargaFamiliars
     * @return Personal
     */
    public function addCargaFamiliar(\Cet\NominaBundle\Entity\CargaFamiliar $cargaFamiliars)
    {
        $this->cargaFamiliars[] = $cargaFamiliars;

        return $this;
    }

    /**
     * Remove cargaFamiliars
     *
     * @param \Cet\NominaBundle\Entity\CargaFamiliar $cargaFamiliars
     */
    public function removeCargaFamiliar(\Cet\NominaBundle\Entity\CargaFamiliar $cargaFamiliars)
    {
        $this->cargaFamiliars->removeElement($cargaFamiliars);
    }

    /**
     * Get cargaFamiliars
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCargaFamiliars()
    {
        return $this->cargaFamiliars;
    }

    /**
     * Add correoPersonals
     *
     * @param \Cet\NominaBundle\Entity\CorreoPersonal $correoPersonals
     * @return Personal
     */
    public function addCorreoPersonal(\Cet\NominaBundle\Entity\CorreoPersonal $correoPersonals)
    {
        $this->correoPersonals[] = $correoPersonals;

        return $this;
    }

    /**
     * Remove correoPersonals
     *
     * @param \Cet\NominaBundle\Entity\CorreoPersonal $correoPersonals
     */
    public function removeCorreoPersonal(\Cet\NominaBundle\Entity\CorreoPersonal $correoPersonals)
    {
        $this->correoPersonals->removeElement($correoPersonals);
    }

    /**
     * Get correoPersonals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCorreoPersonals()
    {
        return $this->correoPersonals;
    }

    /**
     * Add estudios
     *
     * @param \Cet\NominaBundle\Entity\Estudio $estudios
     * @return Personal
     */
    public function addEstudio(\Cet\NominaBundle\Entity\Estudio $estudios)
    {
        $this->estudios[] = $estudios;

        return $this;
    }

    /**
     * Remove estudios
     *
     * @param \Cet\NominaBundle\Entity\Estudio $estudios
     */
    public function removeEstudio(\Cet\NominaBundle\Entity\Estudio $estudios)
    {
        $this->estudios->removeElement($estudios);
    }

    /**
     * Get estudios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstudios()
    {
        return $this->estudios;
    }

    /**
     * Add experienciaLaborals
     *
     * @param \Cet\NominaBundle\Entity\ExperienciaLaboral $experienciaLaborals
     * @return Personal
     */
    public function addExperienciaLaboral(\Cet\NominaBundle\Entity\ExperienciaLaboral $experienciaLaborals)
    {
        $this->experienciaLaborals[] = $experienciaLaborals;

        return $this;
    }

    /**
     * Remove experienciaLaborals
     *
     * @param \Cet\NominaBundle\Entity\ExperienciaLaboral $experienciaLaborals
     */
    public function removeExperienciaLaboral(\Cet\NominaBundle\Entity\ExperienciaLaboral $experienciaLaborals)
    {
        $this->experienciaLaborals->removeElement($experienciaLaborals);
    }

    /**
     * Get experienciaLaborals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExperienciaLaborals()
    {
        return $this->experienciaLaborals;
    }

    /**
     * Add historicoConceptos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos
     * @return Personal
     */
    public function addHistoricoConcepto(\Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos)
    {
        $this->historicoConceptos[] = $historicoConceptos;

        return $this;
    }

    /**
     * Remove historicoConceptos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos
     */
    public function removeHistoricoConcepto(\Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos)
    {
        $this->historicoConceptos->removeElement($historicoConceptos);
    }

    /**
     * Get historicoConceptos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoConceptos()
    {
        return $this->historicoConceptos;
    }

    /**
     * Add historicoVariables
     *
     * @param \Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables
     * @return Personal
     */
    public function addHistoricoVariable(\Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables)
    {
        $this->historicoVariables[] = $historicoVariables;

        return $this;
    }

    /**
     * Remove historicoVariables
     *
     * @param \Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables
     */
    public function removeHistoricoVariable(\Cet\NominaBundle\Entity\HistoricoVariable $historicoVariables)
    {
        $this->historicoVariables->removeElement($historicoVariables);
    }

    /**
     * Get historicoVariables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoVariables()
    {
        return $this->historicoVariables;
    }

    /**
     * Add informacionCulturalDeportivas
     *
     * @param \Cet\NominaBundle\Entity\InformacionCulturalDeportiva $informacionCulturalDeportivas
     * @return Personal
     */
    public function addInformacionCulturalDeportiva(\Cet\NominaBundle\Entity\InformacionCulturalDeportiva $informacionCulturalDeportivas)
    {
        $this->informacionCulturalDeportivas[] = $informacionCulturalDeportivas;

        return $this;
    }

    /**
     * Remove informacionCulturalDeportivas
     *
     * @param \Cet\NominaBundle\Entity\InformacionCulturalDeportiva $informacionCulturalDeportivas
     */
    public function removeInformacionCulturalDeportiva(\Cet\NominaBundle\Entity\InformacionCulturalDeportiva $informacionCulturalDeportivas)
    {
        $this->informacionCulturalDeportivas->removeElement($informacionCulturalDeportivas);
    }

    /**
     * Get informacionCulturalDeportivas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInformacionCulturalDeportivas()
    {
        return $this->informacionCulturalDeportivas;
    }

    /**
     * Set informacionLaboral
     *
     * @param \Cet\NominaBundle\Entity\InformacionLaboral $informacionLaboral
     * @return Personal
     */
    public function setInformacionLaboral(\Cet\NominaBundle\Entity\InformacionLaboral $informacionLaboral = null)
    {
        $this->informacionLaboral = $informacionLaboral;
        $informacionLaboral->setPersonal($this);

        return $this;
    }

    /**
     * Get informacionLaboral
     *
     * @return \Cet\NominaBundle\Entity\InformacionLaboral 
     */
    public function getInformacionLaboral()
    {
        return $this->informacionLaboral;
    }

    /**
     * Set informacionMedica
     *
     * @param \Cet\NominaBundle\Entity\InformacionMedica $informacionMedica
     * @return Personal
     */
    public function setInformacionMedica(\Cet\NominaBundle\Entity\InformacionMedica $informacionMedica = null)
    {
        $this->informacionMedica = $informacionMedica;

        return $this;
    }

    /**
     * Get informacionMedica
     *
     * @return \Cet\NominaBundle\Entity\InformacionMedica 
     */
    public function getInformacionMedica()
    {
        return $this->informacionMedica;
    }

    /**
     * Add telefonoPersonals
     *
     * @param \Cet\NominaBundle\Entity\TelefonoPersonal $telefonoPersonals
     * @return Personal
     */
    public function addTelefonoPersonal(\Cet\NominaBundle\Entity\TelefonoPersonal $telefonoPersonals)
    {
        $this->telefonoPersonals[] = $telefonoPersonals;

        return $this;
    }

    /**
     * Remove telefonoPersonals
     *
     * @param \Cet\NominaBundle\Entity\TelefonoPersonal $telefonoPersonals
     */
    public function removeTelefonoPersonal(\Cet\NominaBundle\Entity\TelefonoPersonal $telefonoPersonals)
    {
        $this->telefonoPersonals->removeElement($telefonoPersonals);
    }

    /**
     * Get telefonoPersonals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTelefonoPersonals()
    {
        return $this->telefonoPersonals;
    }

    /**
     * Add vehiculos
     *
     * @param \Cet\NominaBundle\Entity\Vehiculo $vehiculos
     * @return Personal
     */
    public function addVehiculo(\Cet\NominaBundle\Entity\Vehiculo $vehiculos)
    {
        $this->vehiculos[] = $vehiculos;

        return $this;
    }

    /**
     * Remove vehiculos
     *
     * @param \Cet\NominaBundle\Entity\Vehiculo $vehiculos
     */
    public function removeVehiculo(\Cet\NominaBundle\Entity\Vehiculo $vehiculos)
    {
        $this->vehiculos->removeElement($vehiculos);
    }

    /**
     * Get vehiculos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVehiculos()
    {
        return $this->vehiculos;
    }
    
    /**
     * Add datosGenericos
     *
     * @param \Cet\NominaBundle\Entity\DatosGenericos $datosGenericos
     * @return Personal
     */
    public function addDatosGenericos(\Cet\NominaBundle\Entity\DatosGenericos $datosGenericos)
    {
        $this->datosGenericos[] = $datosGenericos;

        return $this;
    }

    /**
     * Remove datosGenericos
     *
     * @param \Cet\NominaBundle\Entity\DatosGenericos $datosGenericos
     */
    public function removeDatosGenericos(\Cet\NominaBundle\Entity\DatosGenericos $datosGenericos)
    {
        $this->datosGenericos->removeElement($datosGenericos);
    }

    /**
     * Get datosGenericos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatosGenericos()
    {
        return $this->datosGenericos;
    }

    /**
     * Set vivienda
     *
     * @param \Cet\NominaBundle\Entity\Vivienda $vivienda
     * @return Personal
     */
    public function setVivienda(\Cet\NominaBundle\Entity\Vivienda $vivienda = null)
    {
        $this->vivienda = $vivienda;

        return $this;
    }

    /**
     * Get vivienda
     *
     * @return \Cet\NominaBundle\Entity\Vivienda 
     */
    public function getVivienda()
    {
        return $this->vivienda;
    }
    
    public function getAbsolutePath()
    {
        return null === $this->rutaFoto
            ? null
            : $this->getUploadRootDir().'/'.$this->id.'.'.$this->rutaFoto;
    }

    public function getWebPath()
    {
        return null === $this->rutaFoto
            ? null
            : $this->getUploadDir().'/'.$this->rutaFoto;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/fotos';
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFoto(UploadedFile $file = null)
    {
        $this->foto = $file;
        // check if we have an old image path
//        if (isset($this->rutaFoto)) {
//            // store the old name to delete after the update
//            $this->temp = $this->rutaFoto;
//            $this->rutaFoto = null;
//        } else {
//            $this->rutaFoto = 'initial';
//        }
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFoto()) {
            // do whatever you want to generate a unique name
            $filename = $this->getFoto()->getClientOriginalName();
            $this->rutaFoto = $this->id.'.'.$this->getFoto()->guessExtension();
//            $this->rutaFoto = $filename.'.'.$this->getFoto()->getClientOriginalExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFoto()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFoto()->move($this->getUploadRootDir(), $this->id.'.'.$this->getFoto()->guessExtension());

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->foto = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('id', new Assert\NotBlank())
                ->addPropertyConstraint ('id', new Assert\Type(array('type'=>'integer','message'=>'El campo debe ser solo números.')))
                ->addPropertyConstraint ('id', new Assert\Length(array(
                    'min'=> 4,
                    'max'=> 8,
                    'minMessage'=> 'El Número de Cédula debe ser mayor a  4 digitos',
                    'maxMessage'=> 'El Número de Cédula no debe tener mas de 8 digitos',
                    )))             
                ->addPropertyConstraint ('nacionalidad', new Assert\NotBlank())
                ->addPropertyConstraint ('primerNombre', new Assert\NotBlank())             
                ->addPropertyConstraint ('primerNombre', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('primerNombre', new Assert\Regex(array(
                        'pattern' => '/[\d^*-+?¿().,¡!=&%"]/', 
                        'match'   => False,
                        'message' => 'El Nombre no puede contener números ni caracteres especiales',
                    )))             
                ->addPropertyConstraint ('segundoNombre', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('segundoNombre', new Assert\Regex(array(
                        'pattern' => '/[\d^*-+?¿().,¡!=&%"]/', 
                        'match'   => False,
                        'message' => 'El Nombre no puede contener números ni caracteres especiales',
                    )))             
                ->addPropertyConstraint ('primerApellido', new Assert\NotBlank())
                ->addPropertyConstraint ('primerApellido', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('primerApellido', new Assert\Regex(array(
                        'pattern' => '/[\d^*-+?¿().,¡!=&%"]/', 
                        'match'   => False,
                        'message' => 'El Nombre no puede contener números ni caracteres especiales',
                    )))              
                ->addPropertyConstraint ('segundoApellido', new Assert\Type(array('type'=>'string'))) 
                ->addPropertyConstraint ('segundoApellido', new Assert\Regex(array(
                        'pattern' => '/[\d^*-+?¿().,¡!=&%"]/', 
                        'match'   => False,
                        'message' => 'El Nombre no puede contener números ni caracteres especiales',
                    )))              
   
                ->addPropertyConstraint ('genero', new Assert\NotBlank())
                ->addPropertyConstraint ('estadoCivil', new Assert\NotBlank())
                ->addPropertyConstraint ('fechaNacimiento', new Assert\NotBlank()) 
                ->addPropertyConstraint ('fechaNacimiento', new Assert\Date())
                ->addPropertyConstraint ('libretaMilitar', new Assert\Type(array('type'=>'string')))
//                ->addPropertyConstraint ('rif', new Assert\NotBlank())
                ->addPropertyConstraint ('rif', new Assert\Type(array('type'=>'string')))
                ->addPropertyConstraint ('rif', new Assert\Regex(array(
                        'pattern' => '/[EeVv]-\d{8}-\d$/', 
                        'match'   => TRUE,
                        'message' => 'El Nro de Rif debe empezar con la letra E o V seguido de un guión (-) luego 8 números seguidos, un guión (-) y un número. Ejemplos V-87654321-0 E-87654321-0 ',
                    )))              
             // ^[JV]-\d{8}-\d$
 
               
                ->addPropertyConstraint ('rif', new Assert\Length(array(
                    'min'=> 12,
                    'max'=> 12,
                    'minMessage'=> 'El RIF debe contener por lo menos 12 caracteres.',
                    'maxMessage'=> 'El RIF debe contener por lo menos 12 caracteres.',
                    )))
//                ->addPropertyConstraint ('licencia', new Assert\Type(array('Type'=>'alpha'))) 
//                ->addPropertyConstraint ('altura', new Assert\NotBlank())
//                ->addPropertyConstraint ('altura', new Assert\Type(array('type'=>'real')))
//                ->addPropertyConstraint ('peso', new Assert\NotBlank())
//                ->addPropertyConstraint ('peso', new Assert\Type(array('type'=>'real')))              
//
//                ->addPropertyConstraint ('tallaCamisa', new Assert\NotBlank())
//                ->addPropertyConstraint ('tallaPantalon', new Assert\NotBlank())
//                ->addPropertyConstraint ('tallaCalzado', new Assert\NotBlank())             
            
          ;
    }
    
    public function __toString()
    {
        return $this->getPrimerNombre().' '.$this->getSegundoNombre().' '.$this->getPrimerApellido().' C.I: '.$this->getId();
    }
    
    /**
     * Add conceptoHasPlantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas
     * @return Personal
     */
    public function addConceptoHasPlantillaNomina(\Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas)
    {
        $this->conceptoHasPlantillaNominas[] = $conceptoHasPlantillaNominas;

        return $this;
    }

    /**
     * Remove conceptoHasPlantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas
     */
    public function removeConceptoHasPlantillaNomina(\Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas)
    {
        $this->conceptoHasPlantillaNominas->removeElement($conceptoHasPlantillaNominas);
    }

    /**
     * Get conceptoHasPlantillaNominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConceptoHasPlantillaNominas()
    {
        return $this->conceptoHasPlantillaNominas;
    }

    /**
     * Add datosVariables
     *
     * @param \Cet\NominaBundle\Entity\DatosVariables $datosVariables
     * @return Personal
     */
    public function addDatosVariable(\Cet\NominaBundle\Entity\DatosVariables $datosVariables)
    {
        $this->datosVariables[] = $datosVariables;

        return $this;
    }

    /**
     * Remove datosVariables
     *
     * @param \Cet\NominaBundle\Entity\DatosVariables $datosVariables
     */
    public function removeDatosVariable(\Cet\NominaBundle\Entity\DatosVariables $datosVariables)
    {
        $this->datosVariables->removeElement($datosVariables);
    }

    /**
     * Get datosVariables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatosVariables()
    {
        return $this->datosVariables;
    }

    /**
     * Add vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $vacaciones
     * @return Personal
     */
    public function addVacaciones(\Cet\NominaBundle\Entity\Vacaciones $vacaciones)
    {
        $this->vacaciones[] = $vacaciones;

        return $this;
    }

    /**
     * Remove vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $vacaciones
     */
    public function removeVacaciones(\Cet\NominaBundle\Entity\Vacaciones $vacaciones)
    {
        $this->vacaciones->removeElement($vacaciones);
    }

    /**
     * Get vacaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVacaciones()
    {
        return $this->vacaciones;
    }

    /**
     * Add datosGenericos
     *
     * @param \Cet\NominaBundle\Entity\DatosGenericos $datosGenericos
     * @return Personal
     */
    public function addDatosGenerico(\Cet\NominaBundle\Entity\DatosGenericos $datosGenericos)
    {
        $this->datosGenericos[] = $datosGenericos;

        return $this;
    }

    /**
     * Remove datosGenericos
     *
     * @param \Cet\NominaBundle\Entity\DatosGenericos $datosGenericos
     */
    public function removeDatosGenerico(\Cet\NominaBundle\Entity\DatosGenericos $datosGenericos)
    {
        $this->datosGenericos->removeElement($datosGenericos);
    }

    /**
     * Add vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $vacaciones
     * @return Personal
     */
    public function addVacacione(\Cet\NominaBundle\Entity\Vacaciones $vacaciones)
    {
        $this->vacaciones[] = $vacaciones;

        return $this;
    }

    /**
     * Remove vacaciones
     *
     * @param \Cet\NominaBundle\Entity\Vacaciones $vacaciones
     */
    public function removeVacacione(\Cet\NominaBundle\Entity\Vacaciones $vacaciones)
    {
        $this->vacaciones->removeElement($vacaciones);
    }

    /**
     * Set userPersonalM
     *
     * @param \Cet\NominaBundle\Entity\userPersonalM $userPersonalM
     * @return Personal
     */
    public function setUserPersonalM(\Cet\NominaBundle\Entity\userPersonalM $userPersonalM = null)
    {
        $this->userPersonalM = $userPersonalM;

        return $this;
    }

    /**
     * Get userPersonalM
     *
     * @return \Cet\NominaBundle\Entity\userPersonalM 
     */
    public function getUserPersonalM()
    {
        return $this->userPersonalM;
    }

    /**
     * Add diasPendientes
     *
     * @param \Cet\NominaBundle\Entity\DiasPendientes $diasPendientes
     * @return Personal
     */
    public function addDiasPendiente(\Cet\NominaBundle\Entity\DiasPendientes $diasPendientes)
    {
        $this->diasPendientes[] = $diasPendientes;

        return $this;
    }

    /**
     * Remove diasPendientes
     *
     * @param \Cet\NominaBundle\Entity\DiasPendientes $diasPendientes
     */
    public function removeDiasPendiente(\Cet\NominaBundle\Entity\DiasPendientes $diasPendientes)
    {
        $this->diasPendientes->removeElement($diasPendientes);
    }

    /**
     * Get diasPendientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDiasPendientes()
    {
        return $this->diasPendientes;
    }

    /**
     * Add prestaciones
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones
     * @return Personal
     */
    public function addPrestacione(\Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones)
    {
        $this->prestaciones[] = $prestaciones;

        return $this;
    }

    /**
     * Remove prestaciones
     *
     * @param \Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones
     */
    public function removePrestacione(\Cet\NominaBundle\Entity\PrestacionesSociales $prestaciones)
    {
        $this->prestaciones->removeElement($prestaciones);
    }

    /**
     * Get prestaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrestaciones()
    {
        return $this->prestaciones;
    }

    /**
     * Add reposos
     *
     * @param \Cet\NominaBundle\Entity\Reposo $reposos
     * @return Personal
     */
    public function addReposo(\Cet\NominaBundle\Entity\Reposo $reposos)
    {
        $this->reposos[] = $reposos;

        return $this;
    }

    /**
     * Remove reposos
     *
     * @param \Cet\NominaBundle\Entity\Reposo $reposos
     */
    public function removeReposo(\Cet\NominaBundle\Entity\Reposo $reposos)
    {
        $this->reposos->removeElement($reposos);
    }

    /**
     * Get reposos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReposos()
    {
        return $this->reposos;
    }
}
