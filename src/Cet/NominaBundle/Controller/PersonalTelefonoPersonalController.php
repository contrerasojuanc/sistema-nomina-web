<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\TelefonoPersonal;
use Cet\NominaBundle\Form\PersonalTelefonoPersonalType;

/**
 * TelefonoPersonal controller.
 *
 */
class PersonalTelefonoPersonalController extends Controller
{

    /**
     * Lists all TelefonoPersonal entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:TelefonoPersonal')->findAll();

        return $this->render('CetNominaBundle:TelefonoPersonal:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TelefonoPersonal entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new TelefonoPersonal();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkTelefonoPersonalPersonal($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkTelefonoPersonalPersonal()->getId())));
        }

        return $this->render('CetNominaBundle:TelefonoPersonal:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a TelefonoPersonal entity.
    *
    * @param TelefonoPersonal $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TelefonoPersonal $entity,$id)
    {
        $form = $this->createForm(new PersonalTelefonoPersonalType(), $entity, array(
            'action' => $this->generateUrl('personaltelefonopersonal_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new TelefonoPersonal entity.
     *
     */
    public function newAction($id)
    {
        $entity = new TelefonoPersonal();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkTelefonoPersonalPersonal($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:TelefonoPersonal:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TelefonoPersonal entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:TelefonoPersonal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad TelefonoPersonal.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:TelefonoPersonal:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing TelefonoPersonal entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:TelefonoPersonal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad TelefonoPersonal.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:TelefonoPersonal:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TelefonoPersonal entity.
    *
    * @param TelefonoPersonal $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TelefonoPersonal $entity)
    {
        $form = $this->createForm(new PersonalTelefonoPersonalType(), $entity, array(
            'action' => $this->generateUrl('personaltelefonopersonal_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing TelefonoPersonal entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:TelefonoPersonal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad TelefonoPersonal.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkTelefonoPersonalPersonal()->getId())));
            
        }

        return $this->render('CetNominaBundle:TelefonoPersonal:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TelefonoPersonal entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:TelefonoPersonal')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad TelefonoPersonal.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        //return $this->redirect($this->generateUrl('personal'));
        return $this->redirect($this->generateUrl('personal_show', array('id' => $entity->getFkTelefonoPersonalPersonal()->getId())));    
    }

    /**
     * Creates a form to delete a TelefonoPersonal entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personaltelefonopersonal_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
