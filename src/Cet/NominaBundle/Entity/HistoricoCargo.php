<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\HistoricoCargo
 *
 * @ORM\Entity(repositoryClass="HistoricoCargoRepository")
 * @ORM\Table(name="sis_nomina.historico_cargo", indexes={@ORM\Index(name="fk_informacion_laboral_has_cargo_cargo1_idx", columns={"cargo_id"}), @ORM\Index(name="fk_informacion_laboral_has_cargo_informacion_laboral1_idx", columns={"informacion_laboral_id"}), @ORM\Index(name="fk_unidad_organizativa_has_informacion_laboral_unidad_organ_idx", columns={"unidad_organizativa_id"})})
 */
class HistoricoCargo
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicio", type="date", nullable=true)
     */
    private $fechaInicio;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_fin", type="date", nullable=true)
     */
    private $fechaFin;
    
    /**
     * @var boolean
     * @ORM\Column(name="es_encargado", type="boolean", nullable=true)
     */
    private $esEncargado;
    
    /**
     * @var integer
     * @ORM\Column(name="numero_resolucion", type="integer", nullable=false)
     */
    private $numeroResolucion;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_resolucion", type="date", nullable=false)
     */
    private $fechaResolucion;


    /**
     * @ORM\ManyToOne(targetEntity="InformacionLaboral", inversedBy="historicoCargos")
     * @ORM\JoinColumn(name="informacion_laboral_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_informacion_laboral_has_cargo_informacion_laboral1;

    /**
     * @ORM\ManyToOne(targetEntity="Cargo", inversedBy="historicoCargos")
     * @ORM\JoinColumn(name="cargo_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_informacion_laboral_has_cargo_cargo1;
    
    /**
     * @ORM\ManyToOne(targetEntity="UnidadOrganizativa", inversedBy="historicoCargos")
     * @ORM\JoinColumn(name="unidad_organizativa_id", referencedColumnName="id", nullable=true)
     */
    protected $fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1;

    public function __construct()
    {
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return HistoricoCargo
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return HistoricoCargo
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }
    
    /**
     * Set esEncargado
     *
     * @param boolean $esEncargado
     * @return HistoricoCargo
     */
    public function setEsEncargado($esEncargado)
    {
        $this->esEncargado = $esEncargado;

        return $this;
    }

    /**
     * Get esEncargado
     *
     * @return boolean 
     */
    public function getEsEncargado()
    {
        return $this->esEncargado;
    }
    
    /**
     * Set numeroResolucion
     *
     * @param integer $numeroResolucion
     * @return InformacionLaboral
     */
    public function setNumeroResolucion($numeroResolucion)
    {
        $this->numeroResolucion = $numeroResolucion;

        return $this;
    }

    /**
     * Get numeroResolucion
     *
     * @return integer 
     */
    public function getNumeroResolucion()
    {
        return $this->numeroResolucion;
    }
    
    /**
     * Get fechaResolucion
     *
     * @return \DateTime 
     */
    public function getFechaResolucion()
    {
        return $this->fechaResolucion;
    }
    
    

    /**
     * Set fechaResolucion
     *
     * @param \DateTime fechaResolucion
     * @return InformacionLaboral
     */
    public function setFechaResolucion($fechaResolucion)
    {
        $this->fechaResolucion = $fechaResolucion;

        return $this;
    }
    
    

    /**
     * Set fk_informacion_laboral_has_cargo_informacion_laboral1
     *
     * @param \Cet\NominaBundle\Entity\InformacionLaboral $fkInformacionLaboralHasCargoInformacionLaboral1
     * @return HistoricoCargo
     */
    public function setFkInformacionLaboralHasCargoInformacionLaboral1(\Cet\NominaBundle\Entity\InformacionLaboral $fkInformacionLaboralHasCargoInformacionLaboral1)
    {
        $this->fk_informacion_laboral_has_cargo_informacion_laboral1 = $fkInformacionLaboralHasCargoInformacionLaboral1;

        return $this;
    }

    /**
     * Get fk_informacion_laboral_has_cargo_informacion_laboral1
     *
     * @return \Cet\NominaBundle\Entity\InformacionLaboral 
     */
    public function getFkInformacionLaboralHasCargoInformacionLaboral1()
    {
        return $this->fk_informacion_laboral_has_cargo_informacion_laboral1;
    }

    /**
     * Set fk_informacion_laboral_has_cargo_cargo1
     *
     * @param \Cet\NominaBundle\Entity\Cargo $fkInformacionLaboralHasCargoCargo1
     * @return HistoricoCargo
     */
    public function setFkInformacionLaboralHasCargoCargo1(\Cet\NominaBundle\Entity\Cargo $fkInformacionLaboralHasCargoCargo1)
    {
        $this->fk_informacion_laboral_has_cargo_cargo1 = $fkInformacionLaboralHasCargoCargo1;

        return $this;
    }

    /**
     * Get fk_informacion_laboral_has_cargo_cargo1
     *
     * @return \Cet\NominaBundle\Entity\Cargo 
     */
    public function getFkInformacionLaboralHasCargoCargo1()
    {
        return $this->fk_informacion_laboral_has_cargo_cargo1;
    }
    
    /**
     * Set fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1
     *
     * @param \Cet\NominaBundle\Entity\UnidadOrganizativa $fkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1
     * @return HistoricoCargo
     */
    public function setFkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1(\Cet\NominaBundle\Entity\UnidadOrganizativa $fkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1)
    {
        $this->fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 = $fkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1;

        return $this;
    }

    /**
     * Get fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1
     *
     * @return \Cet\NominaBundle\Entity\UnidadOrganizativa 
     */
    public function getFkUnidadOrganizativaHasInformacionLaboralUnidadOrganiz1()
    {
        return $this->fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint('fechaInicio', new Assert\NotBlank(array('message' => 'El campo no puede ser vacío.')))
                ->addPropertyConstraint('fechaInicio', new Assert\Date())
             
                ->addPropertyConstraint ('numeroResolucion', new Assert\NotBlank())
                //->addPropertyConstraint ('numeroResolucion', new Assert\Type(array('type'=>'digit','message'=>'El campo debe ser solo números.')))
             
                ->addPropertyConstraint ('fechaResolucion', new Assert\NotBlank())
 
            ;      
    }
}
