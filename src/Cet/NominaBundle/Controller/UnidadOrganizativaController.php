<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\UnidadOrganizativa;
use Cet\NominaBundle\Form\UnidadOrganizativaType;

use Doctrine\Common\Collections\ArrayCollection;
use Cet\NominaBundle\Entity\RecursiveCategoryIterator;
use RecursiveIteratorIterator;


/**
 * UnidadOrganizativa controller.
 *
 */
class UnidadOrganizativaController extends Controller
{

    /**
     * Lists all UnidadOrganizativa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->findAll();

        return $this->render('CetNominaBundle:UnidadOrganizativa:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new UnidadOrganizativa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new UnidadOrganizativa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        
        $unidadpadreid = $form->get('unidad')->getData();//obtener valor de un elemento del formulario (no mapeado)
        if($unidadpadreid <> null){
            $unidadpadre = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($unidadpadreid);
            $entity->setFkUnidadOrganizativaUnidadOrganizativa1($unidadpadre);
        }
        
        $institucion=$em->getRepository('CetNominaBundle:Institucion')->find(1);
        $entity->setFkUnidadOrganizativaInstitucion1($institucion);
        
        if ($form->isValid()) {
            //$em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('unidadorganizativa_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:UnidadOrganizativa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a UnidadOrganizativa entity.
    *
    * @param UnidadOrganizativa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(UnidadOrganizativa $entity)
    {
        
        $em = $this->getDoctrine()->getManager();
        /** @var $em \Doctrine\ORM\EntityManager */
        $root_categories = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->findBy(array('fk_unidad_organizativa_unidad_organizativa1' => null));
        
        
        $collection = new ArrayCollection($root_categories);
        $category_iterator = new RecursiveCategoryIterator($collection);
        $recursive_iterator = new RecursiveIteratorIterator($category_iterator, RecursiveIteratorIterator::SELF_FIRST);
        $unidades = array();
        foreach ($recursive_iterator as $index => $child_category)
        {
            $unidades[$child_category->getId()]=str_repeat("─", $recursive_iterator->getDepth()).'>'. $child_category->getNombre();
        }
        $form = $this->createForm(new UnidadOrganizativaType($unidades), $entity, array(
            'action' => $this->generateUrl('unidadorganizativa_create'),
            'method' => 'POST',
        ));
        
        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new UnidadOrganizativa entity.
     *
     */
    public function newAction()
    {
        $entity = new UnidadOrganizativa();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:UnidadOrganizativa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UnidadOrganizativa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad UnidadOrganizativa.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:UnidadOrganizativa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing UnidadOrganizativa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad UnidadOrganizativa.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:UnidadOrganizativa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a UnidadOrganizativa entity.
    *
    * @param UnidadOrganizativa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UnidadOrganizativa $entity)
    {   
        $em = $this->getDoctrine()->getManager();
        /** @var $em \Doctrine\ORM\EntityManager */
        $root_categories = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->findBy(array('fk_unidad_organizativa_unidad_organizativa1' => null));
        
        
        $collection = new ArrayCollection($root_categories);
        $category_iterator = new RecursiveCategoryIterator($collection);
        $recursive_iterator = new RecursiveIteratorIterator($category_iterator, RecursiveIteratorIterator::SELF_FIRST);
        $unidades = array();
        
        foreach ($recursive_iterator as $index => $child_category)
        {
            $unidades[$child_category->getId()]=str_repeat("─", $recursive_iterator->getDepth()).'>'. $child_category->getNombre();
            
        }
        $seleccionado=$entity->getFkUnidadOrganizativaUnidadOrganizativa1();
        if ($seleccionado<> null){ $seleccionado=$entity->getFkUnidadOrganizativaUnidadOrganizativa1()->getId();}
        
        $form = $this->createForm(new UnidadOrganizativaType($unidades,$seleccionado), $entity, array(
            'action' => $this->generateUrl('unidadorganizativa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        
        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing UnidadOrganizativa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($id);

        //inicio
        
        $form = $this->createEditForm($entity);
        $form->handleRequest($request);
        
        $unidadpadreid = $form->get('unidad')->getData();//obtener valor de un elemento del formulario (no mapeado)
        if($unidadpadreid == null){
            
        }
        else{
            $unidadpadre = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($unidadpadreid);
            $entity->setFkUnidadOrganizativaUnidadOrganizativa1($unidadpadre);
        }
        //fin
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad UnidadOrganizativa.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('unidadorganizativa_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:UnidadOrganizativa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a UnidadOrganizativa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:UnidadOrganizativa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad UnidadOrganizativa.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('unidadorganizativa'));
    }

    /**
     * Creates a form to delete a UnidadOrganizativa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('unidadorganizativa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
