<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\userPersonalSistemasD;
use Cet\NominaBundle\Form\userPersonalSistemasDType;

/**
 * userPersonalSistemasD controller.
 *
 */
class userPersonalSistemasDController extends Controller
{

    /**
     * Lists all userPersonalSistemasD entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:userPersonalSistemasD')->findAll();

        return $this->render('CetNominaBundle:userPersonalSistemasD:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new userPersonalSistemasD entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new userPersonalSistemasD();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('userpersonalsistemasd_show', array('id' => $entity->getId())));
        }

        return $this->render('CetNominaBundle:userPersonalSistemasD:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a userPersonalSistemasD entity.
    *
    * @param userPersonalSistemasD $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(userPersonalSistemasD $entity)
    {
        $form = $this->createForm(new userPersonalSistemasDType(), $entity, array(
            'action' => $this->generateUrl('userpersonalsistemasd_create'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager(),
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new userPersonalSistemasD entity.
     *
     */
    public function newAction()
    {
        $entity = new userPersonalSistemasD();
        $form   = $this->createCreateForm($entity);

        return $this->render('CetNominaBundle:userPersonalSistemasD:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a userPersonalSistemasD entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:userPersonalSistemasD')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalSistemasD.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:userPersonalSistemasD:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing userPersonalSistemasD entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:userPersonalSistemasD')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalSistemasD.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:userPersonalSistemasD:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a userPersonalSistemasD entity.
    *
    * @param userPersonalSistemasD $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(userPersonalSistemasD $entity)
    {
        $form = $this->createForm(new userPersonalSistemasDType(), $entity, array(
            'action' => $this->generateUrl('userpersonalsistemasd_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $this->getDoctrine()->getManager(),
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')));

        return $form;
    }
    /**
     * Edits an existing userPersonalSistemasD entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:userPersonalSistemasD')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalSistemasD.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('userpersonalsistemasd_show', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:userPersonalSistemasD:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a userPersonalSistemasD entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:userPersonalSistemasD')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad userPersonalSistemasD.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('userpersonalsistemasd'));
    }

    /**
     * Creates a form to delete a userPersonalSistemasD entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('userpersonalsistemasd_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
