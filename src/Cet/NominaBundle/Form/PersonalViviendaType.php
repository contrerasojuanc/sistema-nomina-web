<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Cet\NominaBundle\Form\Subscribers\EstadoSubscriber;
use Cet\NominaBundle\Form\Subscribers\MunicipioSubscriber;
use Cet\NominaBundle\Form\Subscribers\ParroquiaSubscriber;

class PersonalViviendaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('condicion', 'choice', array(
                'label'=> 'Condición ',
                'attr' => array('help'=>'Seleccione de la lista en que condición es la vivienda. Ejemplo: Propia, Alquilada, Compartida u Otra'),
                'choices' => array('' => 'Seleccione una opción','P' => 'Propia', 'A' => 'Alquilada', 'C' => 'Compartida', 'O' => 'Otra'),
            ))
            ->add('direccion',null,array(
                'label'=> 'Dirección',
                'attr' => array('help'=>'Ingrese la dirección donde se encuetra ubicada la vivienda.'),
                ))
            ->add('codigoPostal',null,array(
                'label'=> 'Código Postal',
                'attr' => array('help'=>'Ingrese el código postal. Ejemplo: 5001'),
                ))
            ->add('referencia',null,array(
                'label'=> 'Punto de Referencia',
                'attr' => array('help'=>'Ingrese un punto de referencia de la vivienda. Ejemplo: Al lado de la Iglesia El Santuario'),
                ))
            //->add('fk_vivienda_parroquia1')
        ;
        
        $factory = $builder->getFormFactory();
        $estadoSubscriber = new EstadoSubscriber($factory);
        $builder->addEventSubscriber($estadoSubscriber);
        $municipioSubscriber = new MunicipioSubscriber($factory);
        $builder->addEventSubscriber($municipioSubscriber);
        $parroquiaSubscriber = new ParroquiaSubscriber($factory);
        $builder->addEventSubscriber($parroquiaSubscriber);       
        

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Vivienda'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_vivienda';
    }
}
