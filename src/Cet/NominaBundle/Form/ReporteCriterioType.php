<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Cet\NominaBundle\Form\ReporteCriteriosType;
use Cet\NominaBundle\Entity\Criterio;
use Cet\NominaBundle\Entity\Reporte;

class ReporteCriterioType extends AbstractType
{
    public $entidad_array = array();
    
    public function __construct(Array $entidad_array = null)
    {
        $this->entidad_array = $entidad_array;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {  
        $builder
            ->add('descripcion',null, array('read_only' => true))
            ->add('dql', 'hidden')
            ->add('criterios','collection',array(
//                'type' => new ReporteCriteriosType($this->entidad_array),
                'type' => new ReporteCriteriosType(),
                'by_reference' => false,
                'required'  => true,                
                'options' => array(
                    'em' => $options['em'],
//                    'label' => false,                    
//                    'data_class' => null,
////                    'data' => new Criterio()
                    ),
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Reporte'
        ))
        ->setRequired(array(
            'em'
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager'
        ))
        ;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_reporte';
    }
}
