<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cet\NominaBundle\Entity\PlantillaNomina
 *
 * @ORM\Entity(repositoryClass="PlantillaNominaRepository")
 * @ORM\Table(name="sis_nomina.plantilla_nomina", indexes={@ORM\Index(name="fk_plantilla_nomina_tipo_nomina1_idx", columns={"tipo_nomina_id"})})
 */
class PlantillaNomina
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @var string
     * @ORM\Column(name="frecuencia", type="text", nullable=true)
     */
    private $frecuencia;

    /**
     * @ORM\ManyToOne(targetEntity="TipoNomina", inversedBy="plantillaNominas")
     * @ORM\JoinColumn(name="tipo_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_plantilla_nomina_tipo_nomina1;

    /**
     * @ORM\OneToMany(targetEntity="ConceptoHasPlantillaNomina", mappedBy="fk_concepto_has_plantilla_nomina_plantilla_nomina1")
     * @ORM\JoinColumn(name="plantilla_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $conceptoHasPlantillaNominas;
    
    /**
     * @ORM\OneToMany(targetEntity="Nomina", mappedBy="fk_nomina_plantilla_nomina1")
     * @ORM\JoinColumn(name="plantilla_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $nominas;
    
    /**
     * @ORM\ManyToMany(targetEntity="Concepto", inversedBy="plantillaNominas")
     * @ORM\JoinTable(name="sis_nomina.concepto_has_plantilla_nomina1",
     *     joinColumns={@ORM\JoinColumn(name="plantilla_nomina_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="concepto_id", referencedColumnName="id")}
     * )
     */
    protected $conceptos;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoBanco", inversedBy="plantillaNominas")
     * @ORM\JoinColumn(name="tipo_banco_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoBanco;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoCuenta", inversedBy="plantillaNominas")
     * @ORM\JoinColumn(name="tipo_cuenta_id", referencedColumnName="id", nullable=true)
     */
    protected $tipoCuenta;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoCalculo", inversedBy="nominas")
     * @ORM\JoinColumn(name="tipocalculo_id", referencedColumnName="id", nullable=true)
     */
    protected $fk_tipocalculo;

    public function __construct()
    {
        $this->nominas = new ArrayCollection();
        $this->conceptoHasPlantillaNominas = new ArrayCollection();
        $this->conceptos = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\PlantillaNomina
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of nombre.
     *
     * @param string $nombre
     * @return \Entity\PlantillaNomina
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of frecuencia.
     *
     * @param string $frecuencia
     * @return \Entity\PlantillaNomina
     */
    public function setFrecuencia($frecuencia)
    {
        $this->frecuencia = $frecuencia;

        return $this;
    }

    /**
     * Get the value of frecuencia.
     *
     * @return string
     */
    public function getFrecuencia()
    {
        return $this->frecuencia;
    }

    /**
     * Set TipoNomina entity (many to one).
     *
     * @param TipoNomina $tipoNomina
     * @return \Entity\PlantillaNomina
     */
    public function setFkPlantillaNominaTipoNomina1(TipoNomina $tipoNomina)
    {
        $this->fk_plantilla_nomina_tipo_nomina1 = $tipoNomina;

        return $this;
    }

    /**
     * Get TipoNomina entity (many to one).
     *
     * @return TipoNomina
     */
    public function getFkPlantillaNominaTipoNomina1()
    {
        return $this->fk_plantilla_nomina_tipo_nomina1;
    }

    /**
     * Add Nomina entity to collection (one to many).
     *
     * @param Nomina $nomina
     * @return \Entity\PlantillaNomina
     */
    public function addNomina(Nomina $nomina)
    {
        $this->nominas[] = $nomina;

        return $this;
    }

    /**
     * Remove Nomina entity from collection (one to many).
     *
     * @param Nomina $nomina
     * @return \Entity\PlantillaNomina
     */
    public function removeNomina(Nomina $nomina)
    {
        $this->nominas->removeElement($nomina);

        return $this;
    }

    /**
     * Get Nomina entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNominas()
    {
        return $this->nominas;
    }
    
    public function __sleep()
    {
        return array('id', 'nombre', 'frecuencia', 'fk_plantilla_nomina_tipo_nomina1');
    }
    
    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Add conceptoHasPlantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas
     * @return PlantillaNomina
     */
    public function addConceptoHasPlantillaNomina(\Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas)
    {
        $this->conceptoHasPlantillaNominas[] = $conceptoHasPlantillaNominas;

        return $this;
    }

    /**
     * Remove conceptoHasPlantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas
     */
    public function removeConceptoHasPlantillaNomina(\Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas)
    {
        $this->conceptoHasPlantillaNominas->removeElement($conceptoHasPlantillaNominas);
    }

    /**
     * Get conceptoHasPlantillaNominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConceptoHasPlantillaNominas()
    {
        return $this->conceptoHasPlantillaNominas;
    }


    /**
     * Add conceptos
     *
     * @param \Cet\NominaBundle\Entity\Concepto $conceptos
     * @return PlantillaNomina
     */
    public function addConcepto(\Cet\NominaBundle\Entity\Concepto $conceptos)
    {
        $this->conceptos[] = $conceptos;

        return $this;
    }

    /**
     * Remove conceptos
     *
     * @param \Cet\NominaBundle\Entity\Concepto $conceptos
     */
    public function removeConcepto(\Cet\NominaBundle\Entity\Concepto $conceptos)
    {
        $this->conceptos->removeElement($conceptos);
    }

    /**
     * Get conceptos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConceptos()
    {
        return $this->conceptos;
    }

    /**
     * Set tipoBanco
     *
     * @param \Cet\NominaBundle\Entity\TipoBanco $tipoBanco
     * @return PlantillaNomina
     */
    public function setTipoBanco(\Cet\NominaBundle\Entity\TipoBanco $tipoBanco)
    {
        $this->tipoBanco = $tipoBanco;

        return $this;
    }

    /**
     * Get tipoBanco
     *
     * @return \Cet\NominaBundle\Entity\TipoBanco 
     */
    public function getTipoBanco()
    {
        return $this->tipoBanco;
    }

    /**
     * Set tipoCuenta
     *
     * @param \Cet\NominaBundle\Entity\TipoCuenta $tipoCuenta
     * @return PlantillaNomina
     */
    public function setTipoCuenta(\Cet\NominaBundle\Entity\TipoCuenta $tipoCuenta)
    {
        $this->tipoCuenta = $tipoCuenta;

        return $this;
    }

    /**
     * Get tipoCuenta
     *
     * @return \Cet\NominaBundle\Entity\TipoCuenta 
     */
    public function getTipoCuenta()
    {
        return $this->tipoCuenta;
    }

    /**
     * Set fk_tipocalculo
     *
     * @param \Cet\NominaBundle\Entity\TipoCalculo $fkTipocalculo
     * @return PlantillaNomina
     */
    public function setFkTipocalculo(\Cet\NominaBundle\Entity\TipoCalculo $fkTipocalculo = null)
    {
        $this->fk_tipocalculo = $fkTipocalculo;

        return $this;
    }

    /**
     * Get fk_tipocalculo
     *
     * @return \Cet\NominaBundle\Entity\TipoCalculo 
     */
    public function getFkTipocalculo()
    {
        return $this->fk_tipocalculo;
    }
}
