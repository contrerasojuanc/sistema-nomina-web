<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Concepto;
use Cet\NominaBundle\Entity\HistoricoConcepto;
use Cet\NominaBundle\Form\ConceptoType;
use DateTime;
use Symfony\Component\Form\FormError;
use Cet\NominaBundle\Entity\VariableFormula;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\ParsedExpression;

use Symfony\Component\Debug\ErrorHandler;
use Cet\NominaBundle\Entity\LenguajeFormula;
/**
 * Concepto controller.
 *
 */
class ConceptoController extends Controller
{

    /**
     * Lists all Concepto entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Concepto')->findAll();

        return $this->render('CetNominaBundle:Concepto:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Concepto entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Concepto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $formula_php=$entity->getSql();
        //$esAcumulado=$request->request->parameters['cet_nominabundle_concepto']['acumulado'];
//        $esAcumulado=$request->request->get('esacumulado');
//        $esAcumulado=$_POST['cet_nominabondle_concepto']['acumulado'];
        
//        $esAcumulado=$form->getExtradata()['esacumulado'];
        //$acumulado=$entity->g();
        $formulaaporte_php=($entity->getFormulaPatronal()==""?0:$entity->getFormulaPatronal());
        
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
        
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        if ($form->isValid()) {
            //Validar que  otro concepto no utilice el mismo identificador
            $valido=true;
            $identificador=$entity->getIdentificador();
             $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findby(array('identificador'=>$identificador));
             foreach ($conceptos as $concepto){
                $error = new FormError("El identificador que intenta usar ya esta utilizado por otro concepto");
                $form->get('identificador')->addError($error); 
                $valido=false;
            }
            //Validar que no use  el identificador del nombre de una variable
            $variables = $em->getRepository('CetNominaBundle:Variable')->findby(array('nombre'=>$identificador));
            foreach ($variables as $variable){
                $error = new FormError("El identificador que intenta usar ya esta utilizado por una variable");
                $editForm->get('identificador')->addError($error); 
                $valido=false;
            } 
            $infoerr=""; 
            if(preg_match("/\b$identificador\b/", $formula_php)){
                $error = new FormError("El concepto no debe emplearse como parte de su propia fórmula");
                $form->get('sql')->addError($error);    
                $valido=false;
            }
            $infoerr=""; 
            if(preg_match("/\b$identificador\b/", $formulaaporte_php)){
                $error = new FormError("El concepto no debe emplearse como parte de su propia fórmula de aporte patronal");
                $form->get('formulaPatronal')->addError($error);    
                $valido=false;
            }
            $infoerr="";  
            if(!$this->pruebaFormula($formula_php, $infoerr)){
                $valido=false;
                $error = new FormError("Error en la fórmula del concepto: $infoerr");
                $form->get('sql')->addError($error);
            }
            $infoerr1="";  
            if(!$this->pruebaFormula($formulaaporte_php, $infoerr1)){
                $valido=false;
                $error1 = new FormError("Error en la fórmula del concepto de aporte patronal: $infoerr1");
                $form->get('formulaPatronal')->addError($error1);
            }
            
            if($valido){
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                return $this->redirect($this->generateUrl('concepto_show', array('id' => $entity->getId())));
            }
            
        }

        return $this->render('CetNominaBundle:Concepto:new.html.twig', array(
            'entity' => $entity,
            'lista_personal' => $informacionpersonal,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Concepto entity.
    *
    * @param Concepto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Concepto $entity)
    {
        $form = $this->createForm(new ConceptoType(), $entity, array(
            'action' => $this->generateUrl('concepto_create'),
            'method' => 'POST',
        ));
        
        $form->add('submit', 'submit', array('label' => 'Crear'))
//             ->add('reset', 'reset', array('label' => 'Limpiar'));
                ->add('reset', 'reset', array('label' => 'Probar Cálculo','attr' => array('data-toggle' => 'modal','href'=>'#probar')))
        ;
            
        return $form;
    }

    /**
     * Displays a form to create a new Concepto entity.
     *
     */
    public function newAction()
    {
        $entity = new Concepto();
        $form   = $this->createCreateForm($entity);
        
        $em = $this->getDoctrine()->getManager();      
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
        
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        return $this->render('CetNominaBundle:Concepto:new.html.twig', array(
            'entity' => $entity,
            'lista_personal' => $informacionpersonal,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Concepto entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Concepto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Concepto.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Concepto:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Concepto entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Concepto')->find($id);

//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Concepto.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Concepto:edit.html.twig', array(
            'entity'      => $entity,
            'lista_personal' => $informacionpersonal,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Concepto entity.
    *
    * @param Concepto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Concepto $entity)
    {
        $form = $this->createForm(new ConceptoType(), $entity, array(
            'action' => $this->generateUrl('concepto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
//             ->add('reset', 'reset', array('label' => 'Restablecer'))             
             ->add('reset', 'reset', array('label' => 'Probar Cálculo','attr' => array('data-toggle' => 'modal','href'=>'#probar')))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')))
        ;
        
        return $form;
    }
    /**
     * Edits an existing Concepto entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CetNominaBundle:Concepto')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Concepto.');
        }
        $identificadorAntes=$entity->getIdentificador();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        $identificadorNuevo=$editForm->get('identificador')->getData();
        if ($editForm->isValid()) {
            //Validar que no use  otro identificador de otro concepto
            $valido=true;
            $identificador=$entity->getIdentificador();
            $idEdit=$entity->getId();
            $formula_php=$entity->getSql();
            $formulaaporte_php=($entity->getFormulaPatronal()==""?0:$entity->getFormulaPatronal());
            $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findby(array('identificador'=>$identificador));
            foreach ($conceptos as $concepto){
                $idEncontro=$concepto->getId();
                if($idEncontro<>$idEdit){
                    $error = new FormError("El identificador que intenta usar ya esta utilizado por otro concepto");
                    $editForm->get('identificador')->addError($error); 
                    $valido=false;
                }
            }   
            //Validar que no use  el identificador del nombre de una variable
            $variables = $em->getRepository('CetNominaBundle:Variable')->findby(array('nombre'=>$identificador));
            foreach ($variables as $variable){
                $error = new FormError("El identificador que intenta usar ya esta utilizado por una variable");
                $editForm->get('identificador')->addError($error); 
                $valido=false;
            }
            $infoerr=""; 
            if(preg_match("/\b$identificador\b/", $formula_php)){
                $error = new FormError("El concepto no debe emplearse como parte de su propia fórmula");
                $form->get('sql')->addError($error);    
                $valido=false;
            }
            $infoerr=""; 
            if(preg_match("/\b$identificador\b/", $formulaaporte_php)){
                $error = new FormError("El concepto no debe emplearse como parte de su propia fórmula de aporte patronal");
                $form->get('formulaPatronal')->addError($error);    
                $valido=false;
            }
            
            $infoerr="";  
            if(!$this->pruebaFormula($formula_php, $infoerr)){
                $valido=false;
                $error = new FormError("Error en la fórmula del concepto: $infoerr");
                $editForm->get('sql')->addError($error);
            }
            $infoerr1="";  
            if(!$this->pruebaFormula($formulaaporte_php, $infoerr1)){
                $valido=false;
                $error1 = new FormError("Error en la fórmula del concepto de aporte patronal: $infoerr1");
                $editForm->get('formulaPatronal')->addError($error1);
            }
            
            if($valido){
//                $em = $this->getDoctrine()->getManager();
//                $em->persist($entity);
                $em->flush();
                if($identificadorNuevo!=$identificadorAntes){
                    //recorrer los demas conceptos y cambiar su formula
                    $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
                    $patron[0]="/$identificadorAntes/";
                    $sustituir[0]="$identificadorNuevo";
                    foreach ($conceptos as $concep){
                        $formula=$concep->getSql();
                        $formulaNueva=preg_replace($patron,$sustituir, $formula);
                        $concep->setSql($formulaNueva);
                         $formulaPatro=$concep->getFormulaPatronal();
                         $formulaNuevaPatro=preg_replace($patron,$sustituir, $formulaPatro);
                        $concep->setFormulaPatronal($formulaNuevaPatro);
                         $em->persist($concep);
                         $em->flush();
                    } 
                }
                return $this->redirect($this->generateUrl('concepto_show', array('id' => $id)));
            }
        }
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        return $this->render('CetNominaBundle:Concepto:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'lista_personal' => $informacionpersonal,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Concepto entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Concepto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Concepto.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('concepto'));
    }

    /**
     * Creates a form to delete a Concepto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('concepto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    /**
     * Prueba los conceptos
     *
     */
    public function pruebaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //pregacargar la variables en memoria
        $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
        $vector_conceptos=array();
        foreach ($conceptos as $concepto){
            $nombreConcepto=$concepto->getIdentificador();
            $vector_conceptos[]=$nombreConcepto;
        }   
        //Obtengo el id del personal seleccionado para realizar la prueba asi como el sql
        $id_personal = $request->request->get('id_personal');
        $php_concepto = $request->request->get('php_concepto');
        $identificador = $request->request->get('identificador_concepto');
        
//        if (preg_match('/\bcall\b|\brequire\b|\bfunction\b|\beval\b|\btrait\b|\brequire_once\b|\binclude\b|\bextends\b|\bclass\b|\bimplements\b|\bdeclare\b|\b__CLASS__\b|\b__NAMESPACE__\b|\b__DIR__\b|\b__FILE__\b|\b__FUNCTION__\b|\b__LINE__\b|\b__METHOD__\b|\b__NAMESPACE__\b|\b__TRAIT__\b/i', $php_concepto)){
//            return new JsonResponse("!No ingresar palabras claves en la formula");
//        }
        $infoerr="";
//        if(!$this->pruebaFormula($php_concepto, $infoerr)){
//            return new JsonResponse("Error en la fórmula del concepto: $infoerr");
//        }
        
        $em = $this->getDoctrine()->getManager();
        // Revisar si es necesario precargar las variables en memoria
        //
        //VARIABLES
        //
        //Obtiene las variables de cada funcionario
//        $query = "SELECT hv,v FROM Cet\NominaBundle\Entity\HistoricoVariable hv "
//                . "JOIN hv.fk_variable_has_personal_variable1 v "
//                . "WHERE hv.fk_variable_has_personal_personal1 = :personal "
//                . "AND hv.fechaFin IS NULL";
//
//        $consulta=$em->createQuery($query)->setParameter('personal', $id_personal);
//        
//        $datos=$consulta->getResult();
        
        //Cargar todos los calculos de variables 
        $datos = $em->getRepository('CetNominaBundle:Variable')->findAll();
        
        //Asignamos las variables de cada funcionario en memoria utilizando $$ como variables dinamicas
        foreach($datos as $objeto){            
//            $sqlvariable=$objeto->getFkVariableHasPersonalVariable1()->getSql();
//            $nombreVariable=$objeto->getFkVariableHasPersonalVariable1()->getNombre();
            
            $sqlvariable=$objeto->getSql();
            $nombreVariable=$objeto->getNombre();
            
            $consultasql=$em->createQuery($sqlvariable);
            $consultasql->setParameter('personal_cedula', $id_personal);
            $consultasql->setMaxResults(1);
            $valorVariable=0;
            if ($consultasql->getResult()){
                foreach($consultasql->getSingleResult() as $dato){
                    if(!is_object($dato)){
                        $valorVariable=$dato;
                    }                    
                    elseif($dato instanceof \DateTime){
//                        $valorVariable=$dato->format('d/m/Y');
//                        $valorVariable=new \DateTime($valorVariable);
                        $valorVariable=$dato;
                    }
                }
            }            
            $$nombreVariable=$valorVariable;
            $vector_variables[$nombreVariable]=$valorVariable;
        }
        
        /////////////////////////////
        //Inicio Variables Predefinidas
        /////////////////////////////
        
        //Cuenta cantidad de lunes en un lapso de fechas
        $lunes=1;        
        $vector_variables["cantidad_lunes"]=$lunes;
        
        //Verifica si Fecha Desde es mayor que 9 probablemente sea segunda quincena
        $vector_variables["primera_quincena"] = true;       
        
        /////////////////////////////
        //Fin Variables Predefinidas
        /////////////////////////////
        
        //Si se consigue a si mismo en el campo de formula se considera como acumulador
        if(preg_match("/\b$identificador\b/", $php_concepto)){
//            $montoConcepto=0;
//            $concepto = $em->getRepository('CetNominaBundle:Concepto')->findBy(array('identificador'=>$identificador));            
//            if(isset($concepto)){
//                foreach($concepto as $campo){
//                    $historicoConcepto = $em->getRepository('CetNominaBundle:HistoricoConcepto')->encuentra($campo->getId(),$id_personal);
//                    
//                    if ($historicoConcepto){
//                        $montoConcepto=$historicoConcepto->getMonto();
//                    }
//                    
//                }
//            }
//            $vector_variables[$identificador]=$montoConcepto;
            return new JsonResponse("!El concepto no debe emplearse como parte de su propia fórmula¡");
        }
        $var=new VariableFormula();        
        $vector_variables['Variable']=$var;
        $language = new LenguajeFormula();
//        
        $vector_evaluado=array();        
        try {  
//            $valor = $language->evaluate("3+4");
            //Funcion Recursiva para calcular conceptos anidados            
            $valor = $this->evaluarConcepto($php_concepto, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
//            $valor = round($valor,2);
        } catch (\Exception $exc) {
            return new JsonResponse($exc->getMessage());
        }
        
        
//        try {
//            $valor = eval("$php_concepto;");
//        } catch (\Symfony\Component\Debug\Exception\ContextErrorException $exc) {
//            return new JsonResponse($exc->getTraceAsString());
//        }

        if (isset($valor)){
            if(!is_object($valor)){
                return new JsonResponse($valor);
            }                    
            elseif($valor instanceof \DateTime){
                return new JsonResponse($valor->format('d/m/Y'));
            }
            
        }
        
        return new JsonResponse("!No hay resultado¡");
    }
    
    /**
     * Calcula el valor de las variables y lo almacena en historicovariable
     * Parametros: $idvariable = Variable
     * Parametros: $idpersonal = Cedula
     */
    public function calculaVariable($idvariable,$idpersonal)
    {
       try{
        $em = $this->getDoctrine()->getManager();
        
        //Para evitar el desbordamiento de memoria
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $variable = $em->getRepository('CetNominaBundle:Variable')->find($idvariable);
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($idpersonal);
       
        //Se hace la consulta del DQL almacenado en la variable
        $consulta = $em->createQuery($variable->getSql());
        $formula=$variable->getSql();
        $consulta->setParameter('personal_cedula', $idpersonal);
//        try {
            $datos=$consulta->getOneOrNullResult();
//        } 
        //catch (\Doctrine\ORM\NonUniqueResultException $exc) {
//        catch (\Exception $exc) {
//            $info = toString($e);
//            $datos=array();
//        }
        if ($datos==null){
            $datos=array();
        }
        foreach($datos as $dato){
            if(!is_object($dato)){
                //return new JsonResponse(array($dato));
                $valor = $dato;
            }                
            elseif($dato instanceof \DateTime){
                    $resul=$dato->format('d/m/Y');
                    //return new JsonResponse(array($resul));
                    $valor=$resul;

            }
            else {
                //return new JsonResponse(array("!No hay resultado¡"));
                $valor=0;
            }
        }

//            $valor = $consulta->getSingleScalarResult();
        $hoy = new DateTime('NOW');
                        
        //Se verifica si la variable existe
        $historicoVariable = $em->getRepository('CetNominaBundle:HistoricoVariable')->encuentra($idvariable,$idpersonal);
        
        $historicoVariablenuevo = new HistoricoVariable(); 
        if (isset($valor)){
            if($historicoVariable){
                //Se verifica si el valor cambia
                //if($historicoVariable->getValor()!=$valor ){                       
                if($historicoVariable->getValor()!=$valor or $historicoVariable->getSql()!=$formula ){                       
                    $historicoVariablenuevo->setFkVariableHasPersonalVariable1($variable);
                    $historicoVariablenuevo->setFkVariableHasPersonalPersonal1($personal);
                    $historicoVariablenuevo->setValor($valor);        
                    $historicoVariablenuevo->setSql($formula);    
                    $historicoVariablenuevo->setFechaInicio($hoy);

                    $historicoVariable->setFechaFin($hoy);   

                    $em->persist($historicoVariable); 
                    $em->persist($historicoVariablenuevo); 
                    $em->flush();
                }
            }else{
                //Se almacena el valor de la variable en historicovariable       
                $historicoVariablenuevo->setFkVariableHasPersonalVariable1($variable);
                $historicoVariablenuevo->setFkVariableHasPersonalPersonal1($personal);
                $historicoVariablenuevo->setValor($valor);
                $historicoVariablenuevo->setSql($formula); 
                $historicoVariablenuevo->setFechaInicio($hoy);        

                $em->persist($historicoVariablenuevo);        
                $em->flush(); 
            }    
        }  
         }
        catch (\Exception $e) {
            //$info = toString($e);
            $info = $e->getMessage();
            $e=null;
        }
        //if (trim($info)== )
        $em->clear();
        return $this->redirect($this->generateUrl('variable'));
    }
    
    /**
     * Calcula el valor de los conceptos y lo almacena en historicoconcepto
     * Parametros: $idconcepto = Concepto
     * Parametros: $idpersonal = Cedula
     */
    public function calculaAction($idconcepto,$idpersonal,$idnomina)
    {
        $em = $this->getDoctrine()->getManager();     
        
        //Para evitar el desbordamiento de memoria
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $concepto = $em->getRepository('CetNominaBundle:Concepto')->find($idconcepto);
        
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($idpersonal);
        
        $nomina = $em->getRepository('CetNominaBundle:Nomina')->find($idnomina);
        $identificador=$concepto->getIdentificador();
        //Se hace la consulta de la formula almacenada en Concepto
        $formula_concepto = $concepto->getSql();
        $formula_concepto_patronal = $concepto->getFormulaPatronal();
        
//        //Obtiene las variables de cada funcionario
//        $query = "SELECT hv,v FROM Cet\NominaBundle\Entity\HistoricoVariable hv "
//                . "JOIN hv.fk_variable_has_personal_variable1 v "
//                . "WHERE hv.fk_variable_has_personal_personal1 = :personal "
//                . "AND hv.fechaFin IS NULL";
//
//        $consulta=$em->createQuery($query)->setParameter('personal', $idpersonal);
////        $consulta->setParameter('personal', $idpersonal);
//        
//        $datos=$consulta->getResult();
        
        $datos = $em->getRepository('CetNominaBundle:Variable')->findAll();
        
        //Asignamos las variables de cada funcionario en memoria utilizando $$ como variables dinamicas
        foreach($datos as $objeto){       
            $sqlvariable=$objeto->getSql();
            $nombreVariable = $objeto->getNombre();
//            $sqlvariable=$objeto->getFkVariableHasPersonalVariable1()->getSql();
//            $nombreVariable=$objeto->getFkVariableHasPersonalVariable1()->getNombre();
            
            $consultasql=$em->createQuery($sqlvariable);
            $consultasql->setParameter('personal_cedula', $idpersonal);
            $consultasql->setMaxResults(1);
            $valorVariable=0;
            if ($consultasql->getResult()){
                foreach($consultasql->getSingleResult() as $dato){
                    if(!is_object($dato)){
                        $valorVariable=$dato;
                    }                    
                    elseif($dato instanceof \DateTime){
//                        $valorVariable=$dato->format('d/m/Y');
//                        $valorVariable=new \DateTime($valorVariable);
                        $valorVariable=$dato;
                    }
                }
            }            
            $$nombreVariable=$valorVariable;
            $vector_variables[$nombreVariable]=$valorVariable;
        }
        
        /////////////////////////////
        //Inicio Variables Predefinidas
        /////////////////////////////
        
        //Cuenta cantidad de lunes en un lapso de fechas
        $helper = $this->get('cet.nomina.globales');
        $lunes = 1;
        $lunes = $helper->countDays(1,strtotime($nomina->getDesde()->format('d-m-Y')),strtotime($nomina->getHasta()->format('d-m-Y')));
//        $lunes=$this->countDays(1,strtotime($nomina->getDesde()->format('d-m-Y')),strtotime($nomina->getHasta()->format('d-m-Y')));
        $vector_variables["cantidad_lunes"] = $lunes;
        
        /////////////////////////////
        //Fin Variables Predefinidas
        /////////////////////////////
        
        ////////////////////////////
        $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
        foreach ($conceptos as $concep){
            $nombreConcepto=$concep->getIdentificador();
            $vector_conceptos[]=$nombreConcepto;
        }   
        $vector_conceptosInicial= $vector_conceptos;
        $var=new VariableFormula();        
        $vector_variables['Variable']=$var;
        $vector_variablesInicial=$vector_variables;
        $language = new LenguajeFormula();

        //Calcula el valor para la sección principal
        $vector_evaluado=array();        
        try {  
//            $valor = $language->evaluate("3+4");
            //Funcion Recursiva para calcular conceptos anidados            
            $valor = $this->evaluarConcepto($formula_concepto, $vector_variables, $vector_conceptos, $vector_evaluado, $idpersonal);
//            $valor = round($valor,2);            
        } catch (\Exception $exc) {
            //Retornar error al calcular en caso de no poder evaluar recursivamente la formula
//            return false;
            $valor = 0;
        }
        
        //Verificar  si este concepto tiene otros que lo acumulen
        $acumuladoOtros=0;
        foreach($concepto->getFkConceptoHasConcepto2() as $conceptoAcumulador){
            $acm[]=$conceptoAcumulador->getId();
            $conceptoId=$conceptoAcumulador->getId();
            $formula_concepto=$conceptoAcumulador->getSql();
            $vector_evaluadoA=array();
            $vector_conceptosA=$vector_conceptosInicial;
            $vector_variablesA=$vector_variablesInicial;
            try {  
                $valorA = $this->evaluarConcepto($formula_concepto, $vector_variablesA, $vector_conceptosA, $vector_evaluadoA, $idpersonal);
//                $valorA = round($valorA,2);
            } catch (\Exception $exc) {
                $valorA = 0;
            }
            $acumuladoOtros+=$valorA;
        }
        
        
        
        //Calcula el valor de la sección de aporte patronal
        $vector_evaluado=array();
        try {  
            //Funcion Recursiva para calcular conceptos anidados en aporte patronal             
            $valorPatronal = $this->evaluarConcepto($formula_concepto_patronal, $vector_variables, $vector_conceptos, $vector_evaluado, $idpersonal);
//            $valorPatronal = round($valorPatronal,2);
        } catch (\Exception $exc) {
            //Retornar error al calcular en caso de no poder evaluar recursivamente la formula
//            return false;
            $valorPatronal = 0;
        }
//        $valor=eval("$consulta;");
       
        //Buscar si el concepto esta en datos variables para esa persona
        $datosvariables = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datos_variables_concepto1'=>$idconcepto,'fk_datos_variables_personal1'=>$idpersonal));
        if($datosvariables!=null){
            foreach($datosvariables as $datosvariable){
                if($datosvariable->getModoAfectacion()=='sustitucion'){
                    if($datosvariable->getSeccionAfectacion()=='principal'){
                         $valor=$datosvariable->getValor();
                    }
                    elseif($datosvariable->getSeccionAfectacion()=='patronal'){
                         $valorPatronal=$datosvariable->getValor();
                    }                                   
                }
                elseif($datosvariable->getModoAfectacion()=='adicion'){
                    if($datosvariable->getSeccionAfectacion()=='principal'){
                         $valor=$valor+$datosvariable->getValor();
                    }
                    elseif($datosvariable->getSeccionAfectacion()=='patronal'){
                         $valorPatronal=$valorPatronal+$datosvariable->getValor();
                    }
                }
                elseif($datosvariable->getModoAfectacion()=='sustraccion'){
                    if($datosvariable->getSeccionAfectacion()=='principal'){
                         $valor=$valor-$datosvariable->getValor();
                    }
                    elseif($datosvariable->getSeccionAfectacion()=='patronal'){
                         $valorPatronal=$valorPatronal- $datosvariable->getValor();
                    }
                }
            }
        }
        //Calcula el valor para la sección de acumulado
        //$acumulado
        $acumulado=0;
        $valoracumulado=0;
        $historicoConceptoAcumulado = $em->getRepository('CetNominaBundle:HistoricoConcepto')->encuentra($idconcepto,$idpersonal);
        if($historicoConceptoAcumulado){
            $valoracumulado=$historicoConceptoAcumulado->getAcumulado();
        }        
        $acumulado=(($valoracumulado==null)?0:$valoracumulado)+ $valor + $valorPatronal + $acumuladoOtros;
        $hoy = new DateTime('NOW');
        $historicoConceptonuevo = new HistoricoConcepto(); 
        if($datosvariables!=null){
                foreach($datosvariables as $datosvariable){
                    if($datosvariable->getModoAfectacion()=='sustitucion'){
                        if($datosvariable->getSeccionAfectacion()=='acumulado'){
                             $acumulado=$datosvariable->getValor();
                        }
                    }
                    elseif($datosvariable->getModoAfectacion()=='adicion'){
                        if($datosvariable->getSeccionAfectacion()=='acumulado'){
                             $acumulado=$acumulado+$datosvariable->getValor();
                        }
                       
                    }
                    elseif($datosvariable->getModoAfectacion()=='sustraccion'){
                        if($datosvariable->getSeccionAfectacion()=='acumulado'){
                             $acumulado=$acumulado-$datosvariable->getValor();
                        }
                    }
                }
        }
        
//        if($historicoConcepto){
//            //Se verifica si el valor cambia
//            if($historicoConcepto->getMonto()!=$valor){   
//                $historicoConceptonuevo->setConceptoId($idconcepto);
//                $historicoConceptonuevo->setFkHistoricoConceptoConcepto1($concepto);
//                $historicoConceptonuevo->setPersonalCedula($idpersonal);
//                $historicoConceptonuevo->setFkHistoricoConceptoPersonal1($personal);
//                $historicoConceptonuevo->setNominaId($idnomina);
//                $historicoConceptonuevo->setFkHistoricoConceptoNomina1($nomina);
//                $historicoConceptonuevo->setMonto($valor);        
//                $historicoConceptonuevo->setFechaInicio($hoy); 
//                
//                $historicoConcepto->setFechaFin($hoy);   
//                
//                $em->persist($historicoConcepto); 
//                $em->persist($historicoConceptonuevo); 
//                $em->flush();
//            }
//        }else{
//            //Se almacena el valor de la variable en historicovariable      
//            $historicoConceptonuevo->setConceptoId($idconcepto);
//            $historicoConceptonuevo->setFkHistoricoConceptoConcepto1($concepto);
//            $historicoConceptonuevo->setPersonalCedula($idpersonal);
//            $historicoConceptonuevo->setFkHistoricoConceptoPersonal1($personal);
//            $historicoConceptonuevo->setNominaId($idnomina);
//            $historicoConceptonuevo->setFkHistoricoConceptoNomina1($nomina);
//            $historicoConceptonuevo->setMonto($valor);        
//            $historicoConceptonuevo->setFechaInicio($hoy);        
//
//            $em->persist($historicoConceptonuevo);        
//            $em->flush(); 
//        }    
        
        //No guardar en historico si el valor o aporte patronal es cero 0
        if($valor<>0 || $valorPatronal<>0)
        {
                $historicoConceptonuevo->setConceptoId($idconcepto);
                $historicoConceptonuevo->setFkHistoricoConceptoConcepto1($concepto);
                $historicoConceptonuevo->setPersonalCedula($idpersonal);
                $historicoConceptonuevo->setFkHistoricoConceptoPersonal1($personal);
                $historicoConceptonuevo->setNominaId($idnomina);
                $historicoConceptonuevo->setFkHistoricoConceptoNomina1($nomina);
                $historicoConceptonuevo->setMonto($valor);
                $historicoConceptonuevo->setMontoPatronal($valorPatronal);
                $historicoConceptonuevo->setAcumulado($acumulado);
                $historicoConceptonuevo->setFechaInicio($nomina->getDesde()); 
                $historicoConceptonuevo->setFechaFin($nomina->getHasta());                 
                
                $em->persist($historicoConceptonuevo); 
                $em->flush();
        } 
        $em->clear();
//        return new JsonResponse("Concepto: ".$idconcepto." Funcionario: ".$idpersonal);
        return $this->redirect($this->generateUrl('concepto'));
    }
    //Verificar si la formula del concepto es correcta y su sintaxis no genera errores
    public function pruebaFormula($formula_php, &$info)
    {
        $em = $this->getDoctrine()->getManager();
        //pregacargar la variables en memoria
        $variables = $em->getRepository('CetNominaBundle:Variable')->findAll();
        foreach ($variables as $variable){
            $nombreVariable=$variable->getNombre();
            $vector_variables[$nombreVariable]=$nombreVariable;
        }                                        
        
        $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
        foreach ($conceptos as $concepto){
            $nombreVariable=$concepto->getIdentificador();
            $vector_variables[$nombreVariable]=$nombreVariable;
        }                                        
        
        $var=new VariableFormula();        
        $vector_variables['Variable']='Variable';
        //Se hace la consulta de la formula almacenada en Concepto
        $language = new LenguajeFormula();
        
        try {  
            $valor = $language->compile($formula_php, $vector_variables);
        } catch (\Exception $exc) {
            $info = $exc->getMessage();
            return false;
        }  
       return true;         
    }

    public function evaluarConcepto($formula,&$vector_variables,$vector_conceptos,&$vector_evaluado,$id_personal){
        //Buscar un concepto en la formula
        foreach (array_diff($vector_conceptos,$vector_evaluado) as $concepto){
            if(preg_match("/\b$concepto\b/", $formula)){
                $seleccionado=$concepto;
                break;
            }                    
        }
        $language = new LenguajeFormula();
        //Confirmar que el concepto hallado no haya sido evaluado($vector_evaluado)
        //En caso afirmativo devolver error de redundancia ciclica
        if(isset($seleccionado)){
            if(array_search($seleccionado,$vector_evaluado)){
                return false;
            }
            //Hallar formula del concepto seleccionado
            $em = $this->getDoctrine()->getManager();
            $concepto = $em->getRepository('CetNominaBundle:Concepto')->findBy(array('identificador'=>$seleccionado));
            if(isset($concepto[0])){
                $formulaNueva=$concepto[0]->getSql();
            }
            else{
                throw new \Exception('Error no se consigue el concepto: '.$seleccionado);
            }
            

            //Agregamos a $vector_evaluado el concepto hallado
            $vector_evaluado[]=$seleccionado;
//            foreach($vector_variables as $key=>$value){
//                $vector_nombres_variables[$key]=$key;
//            }            
//            //Agregamos a $vector_variables el identificador del concepto hallado
//            $vector_nombres_variables[$seleccionado]=$seleccionado;
            
            //Compilar formula dentro de un try catch
            //En caso de excepcion arrojar la excepcion con throw
//            try {  
//                $compilado = $language->compile($formula, $vector_nombres_variables);
//            } catch (\Exception $exc) {
//                throw new \Exception('Error compilando fórmula de concepto: '.$seleccionado);
//                return false;
//            }

            try {
                //Agregamos a $vector_variables el valor del concepto hallado
                $vector_variables[$seleccionado] = $this->evaluarConcepto($formulaNueva, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
            } catch (Exception $exc) {
                throw $exc;
            }

            try {  
                return round( $language->evaluate($formula, $vector_variables),2 );
            } catch (\Exception $exc) {
//                $vector_variables[$seleccionado] = $this->evaluarConcepto($formula, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
                return $this->evaluarConcepto($formula, $vector_variables, $vector_conceptos, $vector_evaluado, $id_personal);
                //throw new \Exception('Error ejecutando fórmula de concepto: '.$seleccionado);
                //return false;
            }  
        }
        else{ //En caso de no conseguir algun concepto en la formula
            try {  
                return round( $language->evaluate($formula, $vector_variables),2 );
            } catch (\Exception $exc) {
                throw $exc;
            }
        }
    }
}
