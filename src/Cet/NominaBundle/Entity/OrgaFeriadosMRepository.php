<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * OrgaFeriadosMRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrgaFeriadosMRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('fecha' => 'DESC'));
    }
}
