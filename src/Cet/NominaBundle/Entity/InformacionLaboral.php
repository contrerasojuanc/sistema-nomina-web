<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

//Desde Symfony 2.5
//use Symfony\Component\Validator\Context\ExecutionContextInterface;

//Antes de Symfony 2.4
use Symfony\Component\Validator\ExecutionContextInterface;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\InformacionLaboral
 *
 * @ORM\Entity(repositoryClass="InformacionLaboralRepository")
 * @ORM\Table(name="sis_nomina.informacion_laboral", indexes={@ORM\Index(name="fk_informacio_laboral_base_legal1_idx", columns={"base_legal_id"}), @ORM\Index(name="fk_informacion_laboral_personal1_idx", columns={"personal_cedula"})}, uniqueConstraints={@ORM\UniqueConstraint(name="cuenta_nomina_UNIQUE", columns={"cuenta_nomina"}), @ORM\UniqueConstraint(name="cuenta_fideicomiso_UNIQUE", columns={"cuenta_fideicomiso"})})
 * @UniqueEntity(
 *     fields={"cuentaNomina"},
 *     message="Ya existe una cuenta con este mismo número."
 * )
 * @UniqueEntity(
 *     fields={"cuentaNomina"},
 *     message="Ya existe una cuenta de fideicomiso con este mismo número.",
 *     repositoryMethod="cuentarepetida"
 * )
 * @UniqueEntity(
 *     fields={"cuentaFideicomiso"},
 *     message="Ya existe una cuenta con este mismo número."
 * )
 * @UniqueEntity(
 *     fields={"expediente"},
 *     message="Ya existe un expediente con este mismo número."
 * )
 * @UniqueEntity(
 *     fields={"cuentaFideicomiso"},
 *     message="Ya existe una cuenta de nómina con este mismo número.",
 *     repositoryMethod="cuentafideicomisorepetida"
 * )
 */
class InformacionLaboral
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="cuenta_nomina", type="text", unique=true, nullable=true)
     */
    private $cuentaNomina;

    /**
     * @var integer
     * @ORM\Column(name="tipo_cuenta_nomina", type="integer", nullable=true, options={"default" = 0})
     */
    private $tipoCuentaNomina;
    
    /**
     * @var string
     * @ORM\Column(name="titulo_profesional", type="text", nullable=true)
     */
    private $tituloProfesional;
    
        /**
     * @var string
     * @ORM\Column(name="expediente", type="text", unique=true, nullable=true)
     */
    private $expediente;
    
    /**
     * @var string
     * @ORM\Column(name="cuenta_fideicomiso", type="text", unique=true, nullable=true)
     */
    private $cuentaFideicomiso;

    /**
     * @var integer
     * @ORM\Column(name="tipo_cuenta_fideicomiso", type="integer", nullable=true, options={"default" = 0})
     */
    private $tipoCuentaFideicomiso;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_ingreso", type="date", nullable=true)
     */
    private $fechaIngreso;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_egreso", type="date", nullable=true)
     */
    private $fechaEgreso;
    
      /**
     * Solo valores:
     * R = Renuncia
     * D = Despido
     * H = Inhabilitación
     * I = Incapacitación
     * J = Jubilación
     * O = Otro
     *
     * @var string
     * @ORM\Column(name="condicion_egreso", type="string", length=1, nullable=true)
     */
    private $condicionEgreso;



    /**
     * @ORM\OneToOne(targetEntity="BaseLegal", inversedBy="informacionLaboral")
     * @ORM\JoinColumn(name="base_legal_id", referencedColumnName="id")
     */
    protected $baseLegal;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoCargo", mappedBy="fk_informacion_laboral_has_cargo_informacion_laboral1",cascade={"remove"})
     * @ORM\JoinColumn(name="informacion_laboral_id", referencedColumnName="id", nullable=false)
     */
    protected $historicoCargos;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoUnidadOrganizativa", mappedBy="fk_unidad_organizativa_has_informacion_laboral_informacion_la1",cascade={"remove"})
     * @ORM\JoinColumn(name="informacion_laboral_id", referencedColumnName="id", nullable=false)
     */
    protected $historicoUnidadOrganizativas;

    /**
     * @ORM\OneToOne(targetEntity="Personal", inversedBy="informacionLaboral")
     * @ORM\JoinColumn(name="personal_cedula", referencedColumnName="cedula", nullable=false)
     */
    protected $personal;
    
    /**
     * @ORM\ManyToOne(targetEntity="TipoNomina", inversedBy="informacionLaborals")
     * @ORM\JoinColumn(name="tipo_nomina_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_informacion_laboral_tipo_nomina1;
    
    public function __construct()
    {
        $this->historicoCargos = new ArrayCollection();
        $this->historicoUnidadOrganizativas = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cuentaNomina
     *
     * @param string $cuentaNomina
     * @return InformacionLaboral
     */
    public function setCuentaNomina($cuentaNomina)
    {
        $this->cuentaNomina = $cuentaNomina;

        return $this;
    }

    /**
     * Get cuentaNomina
     *
     * @return string 
     */
    public function getCuentaNomina()
    {
        return $this->cuentaNomina;
    }

    /**
     * Set cuentaFideicomiso
     *
     * @param string $cuentaFideicomiso
     * @return InformacionLaboral
     */
    public function setCuentaFideicomiso($cuentaFideicomiso)
    {
        $this->cuentaFideicomiso = $cuentaFideicomiso;

        return $this;
    }

    /**
     * Get cuentaFideicomiso
     *
     * @return string 
     */
    public function getCuentaFideicomiso()
    {
        return $this->cuentaFideicomiso;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return InformacionLaboral
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }
    
    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Get fechaEgreso
     *
     * @return \DateTime 
     */
    public function getFechaEgreso()
    {
        return $this->fechaEgreso;
    }
    
    

    /**
     * Set fechaEgreso
     *
     * @param \DateTime $fechaEgreso
     * @return InformacionLaboral
     */
    public function setFechaEgreso($fechaEgreso)
    {
        $this->fechaEgreso = $fechaEgreso;

        return $this;
    }

    
    
    /**
     * Set condicionEgreso
     *
     * @param string $condicionEgreso
     * @return InformacionLaboral
     */
    public function setCondicionEgreso($condicionEgreso)
    {
        $this->condicionEgreso = $condicionEgreso;

        return $this;
    }

    /**
     * Get condicionEgreso
     *
     * @return string 
     */
    public function getCondicionEgreso()
    {
        return $this->condicionEgreso;
    }
    


    /**
     * Add historicoCargos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos
     * @return InformacionLaboral
     */
    public function addHistoricoCargo(\Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos)
    {
        $this->historicoCargos[] = $historicoCargos;

        return $this;
    }

    /**
     * Remove historicoCargos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos
     */
    public function removeHistoricoCargo(\Cet\NominaBundle\Entity\HistoricoCargo $historicoCargos)
    {
        $this->historicoCargos->removeElement($historicoCargos);
    }

    /**
     * Get historicoCargos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoCargos()
    {
        return $this->historicoCargos;
    }

    /**
     * Add historicoUnidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas
     * @return InformacionLaboral
     */
    public function addHistoricoUnidadOrganizativa(\Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas)
    {
        $this->historicoUnidadOrganizativas[] = $historicoUnidadOrganizativas;

        return $this;
    }

    /**
     * Remove historicoUnidadOrganizativas
     *
     * @param \Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas
     */
    public function removeHistoricoUnidadOrganizativa(\Cet\NominaBundle\Entity\HistoricoUnidadOrganizativa $historicoUnidadOrganizativas)
    {
        $this->historicoUnidadOrganizativas->removeElement($historicoUnidadOrganizativas);
    }

    /**
     * Get historicoUnidadOrganizativas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoUnidadOrganizativas()
    {
        return $this->historicoUnidadOrganizativas;
    }

    /**
     * Set personal
     *
     * @param \Cet\NominaBundle\Entity\Personal $personal
     * @return InformacionLaboral
     */
    public function setPersonal(\Cet\NominaBundle\Entity\Personal $personal)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * Get personal
     *
     * @return \Cet\NominaBundle\Entity\Personal 
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * Set baseLegal
     *
     * @param \Cet\NominaBundle\Entity\BaseLegal $baseLegal
     * @return InformacionLaboral
     */
    public function setBaseLegal(\Cet\NominaBundle\Entity\BaseLegal $baseLegal = null)
    {
        $this->baseLegal = $baseLegal;

        return $this;
    }

    /**
     * Get baseLegal
     *
     * @return \Cet\NominaBundle\Entity\BaseLegal 
     */
    public function getBaseLegal()
    {
        return $this->baseLegal;
    }
    
    public function validarCuentas(ExecutionContextInterface $context)
    {
        if($this->getCuentaNomina()===$this->getCuentaFideicomiso()){
            //Desde Symfony 2.5
//            $context->buildViolation('La cuenta nómina y de fideicomiso no deben ser iguales')
//                ->atPath('cuentaNomina')
//                ->addViolation();
            
            //Antes de Symfony 2.4
            $context->addViolationAt(
                'cuentaNomina',
                'La cuenta nómina y de fideicomiso no deben ser iguales'
            );
        }        
    }
        
    /**
     * Set fk_informacion_laboral_tipo_nomina1
     *
     * @param \Cet\NominaBundle\Entity\TipoNomina $fkInformacionLaboralTipoNomina1
     * @return InformacionLaboral
     */
    public function setFkInformacionLaboralTipoNomina1(\Cet\NominaBundle\Entity\TipoNomina $fkInformacionLaboralTipoNomina1)
    {
        $this->fk_informacion_laboral_tipo_nomina1 = $fkInformacionLaboralTipoNomina1;

        return $this;
    }

    /**
     * Get fk_informacion_laboral_tipo_nomina1
     *
     * @return \Cet\NominaBundle\Entity\TipoNomina 
     */
    public function getFkInformacionLaboralTipoNomina1()
    {
        return $this->fk_informacion_laboral_tipo_nomina1;
    }

    /**
     * Set tipoCuentaNomina
     *
     * @param integer $tipoCuentaNomina
     * @return InformacionLaboral
     */
    public function setTipoCuentaNomina($tipoCuentaNomina)
    {
        $this->tipoCuentaNomina = $tipoCuentaNomina;

        return $this;
    }

    /**
     * Get tipoCuentaNomina
     *
     * @return integer 
     */
    public function getTipoCuentaNomina()
    {
        return $this->tipoCuentaNomina;
    }

    /**
     * Set tipoCuentaFideicomiso
     *
     * @param integer $tipoCuentaFideicomiso
     * @return InformacionLaboral
     */
    public function setTipoCuentaFideicomiso($tipoCuentaFideicomiso)
    {
        $this->tipoCuentaFideicomiso = $tipoCuentaFideicomiso;

        return $this;
    }

    /**
     * Get tipoCuentaFideicomiso
     *
     * @return integer 
     */
    public function getTipoCuentaFideicomiso()
    {
        return $this->tipoCuentaFideicomiso;
    }
    
 /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('cuentaNomina', new Assert\NotBlank())
                ->addPropertyConstraint ('cuentaNomina', new Assert\Type(array('type'=>'digit','message'=>'El campo debe ser solo números.')))
                ->addPropertyConstraint ('cuentaNomina', new Assert\Length(array(
                    'min'=> 20,
                    'max'=> 20,
                    'minMessage'=> 'El Número de cuenta debe contener exactamente 20 digitos',
                    'maxMessage'=> 'El Número de cuenta debe contener exactamente 20 digitos',
                    )))
//                ->addPropertyConstraint ('cuentaFideicomiso', new Assert\NotBlank())
                ->addPropertyConstraint ('cuentaFideicomiso', new Assert\Type(array('type'=>'digit','message'=>'El campo debe ser solo números.')))
                ->addPropertyConstraint ('cuentaFideicomiso', new Assert\Length(array(
                    'min'=> 20,
                    'max'=> 20,
                    'minMessage'=> 'El Número de cuenta debe contener exactamente 20 digitos',
                    'maxMessage'=> 'El Número de cuenta debe contener exactamente 20 digitos',
                    )))

                ->addPropertyConstraint ('fechaIngreso', new Assert\NotBlank())

//                ->addPropertyConstraint ('tipo', new Assert\NotBlank())
             ;
    }
    
    public function __toString()
    {
        return 'Información laboral de: '.$this->getPersonal();
    }

    /**
     * Set tituloProfesional
     *
     * @param string $tituloProfesional
     * @return InformacionLaboral
     */
    public function setTituloProfesional($tituloProfesional)
    {
        $this->tituloProfesional = $tituloProfesional;

        return $this;
    }

    /**
     * Get tituloProfesional
     *
     * @return string 
     */
    public function getTituloProfesional()
    {
        return $this->tituloProfesional;
    }

    /**
     * Set expediente
     *
     * @param string $expediente
     * @return InformacionLaboral
     */
    public function setExpediente($expediente)
    {
        $this->expediente = $expediente;

        return $this;
    }

    /**
     * Get expediente
     *
     * @return string 
     */
    public function getExpediente()
    {
        return $this->expediente;
    }
}
