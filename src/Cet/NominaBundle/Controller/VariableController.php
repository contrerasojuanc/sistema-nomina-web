<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\Variable;
use Cet\NominaBundle\Form\VariableType;
use Cet\NominaBundle\Entity\HistoricoVariable;
use Symfony\Component\Form\Form;
use DateTime;
use Symfony\Component\Form\FormError;

/**
 * Variable controller.
 *
 */
class VariableController extends Controller
{
    
    /**
     * Lists all Variable entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:Variable')->findAll();

        return $this->render('CetNominaBundle:Variable:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Variable entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Variable();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            //Validar que no use  el identificador del nombre de una variable
            $valido=true;
            $nombre=$entity->getNombre();
            $variables = $em->getRepository('CetNominaBundle:Variable')->findby(array('nombre'=>$nombre));
            foreach ($variables as $variable){
                $error = new FormError("El Nombre que intenta usar ya esta siendo utilizado por otra variable");
                $form->get('nombre')->addError($error); 
                $valido=false;
            }
            $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findby(array('identificador'=>$nombre));
            foreach ($conceptos as $concepto){
                $error = new FormError("El identificador que intenta usar ya esta utilizado por un concepto");
                $form->get('nombre')->addError($error); 
                $valido=false;
                //}
            }   
            if($valido){
                $dql=$entity->getSql();
                $infoerr='';
                if ($this->pruebaDQL($dql,$infoerr)){
                    $em->persist($entity);
                    $em->flush();
                     //Se ejecuta el procedimiento en todos los funcionarios para actualizar historico_varaible
                    $personales = $em->getRepository('CetNominaBundle:Personal')->findAll();
                    foreach ($personales as $personal){
                        $this->calculaAction($entity->getId(), $personal->getId());
                    }
                    return $this->redirect($this->generateUrl('variable_show', array('id' => $entity->getId())));
                }
                else{
                    $error = new FormError("Expresion DQL no valida: $infoerr");
                    $form->get('sql')->addError($error);
                    $error=null;
                }
            }
        }
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        
        return $this->render('CetNominaBundle:Variable:new.html.twig', array(
            'entity' => $entity,
            'lista_personal' => $informacionpersonal,  
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Variable entity.
    *
    * @param Variable $entity The entity
    *
    * @return Form The form
    */
    private function createCreateForm(Variable $entity)
    {
        $matriz=$this->llenar_matriz();
        
           $form = $this->createForm(new VariableType(array_keys($matriz),$matriz), $entity, array(
            'action' => $this->generateUrl('variable_create'),
            'method' => 'POST',
        ));
        

        $form->add('submit', 'submit', array('label' => 'Crear'))
//             ->add('reset', 'reset', array('label' => 'Limpiar'))
             ->add('reset', 'reset', array('label' => 'Probar Cálculo','attr' => array('data-toggle' => 'modal','href'=>'#probar')))
        ;

        return $form;
    }

    /**
     * Displays a form to create a new Variable entity.
     *
     */
    public function newAction()
    {
        $entity = new Variable();
        $form   = $this->createCreateForm($entity);
        
        $em = $this->getDoctrine()->getManager();
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        return $this->render('CetNominaBundle:Variable:new.html.twig', array(
            'entity' => $entity,
            'lista_personal' => $informacionpersonal, 
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Variable entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Variable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Variable.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Variable:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Variable entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Variable')->find($id);
        
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Variable.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:Variable:edit.html.twig', array(
            'entity'      => $entity,
//            'prueba_form' => $pruebaForm->createView(),
            'lista_personal' => $informacionpersonal,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Variable entity.
    *
    * @param Variable $entity The entity
    *
    * @return Form The form
    */
    private function createEditForm(Variable $entity)
    {
        $matriz=$this->llenar_matriz();

        $form = $this->createForm(new VariableType(array_keys($matriz),$matriz), $entity, array(
            'action' => $this->generateUrl('variable_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
//             ->add('reset', 'reset', array('label' => 'Restablecer'))             
             ->add('reset', 'reset', array('label' => 'Probar Cálculo','attr' => array('data-toggle' => 'modal','href'=>'#probar')))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#static')))
             ;

        return $form;
    }
    /**
     * Edits an existing Variable entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:Variable')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad Variable.');
        }
        $variableAntes=$entity->getNombre();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        //$variableNueva=$editForm->get('nombre')->getData();
        $variableNueva=$editForm->getViewData()->getNombre();
        if ($editForm->isValid()) {
            //Validar que no use  el identificador del nombre de una variable
            $valido=true;
            $nombre=$entity->getNombre();
            $idEdit=$entity->getId();
            $variables = $em->getRepository('CetNominaBundle:Variable')->findby(array('nombre'=>$nombre));
            foreach ($variables as $variable){
                $idEncontro=$variable->getId();
                if($idEncontro<>$idEdit){
                    $error = new FormError("El Nombre que intenta usar ya esta siendo utilizado por otra variable");
                    $editForm->get('nombre')->addError($error); 
                    $valido=false;
                }
            }
            $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findby(array('identificador'=>$nombre));
            foreach ($conceptos as $concepto){
                    $error = new FormError("El identificador que intenta usar ya esta utilizado por un concepto");
                    $editForm->get('nombre')->addError($error); 
                    $valido=false;
                //}
            }   
            if($valido){   
                $dql=$entity->getSql();
                $infoerr='';
                if ($this->pruebaDQL($dql,$infoerr)){
                    $em->flush();
                    //Se ejecuta el procedimiento en todos los funcionarios para actualizar historico_varaible
                    $personales = $em->getRepository('CetNominaBundle:Personal')->findAll();
                    foreach ($personales as $personal){
                        $this->calculaAction($entity->getId(), $personal->getId());
                    }
                    if($variableNueva!=$variableAntes){
                        //recorrer los demas conceptos y cambiar su formula
                        $conceptos = $em->getRepository('CetNominaBundle:Concepto')->findAll();
                        $patron[0]="/$variableAntes/";
                        $sustituir[0]="$variableNueva";
                        foreach ($conceptos as $concep){
                            $formula=$concep->getSql();
                            $formulaNueva=preg_replace($patron,$sustituir, $formula);
                            $concep->setSql($formulaNueva);    
                            $formulaPatro=$concep->getFormulaPatronal();
                            $formulaNuevaPatro=preg_replace($patron,$sustituir, $formulaPatro);
                            $concep->setFormulaPatronal($formulaNuevaPatro);
                            $em->persist($concep);
                            $em->flush();
                        } 
                    }
                    return $this->redirect($this->generateUrl('variable_show', array('id' => $id)));
                 }
                 else{
                    $error = new FormError("Expresion DQL no valida: $infoerr");
                    $editForm->get('sql')->addError($error);
                 }
            }
        }
//        $consulta = $em->createQuery('select p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido, uo.nombre from CetNominaBundle:Personal p left join p.informacionLaboral il left join il.historicoUnidadOrganizativas huo left join huo.fk_unidad_organizativa_has_informacion_laboral_unidad_organiz1 uo where huo.fechaInicio in (select max(huo2.fechaInicio) from CetNominaBundle:HistoricoUnidadOrganizativa huo2 where huo2.fk_unidad_organizativa_has_informacion_laboral_informacion_la1=il.id)');
                
        $consulta = $em->createQuery("SELECT p.primerNombre,p.segundoNombre,p.id,p.primerApellido,p.segundoApellido "
                . "FROM CetNominaBundle:Personal p ");
        
        $informacionpersonal = $consulta->getResult();
        return $this->render('CetNominaBundle:Variable:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'lista_personal' => $informacionpersonal,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Variable entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:Variable')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad Variable.');
            }

            $em->remove($entity);
            $em->flush();
//        }

        return $this->redirect($this->generateUrl('variable'));
    }

    /**
     * Creates a form to delete a Variable entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('variable_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
    
    private function llenar_matriz(){
        $em = $this->getDoctrine()->getManager();
        $metadata = $this->getDoctrine()->getManager()->getMetadataFactory()->getAllMetadata();
        $entidades = array();
        $matriz = array();
        
        //Lista de entidades que no se mostrarán
        $patrones = array(
            "userPersonalM",
            "userPersonalSistemasD",
            "sistSistemasM",
            "Usuario",
        );
        $i=0;
        foreach($metadata as $entidad){
            $bandera=0;
            foreach ($patrones as $patron) {
                if (preg_match('/' . $patron . '/', $entidad->getName())) {
                    $bandera=1;
                    continue;
                }
            }
            if($bandera==1)continue;
            $entidades[]=$entidad->getName();
            $nombre=$entidad->getName();
            $columnas_array[] = $em->getClassMetadata($nombre)->getFieldNames();
            $columnas = $em->getClassMetadata($nombre)->getFieldNames();
            $relaciones = $em->getClassMetadata($nombre)->getAssociationNames();
            $a=1;
                $tipos_campos_tabla = array();
                //Campos
                foreach($columnas as $columna){
                    $tipos_campos_tabla[$a] = $columna.' - '.$em->getClassMetadata($nombre)->getTypeOfField($columna);
                    //$tipos_campos_tabla[$a] = $em->getMetadataFactory();
                    //$tipos_campos_tabla[$a] = $em->getClassMetadata($nombre)->isCollectionValuedAssociation($nombre);
                    $a++;
                }
                //Relaciones
                foreach($relaciones as $relacion){
                    $tipos_campos_tabla[$a] = $relacion.' - Relación';
                    //$tipos_campos_tabla[$a] = $em->getMetadataFactory();
                    //$tipos_campos_tabla[$a] = $em->getClassMetadata($nombre)->isCollectionValuedAssociation($nombre);
                    $a++;
                }
                
                $tipos_campos[] = $tipos_campos_tabla;
                
            $matriz[$entidad->getName()]['campos']=$tipos_campos_tabla;             
            $i++;
        }
        return $matriz;
    }       
    
    /**
     * Muestra lista de atributos
     *
     */
    public function atributosAction(Request $request)
    {
        $entidad_id = $request->request->get('entidad_id');
                
        $matriz=$this->llenar_matriz();
        
        return new JsonResponse(array_merge(array(0),$matriz[$entidad_id]['campos']));
    }
    
    /**
     * Prueba las varaibles
     *
     */
    public function pruebaAction(Request $request)
    {
        //Obtengo el id del personal seleccionado para realizar la prueba asi como el sql
        $id_personal = $request->request->get('id_personal');
        $sql_variable = $request->request->get('sql_variable');
        //verificar que contenga palabras clave
        if (preg_match('/\binsert\b|\bdelete\b|\bupdate\b|\bcreate\b|\balter\b;/i', $sql_variable)){
            return new JsonResponse(array("!No ingresar palabras claves en $sql_variable"));
        }
        $errinfo="";
        if(!$this->pruebaDQL($sql_variable, $errinfo)) {
            return new JsonResponse(array("Error en la Fórmula DQL:$errinfo"));
        }       
//        ->addPropertyConstraint ('sql', new Assert\Regex(array(
//                     'pattern' => '/\binsert\b|\bdelete\b|\bupdate\b|;/i',
//                     'match'   => false,
//                     'message' => 'El formato no debe contener palabras de definición de base de datos',
//                    )))
                
        $em = $this->getDoctrine()->getManager();
        $consulta=$em->createQuery($sql_variable);
        $consulta->setParameter('personal_cedula', $id_personal);
        $consulta->setMaxResults(1);
        
        if ($consulta->getResult()){
            foreach($consulta->getSingleResult() as $dato){
                if(!is_object($dato)){
                    return new JsonResponse(array($dato));
                }                
//                elseif(get_class($dato)=='DateTime'){                    
                elseif($dato instanceof \DateTime){
                        $resul=$dato->format('d/m/Y');
                        return new JsonResponse(array($resul));
                }
                else 
                    return new JsonResponse(array("!No hay resultado¡"));
            }
//            return new JsonResponse($consulta->getSingleResult());
        }
        
        return new JsonResponse(array("!No hay resultado¡"));
    }
    
    /**
     * Calcula el valor de las variables y lo almacena en historicovariable
     * Parametros: $idvariable = Variable
     * Parametros: $idpersonal = Cedula
     */
    public function calculaAction($idvariable,$idpersonal)
    {
       try{
        $em = $this->getDoctrine()->getManager();
        
        //Para evitar el desbordamiento de memoria
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $variable = $em->getRepository('CetNominaBundle:Variable')->find($idvariable);
        $personal = $em->getRepository('CetNominaBundle:Personal')->find($idpersonal);
       
        //Se hace la consulta del DQL almacenado en la variable
        $consulta = $em->createQuery($variable->getSql());
        $formula=$variable->getSql();
        $consulta->setParameter('personal_cedula', $idpersonal);
//        try {
            $datos=$consulta->getOneOrNullResult();
//        } 
        //catch (\Doctrine\ORM\NonUniqueResultException $exc) {
//        catch (\Exception $exc) {
//            $info = toString($e);
//            $datos=array();
//        }
        if ($datos==null){
            $datos=array();
        }
        foreach($datos as $dato){
            if(!is_object($dato)){
                //return new JsonResponse(array($dato));
                $valor = $dato;
            }                
            elseif($dato instanceof \DateTime){
                    $resul=$dato->format('d/m/Y');
                    //return new JsonResponse(array($resul));
                    $valor=$resul;

            }
            else {
                //return new JsonResponse(array("!No hay resultado¡"));
                $valor=0;
            }
        }

//            $valor = $consulta->getSingleScalarResult();
        $hoy = new DateTime('NOW');
                        
        //Se verifica si la variable existe
        $historicoVariable = $em->getRepository('CetNominaBundle:HistoricoVariable')->encuentra($idvariable,$idpersonal);
        
        $historicoVariablenuevo = new HistoricoVariable(); 
        if (isset($valor)){
            if($historicoVariable){
                //Se verifica si el valor cambia
                //if($historicoVariable->getValor()!=$valor ){                       
                if($historicoVariable->getValor()!=$valor or $historicoVariable->getSql()!=$formula ){                       
                    $historicoVariablenuevo->setFkVariableHasPersonalVariable1($variable);
                    $historicoVariablenuevo->setFkVariableHasPersonalPersonal1($personal);
                    $historicoVariablenuevo->setValor($valor);        
                    $historicoVariablenuevo->setSql($formula);    
                    $historicoVariablenuevo->setFechaInicio($hoy);

                    $historicoVariable->setFechaFin($hoy);   

                    $em->persist($historicoVariable); 
                    $em->persist($historicoVariablenuevo); 
                    $em->flush();
                }
            }else{
                //Se almacena el valor de la variable en historicovariable       
                $historicoVariablenuevo->setFkVariableHasPersonalVariable1($variable);
                $historicoVariablenuevo->setFkVariableHasPersonalPersonal1($personal);
                $historicoVariablenuevo->setValor($valor);
                $historicoVariablenuevo->setSql($formula); 
                $historicoVariablenuevo->setFechaInicio($hoy);        

                $em->persist($historicoVariablenuevo);        
                $em->flush(); 
            }    
        }  
         }
        catch (\Exception $e) {
            //$info = toString($e);
            $info = $e->getMessage();
            $e=null;
        }
        //if (trim($info)== )
        $em->clear();
        return $this->redirect($this->generateUrl('variable'));
    }
    
   //Validar si la sintaxis DQL  es correcta y 
    public function pruebaDQL($dql_eval, &$info)
    {
    try{
        $em = $this->getDoctrine()->getManager();
        $consulta=$em->createQuery($dql_eval);
        $consulta->setParameter('personal_cedula','0');
        $datos=$consulta->getOneOrNullResult();
        if ($datos==null){
            $datos=array();
        }
        $resul=true;
    }
    catch (\Exception $e) {
            //$info = toString($e);
            $info = $e->getMessage();
            $resul=false;
        }
        return $resul;
    }
    
}
