<?php

namespace Cet\NominaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Cet\NominaBundle\Entity\DatosVariables;
use Cet\NominaBundle\Form\PersonalDatosVariablesType;

/**
 * DatosVariables controller.
 *
 */
class PersonalDatosVariablesController extends Controller
{

    /**
     * Lists all DatosVariables entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CetNominaBundle:DatosVariables')->findAll();

        return $this->render('CetNominaBundle:DatosVariables:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Lists all DatosGenericos entities.
     *
     */
    public function listaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('CetNominaBundle:DatosVariables')->findBy(array('fk_datos_variables_personal1' => $id));
        $personal= $em->getRepository('CetNominaBundle:Personal')->find($id);
        
        return $this->render('CetNominaBundle:DatosVariables:index.html.twig', array(
            'entities' => $entities,
            'personal' => $personal,
        ));
    }
    /**
     * Creates a new DatosVariables entity.
     *
     */
    public function createAction(Request $request,$id)
    {
        $entity = new DatosVariables();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkDatosVariablesPersonal1($personalbuscar);
        
        $form = $this->createCreateForm($entity,$id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('personaldatosvariables_lista', array('id' => $id)));
        }

        return $this->render('CetNominaBundle:DatosVariables:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a DatosVariables entity.
    *
    * @param DatosVariables $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(DatosVariables $entity,$id)
    {
        $form = $this->createForm(new PersonalDatosVariablesType(), $entity, array(
            'action' => $this->generateUrl('personaldatosvariables_create', array('id' => $id)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear'))
             ->add('reset', 'reset', array('label' => 'Limpiar'));

        return $form;
    }

    /**
     * Displays a form to create a new DatosVariables entity.
     *
     */
    public function newAction($id)
    {
        $entity = new DatosVariables();
        
        $em = $this->getDoctrine()->getManager();
        $personalbuscar = $em->getRepository('CetNominaBundle:Personal')->findOneBy(array('id' => $id));
        $entity->setFkDatosVariablesPersonal1($personalbuscar);
        
        $form   = $this->createCreateForm($entity,$id);

        return $this->render('CetNominaBundle:DatosVariables:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a DatosVariables entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosVariables')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosVariables.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DatosVariables:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing DatosVariables entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CetNominaBundle:DatosVariables')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosVariables.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('CetNominaBundle:DatosVariables:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a DatosVariables entity.
    *
    * @param DatosVariables $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DatosVariables $entity)
    {
        $form = $this->createForm(new PersonalDatosVariablesType(), $entity, array(
            'action' => $this->generateUrl('personaldatosvariables_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'))
             ->add('reset', 'reset', array('label' => 'Restablecer'))
             ->add('button', 'submit', array('label' => 'Eliminar','attr' => array('data-toggle' => 'modal','href'=>'#datospersonal')));

        return $form;
    }
    /**
     * Edits an existing DatosVariables entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('CetNominaBundle:DatosVariables')->find($id);
        $personal=$entity->getFkDatosVariablesPersonal1()->getId();
        
        
        if (!$entity) {
            throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosVariables.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            //return $this->redirect($this->generateUrl('datosvariables_show', array('id' => $id)));
            return $this->redirect($this->generateUrl('personaldatosvariables_lista', array('id' => $personal)));
        }

        return $this->render('CetNominaBundle:DatosVariables:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a DatosVariables entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

//        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CetNominaBundle:DatosVariables')->find($id);
            $personal=$entity->getFkDatosVariablesPersonal1()->getId();

            if (!$entity) {
                throw $this->createNotFoundException('No se ha podido encontrar la entidad DatosVariables.');
            }

            $em->remove($entity);
            $em->flush();
//        }
        return $this->redirect($this->generateUrl('personaldatosvariables_lista', array('id' => $personal)));        
        //return $this->redirect($this->generateUrl('datosvariables'));
    }

    /**
     * Creates a form to delete a DatosVariables entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personaldatosvariables_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Eliminar'))
            ->getForm()
        ;
    }
}
