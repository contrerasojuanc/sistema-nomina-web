<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\Concepto
 *
 * @ORM\Entity(repositoryClass="ConceptoRepository")
 * @ORM\Table(name="sis_nomina.concepto")
 * @UniqueEntity(
 *     fields={"identificador"},
 *     message="Ya existe un identificador de concepto con el mismo nombre"
 * )
 * @UniqueEntity(
 *     fields={"codigo"},
 *     message="Ya existe un código de concepto igual"
 * )
 */
class Concepto
{
    /**
     * Relacionar muchos a muchos con personal
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="codigo", type="integer", nullable=false)
     */
    private $codigo;
    
     /**
     * @var string
     * @ORM\Column(name="identificador", type="text", nullable=false, unique=true)
     */
    private $identificador;
    
    /**
     * @var string
     * @ORM\Column(name="denominacion", type="text", nullable=false)
     */
    private $denominacion;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     * @ORM\Column(name="sql", type="text", nullable=true)
     */
    private $sql;

    /**
     * @var string
     * @ORM\Column(name="formula_patronal", type="text", nullable=true)
     */
    private $formulaPatronal;
    
    /**
     * @var string
     * * @ORM\Column(name="tipo", type="text", nullable=false)
     */
    private $tipo;
    
    /**
     * @var datetime
     * @ORM\Column(name="fecha_inicia", type="datetime", nullable=true)
     */
    private $fechaInicia;

    /**
     * @var datetime
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true)
     */
    private $fechaFin;
    
    /**
     * @var boolean
     * @ORM\Column(name="es_acumulado", type="boolean", nullable=true)
     */
    private $esAcumulado;
    
    /**
     * @ORM\OneToMany(targetEntity="Formula", mappedBy="fk_variable_has_concepto_concepto1")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=true)
     */
    protected $formulas;

    /**
     * @ORM\OneToMany(targetEntity="HistoricoConcepto", mappedBy="fk_historico_concepto_concepto1")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=true)
     */
    protected $historicoConceptos;
    

    /**
     * @ORM\OneToMany(targetEntity="ConceptoHasPlantillaNomina", mappedBy="fk_concepto_has_plantilla_nomina_concepto1")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $conceptoHasPlantillaNominas;

    /**
     * @ORM\OneToMany(targetEntity="DatosVariables", mappedBy="fk_datos_variables_concepto1")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $datosVariables;
    
    /**
     * @ORM\OneToMany(targetEntity="CargaMasiva", mappedBy="fk_concepto1")
     * @ORM\JoinColumn(name="concepto_id", referencedColumnName="id", nullable=false)
     */
    protected $cargaMasiva;
    
    /**
     * @ORM\ManyToMany(targetEntity="PlantillaNomina", mappedBy="conceptos")
     */
    protected $plantillaNominas;
    
    /**
     * @ORM\ManyToMany(targetEntity="Concepto", mappedBy="fk_concepto_has_concepto2")
     */
    protected $fk_concepto_has_concepto1;
    
    /**
     * @ORM\ManyToMany(targetEntity="Concepto", inversedBy="fk_concepto_has_concepto1")
     * @ORM\JoinTable(name="sis_nomina.concepto_has_concepto",
     *     joinColumns={@ORM\JoinColumn(name="concepto_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="concepto_id1", referencedColumnName="id")}
     * )
     */
    protected $fk_concepto_has_concepto2;

    public function __construct()
    {
        $this->fk_concepto_has_concepto1 = new ArrayCollection();
        $this->fk_concepto_has_concepto2 = new ArrayCollection();
        $this->conceptoHasPlantillaNominas = new ArrayCollection();
        $this->datosVariables = new ArrayCollection();
        $this->cargaMasiva = new ArrayCollection();
        $this->formulas = new ArrayCollection();
        $this->historicoConceptos = new ArrayCollection();
        $this->plantillaNominas = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param integer $codigo
     * @return Concepto
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
    
     /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Concepto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDenominacion()
    {
        return $this->denominacion;
    }


    /**
     * Set denominacion
     *
     * @param string $denominacion
     * @return Concepto
     */
    public function setDenominacion($denominacion)
    {
        $this->denominacion = $denominacion;

        return $this;
    }

    /**
     * Get denominacion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set sql
     *
     * @param string $sql
     * @return Concepto
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * Get sql
     *
     * @return string 
     */
    public function getSql()
    {
        return $this->sql;
    }
    
        /**
     * Set identificador
     *
     * @param string $identificador
     * @return Identificador
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;

        return $this;
    }

    /**
     * Get identificador
     *
     * @return string 
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }
    

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Concepto
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add formulas
     *
     * @param \Cet\NominaBundle\Entity\Formula $formulas
     * @return Concepto
     */
    public function addFormula(\Cet\NominaBundle\Entity\Formula $formulas)
    {
        $this->formulas[] = $formulas;

        return $this;
    }

    /**
     * Remove formulas
     *
     * @param \Cet\NominaBundle\Entity\Formula $formulas
     */
    public function removeFormula(\Cet\NominaBundle\Entity\Formula $formulas)
    {
        $this->formulas->removeElement($formulas);
    }

    /**
     * Get formulas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormulas()
    {
        return $this->formulas;
    }

    /**
     * Add historicoConceptos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos
     * @return Concepto
     */
    public function addHistoricoConcepto(\Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos)
    {
        $this->historicoConceptos[] = $historicoConceptos;

        return $this;
    }

    /**
     * Remove historicoConceptos
     *
     * @param \Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos
     */
    public function removeHistoricoConcepto(\Cet\NominaBundle\Entity\HistoricoConcepto $historicoConceptos)
    {
        $this->historicoConceptos->removeElement($historicoConceptos);
    }

    /**
     * Get historicoConceptos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHistoricoConceptos()
    {
        return $this->historicoConceptos;
    }

    /**
     * Set fechaInicia
     *
     * @param \DateTime $fechaInicia
     * @return Concepto
     */
    public function setFechaInicia($fechaInicia)
    {
        $this->fechaInicia = $fechaInicia;

        return $this;
    }

    /**
     * Get fechaInicia
     *
     * @return \DateTime 
     */
    public function getFechaInicia()
    {
        return $this->fechaInicia;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Concepto
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {

       $metadata->addPropertyConstraint('descripcion', new Assert\NotBlank())
                ->addPropertyConstraint('denominacion', new Assert\NotBlank())
                 
                 ->addPropertyConstraint ('identificador', new Assert\Regex(array(
                     'pattern' => '/^[a-zA-Z][a-zA-z0-9\_]{0,}[a-zA-Z0-9]$/i',
                     'match'   => true,
                     'message' => 'El formato debe comenzar con una letra y no debe contener espacios ni caracteres especiales intermedios',
                    )))
                //Seguridad contra PHP inyection
                 ->addPropertyConstraint ('sql', new Assert\Regex(array(
                     'pattern' => '/\bcall\b|\brequire\b|\bfunction\b|\beval\b|\btrait\b|\brequire_once\b|\binclude\b|\bextends\b|\bclass\b|\bimplements\b|\bdeclare\b|\b__CLASS__\b|\b__NAMESPACE__\b|\b__DIR__\b|\b__FILE__\b|\b__FUNCTION__\b|\b__LINE__\b|\b__METHOD__\b|\b__NAMESPACE__\b|\b__TRAIT__\b/i',
                     'match'   => false,
                     'message' => 'El formato no debe contener palabras de definición de clave',
                    )))
            ;
        
    }

    /**
     * Set esAcumulado
     *
     * @param boolean $esAcumulado
     * @return Concepto
     */
    public function setEsAcumulado($esAcumulado)
    {
        $this->esAcumulado = $esAcumulado;

        return $this;
    }

    /**
     * Get esAcumulado
     *
     * @return boolean 
     */
    public function getEsAcumulado()
    {
        return $this->esAcumulado;
    }

    /**
     * Set concepto
     *
     * @param \Cet\NominaBundle\Entity\Concepto $concepto
     * @return Concepto
     */
    public function setConcepto(\Cet\NominaBundle\Entity\Concepto $concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return \Cet\NominaBundle\Entity\Concepto 
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Add plantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas
     * @return Concepto
     */
    public function addPlantillaNomina(\Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas)
    {
        $this->plantillaNominas[] = $plantillaNominas;

        return $this;
    }

    /**
     * Remove plantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas
     */
    public function removePlantillaNomina(\Cet\NominaBundle\Entity\PlantillaNomina $plantillaNominas)
    {
        $this->plantillaNominas->removeElement($plantillaNominas);
    }

    /**
     * Get plantillaNominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlantillaNominas()
    {
        return $this->plantillaNominas;
    }
    
    public function __toString()
    {
        return $this->getDenominacion();
    }
    
    public function getNombreCompleto()
    {
        return str_pad($this->getCodigo(),3,"0",STR_PAD_LEFT)." - ".$this->getDenominacion();
    }
    public function getNombreFormula()
    {
        return str_pad($this->getCodigo(),3,"0",STR_PAD_LEFT)." - ".$this->getDenominacion();
    }

    /**
     * Add conceptoHasPlantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas
     * @return Concepto
     */
    public function addConceptoHasPlantillaNomina(\Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas)
    {
        $this->conceptoHasPlantillaNominas[] = $conceptoHasPlantillaNominas;

        return $this;
    }

    /**
     * Remove conceptoHasPlantillaNominas
     *
     * @param \Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas
     */
    public function removeConceptoHasPlantillaNomina(\Cet\NominaBundle\Entity\ConceptoHasPlantillaNomina $conceptoHasPlantillaNominas)
    {
        $this->conceptoHasPlantillaNominas->removeElement($conceptoHasPlantillaNominas);
    }

    /**
     * Get conceptoHasPlantillaNominas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConceptoHasPlantillaNominas()
    {
        return $this->conceptoHasPlantillaNominas;
    }

    /**
     * Add datosVariables
     *
     * @param \Cet\NominaBundle\Entity\DatosVariables $datosVariables
     * @return Concepto
     */
    public function addDatosVariable(\Cet\NominaBundle\Entity\DatosVariables $datosVariables)
    {
        $this->datosVariables[] = $datosVariables;

        return $this;
    }

    /**
     * Remove datosVariables
     *
     * @param \Cet\NominaBundle\Entity\DatosVariables $datosVariables
     */
    public function removeDatosVariable(\Cet\NominaBundle\Entity\DatosVariables $datosVariables)
    {
        $this->datosVariables->removeElement($datosVariables);
    }

    /**
     * Get datosVariables
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDatosVariables()
    {
        return $this->datosVariables;
    }

    /**
     * Add cargaMasiva
     *
     * @param \Cet\NominaBundle\Entity\CargaMasiva $cargaMasiva
     * @return Concepto
     */
    public function addCargaMasiva(\Cet\NominaBundle\Entity\CargaMasiva $cargaMasiva)
    {
        $this->cargaMasiva[] = $cargaMasiva;

        return $this;
    }

    /**
     * Remove cargaMasiva
     *
     * @param \Cet\NominaBundle\Entity\CargaMasiva $cargaMasiva
     */
    public function removeCargaMasiva(\Cet\NominaBundle\Entity\CargaMasiva $cargaMasiva)
    {
        $this->datosVariables->removeElement($cargaMasiva);
    }

    /**
     * Get cargaMasiva
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCargaMasiva()
    {
        return $this->cargaMasiva;
    }

    /**
     * Add fk_concepto_has_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto1
     * @return Concepto
     */
    public function addFkConceptoHasConcepto1(\Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto1)
    {
        $this->fk_concepto_has_concepto1[] = $fkConceptoHasConcepto1;

        return $this;
    }

    /**
     * Remove fk_concepto_has_concepto1
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto1
     */
    public function removeFkConceptoHasConcepto1(\Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto1)
    {
        $this->fk_concepto_has_concepto1->removeElement($fkConceptoHasConcepto1);
    }

    /**
     * Get fk_concepto_has_concepto1
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFkConceptoHasConcepto1()
    {
        return $this->fk_concepto_has_concepto1;
    }

    /**
     * Add fk_concepto_has_concepto2
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto2
     * @return Concepto
     */
    public function addFkConceptoHasConcepto2(\Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto2)
    {
        $this->fk_concepto_has_concepto2[] = $fkConceptoHasConcepto2;

        return $this;
    }

    /**
     * Remove fk_concepto_has_concepto2
     *
     * @param \Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto2
     */
    public function removeFkConceptoHasConcepto2(\Cet\NominaBundle\Entity\Concepto $fkConceptoHasConcepto2)
    {
        $this->fk_concepto_has_concepto2->removeElement($fkConceptoHasConcepto2);
    }

    /**
     * Get fk_concepto_has_concepto2
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFkConceptoHasConcepto2()
    {
        return $this->fk_concepto_has_concepto2;
    }

    /**
     * Set formulaPatronal
     *
     * @param string $formulaPatronal
     * @return Concepto
     */
    public function setFormulaPatronal($formulaPatronal)
    {
        $this->formulaPatronal = $formulaPatronal;

        return $this;
    }

    /**
     * Get formulaPatronal
     *
     * @return string 
     */
    public function getFormulaPatronal()
    {
        return $this->formulaPatronal;
    }
}
