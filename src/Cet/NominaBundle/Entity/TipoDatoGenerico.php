<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Cet\NominaBundle\Entity\TipoDatoGenerico
 *
 * @ORM\Entity(repositoryClass="TipoDatoGenericoRepository")
 * @ORM\Table(name="sis_nomina.tipo_dato_generico")
 * @UniqueEntity(
 *     fields={"nombre"},
 *     message="Ya existe un dato generico con este nombre."
 * )
 */
class TipoDatoGenerico
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="text", nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="DatosGenericos", mappedBy="fk_dato_generico_tipo_dato_generico1")
     * @ORM\JoinColumn(name="tipo_dato_generico_id", referencedColumnName="id", nullable=false)
     */
    protected $datosGenericos;
    
    /**
     * @ORM\OneToMany(targetEntity="CargaMasiva", mappedBy="fk_dato_generico_tipo_dato_generico1")
     * @ORM\JoinColumn(name="tipo_dato_generico_id", referencedColumnName="id", nullable=false)
     */
    protected $cargaMasiva;

    public function __construct()
    {
        $this->datosGenericos = new ArrayCollection();
        $this->cargaMasiva = new ArrayCollection();
    }

    /**
     * Set the value of id.
     *
     * @param integer $id
     * @return \Entity\TipoDatoGenerico
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of nombre.
     *
     * @param string $nombre
     * @return \Entity\TipoDatoGenerico
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of nombre.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add DatosGenericos entity to collection (one to many).
     *
     * @param DatosGenericos $datosGenericos
     * @return \Entity\TipoDatoGenerico
     */
    public function addDatosGenericos(DatosGenericos $DatosGenericos)
    {
        $this->datosGenericos[] = $DatosGenericos;

        return $this;
    }

    /**
     * Remove DatosGenericos entity from collection (one to many).
     *
     * @param DatosGenericos $datosGenericos
     * @return \Entity\TipodatoGenerico
     */
    public function removeDatosGenericos(DatosGenericos $datosGenericos)
    {
        $this->datosGenericos->removeElement($datosGenericos);

        return $this;
    }

    /**
     * Get PlantillaNomina entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDatosGenericos()
    {
        return $this->datosGenericos;
    }
    
    /**
     * Add DatosGenericos entity to collection (one to many).
     *
     * @param CargaMasiva $cargaMasiva
     * @return \Entity\TipoDatoGenerico
     */
    public function addCargaMasiva(CargaMasiva $CargaMasiva)
    {
        $this->cargaMasiva[] = $CargaMasiva;

        return $this;
    }

    /**
     * Remove CargaMasiva entity from collection (one to many).
     *
     * @param CargaMasiva $cargaMasiva
     * @return \Entity\TipodatoGenerico
     */
    public function removeCargaMasiva(CargaMasiva $cargaMasiva)
    {
        $this->cargaMasiva->removeElement($cargaMasiva);

        return $this;
    }

    /**
     * Get PlantillaNomina entity collection (one to many).
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCargaMasiva()
    {
        return $this->cargaMasiva;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
}
