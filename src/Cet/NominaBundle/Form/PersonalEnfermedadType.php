<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonalEnfermedadType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
            ->add('descripcion',null,array('label' => 'Descripción','attr' => array('help'=>'Ingrese la descripción de la enfermedad en esta área.')))
            ->add('tratamiento',null,array('label' => 'Tratamiento','attr' => array('help'=>'Ingrese la descripción del tratamiento en esta área.')))
            ->add('costoTratamiento',null,array('label' => 'Costo del Tratamiento','attr' => array('help'=>'Introduzca el costo del tratamiento. Ejemplo:1500,15')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\Enfermedad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_enfermedad';
    }
}
