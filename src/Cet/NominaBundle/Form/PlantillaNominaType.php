<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlantillaNominaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null,array('label' => 'Denominación de Plantilla','attr' => array('help'=>'Ingrese el nombre con el que se podrá identificar la plantilla de nómina')))
            ->add('frecuencia', null,array('label' => 'Frecuencia de Pago','attr' => array('help'=>'Ingrese la frecuencia de pago por ejemplo: anual, mensual, quincenal, diaria, etc.')))
            ->add('fk_plantilla_nomina_tipo_nomina1', null,array('label' => 'Tipo de Nómina','attr' => array('help'=>'Seleccione el tipo de nómina que será procesado')))
            ->add('fk_tipocalculo', null,array('label' => 'Tipo de Cálculo',
                'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione tipo de cálculo','help'=>'Seleccione el tipo de cálculo: Si la selección es (Cálculo de Bono Vacacional) o (Cálculo de Garantía de Prestaciones Sociales); Previamente deben estar ejecutados  estos procesos desde el menú Procesos de Nomina->Beneficios Laborales')))
             
            ->add('tipoBanco', null,array('label' => 'Banco para Abonar','attr' => array('help'=>'Seleccione el Banco en el que será procesado el pago de la nómina')))
            ->add('tipoCuenta', null,array('label' => 'Tipo de Cuenta Bancaria','attr' => array('help'=>'Seleccione el tipo de cuenta del funcionario en la que será acreditado el dinero, por ejemplo: Cuenta Nómina o Cuenta de Fideicomiso')))
            ->add('conceptos',null,array(
                   'query_builder' => function($r) {
                        return $r->createQueryBuilder('concepto')
                                   ->orderBy('concepto.codigo','ASC');
                        },
                   'label'=>'Asignar Conceptos','property'=>'getNombreCompleto','attr' => array('help'=>'Seleccione el (los) concepto(s) que conforman la plantilla actual')))                
            ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\PlantillaNomina'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_plantillanomina';
    }
}
