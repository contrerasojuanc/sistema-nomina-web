<?php

namespace Cet\NominaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UnidadOrganizativaType extends AbstractType
{   
    public $unidades_array=array();
    public function __construct(Array $unidades_array=null,$selccionado=null)
    {
        $this->unidades_array = $unidades_array;
        $this->selecionado = $selccionado;
        
    }
    
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',null,array(
                'attr' => array('help'=>'Ingrese el nombre de la Unidad Organizativa que desea registrar o editar.'),
                'label'=> 'Nombre',
            )) 
            ->add('nivel',null,array(
                'attr' => array('help'=>'Ingrese el codigo de ubicación de la Unidad Organizativa.'),
                'label'=> 'Codigo de Ubicación',
            )) 
            //->add('fk_unidad_organizativa_unidad_organizativa1')
            ->add('unidad','choice', array(
                'empty_value' => 'Seleccione unidad organizativa superior',
                'choices' => $this->unidades_array,
                'required' => false,
                'mapped' => false,
                'label'=> 'Unidad Organizativa Superior',
                'attr' => array('help'=>'Seleccione la unidad organizativa a la cual se encuentra adscrita.'),
                'data' => $this->selecionado,
                'attr' => array('class' => 'select2me','data-placeholder' => 'Seleccione unidad organizativa') 
            )) 
            //->add('fk_unidad_organizativa_institucion1')  
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cet\NominaBundle\Entity\UnidadOrganizativa'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cet_nominabundle_unidadorganizativa';
    }
}
