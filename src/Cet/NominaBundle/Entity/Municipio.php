<?php

namespace Cet\NominaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cet\NominaBundle\Entity\Municipio
 *
 * @ORM\Entity(repositoryClass="MunicipioRepository")
 * @ORM\Table(name="sis_nomina.municipio", indexes={@ORM\Index(name="fk_municipio_estado1_idx", columns={"estado_id"})})
 */
class Municipio
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="Parroquia", mappedBy="fk_parroquia_municipio1")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id", nullable=false)
     */
    protected $parroquias;

    /**
     * @ORM\ManyToOne(targetEntity="Estado", inversedBy="municipios")
     * @ORM\JoinColumn(name="estado_id", referencedColumnName="id", nullable=false)
     */
    protected $fk_municipio_estado1;
    
    public function __construct()
    {
        $this->parroquias = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Municipio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add parroquias
     *
     * @param \Cet\NominaBundle\Entity\Parroquia $parroquias
     * @return Municipio
     */
    public function addParroquia(\Cet\NominaBundle\Entity\Parroquia $parroquias)
    {
        $this->parroquias[] = $parroquias;

        return $this;
    }

    /**
     * Remove parroquias
     *
     * @param \Cet\NominaBundle\Entity\Parroquia $parroquias
     */
    public function removeParroquia(\Cet\NominaBundle\Entity\Parroquia $parroquias)
    {
        $this->parroquias->removeElement($parroquias);
    }

    /**
     * Get parroquias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParroquias()
    {
        return $this->parroquias;
    }

    /**
     * Set fk_municipio_estado1
     *
     * @param \Cet\NominaBundle\Entity\Estado $fkMunicipioEstado1
     * @return Municipio
     */
    public function setFkMunicipioEstado1(\Cet\NominaBundle\Entity\Estado $fkMunicipioEstado1)
    {
        $this->fk_municipio_estado1 = $fkMunicipioEstado1;

        return $this;
    }

    /**
     * Get fk_municipio_estado1
     *
     * @return \Cet\NominaBundle\Entity\Estado 
     */
    public function getFkMunicipioEstado1()
    {
        return $this->fk_municipio_estado1;
    }
    
    /**
     * Validador
     * @param $metadata
     * @return Validado 
     */   
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
     $metadata  ->addPropertyConstraint ('nombre', new Assert\NotBlank())
            ;
    } 
    
    public function __toString()
    {
        return $this->getNombre();
    }
}
